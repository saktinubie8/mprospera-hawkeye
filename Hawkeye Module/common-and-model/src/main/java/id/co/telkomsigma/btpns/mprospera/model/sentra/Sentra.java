package id.co.telkomsigma.btpns.mprospera.model.sentra;

import java.sql.Time;
import java.util.Date;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_SENTRA")
public class Sentra extends GenericModel {
    /**
     *
     */
    private static final long serialVersionUID = 8798380267242491364L;

    private Long sentraId;
    private String areaId;
    private String sentraName;
    private String sentraLeader;
    private String sentraOwnerName;
    private Date startedDate;
    private String sentraAddress;
    private String rtRw;
    private char prsDay;
    private String longitude;
    private String latitude;
    private Boolean isDeleted = false;
    private String status;
    private Date createdDate;
    private String createdBy;
    private String prosperaId;
    private String locationId;
    private String meetingPlace;
    private String approvedBy;
    private Date approvedDate;
    private String assignedTo;
    private String rrn;
    private Time meetingTime;
    private String province;
    private String city;
    private String kecamatan;
    private String kelurahan;
    private String sentraCode;
    private Date lastPrs;
    private Date updatedDate;

    @Column(name = "approved_by")
    public String getApprovedBy() {
        return approvedBy;
    }

    private String postCode;

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    @Column(name = "meeting_place", length = MAX_LENGTH_ADDRESS)
    public String getMeetingPlace() {
        return meetingPlace;
    }

    public void setMeetingPlace(String meetingPlace) {
        this.meetingPlace = meetingPlace;
    }

    @Column(name = "location_id", nullable = false)
    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getSentraId() {
        return sentraId;
    }

    public void setSentraId(Long sentraId) {
        this.sentraId = sentraId;
    }

    @Column(name = "area_id")
    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    @Column(name = "sentra_name")
    public String getSentraName() {
        return sentraName;
    }

    public void setSentraName(String sentraName) {
        this.sentraName = sentraName;
    }

    @Column(name = "owner_name")
    public String getSentraOwnerName() {
        return sentraOwnerName;
    }

    @Column(name = "leader")
    public String getSentraLeader() {
        return sentraLeader;
    }

    public void setSentraLeader(String sentraLeader) {
        this.sentraLeader = sentraLeader;
    }

    public void setSentraOwnerName(String sentraOwnerName) {
        this.sentraOwnerName = sentraOwnerName;
    }

    @Column(name = "started_date")
    public Date getStartedDate() {
        return startedDate;
    }

    @Column(name = "CREATED_DT")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public void setStartedDate(Date startedDate) {
        this.startedDate = startedDate;
    }

    @Column(name = "address")
    public String getSentraAddress() {
        return sentraAddress;
    }

    public void setSentraAddress(String sentraAddress) {
        this.sentraAddress = sentraAddress;
    }

    @Column(name = "rt_rw")
    public String getRtRw() {
        return rtRw;
    }

    public void setRtRw(String rtRw) {
        this.rtRw = rtRw;
    }

    @Column(name = "prs_day")
    public char getPrsDay() {
        return prsDay;
    }

    public void setPrsDay(char prsDay) {
        this.prsDay = prsDay;
    }

    @Column(name = "longitude")
    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Column(name = "latitude")
    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @Column(name = "is_deleted")
    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Column(name = "created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "prospera_id")
    public String getProsperaId() {
        return prosperaId;
    }

    public void setProsperaId(String prosperaId) {
        this.prosperaId = prosperaId;
    }

    @Column(name = "post_code")
    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    @Column(name = "assign_to")
    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    @Column(name = "rrn")
    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    @Column(name = "meeting_time")
    public Time getMeetingTime() {
        return meetingTime;
    }

    public void setMeetingTime(Time meetingTime) {
        this.meetingTime = meetingTime;
    }

    @Column(name = "province")
    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    @Column(name = "city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Column(name = "kecamatan")
    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    @Column(name = "kelurahan")
    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    @Column(name = "sentra_code")
    public String getSentraCode() {
        return sentraCode;
    }

    public void setSentraCode(String sentraCode) {
        this.sentraCode = sentraCode;
    }

    @Column(name = "last_prs")
    public Date getLastPrs() {
        return lastPrs;
    }

    public void setLastPrs(Date lastPrs) {
        this.lastPrs = lastPrs;
    }

    @Column(name = "approved_dt")
    public Date getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }

    @Column(name = "updated_dt")
    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

}