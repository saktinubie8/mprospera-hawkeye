package id.co.telkomsigma.btpns.mprospera.request;

import java.util.Arrays;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class AddSurveyRequest extends BaseRequest {

    private String username;
    private String sessionKey;
    private String imei;
    private String surveyTypeId;
    private String surveyDate;
    private String customerId;
    private String status;
    private Question[] questionList;
    private String longitude;
    private String latitude;
    private String action;
    private String surveyId;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getSurveyTypeId() {
        return surveyTypeId;
    }

    public void setSurveyTypeId(String surveyTypeId) {
        this.surveyTypeId = surveyTypeId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Question[] getQuestionList() {
        return questionList;
    }

    public void setQuestionList(Question[] question) {
        this.questionList = question;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSurveyDate() {
        return surveyDate;
    }

    public void setSurveyDate(String surveyDate) {
        this.surveyDate = surveyDate;
    }

    @Override
    public String toString() {
        return "SurveyRequest [username=" + username + ", sessionKey=" + sessionKey + ", imei=" + imei
                + ", surveyTypeId=" + surveyTypeId + ", surveyDate=" + surveyDate + ", customerId=" + customerId
                + ", status=" + status + ", question=" + Arrays.toString(questionList) + ", longitude=" + longitude
                + ", latitude=" + latitude + ", action=" + action + ", surveyId=" + surveyId
                + ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime() + ", getRetrievalReferenceNumber()="
                + getRetrievalReferenceNumber() + "]";
    }

}