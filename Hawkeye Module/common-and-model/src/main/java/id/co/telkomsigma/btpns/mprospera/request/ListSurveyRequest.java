package id.co.telkomsigma.btpns.mprospera.request;

public class ListSurveyRequest extends BaseRequest {

    private String username;
    private String sessionKey;
    private String imei;
    private String getCountData;
    private String page;
    private String startLookupDate;
    private String endLookupDate;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getGetCountData() {
        return getCountData;
    }

    public void setGetCountData(String getCountData) {
        this.getCountData = getCountData;
    }

    public String getStartLookupDate() {
        return startLookupDate;
    }

    public void setStartLookupDate(String startLookupDate) {
        this.startLookupDate = startLookupDate;
    }

    public String getEndLookupDate() {
        return endLookupDate;
    }

    public void setEndLookupDate(String endLookupDate) {
        this.endLookupDate = endLookupDate;
    }

    @Override
    public String toString() {
        return "ListMMRequest [username=" + username + ", sessionKey=" + sessionKey + ", imei=" + imei
                + ", getCountData=" + getCountData + ", page=" + page + ", startLookupDate=" + startLookupDate
                + ", endLookupDate=" + endLookupDate + ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime()
                + ", getRetrievalReferenceNumber()=" + getRetrievalReferenceNumber() + "]";
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

}