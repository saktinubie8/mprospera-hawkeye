package id.co.telkomsigma.btpns.mprospera.model.terminal;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name = "T_TERMINAL_ACTIVITY")
public class TerminalActivity extends GenericModel {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String terminalActivityId;
    private Date createdDate;
    private Terminal terminal;
    // 0:undefined,1=Login,2=Logout,3=SubmitTerminalData,4=Get Location,5=Get
    // Location Detail,6=Submit Picture
    private Integer activityType = 0;
    private String reffNo;
    private String username;
    private String sessionKey;

    public static final int ACTIVITY_UNDEFINED = 0;
    public static final int ACTIVITY_LOGIN = 1;
    public static final int ACTIVITY_LOGOUT = 2;
    public static final int ACTIVITY_SUBMIT_ANDROID_DATA = 3;
    public static final int ACTIVITY_GET_LOCATION = 4;
    public static final int ACTIVITY_GET_DETAIL = 5;
    public static final int ACTIVITY_SUBMIT_LOCATION_DATA = 6;
    public static final int ACTIVITY_MOBILE_LOGIN = 7;
    public static final int ACTIVITY_MOBILE_LOGOUT = 8;
    public static final int ACTIVITY_SYNC_AREA = 9;
    public static final int ACTIVITY_MODY_SDA = 10;
    public static final int ACTIVITY_SYNC_SDA = 11;
    public static final int ACTIVITY_SYNC_CUSTOMER = 12;
    public static final int ACTIVITY_MODY_PM = 13;
    public static final int ACTIVITY_SYNC_SW = 14;
    public static final int ACTIVITY_MODY_SW = 15;
    public static final int ACTIVITY_SYNC_SENTRA = 16;
    public static final int ACTIVITY_MODY_SENTRA = 17;
    public static final int ACTIVITY_SYNC_SENTRA_GROUP = 18;
    public static final int ACTIVITY_MODY_SENTRA_GROUP = 19;
    public static final int ACTIVITY_SYNC_PDK = 20;
    public static final int ACTIVITY_MODY_PDK = 21;
    public static final int ACTIVITY_MODY_CUSTOMER = 22;
    public static final int ACTIVITY_SYNC_LOAN = 23;
    public static final int ACTIVITY_MODY_LOAN = 24;
    public static final int ACTIVITY_MODY_MM = 25;
    public static final int ACTIVITY_SYNC_MM = 26;
    public static final int ACTIVITY_SYNC_PM = 27;
    public static final int ACTIVITY_SYNC_SURVEY = 28;
    public static final int ACTIVITY_MODY_SURVEY = 29;
    public static final int ACTIVITY_SYNC_MASTER_SURVEY = 30;
    public static final int ACTIVITY_TOKEN_REQUEST = 31;
    public static final int ACTIVITY_PASS_RESET = 32;
    public static final int ACTIVITY_SYNC_EARLY_TERMINATION_REASON = 33;
    public static final int ACTIVITY_SYNC_DAYA_FILE = 34;
    public static final int ACTIVITY_SYNC_DENOM = 35;
    public static final int ACTIVITY_SYNC_PRAYER_PROMISE = 36;
    public static final int ACTIVITY_SYNC_NOT_ATTEND_REASON = 37;
    public static final int ACTIVITY_GET_DISBURSEMENT_PHOTO = 38;
    public static final int ACTIVITY_SUBMIT_PRS = 39;
    public static final int ACTIVITY_SYNC_PRS = 40;
    public static final int ACTIVITY_GET_ID_PHOTO = 41;
    public static final int ACTIVITY_GET_SURVEY_PHOTO = 42;
    public static final int ACTIVITY_PRS_PHOTO = 43;
    public static final int ACTIVITY_LOAN_REFRESH = 44;
    public static final int ACTIVITY_APPROVAL_PRS = 45;
    public static final int ACTIVITY_SUBMIT_DISBURSEMENT_PHOTO = 46;
    public static final int ACTIVITY_CHECK_PRS_STATUS = 47;
    public static final int ACTIVITY_ASSIGN_PS = 48;
    public static final int ACTIVITY_SYNC_NON_PRS = 49;
    public static final int ACTIVITY_MODY_DEVIATION = 50;
    public static final int ACTIVITY_SYNC_DEVIATION_CODE = 51;
    public static final int ACTIVITY_SYNC_DEVIATION = 52;
    public static final int ACTIVITY_SUBMIT_DEVIATION_PHOTO = 53;
    public static final int ACTIVITY_GET_HOLIDAY = 54;

    @Id
    @Column(name = "id", nullable = false, unique = true, length = 40)
    public String getTerminalActivityId() {
        return terminalActivityId;
    }

    @Column(name = "CREATED_DT")
    public Date getCreatedDate() {
        return createdDate;
    }

    @ManyToOne(targetEntity = Terminal.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "TERMINAL_ID", nullable = false)
    public Terminal getTerminal() {
        return terminal;
    }

    @Column(name = "ACTIVITY_TYPE", nullable = true)
    public Integer getActivityType() {
        return activityType;
    }

    @Transient
    public String getActivityTypeLabel() {
        if (activityType == ACTIVITY_LOGIN)
            return "Login";
        else if (activityType == ACTIVITY_LOGOUT)
            return "Logout";
        else if (activityType == ACTIVITY_SUBMIT_ANDROID_DATA)
            return "Submit Android Data";
        else if (activityType == ACTIVITY_SUBMIT_LOCATION_DATA)
            return "Submit Geotag/Gambar";
        else if (activityType == ACTIVITY_GET_LOCATION)
            return "Get Location";
        else if (activityType == ACTIVITY_GET_DETAIL)
            return "Get Detail Location";
        else if (activityType == ACTIVITY_MOBILE_LOGIN)
            return "Login From Mobile";
        else if (activityType == ACTIVITY_MOBILE_LOGOUT)
            return "Logout from Mobile";
        else if (activityType == ACTIVITY_SYNC_AREA)
            return "Sync Data Area";
        else if (activityType == ACTIVITY_SYNC_SW)
            return "Sync Data Survey Wawancara";
        else if (activityType == ACTIVITY_MODY_SW)
            return "Modifikasi Data Survey Wawancara";
        else if (activityType == ACTIVITY_SYNC_PDK)
            return "Sync Data Pelatihan Dasar Keanggotaan";
        else if (activityType == ACTIVITY_MODY_PDK)
            return "Modifikasi Data Pelatihan Dasar Keanggotaan";
        else if (activityType == ACTIVITY_MODY_CUSTOMER)
            return "Modifikasi Data Customer";
        else if (activityType == ACTIVITY_MODY_MM)
            return "Modifikasi Data Mini Meeting";
        else if (activityType == ACTIVITY_SYNC_MM)
            return "Sync Data Mini Meeting";
        else if (activityType == ACTIVITY_MODY_SENTRA)
            return "Modifikasi Data Sentra";
        else if (activityType == ACTIVITY_SYNC_SENTRA)
            return "Sync Data Sentra";
        else if (activityType == ACTIVITY_MODY_SENTRA_GROUP)
            return "Modifikasi Data Grup Sentra";
        else if (activityType == ACTIVITY_SYNC_SENTRA_GROUP)
            return "Sync Data Grup Sentra";
        else if (activityType == ACTIVITY_MODY_PM)
            return "Modifikasi Data Projection Meeting";
        else if (activityType == ACTIVITY_SYNC_PM)
            return "Sync Data Projection Meeting";
        else if (activityType == ACTIVITY_MODY_SDA)
            return "Modifikasi Data SDA";
        else if (activityType == ACTIVITY_SYNC_SDA)
            return "Sync Data SDA";
        else if (activityType == ACTIVITY_TOKEN_REQUEST)
            return "Token Request";
        else if (activityType == ACTIVITY_PASS_RESET)
            return "Reset Password";
        else if (activityType == ACTIVITY_SYNC_CUSTOMER)
            return "Sync Data Customer";
        else if (activityType == ACTIVITY_SYNC_EARLY_TERMINATION_REASON)
            return "Sync Data Alasan Pelunasan Dipercepat";
        else if (activityType == ACTIVITY_SYNC_DAYA_FILE)
            return "Sync Data File Daya";
        else if (activityType == ACTIVITY_SYNC_DENOM)
            return "Sync Data Denom";
        else if (activityType == ACTIVITY_SYNC_PRAYER_PROMISE)
            return "Sync Data Doa dan Janji";
        else if (activityType == ACTIVITY_SYNC_NOT_ATTEND_REASON)
            return "Sync Data Alasan Tidak Hadir";
        else if (activityType == ACTIVITY_GET_DISBURSEMENT_PHOTO)
            return "Ambil Foto Pencairan";
        else if (activityType == ACTIVITY_SYNC_PRS)
            return "Sync Data PRS";
        else if (activityType == ACTIVITY_SUBMIT_PRS)
            return "Modifikasi Data PRS";
        else if (activityType == ACTIVITY_GET_ID_PHOTO)
            return "Ambil Foto KTP";
        else if (activityType == ACTIVITY_GET_SURVEY_PHOTO)
            return "Ambil Foto Tempat Usaha";
        else if (activityType == ACTIVITY_PRS_PHOTO)
            return "Ambil Foto Aktifitas PRS";
        else if (activityType == ACTIVITY_LOAN_REFRESH)
            return "Refresh Kewajiban";
        else if (activityType == ACTIVITY_APPROVAL_PRS)
            return "Approval PRS";
        else if (activityType == ACTIVITY_SUBMIT_DISBURSEMENT_PHOTO)
            return "Submit Foto Pencairan";
        else if (activityType == ACTIVITY_CHECK_PRS_STATUS)
            return "Check PRS Status";
        else if (activityType == ACTIVITY_ASSIGN_PS)
            return "Assign PS";
        else if (activityType == ACTIVITY_SYNC_NON_PRS)
            return "Sync Data non PRS";
        else if (activityType == ACTIVITY_MODY_DEVIATION)
            return "Submit Deviasi";
        else if (activityType == ACTIVITY_SYNC_DEVIATION_CODE)
            return "Sync Kode Deviasi";
        else if (activityType == ACTIVITY_SYNC_DEVIATION)
            return "Sync Deviasi";
        else if (activityType == ACTIVITY_SUBMIT_DEVIATION_PHOTO)
            return "Submit Foto Deviasi";
        return "Undefined";
    }

    @Column(name = "REFF_NO", nullable = false, length = MAX_LENGTH_REFF_NO)
    public String getReffNo() {
        return reffNo;
    }

    @Column(name = "username", nullable = true, length = 40)
    public String getUsername() {
        return username;
    }

    @Column(name = "session_key", nullable = true, length = 40)
    public String getSessionKey() {
        return sessionKey;
    }

    public void setTerminalActivityId(String terminalActivityId) {
        this.terminalActivityId = terminalActivityId;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public void setTerminal(Terminal terminal) {
        this.terminal = terminal;
    }

    public void setActivityType(Integer activityType) {
        this.activityType = activityType;
    }

    public void setReffNo(String reffNo) {
        this.reffNo = reffNo;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

}