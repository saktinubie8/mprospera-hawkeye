package id.co.telkomsigma.btpns.mprospera.response;

public class LoanRefreshListResponse {

    private String appId;
    private String amount;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "LoanRefreshListResponse [appId=" + appId + ", amount=" + amount + ", getClass()=" + getClass()
                + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
    }

}