package id.co.telkomsigma.btpns.mprospera.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ESBLoanRequest extends BaseRequest {

    private String username;
    private String imei;
    private String swId;
    private String custReffId;
    private String loanReffId;
    private String perspective;
    private String prdOfferingShortName;
    private String tipeNasabah;
    private String tipePembiayaan;
    private String applicationDate;
    private String pembiayaanKe;
    private String penambahan;
    private String namaJenisUsaha;
    private String perlengkapanWarung;
    private String hargaPerlengkapanWarung;
    private String ternak;
    private String hargaTernak;
    private String bahanBaku;
    private String hargaBahanBaku;
    private String pulsaHandphone;
    private String hargaPulsaHandphone;
    private String other;
    private String hargaOther;
    private String totalHarga;
    private String loanAmount;
    private String interestRate;
    private String noOfInstallments;
    private String disbursementDate;
    private String gracePeriodDuration;
    private String fundId;
    private String businessActivitiesId;
    private String externalId;
    private String otherInstitutionFlag;
    private String institutionName;
    private String principalResidue;
    private String installmentResidue;
    private String installmentAmount;
    private String biCheckingResult;
    private String collectAbility;
    private String businessFlag;
    private String businessType;
    private String businessTypeValue;
    private String placeStatus;
    private String placeStatusValue;
    private String age;
    private String income;
    private String expense;
    private String netIncome;
    private String incomeFamily;
    private String businessFlagExp;
    private String businessTypeExp;
    private String businessTypeValueExp;
    private String ageExp;
    private String businessTypePlan;
    private String businessTypeValuePlan;
    private String placeStatusPlan;
    private String placeStatusValuePlan;
    private String hrb;
    private String hrc;
    private String deviation;
    private String flagId;
    private String comment;
    private String revenuePerMonth;
    private String customerNumber;
    private String maxLoanAmount;
    private String minLoanAmount;
    private String maxNoOfInstall;
    private String minNoOfInstall;
    private String status;
    private String interest;
    private String holidayBonus;
    private String orgDueDate;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;

    public ESBLoanRequest(LoanRequest request) {
        setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
        setTransmissionDateAndTime(request.getTransmissionDateAndTime());
        this.username = request.getUsername();
        this.imei = request.getImei();
        this.swId = request.getSwId();
        this.custReffId = request.getCustReffId();
        this.loanReffId = request.getLoanReffId();
        this.perspective = request.getPerspective();
        this.prdOfferingShortName = request.getPrdOfferingShortName();
        this.tipeNasabah = request.getTipeNasabah();
        this.tipePembiayaan = request.getTipePembiayaan();
        this.applicationDate = request.getApplicationDate();
        this.pembiayaanKe = request.getPembiayaanKe();
        this.penambahan = request.getPenambahan();
        this.namaJenisUsaha = request.getNamaJenisUsaha();
        this.perlengkapanWarung = request.getPerlengkapanWarung();
        this.hargaPerlengkapanWarung = request.getHargaPerlengkapanWarung();
        this.ternak = request.getTernak();
        this.hargaTernak = request.getHargaTernak();
        this.bahanBaku = request.getBahanBaku();
        this.hargaBahanBaku = request.getHargaBahanBaku();
        this.pulsaHandphone = request.getPulsaHandphone();
        this.hargaPulsaHandphone = request.getHargaPulsaHandphone();
        this.other = request.getOther();
        this.hargaOther = request.getHargaOther();
        this.totalHarga = request.getTotalHarga();
        this.loanAmount = request.getLoanAmount();
        this.interestRate = request.getInterestRate();
        this.noOfInstallments = request.getNoOfInstallments();
        this.disbursementDate = request.getDisbursementDate();
        this.gracePeriodDuration = request.getGracePeriodDuration();
        this.fundId = request.getFundId();
        this.businessActivitiesId = request.getBusinessActivitiesId();
        this.externalId = request.getExternalId();
        this.otherInstitutionFlag = request.getOtherInstitutionFlag();
        this.institutionName = request.getInstitutionName();
        this.principalResidue = request.getPrincipalResidue();
        this.installmentResidue = request.getInstallmentResidue();
        this.installmentAmount = request.getInstallmentAmount();
        this.biCheckingResult = request.getBiCheckingResult();
        this.collectAbility = request.getCollectAbility();
        this.businessFlag = request.getBusinessFlag();
        this.businessType = request.getBusinessType();
        this.businessTypeValue = request.getBusinessTypeValue();
        this.placeStatus = request.getPlaceStatus();
        this.placeStatusValue = request.getPlaceStatusValue();
        this.age = request.getAge();
        this.income = request.getIncome();
        this.expense = request.getExpense();
        this.netIncome = request.getNetIncome();
        this.incomeFamily = request.getIncomeFamily();
        this.businessFlagExp = request.getBusinessFlagExp();
        this.businessTypeExp = request.getBusinessTypeExp();
        this.businessTypeValueExp = request.getBusinessTypeValueExp();
        this.ageExp = request.getAgeExp();
        this.businessTypePlan = request.getBusinessTypePlan();
        this.businessTypeValuePlan = request.getBusinessTypeValuePlan();
        this.placeStatusPlan = request.getPlaceStatusPlan();
        this.placeStatusValuePlan = request.getPlaceStatusValuePlan();
        this.hrb = request.getHrb();
        this.hrc = request.getHrc();
        this.deviation = request.getDeviation();
        this.flagId = request.getFlagId();
        this.comment = request.getComment();
        this.revenuePerMonth = request.getRevenuePerMonth();
        this.customerNumber = request.getCustomerNumber();
        this.maxLoanAmount = request.getMaxLoanAmount();
        this.minLoanAmount = request.getMinLoanAmount();
        this.maxNoOfInstall = request.getMaxNoOfInstall();
        this.minNoOfInstall = request.getMinNoOfInstall();
        this.status = request.getStatus();
        this.interest = request.getInterest();
        this.holidayBonus = request.getHolidayBonus();
        this.orgDueDate = request.getOrgDueDate();
        this.createdBy = request.getCreatedBy();
        this.createdDate = request.getCreatedDate();
        this.modifiedBy = request.getModifiedBy();
        this.modifiedDate = request.getModifiedDate();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getSwId() {
        return swId;
    }

    public void setSwId(String swId) {
        this.swId = swId;
    }

    public String getCustReffId() {
        return custReffId;
    }

    public void setCustReffId(String custReffId) {
        this.custReffId = custReffId;
    }

    public String getLoanReffId() {
        return loanReffId;
    }

    public void setLoanReffId(String loanReffId) {
        this.loanReffId = loanReffId;
    }

    public String getPerspective() {
        if (perspective == null)
            return "";
        return perspective;
    }

    public void setPerspective(String perspective) {
        this.perspective = perspective;
    }

    public String getPrdOfferingShortName() {
        return prdOfferingShortName;
    }

    public void setPrdOfferingShortName(String prdOfferingShortName) {
        this.prdOfferingShortName = prdOfferingShortName;
    }

    public String getTipeNasabah() {
        return tipeNasabah;
    }

    public void setTipeNasabah(String tipeNasabah) {
        this.tipeNasabah = tipeNasabah;
    }

    public String getTipePembiayaan() {
        return tipePembiayaan;
    }

    public void setTipePembiayaan(String tipePembiayaan) {
        this.tipePembiayaan = tipePembiayaan;
    }

    public String getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(String applicationDate) {
        this.applicationDate = applicationDate;
    }

    public String getPembiayaanKe() {
        if (pembiayaanKe == null)
            return "";
        return pembiayaanKe;
    }

    public void setPembiayaanKe(String pembiayaanKe) {
        this.pembiayaanKe = pembiayaanKe;
    }

    public String getPenambahan() {
        return penambahan;
    }

    public void setPenambahan(String penambahan) {
        this.penambahan = penambahan;
    }

    public String getNamaJenisUsaha() {
        return namaJenisUsaha;
    }

    public void setNamaJenisUsaha(String namaJenisUsaha) {
        this.namaJenisUsaha = namaJenisUsaha;
    }

    public String getPerlengkapanWarung() {
        if (this.perlengkapanWarung == null)
            return "";
        return perlengkapanWarung;
    }

    public void setPerlengkapanWarung(String perlengkapanWarung) {
        this.perlengkapanWarung = perlengkapanWarung;
    }

    public String getHargaPerlengkapanWarung() {
        if (this.hargaPerlengkapanWarung == null)
            return "";
        return hargaPerlengkapanWarung;
    }

    public void setHargaPerlengkapanWarung(String hargaPerlengkapanWarung) {
        this.hargaPerlengkapanWarung = hargaPerlengkapanWarung;
    }

    public String getTernak() {
        if (this.ternak == null)
            return "";
        return ternak;
    }

    public void setTernak(String ternak) {
        this.ternak = ternak;
    }

    public String getHargaTernak() {
        if (this.hargaTernak == null)
            return "";
        return hargaTernak;
    }

    public void setHargaTernak(String hargaTernak) {
        this.hargaTernak = hargaTernak;
    }

    public String getBahanBaku() {
        if (this.bahanBaku == null)
            return "";
        return bahanBaku;
    }

    public void setBahanBaku(String bahanBaku) {
        this.bahanBaku = bahanBaku;
    }

    public String getHargaBahanBaku() {
        if (this.hargaBahanBaku == null)
            return "";
        return hargaBahanBaku;
    }

    public void setHargaBahanBaku(String hargaBahanBaku) {
        this.hargaBahanBaku = hargaBahanBaku;
    }

    public String getPulsaHandphone() {
        if (this.pulsaHandphone == null)
            return "";
        return pulsaHandphone;
    }

    public void setPulsaHandphone(String pulsaHandphone) {
        this.pulsaHandphone = pulsaHandphone;
    }

    public String getHargaPulsaHandphone() {
        if (this.hargaPulsaHandphone == null)
            return "";
        return hargaPulsaHandphone;
    }

    public void setHargaPulsaHandphone(String hargaPulsaHandphone) {
        this.hargaPulsaHandphone = hargaPulsaHandphone;
    }

    public String getOther() {
        if (this.other == null)
            return "";
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getHargaOther() {
        if (this.hargaOther == null)
            return "";
        return hargaOther;
    }

    public void setHargaOther(String hargaOther) {
        this.hargaOther = hargaOther;
    }

    public String getTotalHarga() {
        if (this.totalHarga == null)
            return "";
        return totalHarga;
    }

    public void setTotalHarga(String totalHarga) {
        this.totalHarga = totalHarga;
    }

    public String getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(String loanAmount) {
        this.loanAmount = loanAmount;
    }

    public String getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(String interestRate) {
        this.interestRate = interestRate;
    }

    public String getNoOfInstallments() {
        return noOfInstallments;
    }

    public void setNoOfInstallments(String noOfInstallments) {
        this.noOfInstallments = noOfInstallments;
    }

    public String getDisbursementDate() {
        return disbursementDate;
    }

    public void setDisbursementDate(String disbursementDate) {
        this.disbursementDate = disbursementDate;
    }

    public String getGracePeriodDuration() {
        return gracePeriodDuration;
    }

    public void setGracePeriodDuration(String gracePeriodDuration) {
        this.gracePeriodDuration = gracePeriodDuration;
    }

    public String getFundId() {
        if (this.fundId == null)
            return "";
        return fundId;
    }

    public void setFundId(String fundId) {
        this.fundId = fundId;
    }

    public String getBusinessActivitiesId() {
        if (this.businessActivitiesId == null)
            return "";
        return businessActivitiesId;
    }

    public void setBusinessActivitiesId(String businessActivitiesId) {
        this.businessActivitiesId = businessActivitiesId;
    }

    public String getExternalId() {
        if (this.externalId == null)
            return "";
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getOtherInstitutionFlag() {
        if (otherInstitutionFlag == null)
            return "";
        return otherInstitutionFlag;
    }

    public void setOtherInstitutionFlag(String otherInstitutionFlag) {
        this.otherInstitutionFlag = otherInstitutionFlag;
    }

    public String getInstitutionName() {
        if (institutionName == null)
            return "";
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getPrincipalResidue() {
        if (principalResidue == null)
            return "";
        return principalResidue;
    }

    public void setPrincipalResidue(String principalResidue) {
        this.principalResidue = principalResidue;
    }

    public String getInstallmentResidue() {
        if (installmentResidue == null)
            return "";
        return installmentResidue;
    }

    public void setInstallmentResidue(String installmentResidue) {
        this.installmentResidue = installmentResidue;
    }

    public String getInstallmentAmount() {
        if (installmentAmount == null)
            return "";
        return installmentAmount;
    }

    public void setInstallmentAmount(String installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    public String getBiCheckingResult() {
        return biCheckingResult;
    }

    public void setBiCheckingResult(String biCheckingResult) {
        this.biCheckingResult = biCheckingResult;
    }

    public String getCollectAbility() {
        return collectAbility;
    }

    public void setCollectAbility(String collectAbility) {
        this.collectAbility = collectAbility;
    }

    public String getBusinessFlag() {
        if (businessFlag == null)
            return "";
        return businessFlag;
    }

    public void setBusinessFlag(String businessFlag) {
        this.businessFlag = businessFlag;
    }

    public String getBusinessType() {
        if (businessType == null)
            return "";
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getBusinessTypeValue() {
        return businessTypeValue;
    }

    public void setBusinessTypeValue(String businessTypeValue) {
        this.businessTypeValue = businessTypeValue;
    }

    public String getPlaceStatus() {
        return placeStatus;
    }

    public void setPlaceStatus(String placeStatus) {
        this.placeStatus = placeStatus;
    }

    public String getPlaceStatusValue() {
        return placeStatusValue;
    }

    public void setPlaceStatusValue(String placeStatusValue) {
        this.placeStatusValue = placeStatusValue;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getExpense() {
        return expense;
    }

    public void setExpense(String expense) {
        this.expense = expense;
    }

    public String getNetIncome() {
        return netIncome;
    }

    public void setNetIncome(String netIncome) {
        this.netIncome = netIncome;
    }

    public String getIncomeFamily() {
        return incomeFamily;
    }

    public void setIncomeFamily(String incomeFamily) {
        this.incomeFamily = incomeFamily;
    }

    public String getBusinessFlagExp() {
        return businessFlagExp;
    }

    public void setBusinessFlagExp(String businessFlagExp) {
        this.businessFlagExp = businessFlagExp;
    }

    public String getBusinessTypeExp() {
        return businessTypeExp;
    }

    public void setBusinessTypeExp(String businessTypeExp) {
        this.businessTypeExp = businessTypeExp;
    }

    public String getBusinessTypeValueExp() {
        return businessTypeValueExp;
    }

    public void setBusinessTypeValueExp(String businessTypeValueExp) {
        this.businessTypeValueExp = businessTypeValueExp;
    }

    public String getAgeExp() {
        return ageExp;
    }

    public void setAgeExp(String ageExp) {
        this.ageExp = ageExp;
    }

    public String getBusinessTypePlan() {
        if (businessTypePlan == null)
            return "";
        return businessTypePlan;
    }

    public void setBusinessTypePlan(String businessTypePlan) {
        this.businessTypePlan = businessTypePlan;
    }

    public String getBusinessTypeValuePlan() {
        if (businessTypeValuePlan == null)
            return "";
        return businessTypeValuePlan;
    }

    public void setBusinessTypeValuePlan(String businessTypeValuePlan) {
        this.businessTypeValuePlan = businessTypeValuePlan;
    }

    public String getPlaceStatusPlan() {
        if (placeStatusPlan == null)
            return "";
        return placeStatusPlan;
    }

    public void setPlaceStatusPlan(String placeStatusPlan) {
        this.placeStatusPlan = placeStatusPlan;
    }

    public String getPlaceStatusValuePlan() {
        if (placeStatusValuePlan == null)
            return "";
        return placeStatusValuePlan;
    }

    public void setPlaceStatusValuePlan(String placeStatusValuePlan) {
        this.placeStatusValuePlan = placeStatusValuePlan;
    }

    public String getHrb() {
        if (this.hrb == null)
            return "";
        return hrb;
    }

    public void setHrb(String hrb) {
        this.hrb = hrb;
    }

    public String getHrc() {
        if (this.hrc == null)
            return "";
        return hrc;
    }

    public void setHrc(String hrc) {
        this.hrc = hrc;
    }

    public String getDeviation() {
        if (this.deviation == null)
            return "";
        return deviation;
    }

    public void setDeviation(String deviation) {
        this.deviation = deviation;
    }

    public String getFlagId() {
        if (this.flagId == null)
            return "";
        return flagId;
    }

    public void setFlagId(String flagId) {
        this.flagId = flagId;
    }

    public String getComment() {
        if (this.comment == null)
            return "";
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getRevenuePerMonth() {
        return revenuePerMonth;
    }

    public void setRevenuePerMonth(String revenuePerMonth) {
        this.revenuePerMonth = revenuePerMonth;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getMaxLoanAmount() {
        if (maxLoanAmount == null)
            return "";
        return maxLoanAmount;
    }

    public void setMaxLoanAmount(String maxLoanAmount) {
        this.maxLoanAmount = maxLoanAmount;
    }

    public String getMinLoanAmount() {
        if (minLoanAmount == null)
            return "";
        return minLoanAmount;
    }

    public void setMinLoanAmount(String minLoanAmount) {
        this.minLoanAmount = minLoanAmount;
    }

    public String getMaxNoOfInstall() {
        if (maxNoOfInstall == null)
            return "";
        return maxNoOfInstall;
    }

    public void setMaxNoOfInstall(String maxNoOfInstall) {
        this.maxNoOfInstall = maxNoOfInstall;
    }

    public String getMinNoOfInstall() {
        if (minNoOfInstall == null)
            return "";
        return minNoOfInstall;
    }

    public void setMinNoOfInstall(String minNoOfInstall) {
        this.minNoOfInstall = minNoOfInstall;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInterest() {
        if (interest == null)
            return "";
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getHolidayBonus() {
        return holidayBonus;
    }

    public void setHolidayBonus(String holidayBonus) {
        this.holidayBonus = holidayBonus;
    }

    public String getOrgDueDate() {
        return orgDueDate;
    }

    public void setOrgDueDate(String orgDueDate) {
        this.orgDueDate = orgDueDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Override
    public String toString() {
        return "ESBLoanRequest [username=" + username + ", imei=" + imei + ", swId=" + swId + ", custReffId=" + custReffId
                + ", loanReffId=" + loanReffId + ", perspective=" + perspective + ", prdOfferingShortName="
                + prdOfferingShortName + ", tipeNasabah=" + tipeNasabah + ", tipePembiayaan=" + tipePembiayaan
                + ", applicationDate=" + applicationDate + ", pembiayaanKe=" + pembiayaanKe + ", penambahan="
                + penambahan + ", namaJenisUsaha=" + namaJenisUsaha + ", perlengkapanWarung=" + perlengkapanWarung
                + ", hargaPerlengkapanWarung=" + hargaPerlengkapanWarung + ", ternak=" + ternak + ", hargaTernak="
                + hargaTernak + ", bahanBaku=" + bahanBaku + ", hargaBahanBaku=" + hargaBahanBaku + ", pulsaHandphone="
                + pulsaHandphone + ", hargaPulsaHandphone=" + hargaPulsaHandphone + ", other=" + other + ", hargaOther="
                + hargaOther + ", totalHarga=" + totalHarga + ", loanAmount=" + loanAmount + ", interestRate="
                + interestRate + ", noOfInstallments=" + noOfInstallments + ", disbursementDate=" + disbursementDate
                + ", gracePeriodDuration=" + gracePeriodDuration + ", fundId=" + fundId + ", businessActivitiesId="
                + businessActivitiesId + ", externalId=" + externalId + ", otherInstitutionFlag=" + otherInstitutionFlag
                + ", institutionName=" + institutionName + ", principalResidue=" + principalResidue
                + ", installmentResidue=" + installmentResidue + ", installmentAmount=" + installmentAmount
                + ", biCheckingResult=" + biCheckingResult + ", collectAbility=" + collectAbility + ", businessFlag="
                + businessFlag + ", businessType=" + businessType + ", businessTypeValue=" + businessTypeValue
                + ", placeStatus=" + placeStatus + ", placeStatusValue=" + placeStatusValue + ", age=" + age
                + ", income=" + income + ", expense=" + expense + ", netIncome=" + netIncome + ", incomeFamily="
                + incomeFamily + ", businessFlagExp=" + businessFlagExp + ", businessTypeExp=" + businessTypeExp
                + ", businessTypeValueExp=" + businessTypeValueExp + ", ageExp=" + ageExp + ", businessTypePlan="
                + businessTypePlan + ", businessTypeValuePlan=" + businessTypeValuePlan + ", placeStatusPlan="
                + placeStatusPlan + ", placeStatusValuePlan=" + placeStatusValuePlan + ", hrb=" + hrb + ", hrc=" + hrc
                + ", deviation=" + deviation + ", flagId=" + flagId + ", comment=" + comment + ", revenuePerMonth="
                + revenuePerMonth + ", customerNumber=" + customerNumber + ", maxLoanAmount=" + maxLoanAmount
                + ", minLoanAmount=" + minLoanAmount + ", maxNoOfInstall=" + maxNoOfInstall + ", minNoOfInstall="
                + minNoOfInstall + ", status=" + status + ", interest=" + interest + ", holidayBonus=" + holidayBonus
                + ", orgDueDate=" + orgDueDate + ", createdBy=" + createdBy + ", createdDate=" + createdDate
                + ", modifiedBy=" + modifiedBy + ", modifiedDate=" + modifiedDate + ", getTransmissionDateAndTime()="
                + getTransmissionDateAndTime() + ", getRetrievalReferenceNumber()=" + getRetrievalReferenceNumber()
                + "]";
    }

}