package id.co.telkomsigma.btpns.mprospera.model.jenisusaha;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name="T_JENIS_USAHA")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class JenisUsaha extends GenericModel{

	private static final long serialVersionUID = 1L;
	private Long id;
	private String tujuanPembiayaan;
	private String katBidangUsaha;
	private String descUsaha;
	private Long kdSectorEkonomi;
	private String sandiBaruDesc;
	private Date createdDate;
	
	@Id
	@Column(name="id", nullable=false, unique=true)
	@GeneratedValue
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="tujuan_pembiayaan", nullable=false)
	public String getTujuanPembiayaan() {
		return tujuanPembiayaan;
	}
	public void setTujuanPembiayaan(String tujuanPembiayaan) {
		this.tujuanPembiayaan = tujuanPembiayaan;
	}
	
	@Column(name="kat_bidang_usaha", nullable=false)
	public String getKatBidangUsaha() {
		return katBidangUsaha;
	}
	public void setKatBidangUsaha(String katBidangUsaha) {
		this.katBidangUsaha = katBidangUsaha;
	}
	
	@Column(name="desc_usaha", nullable=false)
	public String getDescUsaha() {
		return descUsaha;
	}
	public void setDescUsaha(String descUsaha) {
		this.descUsaha = descUsaha;
	}
	
	@Column(name="kd_sector_usaha", nullable=false)
	public Long getKdSectorEkonomi() {
		return kdSectorEkonomi;
	}
	public void setKdSectorEkonomi(Long kdSectorEkonomi) {
		this.kdSectorEkonomi = kdSectorEkonomi;
	}
	
	@Column(name="sandi_baru_desc", nullable=false)
	public String getSandiBaruDesc() {
		return sandiBaruDesc;
	}
	public void setSandiBaruDesc(String sandiBaruDesc) {
		this.sandiBaruDesc = sandiBaruDesc;
	}
	
	@Column(name="created_date", nullable=false)
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	@Override
	public String toString() {
		return "JenisUsaha [id=" + id + ", tujuanPembiayaan=" + tujuanPembiayaan + ", katBidangUsaha=" + katBidangUsaha
				+ ", descUsaha=" + descUsaha + ", kdSectorEkonomi=" + kdSectorEkonomi + ", sandiBaruDesc="
				+ sandiBaruDesc + ", createdDate=" + createdDate + "]";
	}
	

}
