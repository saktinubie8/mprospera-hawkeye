package id.co.telkomsigma.btpns.mprospera.request;

public class PhotoRequest extends BaseRequest {

    private String imei;
    private String username;
    private String sessionKey;
    private String longitude;
    private String latitude;
    private String prsId;
    private String surveyId;
    private String loanPrsId;
    private String deviationId;
    private String disbursementPhoto;
    private String sentraPhoto;
    private String deviationPhoto;
    private String surveyPhoto;
    private String sentraId;

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getPrsId() {
        return prsId;
    }

    public void setPrsId(String prsId) {
        this.prsId = prsId;
    }

    public String getDisbursementPhoto() {
        return disbursementPhoto;
    }

    public void setDisbursementPhoto(String disbursementPhoto) {
        this.disbursementPhoto = disbursementPhoto;
    }

    public String getLoanPrsId() {
        return loanPrsId;
    }

    public void setLoanPrsId(String loanPrsId) {
        this.loanPrsId = loanPrsId;
    }

    public String getSentraId() {
        return sentraId;
    }

    public void setSentraId(String sentraId) {
        this.sentraId = sentraId;
    }

    public String getSentraPhoto() {
        return sentraPhoto;
    }

    public void setSentraPhoto(String sentraPhoto) {
        this.sentraPhoto = sentraPhoto;
    }

    public String getDeviationId() {
        return deviationId;
    }

    public void setDeviationId(String deviationId) {
        this.deviationId = deviationId;
    }

    public String getDeviationPhoto() {
        return deviationPhoto;
    }

    public void setDeviationPhoto(String deviationPhoto) {
        this.deviationPhoto = deviationPhoto;
    }

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public String getSurveyPhoto() {
        return surveyPhoto;
    }

    public void setSurveyPhoto(String surveyPhoto) {
        this.surveyPhoto = surveyPhoto;
    }

    @Override
    public String toString() {
        return "PhotoRequest{" +
                "imei='" + imei + '\'' +
                ", username='" + username + '\'' +
                ", sessionKey='" + sessionKey + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", prsId='" + prsId + '\'' +
                ", surveyId='" + surveyId + '\'' +
                ", loanPrsId='" + loanPrsId + '\'' +
                ", deviationId='" + deviationId + '\'' +
                ", disbursementPhoto='" + disbursementPhoto + '\'' +
                ", sentraPhoto='" + sentraPhoto + '\'' +
                ", deviationPhoto='" + deviationPhoto + '\'' +
                ", surveyPhoto='" + surveyPhoto + '\'' +
                ", sentraId='" + sentraId + '\'' +
                '}';
    }

}