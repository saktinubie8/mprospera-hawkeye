package id.co.telkomsigma.btpns.mprospera.model.loan;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;

/**
 * Created by Dzulfiqar on 29/04/2017.
 */
@Entity
@Table(name = "T_LOAN_EMERGENCY_FUND_MAP")
public class LoanWithEmergencyFund extends GenericModel {

    private Long mappingId;
    private Long loanId;
    private Integer useEmergencyFundTotal;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue
    public Long getMappingId() {
        return mappingId;
    }

    public void setMappingId(Long mappingId) {
        this.mappingId = mappingId;
    }

    @Column(name = "loan_id")
    public Long getLoanId() {
        return loanId;
    }

    public void setLoanId(Long loanId) {
        this.loanId = loanId;
    }

    @Column(name = "total_emergency_fund")
    public Integer getUseEmergencyFundTotal() {
        return useEmergencyFundTotal;
    }

    public void setUseEmergencyFundTotal(Integer useEmergencyFundTotal) {
        this.useEmergencyFundTotal = useEmergencyFundTotal;
    }

}