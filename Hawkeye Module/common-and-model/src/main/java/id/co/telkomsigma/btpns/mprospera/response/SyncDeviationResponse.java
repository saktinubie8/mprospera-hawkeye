package id.co.telkomsigma.btpns.mprospera.response;

import id.co.telkomsigma.btpns.mprospera.request.WismaSWReq;

import java.util.List;

public class SyncDeviationResponse {

    private String deviationId;
    private String status;
    private String createdBy;
    private String createdDate;
    private String loanId;
    private String ap3rId;
    private String plafon;
    private String deviationReason;
    private String deviationCode;
    private String localId;
    private String batch;
    private WismaSWReq wisma;
    private List<DeviationApprovalHistory> approvalHistories;

    public String getDeviationId() {
        return deviationId;
    }

    public void setDeviationId(String deviationId) {
        this.deviationId = deviationId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getLoanId() {
        return loanId;
    }

    public void setLoanId(String loanId) {
        this.loanId = loanId;
    }

    public String getAp3rId() {
        return ap3rId;
    }

    public void setAp3rId(String ap3rId) {
        this.ap3rId = ap3rId;
    }

    public String getDeviationReason() {
        return deviationReason;
    }

    public void setDeviationReason(String deviationReason) {
        this.deviationReason = deviationReason;
    }

    public String getDeviationCode() {
        return deviationCode;
    }

    public void setDeviationCode(String deviationCode) {
        this.deviationCode = deviationCode;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public WismaSWReq getWisma() {
        return wisma;
    }

    public void setWisma(WismaSWReq wisma) {
        this.wisma = wisma;
    }

    public List<DeviationApprovalHistory> getApprovalHistories() {
        return approvalHistories;
    }

    public void setApprovalHistories(List<DeviationApprovalHistory> approvalHistories) {
        this.approvalHistories = approvalHistories;
    }

    public String getPlafon() {
        return plafon;
    }

    public void setPlafon(String plafon) {
        this.plafon = plafon;
    }

}