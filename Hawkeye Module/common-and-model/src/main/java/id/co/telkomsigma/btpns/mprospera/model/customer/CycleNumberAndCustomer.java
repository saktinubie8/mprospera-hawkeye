package id.co.telkomsigma.btpns.mprospera.model.customer;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;


import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;





@SqlResultSetMapping(
        name = "CustomerCycleNumberMapping",
        entities = {
            @EntityResult(
                    entityClass = CycleNumberAndCustomer.class,
                    fields = {
                        @FieldResult(name = "cycleNumber", column = "cycle_number"),
                        @FieldResult(name = "customerId", column = "id"),
                        @FieldResult(name = "approvedBy", column = "approved_by"),
                        @FieldResult(name = "assignedUsername", column = "assigned_username"),
                        @FieldResult(name = "createdBy", column = "created_by"),
                        @FieldResult(name = "createdDate", column = "created_dt"),
                        @FieldResult(name = "customerCifNumber", column = "cif_number"),
                        @FieldResult(name = "customerIdNumber", column = "id_number"),
                        @FieldResult(name = "customerName", column = "name"),
                        @FieldResult(name = "customerStatus", column = "status"),
                        @FieldResult(name = "isDeleted", column = "is_deleted"),
                        @FieldResult(name = "latitude", column = "latitude"),
                        @FieldResult(name = "longitude", column = "longitude"),
                        @FieldResult(name = "pdkId", column = "pdk_id"),
                        @FieldResult(name = "prosperaId", column = "prospera_id"),
                        @FieldResult(name = "rrn", column = "rrn"),
                        @FieldResult(name = "swId", column = "sw_id"),
                        @FieldResult(name = "groupId", column = "group_id"),
                        @FieldResult(name = "customerIdAddress", column = "id_address"),
                        @FieldResult(name = "customerIdName", column = "id_name"),
                        @FieldResult(name = "dateOfBirth", column = "date_of_birth"),
                        @FieldResult(name = "dateOfGovernmentId", column = "date_of_government_id"),
                        @FieldResult(name = "gender", column = "gender"),
                        @FieldResult(name = "maritalStatus", column = "marital_status"),
                        @FieldResult(name = "otherAddress", column = "other_address"),
                        @FieldResult(name = "phoneNumber", column = "phone_number"),
                        @FieldResult(name = "placeOfBirth", column = "place_of_birth"),
                        @FieldResult(name = "religion", column = "religion"),
                        @FieldResult(name = "spouseDateOfBirth", column = "spouse_date_of_birth"),
                        @FieldResult(name = "spouseName", column = "spouse_name"),
                        @FieldResult(name = "spousePlaceOfBirth", column = "spouse_place_of_birth"),
                        @FieldResult(name = "approvedDate", column = "approved_dt"),
                        @FieldResult(name = "city", column = "city"),
                        @FieldResult(name = "customerAlias", column = "alias"),
                        @FieldResult(name = "education", column = "education"),
                        @FieldResult(name = "houseType", column = "house_type"),
                        @FieldResult(name = "jumlahTanggungan", column = "jml_tanggungan"),
                        @FieldResult(name = "kecamatan", column = "kecamatan"),
                        @FieldResult(name = "kelurahan", column = "kelurahan"),
                        @FieldResult(name = "motherName", column = "mother_name"),
                        @FieldResult(name = "postalCode", column = "postal_code"),
                        @FieldResult(name = "province", column = "province"),
                        @FieldResult(name = "rtrw", column = "rt_rw"),
                        @FieldResult(name = "address", column = "address"),
                        @FieldResult(name = "sentraLocation", column = "sentra_location"),
                        
                        }
              )
       }
 )

@Entity
public class CycleNumberAndCustomer implements Serializable {
		
		/**
	 * 
	 */
	private static final long serialVersionUID = 5427108950332130276L;
		protected Integer cycleNumber;
	@Id protected Long customerId;
	    protected String customerCifNumber;
	    protected String customerName;
	    protected String customerAlias;
	    protected String customerIdNumber;
	    protected String customerIdName;
	    protected String customerIdAddress;
	    protected String address;
	    protected String otherAddress;
	    protected String rtrw;
	    protected String kelurahan;
	    protected String kecamatan;
	    protected String city;
	    protected String province;
	    protected String postalCode;
	    protected String motherName;
	    protected String jumlahTanggungan;
	    protected String education;
	    protected String houseType;
	    protected String placeOfBirth;
	    protected Date dateOfBirth;
	    protected String spouseName;
	    protected String spousePlaceOfBirth;
	    protected Date spouseDateOfBirth;
	    protected String phoneNumber;
	    protected String religion;
	    protected String maritalStatus;
	    protected String gender;
	    protected Date dateOfGovernmentId;
	    protected Date createdDate;
	    protected String createdBy;
	    protected String customerStatus;
	    protected String assignedUsername;
	    protected Long pdkId;
	    protected Long swId;
	    protected String longitude;
	    protected String latitude;
	    protected String prosperaId;
	    private String rrn;
	    private String approvedBy;
	    private Date approvedDate;
	    protected Boolean isDeleted = false;
	    protected Long groupId;
	    private Long sentraLocation;
	    
	    
	    
		public Integer getCycleNumber() {
			return cycleNumber;
		}
		public void setCycleNumber(Integer cycleNumber) {
			this.cycleNumber = cycleNumber;
		}
		public Long getCustomerId() {
			return customerId;
		}
		public void setCustomerId(Long customerId) {
			this.customerId = customerId;
		}
		public String getCustomerCifNumber() {
			return customerCifNumber;
		}
		public void setCustomerCifNumber(String customerCifNumber) {
			this.customerCifNumber = customerCifNumber;
		}
		public String getCustomerName() {
			return customerName;
		}
		public void setCustomerName(String customerName) {
			this.customerName = customerName;
		}
		public String getCustomerAlias() {
			return customerAlias;
		}
		public void setCustomerAlias(String customerAlias) {
			this.customerAlias = customerAlias;
		}
		public String getCustomerIdNumber() {
			return customerIdNumber;
		}
		public void setCustomerIdNumber(String customerIdNumber) {
			this.customerIdNumber = customerIdNumber;
		}
		public String getCustomerIdName() {
			return customerIdName;
		}
		public void setCustomerIdName(String customerIdName) {
			this.customerIdName = customerIdName;
		}
		public String getCustomerIdAddress() {
			return customerIdAddress;
		}
		public void setCustomerIdAddress(String customerIdAddress) {
			this.customerIdAddress = customerIdAddress;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getOtherAddress() {
			return otherAddress;
		}
		public void setOtherAddress(String otherAddress) {
			this.otherAddress = otherAddress;
		}
		public String getRtrw() {
			return rtrw;
		}
		public void setRtrw(String rtrw) {
			this.rtrw = rtrw;
		}
		public String getKelurahan() {
			return kelurahan;
		}
		public void setKelurahan(String kelurahan) {
			this.kelurahan = kelurahan;
		}
		public String getKecamatan() {
			return kecamatan;
		}
		public void setKecamatan(String kecamatan) {
			this.kecamatan = kecamatan;
		}
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public String getProvince() {
			return province;
		}
		public void setProvince(String province) {
			this.province = province;
		}
		public String getPostalCode() {
			return postalCode;
		}
		public void setPostalCode(String postalCode) {
			this.postalCode = postalCode;
		}
		public String getMotherName() {
			return motherName;
		}
		public void setMotherName(String motherName) {
			this.motherName = motherName;
		}
		public String getJumlahTanggungan() {
			return jumlahTanggungan;
		}
		public void setJumlahTanggungan(String jumlahTanggungan) {
			this.jumlahTanggungan = jumlahTanggungan;
		}
		public String getEducation() {
			return education;
		}
		public void setEducation(String education) {
			this.education = education;
		}
		public String getHouseType() {
			return houseType;
		}
		public void setHouseType(String houseType) {
			this.houseType = houseType;
		}
		public String getPlaceOfBirth() {
			return placeOfBirth;
		}
		public void setPlaceOfBirth(String placeOfBirth) {
			this.placeOfBirth = placeOfBirth;
		}
		public Date getDateOfBirth() {
			return dateOfBirth;
		}
		public void setDateOfBirth(Date dateOfBirth) {
			this.dateOfBirth = dateOfBirth;
		}
		public String getSpouseName() {
			return spouseName;
		}
		public void setSpouseName(String spouseName) {
			this.spouseName = spouseName;
		}
		public String getSpousePlaceOfBirth() {
			return spousePlaceOfBirth;
		}
		public void setSpousePlaceOfBirth(String spousePlaceOfBirth) {
			this.spousePlaceOfBirth = spousePlaceOfBirth;
		}
		public Date getSpouseDateOfBirth() {
			return spouseDateOfBirth;
		}
		public void setSpouseDateOfBirth(Date spouseDateOfBirth) {
			this.spouseDateOfBirth = spouseDateOfBirth;
		}
		public String getPhoneNumber() {
			return phoneNumber;
		}
		public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}
		public String getReligion() {
			return religion;
		}
		public void setReligion(String religion) {
			this.religion = religion;
		}
		public String getMaritalStatus() {
			return maritalStatus;
		}
		public void setMaritalStatus(String maritalStatus) {
			this.maritalStatus = maritalStatus;
		}
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			this.gender = gender;
		}
		public Date getDateOfGovernmentId() {
			return dateOfGovernmentId;
		}
		public void setDateOfGovernmentId(Date dateOfGovernmentId) {
			this.dateOfGovernmentId = dateOfGovernmentId;
		}
		public Date getCreatedDate() {
			return createdDate;
		}
		public void setCreatedDate(Date createdDate) {
			this.createdDate = createdDate;
		}
		public String getCreatedBy() {
			return createdBy;
		}
		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}
		public String getCustomerStatus() {
			return customerStatus;
		}
		public void setCustomerStatus(String customerStatus) {
			this.customerStatus = customerStatus;
		}
		public String getAssignedUsername() {
			return assignedUsername;
		}
		public void setAssignedUsername(String assignedUsername) {
			this.assignedUsername = assignedUsername;
		}
		public Long getPdkId() {
			return pdkId;
		}
		public void setPdkId(Long pdkId) {
			this.pdkId = pdkId;
		}
		public Long getSwId() {
			return swId;
		}
		public void setSwId(Long swId) {
			this.swId = swId;
		}
		public String getLongitude() {
			return longitude;
		}
		public void setLongitude(String longitude) {
			this.longitude = longitude;
		}
		public String getLatitude() {
			return latitude;
		}
		public void setLatitude(String latitude) {
			this.latitude = latitude;
		}
		public String getProsperaId() {
			return prosperaId;
		}
		public void setProsperaId(String prosperaId) {
			this.prosperaId = prosperaId;
		}
		public String getRrn() {
			return rrn;
		}
		public void setRrn(String rrn) {
			this.rrn = rrn;
		}
		public String getApprovedBy() {
			return approvedBy;
		}
		public void setApprovedBy(String approvedBy) {
			this.approvedBy = approvedBy;
		}
		public Date getApprovedDate() {
			return approvedDate;
		}
		public void setApprovedDate(Date approvedDate) {
			this.approvedDate = approvedDate;
		}
		public Boolean getIsDeleted() {
			return isDeleted;
		}
		public void setIsDeleted(Boolean isDeleted) {
			this.isDeleted = isDeleted;
		}
		public Long getGroupId() {
			return groupId;
		}
		public void setGroupId(Long groupId) {
			this.groupId = groupId;
		}
		public Long getSentraLocation() {
			return sentraLocation;
		}
		public void setSentraLocation(Long sentraLocation) {
			this.sentraLocation = sentraLocation;
		}
}
