package id.co.telkomsigma.btpns.mprospera.model.sw;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;
import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "T_AWGM_BUY")
public class AWGMBuyThings extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Long awgmBuyId;
    private String namaBarang;
    private BigDecimal buyPrice;
    private BigDecimal sellPrice;
    private Integer total;
    private Integer index;
    private Integer type;
    private Integer frequency;
    private SurveyWawancara swId;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getAwgmBuyId() {
        return awgmBuyId;
    }

    public void setAwgmBuyId(Long awgmBuyId) {
        this.awgmBuyId = awgmBuyId;
    }

    @Column(name = "nama_barang")
    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    @Column(name = "buy_price")
    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(BigDecimal buyPrice) {
        this.buyPrice = buyPrice;
    }

    @Column(name = "sell_price")
    public BigDecimal getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(BigDecimal sellPrice) {
        this.sellPrice = sellPrice;
    }

    @Column(name = "total")
    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = SurveyWawancara.class)
    @JoinColumn(name = "sw_id", referencedColumnName = "id", nullable = true)
    public SurveyWawancara getSwId() {
        return swId;
    }

    public void setSwId(SurveyWawancara swId) {
        this.swId = swId;
    }

    @Column(name = "frequency")
    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    @Column(name = "index_item")
    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    @Column(name = "type")
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

}