package id.co.telkomsigma.btpns.mprospera.request;

public class ErrorLogRequest {

    private String id;
    private String created_at;
    private RealTraceRequest realTrace;
    private String stackTrace;
    private String message;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public RealTraceRequest getRealTrace() {
        return realTrace;
    }

    public void setRealTrace(RealTraceRequest realTrace) {
        this.realTrace = realTrace;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStackTrace() {
        return stackTrace;
    }

    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }

    @Override
    public String toString() {
        return "ErrorLogRequest [id=" + id + ", created_at=" + created_at + ", realTrace=" + realTrace
                + ", stackTrace=" + stackTrace + ", messages=" + message + "]";
    }

}