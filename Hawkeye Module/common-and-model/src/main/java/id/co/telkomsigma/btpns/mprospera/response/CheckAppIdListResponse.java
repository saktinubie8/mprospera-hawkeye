package id.co.telkomsigma.btpns.mprospera.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class CheckAppIdListResponse {

    private String appId;
    private String name;
    private String idNumberCustomer;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdNumberCustomer() {
        return idNumberCustomer;
    }

    public void setIdNumberCustomer(String idNumberCustomer) {
        this.idNumberCustomer = idNumberCustomer;
    }

}