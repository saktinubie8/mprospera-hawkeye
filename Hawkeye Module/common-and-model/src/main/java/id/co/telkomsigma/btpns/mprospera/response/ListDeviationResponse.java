package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class ListDeviationResponse extends BaseResponse {

    private List<SyncDeviationResponse> deviationList;
    private List<String> deletedIds;

    public List<String> getDeletedIds() {
        return deletedIds;
    }

    public void setDeletedIds(List<String> deletedIds) {
        this.deletedIds = deletedIds;
    }

    public List<SyncDeviationResponse> getDeviationList() {
        return deviationList;
    }

    public void setDeviationList(List<SyncDeviationResponse> deviationList) {
        this.deviationList = deviationList;
    }

}