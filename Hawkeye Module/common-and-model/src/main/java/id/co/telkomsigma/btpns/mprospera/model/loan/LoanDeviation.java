package id.co.telkomsigma.btpns.mprospera.model.loan;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Dzulfiqar on 25/04/2017.
 */
@Entity
@Table(name = "T_LOAN_DEVIATION")
public class LoanDeviation extends GenericModel {

    private Long deviationId;
    private Long loanId;
    private Long ap3rId;
    private String batch;
    private String status;
    private Long userBwmp;
    private String deviationReason;
    private String deviationCode;
    private String createdBy;
    private Date createdDate;
    protected Boolean isDeleted = false;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue
    public Long getDeviationId() {
        return deviationId;
    }

    public void setDeviationId(Long deviationId) {
        this.deviationId = deviationId;
    }

    @Column(name = "loan_id")
    public Long getLoanId() {
        return loanId;
    }

    public void setLoanId(Long loanId) {
        this.loanId = loanId;
    }

    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "user_bwmp")
    public Long getUserBwmp() {
        return userBwmp;
    }

    public void setUserBwmp(Long userBwmp) {
        this.userBwmp = userBwmp;
    }

    @Column(name = "deviation_reason")
    public String getDeviationReason() {
        return deviationReason;
    }

    public void setDeviationReason(String deviationReason) {
        this.deviationReason = deviationReason;
    }

    @Column(name = "deviation_code")
    public String getDeviationCode() {
        return deviationCode;
    }

    public void setDeviationCode(String deviationCode) {
        this.deviationCode = deviationCode;
    }

    @Column(name = "created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "batch")
    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    @Column(name = "ap3r_id")
    public Long getAp3rId() {
        return ap3rId;
    }

    public void setAp3rId(Long ap3rId) {
        this.ap3rId = ap3rId;
    }

    @Column(name = "created_date")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "is_deleted")
    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

}