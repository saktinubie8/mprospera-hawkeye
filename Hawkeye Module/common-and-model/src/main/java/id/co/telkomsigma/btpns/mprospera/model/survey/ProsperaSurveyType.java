package id.co.telkomsigma.btpns.mprospera.model.survey;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OrderBy;

@Entity
@Table(name = "M_PROSPERA_SURVEY")
public class ProsperaSurveyType extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Long surveyTypeId;
    private String surveyName;
    private Boolean isActive;
    private Boolean isPpi = false;
    private List<ProsperaSurveyQuestion> questions;

    @Column(name = "is_ppi", nullable = true)
    public Boolean getIsPpi() {
        return isPpi;
    }

    public void setIsPpi(Boolean isPpi) {
        this.isPpi = isPpi;
    }

    @OneToMany(fetch = FetchType.LAZY, targetEntity = ProsperaSurveyQuestion.class)
    @JoinColumn(name = "survey_id")
    @OrderBy(clause = "survey_id asc")
    public List<ProsperaSurveyQuestion> getQuestions() {
        return questions;
    }

    public void setQuestions(List<ProsperaSurveyQuestion> questions) {
        this.questions = questions;
    }

    @Id
    @Column(name = "survey_id", unique = true, nullable = false)
    public Long getSurveyTypeId() {
        return surveyTypeId;
    }

    public void setSurveyTypeId(Long surveyId) {
        this.surveyTypeId = surveyId;
    }

    @Column(name = "survey_name")
    public String getSurveyName() {
        return surveyName;
    }

    public void setSurveyName(String surveyName) {
        this.surveyName = surveyName;
    }

    @Column(name = "is_active")
    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

}