package id.co.telkomsigma.btpns.mprospera.model.loan;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;

/**
 * Created by Dzulfiqar on 10/05/2017.
 */
@Entity
@Table(name = "T_DEVIATION_PHOTO")
public class DeviationPhoto extends GenericModel {

    private static final long serialVersionUID = 6559145228034293868L;
    private Long id;
    private Long deviationId;
    private byte[] deviationPhoto;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "deviation_id")
    public Long getDeviationId() {
        return deviationId;
    }

    public void setDeviationId(Long deviationId) {
        this.deviationId = deviationId;
    }

    @Column(name = "deviation_photo")
    public byte[] getDeviationPhoto() {
        return deviationPhoto;
    }

    public void setDeviationPhoto(byte[] deviationPhoto) {
        this.deviationPhoto = deviationPhoto;
    }

}