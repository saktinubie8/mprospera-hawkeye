package id.co.telkomsigma.btpns.mprospera.request;

public class DetailDeviationRequest extends BaseRequest {

    private String username;
    private String sessionKey;
    private String imei;
    private String batch;
    private String startLookupDate;
    private String endLookupDate;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getStartLookupDate() {
        return startLookupDate;
    }

    public void setStartLookupDate(String startLookupDate) {
        this.startLookupDate = startLookupDate;
    }

    public String getEndLookupDate() {
        return endLookupDate;
    }

    public void setEndLookupDate(String endLookupDate) {
        this.endLookupDate = endLookupDate;
    }

    @Override
    public String toString() {
        return "DetailDeviationRequest{" +
                "username='" + username + '\'' +
                ", sessionKey='" + sessionKey + '\'' +
                ", imei='" + imei + '\'' +
                ", batch='" + batch + '\'' +
                ", startLookupDate='" + startLookupDate + '\'' +
                ", endLookupDate='" + endLookupDate + '\'' +
                '}';
    }

}