package id.co.telkomsigma.btpns.mprospera.response;

/**
 * Created by Dzulfiqar on 13/04/2017.
 */
public class SWPartnerResponse {

    private String birthDay;
    private String birthPlace;
    private String birthPlaceId;
    private String job;
    private String name;

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getBirthPlaceId() {
        return birthPlaceId;
    }

    public void setBirthPlaceId(String birthPlaceId) {
        this.birthPlaceId = birthPlaceId;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}