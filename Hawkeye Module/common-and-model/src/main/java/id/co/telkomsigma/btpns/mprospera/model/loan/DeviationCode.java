package id.co.telkomsigma.btpns.mprospera.model.loan;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;

/**
 * Created by Dzulfiqar on 6/05/2017.
 */
@Entity
@Table(name = "T_DEVIATION_CODE")
public class DeviationCode extends GenericModel {

    private Long id;
    private String deviationCode;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "deviation_code")
    public String getDeviationCode() {
        return deviationCode;
    }

    public void setDeviationCode(String deviationCode) {
        this.deviationCode = deviationCode;
    }

}