package id.co.telkomsigma.btpns.mprospera.response;

import id.co.telkomsigma.btpns.mprospera.request.FundedThingsReq;

import java.util.List;

/**
 * Created by Dzulfiqar on 27/04/2017.
 */
public class AP3RList {

    private String ap3rId;
    private String swId;
    private String haveAccount;
    private String accountPurpose;
    private String fundSource;
    private String yearlyTransaction;
    private String financingPurpose;
    private String businessTypeId;
    private String disburseDate;
    private String highRiskCustomer;
    private String highRiskBusiness;
    private String swProductId;
    private List<FundedThingsReq> financedGoods;
    private String status;
    private String createdBy;
    private String dueDate;
    private String rejectionReason;
    private String approvalStatus;
    private String deviationExists;

    public String getAp3rId() {
        return ap3rId;
    }

    public void setAp3rId(String ap3rId) {
        this.ap3rId = ap3rId;
    }

    public String getSwId() {
        return swId;
    }

    public void setSwId(String swId) {
        this.swId = swId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHaveAccount() {
        return haveAccount;
    }

    public void setHaveAccount(String haveAccount) {
        this.haveAccount = haveAccount;
    }

    public String getAccountPurpose() {
        return accountPurpose;
    }

    public void setAccountPurpose(String accountPurpose) {
        this.accountPurpose = accountPurpose;
    }

    public String getFundSource() {
        return fundSource;
    }

    public void setFundSource(String fundSource) {
        this.fundSource = fundSource;
    }

    public String getYearlyTransaction() {
        return yearlyTransaction;
    }

    public void setYearlyTransaction(String yearlyTransaction) {
        this.yearlyTransaction = yearlyTransaction;
    }

    public String getFinancingPurpose() {
        return financingPurpose;
    }

    public void setFinancingPurpose(String financingPurpose) {
        this.financingPurpose = financingPurpose;
    }

    public String getBusinessTypeId() {
        return businessTypeId;
    }

    public void setBusinessTypeId(String businessTypeId) {
        this.businessTypeId = businessTypeId;
    }

    public String getDisburseDate() {
        return disburseDate;
    }

    public void setDisburseDate(String disburseDate) {
        this.disburseDate = disburseDate;
    }

    public String getHighRiskCustomer() {
        return highRiskCustomer;
    }

    public void setHighRiskCustomer(String highRiskCustomer) {
        this.highRiskCustomer = highRiskCustomer;
    }

    public String getHighRiskBusiness() {
        return highRiskBusiness;
    }

    public void setHighRiskBusiness(String highRiskBusiness) {
        this.highRiskBusiness = highRiskBusiness;
    }

    public List<FundedThingsReq> getFinancedGoods() {
        return financedGoods;
    }

    public void setFinancedGoods(List<FundedThingsReq> financedGoods) {
        this.financedGoods = financedGoods;
    }

    public String getSwProductId() {
        return swProductId;
    }

    public void setSwProductId(String swProductId) {
        this.swProductId = swProductId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getRejectionReason() {
        return rejectionReason;
    }

    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getDeviationExists() {
        return deviationExists;
    }

    public void setDeviationExists(String deviationExists) {
        this.deviationExists = deviationExists;
    }

}