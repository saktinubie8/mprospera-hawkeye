package id.co.telkomsigma.btpns.mprospera.model.sw;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;

@Entity
@Table(name = "T_SW_SURVEY_PHOTO")
public class SwSurveyPhoto extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = 1066759672688615103L;
    private Long id;
    private SurveyWawancara swId;
    private byte[] surveyPhoto;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = SurveyWawancara.class)
    @JoinColumn(name = "sw_id", referencedColumnName = "id", nullable = true)
    public SurveyWawancara getSwId() {
        return swId;
    }

    public void setSwId(SurveyWawancara swId) {
        this.swId = swId;
    }

    @Column(name = "survey_photo", nullable = true)
    public byte[] getSurveyPhoto() {
        return surveyPhoto;
    }

    public void setSurveyPhoto(byte[] surveyPhoto) {
        this.surveyPhoto = surveyPhoto;
    }

}