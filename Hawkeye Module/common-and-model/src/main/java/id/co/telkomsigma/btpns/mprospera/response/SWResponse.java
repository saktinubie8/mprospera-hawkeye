package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class SWResponse extends BaseResponse {

    /* untuk addSW */
    private String swId;
    /* untuk list SW */
    private String grandTotal;
    private String currentTotal;
    private String totalPage;
    private List<ListSWResponse> swList;

    public String getSwId() {
        return swId;
    }

    public void setSwId(String swId) {
        this.swId = swId;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getCurrentTotal() {
        return currentTotal;
    }

    public void setCurrentTotal(String currentTotal) {
        this.currentTotal = currentTotal;
    }

    public List<ListSWResponse> getSwList() {
        return swList;
    }

    public void setSwList(List<ListSWResponse> swList) {
        this.swList = swList;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    @Override
    public String toString() {
        return "SWResponse [swId=" + swId + ", grandTotal=" + grandTotal + ", currentTotal=" + currentTotal
                + ", totalPage=" + totalPage + ", swList=" + swList + ", getResponseCode()=" + getResponseCode()
                + ", getResponseMessage()=" + getResponseMessage() + "]";
    }

}