package id.co.telkomsigma.btpns.mprospera.request;

public class LoanRefreshListRequest {

    private String appId;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    @Override
    public String toString() {
        return "LoanRefreshListRequest [appId=" + appId + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
                + ", toString()=" + super.toString() + "]";
    }

}