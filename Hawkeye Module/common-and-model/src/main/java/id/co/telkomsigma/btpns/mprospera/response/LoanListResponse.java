package id.co.telkomsigma.btpns.mprospera.response;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class LoanListResponse {

    private String loanId;
    private String customerName;
    private String customerId;
    private String appId;
    private String status;
    private String accountNumber;
    private String disbursementDate;
    private String swId;
    private String createdBy;
    private String createdDate;
    private String prosperaId;
    private String loanAmountRecommended;
    private String productId;
    private String plafond;
    private String useEmergencyFundTotal;
    private String ap3rId;
    private BigDecimal remainingPrincipal;
    private String dueDate;
    private List<String> productSuggestion;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getDisbursementDate() {
        return disbursementDate;
    }

    public void setDisbursementDate(String disbursementDate) {
        this.disbursementDate = disbursementDate;
    }

    public String getSwId() {
        return swId;
    }

    public void setSwId(String swId) {
        this.swId = swId;
    }

    public String getProsperaId() {
        return prosperaId;
    }

    public void setProsperaId(String prosperaId) {
        this.prosperaId = prosperaId;
    }

    public String getLoanId() {
        return loanId;
    }

    public void setLoanId(String loanId) {
        this.loanId = loanId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getLoanAmountRecommended() {
        return loanAmountRecommended;
    }

    public void setLoanAmountRecommended(String loanAmountRecommended) {
        this.loanAmountRecommended = loanAmountRecommended;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getPlafond() {
        return plafond;
    }

    public void setPlafond(String plafond) {
        this.plafond = plafond;
    }

    public String getUseEmergencyFundTotal() {
        return useEmergencyFundTotal;
    }

    public void setUseEmergencyFundTotal(String useEmergencyFundTotal) {
        this.useEmergencyFundTotal = useEmergencyFundTotal;
    }

    public String getAp3rId() {
        return ap3rId;
    }

    public void setAp3rId(String ap3rId) {
        this.ap3rId = ap3rId;
    }

    public BigDecimal getRemainingPrincipal() {
        return remainingPrincipal;
    }

    public void setRemainingPrincipal(BigDecimal remainingPrincipal) {
        this.remainingPrincipal = remainingPrincipal;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

	public List<String> getProductSuggestion() {
		if(productSuggestion==null) {
			productSuggestion = new ArrayList<String>();
		}
		return productSuggestion;
	}

	public void setProductSuggestion(List<String> productSuggestion) {
		this.productSuggestion = productSuggestion;
	}

	@Override
    public String toString() {
        return "LoanListResponse{" +
                "loanId='" + loanId + '\'' +
                ", customerName='" + customerName + '\'' +
                ", customerId='" + customerId + '\'' +
                ", appId='" + appId + '\'' +
                ", status='" + status + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", disbursementDate='" + disbursementDate + '\'' +
                ", swId='" + swId + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", prosperaId='" + prosperaId + '\'' +
                ", loanAmountRecommended='" + loanAmountRecommended + '\'' +
                ", productId='" + productId + '\'' +
                ", plafond='" + plafond + '\'' +
                ", useEmergencyFundTotal='" + useEmergencyFundTotal + '\'' +
                ", ap3rId='" + ap3rId + '\'' +
                ", remainingPrincipal=" + remainingPrincipal +
                ", dueDate='" + dueDate + '\'' +
                '}';
    }

    //============================================
    private int content;
    private int totalElements;
    private int totalPages;

    public int getContent() {
        return content;
    }

    public void setContent(int content) {
        this.content = content;
    }

    public int getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(int totalElements) {
        this.totalElements = totalElements;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    //==============================================
}