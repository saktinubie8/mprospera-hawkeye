package id.co.telkomsigma.btpns.mprospera.response;

import java.math.BigDecimal;
import java.util.List;

public class LoanHistResponse extends BaseResponse {

    private String appId;
    private BigDecimal outstandingAmount;
    private String outstandingCount;
    private List<HistoryList> historyList;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public BigDecimal getOutstandingAmount() {
        return outstandingAmount;
    }

    public void setOutstandingAmount(BigDecimal outstandingAmount) {
        this.outstandingAmount = outstandingAmount;
    }

    public String getOutstandingCount() {
        return outstandingCount;
    }

    public void setOutstandingCount(String outstandingCount) {
        this.outstandingCount = outstandingCount;
    }

    public List<HistoryList> getHistoryList() {
        return historyList;
    }

    public void setHistoryList(List<HistoryList> historyList) {
        this.historyList = historyList;
    }

    @Override
    public String toString() {
        return "LoanHistResponse [appId=" + appId + ", outstandingAmount=" + outstandingAmount + ", outstandingCount="
                + outstandingCount + ", historyList=" + historyList + ", getResponseCode()=" + getResponseCode()
                + ", getResponseMessage()=" + getResponseMessage() + ", toString()=" + super.toString()
                + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
    }

}