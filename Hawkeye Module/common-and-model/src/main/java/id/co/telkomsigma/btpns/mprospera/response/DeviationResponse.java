package id.co.telkomsigma.btpns.mprospera.response;

import id.co.telkomsigma.btpns.mprospera.request.WismaSWReq;

import java.util.List;

/**
 * Created by Dzulfiqar on 25/04/2017.
 */
public class DeviationResponse extends BaseResponse {

    private String deviationId;
    private String status;
    private String createdBy;
    private String createdDate;
    private String loanId;
    private String ap3rId;
    private String deviationReason;
    private String deviationCode;
    private String localId;
    private String batch;
    private WismaSWReq wisma;
    private List<DeviationApprovalHistory> approvalHistories;
    private List<String> deletedIds;
    private List<String> deletedSwList;
    private List<String> deletedCustomerList;
    // forList
    private List<DeviationResponse> deviationList;
    private List<AP3RList> ap3rList;
    private List<LoanListResponse> loanList;
    private List<CustomerPojo> customerList;
    private List<ListSWResponse> swList;

    public String getDeviationId() {
        return deviationId;
    }

    public void setDeviationId(String deviationId) {
        this.deviationId = deviationId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLoanId() {
        return loanId;
    }

    public void setLoanId(String loanId) {
        this.loanId = loanId;
    }

    public List<DeviationResponse> getDeviationList() {
        return deviationList;
    }

    public void setDeviationList(List<DeviationResponse> deviationList) {
        this.deviationList = deviationList;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public List<DeviationApprovalHistory> getApprovalHistories() {
        return approvalHistories;
    }

    public void setApprovalHistories(List<DeviationApprovalHistory> approvalHistories) {
        this.approvalHistories = approvalHistories;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getAp3rId() {
        return ap3rId;
    }

    public void setAp3rId(String ap3rId) {
        this.ap3rId = ap3rId;
    }

    public String getDeviationReason() {
        return deviationReason;
    }

    public void setDeviationReason(String deviationReason) {
        this.deviationReason = deviationReason;
    }

    public String getDeviationCode() {
        return deviationCode;
    }

    public void setDeviationCode(String deviationCode) {
        this.deviationCode = deviationCode;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public List<AP3RList> getAp3rList() {
        return ap3rList;
    }

    public void setAp3rList(List<AP3RList> ap3rList) {
        this.ap3rList = ap3rList;
    }

    public List<LoanListResponse> getLoanList() {
        return loanList;
    }

    public void setLoanList(List<LoanListResponse> loanList) {
        this.loanList = loanList;
    }

    public List<CustomerPojo> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<CustomerPojo> customerList) {
        this.customerList = customerList;
    }

    public List<ListSWResponse> getSwList() {
        return swList;
    }

    public void setSwList(List<ListSWResponse> swList) {
        this.swList = swList;
    }

    public WismaSWReq getWisma() {
        return wisma;
    }

    public void setWisma(WismaSWReq wisma) {
        this.wisma = wisma;
    }

    public List<String> getDeletedIds() {
        return deletedIds;
    }

    public void setDeletedIds(List<String> deletedIds) {
        this.deletedIds = deletedIds;
    }

    public List<String> getDeletedSwList() {
        return deletedSwList;
    }

    public void setDeletedSwList(List<String> deletedSwList) {
        this.deletedSwList = deletedSwList;
    }

    public List<String> getDeletedCustomerList() {
        return deletedCustomerList;
    }

    public void setDeletedCustomerList(List<String> deletedCustomerList) {
        this.deletedCustomerList = deletedCustomerList;
    }

    @Override
    public String toString() {
        return "DeviationResponse{" +
                "deviationId='" + deviationId + '\'' +
                ", status='" + status + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", loanId='" + loanId + '\'' +
                ", ap3rId='" + ap3rId + '\'' +
                ", deviationReason='" + deviationReason + '\'' +
                ", deviationCode='" + deviationCode + '\'' +
                ", localId='" + localId + '\'' +
                ", batch='" + batch + '\'' +
                ", wisma=" + wisma +
                ", approvalHistories=" + approvalHistories +
                ", deletedIds=" + deletedIds +
                ", deletedSwList=" + deletedSwList +
                ", deletedCustomerList=" + deletedCustomerList +
                ", deviationList=" + deviationList +
                ", ap3rList=" + ap3rList +
                ", loanList=" + loanList +
                ", customerList=" + customerList +
                ", swList=" + swList +
                '}';
    }

}