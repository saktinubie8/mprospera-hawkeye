package id.co.telkomsigma.btpns.mprospera.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class SentraRequest extends BaseRequest {

    // untuk add
    private String transmissionDateAndTime;
    private String retrievalReferenceNumber;
    private String username;
    private String sessionKey;
    private String imei;
    private String areaId;
    private String sentraName;
    private String sentraOwnerName;
    private String startedDate;
    private String sentraAddress;
    private String rtRw;
    private String prsDay;
    private String sentraPicture;
    private String longitude;
    private String latitude;
    private String action;
    private String sentraId;
    private String status;
    private String sentraLeader;
    private String meetingTime;
    private String localId;

    // untuk list
    private String getCountData;
    private String page;
    private String startLookupDate;
    private String endLookupDate;

    // Prospera purpose
    private String provinsi;
    private String kabupaten;
    private String kecamatan;
    private String kelurahan;
    private String officeCode;
    private String meetingPlace;
    private String dayNumber;
    private String recurAfter;
    private String postcode;
    private String reffId;
    private String customerNumberLeader;
    private String actionTime;
    private String tokenKey;

    // private String centerCodeOfficer;

    public String getTransmissionDateAndTime() {
        return transmissionDateAndTime;
    }

    public void setTransmissionDateAndTime(String transmissionDateAndTime) {
        this.transmissionDateAndTime = transmissionDateAndTime;
    }

    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }

    public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
        this.retrievalReferenceNumber = retrievalReferenceNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getSentraName() {
        return sentraName;
    }

    public void setSentraName(String sentraName) {
        this.sentraName = sentraName;
    }

    public String getSentraOwnerName() {
        return sentraOwnerName;
    }

    public void setSentraOwnerName(String sentraOwnerName) {
        this.sentraOwnerName = sentraOwnerName;
    }

    public String getStartedDate() {
        return startedDate;
    }

    public void setStartedDate(String startedDate) {
        this.startedDate = startedDate;
    }

    public String getSentraAddress() {
        return sentraAddress;
    }

    public void setSentraAddress(String sentraAddress) {
        this.sentraAddress = sentraAddress;
    }

    public String getRtRw() {
        return rtRw;
    }

    public void setRtRw(String rtRw) {
        this.rtRw = rtRw;
    }

    public String getPrsDay() {
        return prsDay;
    }

    public void setPrsDay(String prsDay) {
        this.prsDay = prsDay;
    }

    public String getSentraPicture() {
        return sentraPicture;
    }

    public void setSentraPicture(String sentraPicture) {
        this.sentraPicture = sentraPicture;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getSentraId() {
        return sentraId;
    }

    public void setSentraId(String sentraId) {
        this.sentraId = sentraId;
    }

    public String getGetCountData() {
        return getCountData;
    }

    public void setGetCountData(String getCountData) {
        this.getCountData = getCountData;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getStartLookupDate() {
        return startLookupDate;
    }

    public void setStartLookupDate(String startLookupDate) {
        this.startLookupDate = startLookupDate;
    }

    public String getEndLookupDate() {
        return endLookupDate;
    }

    public void setEndLookupDate(String endLookupDate) {
        this.endLookupDate = endLookupDate;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getOfficeCode() {
        return officeCode;
    }

    public void setOfficeCode(String officeCode) {
        this.officeCode = officeCode;
    }

    public String getMeetingPlace() {
        return meetingPlace;
    }

    public void setMeetingPlace(String meetingPlace) {
        this.meetingPlace = meetingPlace;
    }

    public String getDayNumber() {
        return dayNumber;
    }

    public void setDayNumber(String dayNumber) {
        this.dayNumber = dayNumber;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getReffId() {
        return reffId;
    }

    public void setReffId(String reffId) {
        this.reffId = reffId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSentraLeader() {
        return sentraLeader;
    }

    public void setSentraLeader(String sentraLeader) {
        this.sentraLeader = sentraLeader;
    }

    public String getCustomerNumberLeader() {
        return customerNumberLeader;
    }

    public void setCustomerNumberLeader(String customerNumberLeader) {
        this.customerNumberLeader = customerNumberLeader;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }

    public String getMeetingTime() {
        return meetingTime;
    }

    public void setMeetingTime(String meetingTime) {
        this.meetingTime = meetingTime;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public String getActionTime() {
        return actionTime;
    }

    public void setActionTime(String actionTime) {
        this.actionTime = actionTime;
    }

    public String getRecurAfter() {
        return recurAfter;
    }

    public void setRecurAfter(String recurAfter) {
        this.recurAfter = recurAfter;
    }

    public String getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(String tokenKey) {
        this.tokenKey = tokenKey;
    }

    @Override
    public String toString() {
        return "SentraRequest{" +
                "transmissionDateAndTime='" + transmissionDateAndTime + '\'' +
                ", retrievalReferenceNumber='" + retrievalReferenceNumber + '\'' +
                ", username='" + username + '\'' +
                ", sessionKey='" + sessionKey + '\'' +
                ", imei='" + imei + '\'' +
                ", areaId='" + areaId + '\'' +
                ", sentraName='" + sentraName + '\'' +
                ", sentraOwnerName='" + sentraOwnerName + '\'' +
                ", startedDate='" + startedDate + '\'' +
                ", sentraAddress='" + sentraAddress + '\'' +
                ", rtRw='" + rtRw + '\'' +
                ", prsDay='" + prsDay + '\'' +
                ", sentraPicture='" + sentraPicture + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", action='" + action + '\'' +
                ", sentraId='" + sentraId + '\'' +
                ", status='" + status + '\'' +
                ", sentraLeader='" + sentraLeader + '\'' +
                ", meetingTime='" + meetingTime + '\'' +
                ", localId='" + localId + '\'' +
                ", getCountData='" + getCountData + '\'' +
                ", page='" + page + '\'' +
                ", startLookupDate='" + startLookupDate + '\'' +
                ", endLookupDate='" + endLookupDate + '\'' +
                ", provinsi='" + provinsi + '\'' +
                ", kabupaten='" + kabupaten + '\'' +
                ", kecamatan='" + kecamatan + '\'' +
                ", kelurahan='" + kelurahan + '\'' +
                ", officeCode='" + officeCode + '\'' +
                ", meetingPlace='" + meetingPlace + '\'' +
                ", dayNumber='" + dayNumber + '\'' +
                ", recurAfter='" + recurAfter + '\'' +
                ", postcode='" + postcode + '\'' +
                ", reffId='" + reffId + '\'' +
                ", customerNumberLeader='" + customerNumberLeader + '\'' +
                ", actionTime='" + actionTime + '\'' +
                ", tokenKey='" + tokenKey + '\'' +
                '}';
    }

}