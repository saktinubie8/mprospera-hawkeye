package id.co.telkomsigma.btpns.mprospera.response;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class MasterSurveyResponse extends BaseResponse {

    private String grandTotal;

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    private List<SurveyTypeList> surveyTypeList;

    public List<SurveyTypeList> getSurveyTypeList() {
        if (surveyTypeList == null)
            surveyTypeList = new ArrayList<>();
        return surveyTypeList;
    }

    public void setSurveyTypeList(List<SurveyTypeList> surveyTypeList) {
        this.surveyTypeList = surveyTypeList;
    }

}