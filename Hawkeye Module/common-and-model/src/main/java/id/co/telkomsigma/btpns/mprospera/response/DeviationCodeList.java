package id.co.telkomsigma.btpns.mprospera.response;

/**
 * Created by Dzulfiqar on 6/05/2017.
 */
public class DeviationCodeList {

    private String id;
    private String deviationCode;
    private Boolean hasPhoto;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeviationCode() {
        return deviationCode;
    }

    public void setDeviationCode(String deviationCode) {
        this.deviationCode = deviationCode;
    }

    public Boolean getHasPhoto() {
        return hasPhoto;
    }

    public void setHasPhoto(Boolean hasPhoto) {
        this.hasPhoto = hasPhoto;
    }

    @Override
    public String toString() {
        return "DeviationCodeList [id=" + id + ", deviationCode=" + deviationCode + ", hasPhoto=" + hasPhoto + "]";
    }

}