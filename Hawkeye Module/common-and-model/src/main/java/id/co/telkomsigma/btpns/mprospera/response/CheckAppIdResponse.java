package id.co.telkomsigma.btpns.mprospera.response;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class CheckAppIdResponse extends BaseResponse {

    private List<CheckAppIdListResponse> checkAppIdList;

    public List<CheckAppIdListResponse> getCheckAppIdList() {
        if (checkAppIdList == null)
            checkAppIdList = new ArrayList();
        return checkAppIdList;
    }

    public void setCheckAppIdList(List<CheckAppIdListResponse> checkAppIdList) {
        this.checkAppIdList = checkAppIdList;
    }

    @Override
    public String toString() {
        return "CheckAppIdResponse [checkAppIdList=" + checkAppIdList + ", getResponseCode()=" + getResponseCode()
                + ", getResponseMessage()=" + getResponseMessage() + "]";
    }

}