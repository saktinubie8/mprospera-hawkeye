package id.co.telkomsigma.btpns.mprospera.request;

/**
 * Created by Dzulfiqar on 26/04/2017.
 */
public class WismaSWReq {

    private String officeCode;
    private String officeId;
    private String officeName;

    public String getOfficeCode() {
        return officeCode;
    }

    public void setOfficeCode(String officeCode) {
        this.officeCode = officeCode;
    }

    public String getOfficeId() {
        return officeId;
    }

    public void setOfficeId(String officeId) {
        this.officeId = officeId;
    }

    public String getOfficeName() {
        return officeName;
    }

    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }

}