package id.co.telkomsigma.btpns.mprospera.response;

import java.util.ArrayList;
import java.util.List;

public class SyncCustomerResponse extends BaseResponse {

    private String grandTotal;
    private String currentTotal;
    private String totalPage;
    private List<CustomerPojo> customerList;
    List<String> deletedCustomerList;

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getCurrentTotal() {
        return currentTotal;
    }

    public void setCurrentTotal(String currentTotal) {
        this.currentTotal = currentTotal;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    public List<CustomerPojo> getCustomerList() {
        if (customerList == null)
            customerList = new ArrayList<>();
        return customerList;
    }

    public void setCustomerList(List<CustomerPojo> customerList) {
        this.customerList = customerList;
    }

    public List<String> getDeletedCustomerList() {
        return deletedCustomerList;
    }

    public void setDeletedCustomerList(List<String> deletedCustomerList) {
        this.deletedCustomerList = deletedCustomerList;
    }

}