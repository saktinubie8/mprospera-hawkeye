package id.co.telkomsigma.btpns.mprospera.model.sentra;

import java.util.Date;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "T_SENTRA_GROUP")
public class Group extends GenericModel {
    /**
     *
     */
    private static final long serialVersionUID = -5642450623188963070L;
    private Long groupId;
    private String groupName;
    private String groupLeader;
    private String longitude;
    private String latitude;
    private Boolean isDeleted = false;
    private String status;
    private Sentra sentra;
    private Date createdDate;
    private String createdBy;
    private String prosperaId;
    private String approvedBy;
    private Date approvedDate;
    private String rrn;
    private Date updatedDate;
    private String updatedBy;

    @Column(name = "approved_by")
    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    @Column(name = "CREATED_DT")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    @Column(name = "name", nullable = false)
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Column(name = "leader")
    public String getGroupLeader() {
        return groupLeader;
    }

    public void setGroupLeader(String groupLeader) {
        this.groupLeader = groupLeader;
    }

    @Column(name = "longitude")
    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Column(name = "latitude")
    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @Column(name = "is_deleted")
    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = Sentra.class)
    @JoinColumn(name = "sentra_id", referencedColumnName = "id")
    public Sentra getSentra() {
        return sentra;
    }

    public void setSentra(Sentra sentra) {
        this.sentra = sentra;
    }

    @Column(name = "created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "prospera_id")
    public String getProsperaId() {
        return prosperaId;
    }

    public void setProsperaId(String prosperaId) {
        this.prosperaId = prosperaId;
    }

    @Column(name = "rrn")
    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    @Column(name = "approved_dt")
    public Date getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }

    @Column(name = "updated_dt")
    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Column(name = "updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

}