package id.co.telkomsigma.btpns.mprospera.model.survey;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OrderBy;

@Entity
@Table(name = "M_PROSPERA_QUESTION")
public class ProsperaQuestion extends GenericModel {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Long questionId;
    private Integer questionType;
    private Boolean state;
    private String questionText;
    private List<ProsperaQuestionChoice> choices;

    @OneToMany(fetch = FetchType.LAZY, targetEntity = ProsperaQuestionChoice.class)
    @JoinColumn(name = "question_id")
    @OrderBy(clause = "question_id asc")
    public List<ProsperaQuestionChoice> getChoices() {
        return choices;
    }

    public void setChoices(List<ProsperaQuestionChoice> choices) {
        this.choices = choices;
    }

    @Id
    @Column(name = "question_id", unique = true, nullable = false)
    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    @Column(name = "answer_type")
    public Integer getQuestionType() {
        return questionType;
    }

    public void setQuestionType(Integer questionType) {
        this.questionType = questionType;
    }

    @Column(name = "question_state")
    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    @Column(name = "question_text")
    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

}