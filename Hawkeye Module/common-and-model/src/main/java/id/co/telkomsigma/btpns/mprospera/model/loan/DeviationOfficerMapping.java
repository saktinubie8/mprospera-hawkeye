package id.co.telkomsigma.btpns.mprospera.model.loan;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Dzulfiqar on 25/04/2017.
 */
@Entity
@Table(name = "T_DEVIATION_OFFICER_MAP")
public class DeviationOfficerMapping extends GenericModel {

    private Long mapping_id;
    private Long deviationId;
    private String level;
    private Long officer;
    private String status;
    private String updatedBy;
    private Date updatedDate;
    private String date;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue
    public Long getMapping_id() {
        return mapping_id;
    }

    public void setMapping_id(Long mapping_id) {
        this.mapping_id = mapping_id;
    }

    @Column(name = "deviation_id")
    public Long getDeviationId() {
        return deviationId;
    }

    public void setDeviationId(Long deviationId) {
        this.deviationId = deviationId;
    }

    @Column(name = "officer_id")
    public Long getOfficer() {
        return officer;
    }

    public void setOfficer(Long officer) {
        this.officer = officer;
    }

    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "created_by")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Column(name = "updated_date")
    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Column(name = "level")
    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    @Column(name = "date")
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}