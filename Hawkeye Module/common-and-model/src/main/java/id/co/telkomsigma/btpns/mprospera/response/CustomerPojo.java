package id.co.telkomsigma.btpns.mprospera.response;

import java.util.ArrayList;
import java.util.List;

public class CustomerPojo {

    private String customerId;
    private String customerCifNumber;
    private String customerName;
    private String customerAlias;
    private String customerIdNumber;
    private String customerIdName;
    private String customerIdAddress;
    private String otherAddress;
    private String address;
    private String rtRw;
    private String kelurahan;
    private String kecamatan;
    private String kabupaten;
    private String province;
    private String postalCode;
    private String motherName;
    private String jumlahTanggungan;
    private String pendidikan;
    private String statusRumah;
    private String placeOfBirth;
    private String dateOfBirth;
    private String spouseName;
    private String spousePlaceOfBirth;
    private String spouseDateOfBirth;
    private String phoneNumber;
    private String religion;
    private String maritalStatus;
    private String gender;
    private String dateOfGovernmentId;
    private String createdDate;
    private String customerAreaId;
    private String swId;
    private String pdkId;
    private String createdBy;
    private String customerStatus;
    private String customerLevelId;
    private String assignedUsername;
    private String prosperaId;
    private String groupId;
    private String totalMangkir;
    private String placeOfBirthAreaId;
    private String spousePlaceOfBirthAreaId;
    private List<String> productSuggestion;

    public String getPlaceOfBirthAreaId() {
        return placeOfBirthAreaId;
    }

    public void setPlaceOfBirthAreaId(String placeOfBirthAreaId) {
        this.placeOfBirthAreaId = placeOfBirthAreaId;
    }

    public String getSpousePlaceOfBirthAreaId() {
        return spousePlaceOfBirthAreaId;
    }

    public void setSpousePlaceOfBirthAreaId(String spousePlaceOfBirthAreaId) {
        this.spousePlaceOfBirthAreaId = spousePlaceOfBirthAreaId;
    }

    public String getCustomerStatus() {
        return customerStatus;
    }

    public void setCustomerStatus(String customerStatus) {
        this.customerStatus = customerStatus;
    }

    public String getCustomerLevelId() {
        return customerLevelId;
    }

    public void setCustomerLevelId(String customerLevelId) {
        this.customerLevelId = customerLevelId;
    }

    public String getAssignedUsername() {
        return assignedUsername;
    }

    public void setAssignedUsername(String assignedUsername) {
        this.assignedUsername = assignedUsername;
    }

    public String getProsperaId() {
        return prosperaId;
    }

    public void setProsperaId(String prosperaId) {
        this.prosperaId = prosperaId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerCifNumber() {
        return customerCifNumber;
    }

    public void setCustomerCifNumber(String customerCifNumber) {
        this.customerCifNumber = customerCifNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerIdNumber() {
        return customerIdNumber;
    }

    public void setCustomerIdNumber(String customerIdNumber) {
        this.customerIdNumber = customerIdNumber;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCustomerAreaId() {
        return customerAreaId;
    }

    public void setCustomerAreaId(String customerAreaId) {
        this.customerAreaId = customerAreaId;
    }

    public String getSwId() {
        return swId;
    }

    public void setSwId(String swId) {
        this.swId = swId;
    }

    public String getPdkId() {
        return pdkId;
    }

    public void setPdkId(String pdkId) {
        this.pdkId = pdkId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getTotalMangkir() {
        return totalMangkir;
    }

    public void setTotalMangkir(String totalMangkir) {
        this.totalMangkir = totalMangkir;
    }

    public String getCustomerIdName() {
        return customerIdName;
    }

    public void setCustomerIdName(String customerIdName) {
        this.customerIdName = customerIdName;
    }

    public String getCustomerIdAddress() {
        return customerIdAddress;
    }

    public void setCustomerIdAddress(String customerIdAddress) {
        this.customerIdAddress = customerIdAddress;
    }

    public String getOtherAddress() {
        return otherAddress;
    }

    public void setOtherAddress(String otherAddress) {
        this.otherAddress = otherAddress;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getSpouseName() {
        return spouseName;
    }

    public void setSpouseName(String spouseName) {
        this.spouseName = spouseName;
    }

    public String getSpousePlaceOfBirth() {
        return spousePlaceOfBirth;
    }

    public void setSpousePlaceOfBirth(String spousePlaceOfBirth) {
        this.spousePlaceOfBirth = spousePlaceOfBirth;
    }

    public String getSpouseDateOfBirth() {
        return spouseDateOfBirth;
    }

    public void setSpouseDateOfBirth(String spouseDateOfBirth) {
        this.spouseDateOfBirth = spouseDateOfBirth;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfGovernmentId() {
        return dateOfGovernmentId;
    }

    public void setDateOfGovernmentId(String dateOfGovernmentId) {
        this.dateOfGovernmentId = dateOfGovernmentId;
    }

    public String getCustomerAlias() {
        return customerAlias;
    }

    public void setCustomerAlias(String customerAlias) {
        this.customerAlias = customerAlias;
    }

    public String getRtRw() {
        return rtRw;
    }

    public void setRtRw(String rtRw) {
        this.rtRw = rtRw;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getJumlahTanggungan() {
        return jumlahTanggungan;
    }

    public void setJumlahTanggungan(String jumlahTanggungan) {
        this.jumlahTanggungan = jumlahTanggungan;
    }

    public String getPendidikan() {
        return pendidikan;
    }

    public void setPendidikan(String pendidikan) {
        this.pendidikan = pendidikan;
    }

    public String getStatusRumah() {
        return statusRumah;
    }

    public void setStatusRumah(String statusRumah) {
        this.statusRumah = statusRumah;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

	public List<String> getProductSuggestion() {
        if (productSuggestion == null)
        	productSuggestion = new ArrayList<>();
		return productSuggestion;
	}

	public void setProductSuggestion(List<String> productSuggestion) {
		this.productSuggestion = productSuggestion;
	}
    
}