package id.co.telkomsigma.btpns.mprospera.model.survey;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import scala.Serializable;

@Embeddable
public class SurveyDetailId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String questionId;

    public SurveyDetailId() {

    }

    @Column(name = "question_id")
    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    private Survey survey;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Survey.class)
    @JoinColumn(name = "survey_id", referencedColumnName = "id")
    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }

}