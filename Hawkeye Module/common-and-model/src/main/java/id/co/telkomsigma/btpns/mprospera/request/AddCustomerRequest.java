package id.co.telkomsigma.btpns.mprospera.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class AddCustomerRequest extends BaseRequest {

    private String username;
    private String sessionKey;
    private String imei;
    private String pdkId;
    private String swId;
    private String status;
    private String longitude;
    private String latitude;
    private String action;
    private String customerId;
    private String groupId;
    private String localId;
    private String tokenKey;
    // for prospera purpose
    private String customerNumberCenter;
    private String customerNumberGroup;
    private String fullName;
    private String governmentId;
    private String actionFlag;
    private String reffId;
    private String createdBy;
    private String createdDate;
    private String firstName;
    private String nickName;
    private String dateOfGovernmentId;
    private String dateOfBirth;
    private String placeOfBirth;
    private String telephone;
    private String motherName;
    private String gender;
    private String maritalStatus;
    private String occupation;
    private String numberOfChildren;
    private String businessIncomeFamily;
    private String citizenShip;
    private String religion;
    private String ethinicity;
    private String citizenType;
    private String residenceStatus;
    private String latestEducation;
    private String husbandName;
    private String husbandDateOfBirth;
    private String husbandPlaceOfBirth;
    private String address;
    private String rtRw;
    private String kelurahan;
    private String kecamatan;
    private String state;
    private String country;
    private String postalCode;
    private String savingAccountFlag;
    private String reasonOpenAccount;
    private String fundSource;
    private String houseCapacious;
    private String houseCondition;
    private String houseRoofType;
    private String houseWall;
    private String houseFloor;
    private String houseElectricity;
    private String houseSourceWater;
    private String houseIndexTotalScore;
    private String totalAset;
    private String loanAset;
    private String netoAset;
    private String trained;
    private String trainedDate;
    private String formedByPersonnel;
    private String groupFlag;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getPdkId() {
        return pdkId;
    }

    public void setPdkId(String pdkId) {
        this.pdkId = pdkId;
    }

    public String getSwId() {
        return swId;
    }

    public void setSwId(String swId) {
        this.swId = swId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getEthinicity() {
        return ethinicity;
    }

    public void setEthinicity(String ethinicity) {
        this.ethinicity = ethinicity;
    }

    public String getCustomerNumberCenter() {
        return customerNumberCenter;
    }

    public void setCustomerNumberCenter(String customerNumberCenter) {
        this.customerNumberCenter = customerNumberCenter;
    }

    public String getCustomerNumberGroup() {
        return customerNumberGroup;
    }

    public void setCustomerNumberGroup(String customerNumberGroup) {
        this.customerNumberGroup = customerNumberGroup;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGovernmentId() {
        return governmentId;
    }

    public void setGovernmentId(String governmentId) {
        this.governmentId = governmentId;
    }

    public String getActionFlag() {
        return actionFlag;
    }

    public void setActionFlag(String actionFlag) {
        this.actionFlag = actionFlag;
    }

    public String getReffId() {
        return reffId;
    }

    public void setReffId(String reffId) {
        this.reffId = reffId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getDateOfGovernmentId() {
        return dateOfGovernmentId;
    }

    public void setDateOfGovernmentId(String dateOfGovernmentId) {
        this.dateOfGovernmentId = dateOfGovernmentId;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getNumberOfChildren() {
        return numberOfChildren;
    }

    public void setNumberOfChildren(String numberOfChildren) {
        this.numberOfChildren = numberOfChildren;
    }

    public String getBusinessIncomeFamily() {
        return businessIncomeFamily;
    }

    public void setBusinessIncomeFamily(String businessIncomeFamily) {
        this.businessIncomeFamily = businessIncomeFamily;
    }

    public String getCitizenShip() {
        return citizenShip;
    }

    public void setCitizenShip(String citizenShip) {
        this.citizenShip = citizenShip;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getCitizenType() {
        return citizenType;
    }

    public void setCitizenType(String citizenType) {
        this.citizenType = citizenType;
    }

    public String getResidenceStatus() {
        return residenceStatus;
    }

    public void setResidenceStatus(String residenceStatus) {
        this.residenceStatus = residenceStatus;
    }

    public String getLatestEducation() {
        return latestEducation;
    }

    public void setLatestEducation(String latestEducation) {
        this.latestEducation = latestEducation;
    }

    public String getHusbandName() {
        return husbandName;
    }

    public void setHusbandName(String husbandName) {
        this.husbandName = husbandName;
    }

    public String getHusbandDateOfBirth() {
        return husbandDateOfBirth;
    }

    public void setHusbandDateOfBirth(String husbandDateOfBirth) {
        this.husbandDateOfBirth = husbandDateOfBirth;
    }

    public String getHusbandPlaceOfBirth() {
        return husbandPlaceOfBirth;
    }

    public void setHusbandPlaceOfBirth(String husbandPlaceOfBirth) {
        this.husbandPlaceOfBirth = husbandPlaceOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRtRw() {
        return rtRw;
    }

    public void setRtRw(String rtRw) {
        this.rtRw = rtRw;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getSavingAccountFlag() {
        return savingAccountFlag;
    }

    public void setSavingAccountFlag(String savingAccountFlag) {
        this.savingAccountFlag = savingAccountFlag;
    }

    public String getReasonOpenAccount() {
        return reasonOpenAccount;
    }

    public void setReasonOpenAccount(String reasonOpenAccount) {
        this.reasonOpenAccount = reasonOpenAccount;
    }

    public String getFundSource() {
        return fundSource;
    }

    public void setFundSource(String fundSource) {
        this.fundSource = fundSource;
    }

    public String getHouseCapacious() {
        return houseCapacious;
    }

    public void setHouseCapacious(String houseCapacious) {
        this.houseCapacious = houseCapacious;
    }

    public String getHouseCondition() {
        return houseCondition;
    }

    public void setHouseCondition(String houseCondition) {
        this.houseCondition = houseCondition;
    }

    public String getHouseRoofType() {
        return houseRoofType;
    }

    public void setHouseRoofType(String houseRoofType) {
        this.houseRoofType = houseRoofType;
    }

    public String getHouseWall() {
        return houseWall;
    }

    public void setHouseWall(String houseWall) {
        this.houseWall = houseWall;
    }

    public String getHouseFloor() {
        return houseFloor;
    }

    public void setHouseFloor(String houseFloor) {
        this.houseFloor = houseFloor;
    }

    public String getHouseElectricity() {
        return houseElectricity;
    }

    public void setHouseElectricity(String houseElectricity) {
        this.houseElectricity = houseElectricity;
    }

    public String getHouseSourceWater() {
        return houseSourceWater;
    }

    public void setHouseSourceWater(String houseSourceWater) {
        this.houseSourceWater = houseSourceWater;
    }

    public String getHouseIndexTotalScore() {
        return houseIndexTotalScore;
    }

    public void setHouseIndexTotalScore(String houseIndexTotalScore) {
        this.houseIndexTotalScore = houseIndexTotalScore;
    }

    public String getTotalAset() {
        return totalAset;
    }

    public void setTotalAset(String totalAset) {
        this.totalAset = totalAset;
    }

    public String getLoanAset() {
        return loanAset;
    }

    public void setLoanAset(String loanAset) {
        this.loanAset = loanAset;
    }

    public String getNetoAset() {
        return netoAset;
    }

    public void setNetoAset(String netoAset) {
        this.netoAset = netoAset;
    }

    public String getTrained() {
        return trained;
    }

    public void setTrained(String trained) {
        this.trained = trained;
    }

    public String getTrainedDate() {
        return trainedDate;
    }

    public void setTrainedDate(String trainedDate) {
        this.trainedDate = trainedDate;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public String getFormedByPersonnel() {
        return formedByPersonnel;
    }

    public void setFormedByPersonnel(String formedByPersonnel) {
        this.formedByPersonnel = formedByPersonnel;
    }

    public String getGroupFlag() {
        return groupFlag;
    }

    public void setGroupFlag(String groupFlag) {
        this.groupFlag = groupFlag;
    }

    public String getTokenKey() {
        return null;
    }

    public void setTokenKey(String tokenKey) {
        this.tokenKey = tokenKey;
    }

    @Override
    public String toString() {
        return "AddCustomerRequest{" +
                "username='" + username + '\'' +
                ", sessionKey='" + sessionKey + '\'' +
                ", imei='" + imei + '\'' +
                ", pdkId='" + pdkId + '\'' +
                ", swId='" + swId + '\'' +
                ", status='" + status + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", action='" + action + '\'' +
                ", customerId='" + customerId + '\'' +
                ", groupId='" + groupId + '\'' +
                ", localId='" + localId + '\'' +
                ", tokenKey='" + tokenKey + '\'' +
                ", customerNumberCenter='" + customerNumberCenter + '\'' +
                ", customerNumberGroup='" + customerNumberGroup + '\'' +
                ", fullName='" + fullName + '\'' +
                ", governmentId='" + governmentId + '\'' +
                ", actionFlag='" + actionFlag + '\'' +
                ", reffId='" + reffId + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", firstName='" + firstName + '\'' +
                ", nickName='" + nickName + '\'' +
                ", dateOfGovernmentId='" + dateOfGovernmentId + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", placeOfBirth='" + placeOfBirth + '\'' +
                ", telephone='" + telephone + '\'' +
                ", motherName='" + motherName + '\'' +
                ", gender='" + gender + '\'' +
                ", maritalStatus='" + maritalStatus + '\'' +
                ", occupation='" + occupation + '\'' +
                ", numberOfChildren='" + numberOfChildren + '\'' +
                ", businessIncomeFamily='" + businessIncomeFamily + '\'' +
                ", citizenShip='" + citizenShip + '\'' +
                ", religion='" + religion + '\'' +
                ", ethinicity='" + ethinicity + '\'' +
                ", citizenType='" + citizenType + '\'' +
                ", residenceStatus='" + residenceStatus + '\'' +
                ", latestEducation='" + latestEducation + '\'' +
                ", husbandName='" + husbandName + '\'' +
                ", husbandDateOfBirth='" + husbandDateOfBirth + '\'' +
                ", husbandPlaceOfBirth='" + husbandPlaceOfBirth + '\'' +
                ", address='" + address + '\'' +
                ", rtRw='" + rtRw + '\'' +
                ", kelurahan='" + kelurahan + '\'' +
                ", kecamatan='" + kecamatan + '\'' +
                ", state='" + state + '\'' +
                ", country='" + country + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", savingAccountFlag='" + savingAccountFlag + '\'' +
                ", reasonOpenAccount='" + reasonOpenAccount + '\'' +
                ", fundSource='" + fundSource + '\'' +
                ", houseCapacious='" + houseCapacious + '\'' +
                ", houseCondition='" + houseCondition + '\'' +
                ", houseRoofType='" + houseRoofType + '\'' +
                ", houseWall='" + houseWall + '\'' +
                ", houseFloor='" + houseFloor + '\'' +
                ", houseElectricity='" + houseElectricity + '\'' +
                ", houseSourceWater='" + houseSourceWater + '\'' +
                ", houseIndexTotalScore='" + houseIndexTotalScore + '\'' +
                ", totalAset='" + totalAset + '\'' +
                ", loanAset='" + loanAset + '\'' +
                ", netoAset='" + netoAset + '\'' +
                ", trained='" + trained + '\'' +
                ", trainedDate='" + trainedDate + '\'' +
                ", formedByPersonnel='" + formedByPersonnel + '\'' +
                ", groupFlag='" + groupFlag + '\'' +
                '}';
    }

}