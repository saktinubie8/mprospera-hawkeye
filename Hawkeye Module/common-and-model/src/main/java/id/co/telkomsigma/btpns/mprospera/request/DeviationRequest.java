package id.co.telkomsigma.btpns.mprospera.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.co.telkomsigma.btpns.mprospera.response.DeviationApprovalHistory;

import java.util.List;

/**
 * Created by Dzulfiqar on 25/04/2017.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class DeviationRequest extends BaseRequest {

    private String username;
    private String startLookupDate;
    private String endLookupDate;
    private String sessionKey;
    private String imei;
    private List<LoanRequest> loanList;
    private List<AP3RRequest> ap3rList;
    private String userBwmp;
    private String status;
    private String localId;
    private String batch;
    List<DeviationApprovalHistory> approvalHistories;
    //for ESB
    private String noappid;
    private String userApproval;
    private String role;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public List<LoanRequest> getLoanList() {
        return loanList;
    }

    public void setLoanList(List<LoanRequest> loanList) {
        this.loanList = loanList;
    }

    public String getUserBwmp() {
        return userBwmp;
    }

    public void setUserBwmp(String userBwmp) {
        this.userBwmp = userBwmp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNoappid() {
        return noappid;
    }

    public void setNoappid(String noappid) {
        this.noappid = noappid;
    }

    public String getUserApproval() {
        return userApproval;
    }

    public void setUserApproval(String userApproval) {
        this.userApproval = userApproval;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public List<AP3RRequest> getAp3rList() {
        return ap3rList;
    }

    public void setAp3rList(List<AP3RRequest> ap3rList) {
        this.ap3rList = ap3rList;
    }

    public List<DeviationApprovalHistory> getApprovalHistories() {
        return approvalHistories;
    }

    public void setApprovalHistories(List<DeviationApprovalHistory> approvalHistories) {
        this.approvalHistories = approvalHistories;
    }

    public String getStartLookupDate() {
        return startLookupDate;
    }

    public void setStartLookupDate(String startLookupDate) {
        this.startLookupDate = startLookupDate;
    }

    public String getEndLookupDate() {
        return endLookupDate;
    }

    public void setEndLookupDate(String endLookupDate) {
        this.endLookupDate = endLookupDate;
    }

    @Override
    public String toString() {
        return "DeviationRequest{" +
                "username='" + username + '\'' +
                ", sessionKey='" + sessionKey + '\'' +
                ", imei='" + imei + '\'' +
                ", loanList=" + loanList +
                ", ap3rList=" + ap3rList +
                ", userBwmp='" + userBwmp + '\'' +
                ", status='" + status + '\'' +
                ", localId='" + localId + '\'' +
                ", batch='" + batch + '\'' +
                ", approvalHistories=" + approvalHistories +
                ", noappid='" + noappid + '\'' +
                ", userApproval='" + userApproval + '\'' +
                ", role='" + role + '\'' +
                '}';
    }

}