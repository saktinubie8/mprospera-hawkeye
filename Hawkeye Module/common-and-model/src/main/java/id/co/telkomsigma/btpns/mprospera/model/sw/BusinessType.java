package id.co.telkomsigma.btpns.mprospera.model.sw;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;

@Entity
@Table(name = "T_BUSINESS_TYPE")
public class BusinessType extends GenericModel {

    private Long businessId;
    private String businessType;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    @Column(name = "business_type")
    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

}