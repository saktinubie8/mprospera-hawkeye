package id.co.telkomsigma.btpns.mprospera.model.customer;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;

/**
 * Created by Dzulfiqar on 29/04/2017.
 */
@Entity
@Table(name = "T_CUSTOMER_ABSENCE")
public class CustomerWithAbsence extends GenericModel {

    private Long mappingId;
    private Long customerId;
    private Integer totalAbsence;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue
    public Long getMappingId() {
        return mappingId;
    }

    public void setMappingId(Long mappingId) {
        this.mappingId = mappingId;
    }

    @Column(name = "customer_id")
    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    @Column(name = "total_absence")
    public Integer getTotalAbsence() {
        return totalAbsence;
    }

    public void setTotalAbsence(Integer totalAbsence) {
        this.totalAbsence = totalAbsence;
    }

}