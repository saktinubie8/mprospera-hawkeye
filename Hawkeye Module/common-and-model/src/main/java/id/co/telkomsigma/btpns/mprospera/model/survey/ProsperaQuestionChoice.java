package id.co.telkomsigma.btpns.mprospera.model.survey;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "M_PROSPERA_SURVEY_QUESTION_CHOICE")
public class ProsperaQuestionChoice extends GenericModel {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Long choiceId;
    private ProsperaQuestion question;
    private String choiceText;
    private Integer order;

    @Id
    @Column(name = "choice_id", unique = true, nullable = false)
    public Long getChoiceId() {
        return choiceId;
    }

    public void setChoiceId(Long choiceId) {
        this.choiceId = choiceId;
    }

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = ProsperaQuestion.class)
    @JoinColumn(name = "question_id", referencedColumnName = "question_id", nullable = true)
    public ProsperaQuestion getQuestion() {
        return question;
    }

    public void setQuestion(ProsperaQuestion question) {
        this.question = question;
    }

    @Column(name = "choice_text", nullable = true)
    public String getChoiceText() {
        return choiceText;
    }

    public void setChoiceText(String choiceText) {
        this.choiceText = choiceText;
    }

    @Column(name = "choice_order", nullable = true)
    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

}