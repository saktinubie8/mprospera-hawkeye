package id.co.telkomsigma.btpns.mprospera.request;

import java.util.Arrays;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class ESBSurveyRequest extends BaseRequest {

    private String username;
    private String customerId;
    private String surveyId;
    private ESBQuestion[] questions;
    private String reffId;
    private Boolean ppi;
    private String imei;

    public Boolean getPpi() {
        return ppi;
    }

    public void setPpi(Boolean ppi) {
        this.ppi = ppi;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public ESBQuestion[] getQuestions() {
        return questions;
    }

    public void setQuestions(ESBQuestion[] questions) {
        this.questions = questions;
    }

    public String getReffId() {
        return reffId;
    }

    public void setReffId(String reffId) {
        this.reffId = reffId;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    @Override
    public String toString() {
        return "ESBSurveyRequest [username=" + username + ", customerId=" + customerId + ", surveyId=" + surveyId
                + ", questions=" + Arrays.toString(questions) + ", reffId=" + reffId + ", imei=" + imei
                + ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime() + ", getRetrievalReferenceNumber()="
                + getRetrievalReferenceNumber() + "]";
    }

}