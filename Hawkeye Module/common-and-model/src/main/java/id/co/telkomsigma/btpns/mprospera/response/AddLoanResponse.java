package id.co.telkomsigma.btpns.mprospera.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class AddLoanResponse extends BaseResponse {

    private String loanId;
    // for prospera purpose
    private String prospectId;
    private String noappid;
    private String firstInstallmentDate;
    private String lastInstallmentDate;

    public String getLoanId() {
        return loanId;
    }

    public void setLoanId(String loanId) {
        this.loanId = loanId;
    }

    public String getProspectId() {
        return prospectId;
    }

    public void setProspectId(String prospectId) {
        this.prospectId = prospectId;
    }

    public String getNoappid() {
        return noappid;
    }

    public void setNoappid(String noappid) {
        this.noappid = noappid;
    }

    public String getFirstInstallmentDate() {
        return firstInstallmentDate;
    }

    public void setFirstInstallmentDate(String firstInstallmentDate) {
        this.firstInstallmentDate = firstInstallmentDate;
    }

    public String getLastInstallmentDate() {
        return lastInstallmentDate;
    }

    public void setLastInstallmentDate(String lastInstallmentDate) {
        this.lastInstallmentDate = lastInstallmentDate;
    }

}