package id.co.telkomsigma.btpns.mprospera.model.survey;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Id;

import scala.Serializable;

@Entity
@Table(name = "T_SURVEY_DETAIL")
@IdClass(SurveyDetailId.class)
public class SurveyDetail implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private SurveyDetailId surveyDetailId;
    private String questionType;
    private String questionValue;
    private String questionId;
    private Survey survey;

    public SurveyDetail() {

    }

    @Column(name = "question_type", nullable = false)
    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    @Column(name = "question_value", nullable = false)
    public String getQuestionValue() {
        return questionValue;
    }

    public void setQuestionValue(String questionValue) {
        this.questionValue = questionValue;
    }

    @Id
    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    @Id
    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }

}