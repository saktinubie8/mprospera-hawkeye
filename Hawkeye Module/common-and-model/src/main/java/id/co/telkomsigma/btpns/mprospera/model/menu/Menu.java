package id.co.telkomsigma.btpns.mprospera.model.menu;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;
import id.co.telkomsigma.btpns.mprospera.model.user.Role;
import id.co.telkomsigma.btpns.mprospera.model.user.User;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * Created by abet on 12/5/15.
 */
@Entity
@Table(name = "M_MENUS")
public class Menu extends GenericModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8786805085128140615L;
    private Long menuId;
    private String menuName;
    private String menuPath;
    private User createdBy;
    private Date createdDate;
    private User updatedBy;
    private Date updatedDate;
    private Long menuParentId;
    private Set<Role> roles = new HashSet<Role>();
    /**
     * Transient Value
     */
    private String strMenuName;

    @Override
    public boolean equals(Object obj) {
        return this.getMenuName().equals(obj);
    }

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long id) {
        this.menuId = id;
    }

    @Column(name = "MENU_NAME", length = MAX_LENGTH_MENU_NAME, nullable = false)
    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    @Column(name = "MENU_PATH", length = MAX_LENGTH_MENU_PATH, nullable = false)
    public String getMenuPath() {
        return menuPath;
    }

    public void setMenuPath(String menuPath) {
        this.menuPath = menuPath;
    }

    @ManyToOne(targetEntity = User.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "CREATED_BY", nullable = false)
    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "CREATED_DT", nullable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @ManyToOne(targetEntity = User.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "UPDATE_BY", nullable = false)
    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updateBy) {
        this.updatedBy = updateBy;
    }

    @Column(name = "UPDATE_DT", nullable = false)
    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updateDate) {
        this.updatedDate = updateDate;
    }

    @Column(name = "MENU_PARENT_ID")
    public Long getMenuParentId() {
        return menuParentId;
    }

    public void setMenuParentId(Long menuParentId) {
        this.menuParentId = menuParentId;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @Fetch(FetchMode.SELECT)
    @JoinTable(name = "M_ROLE_MENUS", joinColumns = {
            @JoinColumn(name = "menuId")}, inverseJoinColumns = @JoinColumn(name = "roleId"))
    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    @Transient
    public String getStrMenuName() {
        return strMenuName;
    }

    public void setStrMenuName(String strMenuName) {
        this.strMenuName = strMenuName;
    }

}