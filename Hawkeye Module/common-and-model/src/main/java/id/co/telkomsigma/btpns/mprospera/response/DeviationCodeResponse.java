package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

/**
 * Created by Dzulfiqar on 6/05/2017.
 */
public class DeviationCodeResponse extends BaseResponse {

    List<DeviationCodeList> deviationCodeList;

    public List<DeviationCodeList> getDeviationCodeList() {
        return deviationCodeList;
    }

    public void setDeviationCodeList(List<DeviationCodeList> deviationCodeList) {
        this.deviationCodeList = deviationCodeList;
    }

    @Override
    public String toString() {
        return "DeviationCodeResponse{" +
                "deviationCodeList=" + deviationCodeList +
                '}';
    }

}