package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class ListSurveyResponse extends BaseResponse {

    private String grandTotal;
    private String currentTotal;
    private String totalPage;
    private List<SurveyResponse> surveyList;

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getCurrentTotal() {
        return currentTotal;
    }

    public void setCurrentTotal(String currentTotal) {
        this.currentTotal = currentTotal;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    public List<SurveyResponse> getSurveyList() {
        return surveyList;
    }

    public void setSurveyList(List<SurveyResponse> surveyList) {
        this.surveyList = surveyList;
    }

    @Override
    public String toString() {
        return "ListSurveyResponse [grandTotal=" + grandTotal + ", currentTotal=" + currentTotal + ", totalPage="
                + totalPage + ", surveyList=" + surveyList + ", getResponseCode()=" + getResponseCode()
                + ", getResponseMessage()=" + getResponseMessage() + "]";
    }

}