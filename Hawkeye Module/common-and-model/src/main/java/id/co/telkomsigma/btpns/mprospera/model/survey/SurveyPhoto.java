package id.co.telkomsigma.btpns.mprospera.model.survey;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;

/**
 * Created by Dzulfiqar on 12/05/2017.
 */
@Entity
@Table(name = "T_SURVEY_PHOTO")
public class SurveyPhoto extends GenericModel {

    private Long id;
    private Long surveyId;
    private byte[] surveyPhoto;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "survey_id")
    public Long getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Long surveyId) {
        this.surveyId = surveyId;
    }

    @Column(name = "survey_photo")
    public byte[] getSurveyPhoto() {
        return surveyPhoto;
    }

    public void setSurveyPhoto(byte[] surveyPhoto) {
        this.surveyPhoto = surveyPhoto;
    }

}