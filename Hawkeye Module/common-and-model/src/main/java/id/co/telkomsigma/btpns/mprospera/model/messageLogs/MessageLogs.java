package id.co.telkomsigma.btpns.mprospera.model.messageLogs;

import java.util.Date;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "M_MESSAGE_LOGS")
public class MessageLogs extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = 7090117681452784448L;
    private Long messageLogsId;
    private Date createdDate;
    private boolean isRequest;
    private String endpointCode;
    private byte[] messageRaw;
    private TerminalActivity terminalActivity;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getMessageLogsId() {
        return messageLogsId;
    }

    public void setMessageLogsId(Long messageLogsId) {
        this.messageLogsId = messageLogsId;
    }

    @Column(name = "CREATED_DT")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "is_request")
    public boolean isRequest() {
        return isRequest;
    }

    public void setRequest(boolean isRequest) {
        this.isRequest = isRequest;
    }

    @Column(name = "endpoint_code")
    public String getEndpointCode() {
        return endpointCode;
    }

    public void setEndpointCode(String endpointCode) {
        this.endpointCode = endpointCode;
    }

    @Column(name = "raw")
    public byte[] getMessageRaw() {
        return messageRaw;
    }

    @ManyToOne(targetEntity = TerminalActivity.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "terminal_activity_id", nullable = false)
    public TerminalActivity getTerminalActivity() {
        return terminalActivity;
    }

    public void setTerminalActivity(TerminalActivity terminalActivity) {
        this.terminalActivity = terminalActivity;
    }

    @Transient
    public String getRequestLabel() {
        if (isRequest)
            return "request";
        else
            return "response";
    }

    public void setMessageRaw(byte[] messageRaw) {
        this.messageRaw = messageRaw;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}