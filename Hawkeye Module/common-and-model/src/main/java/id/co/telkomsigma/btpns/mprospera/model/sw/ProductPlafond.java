package id.co.telkomsigma.btpns.mprospera.model.sw;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "T_PRODUCT_PLAFOND")
public class ProductPlafond extends GenericModel {

    Long plafondId;
    BigDecimal plafond;
    LoanProduct productId;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getPlafondId() {
        return plafondId;
    }

    public void setPlafondId(Long plafondId) {
        this.plafondId = plafondId;
    }

    @Column(name = "plafond")
    public BigDecimal getPlafond() {
        return plafond;
    }

    public void setPlafond(BigDecimal plafond) {
        this.plafond = plafond;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = LoanProduct.class)
    @JoinColumn(name = "product_id", referencedColumnName = "id")
    public LoanProduct getProductId() {
        return productId;
    }

    public void setProductId(LoanProduct productId) {
        this.productId = productId;
    }

}