package id.co.telkomsigma.btpns.mprospera.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.co.telkomsigma.btpns.mprospera.request.ItemCalcLainReq;

import java.util.List;

/**
 * Created by Dzulfiqar on 13/04/2017.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class SWCalculationVariable {

    private String hariOperasional;
    private List<ItemCalcLainReq> itemCalcLains;
    private String jamBuka;
    private String jamTutup;
    private String jenisSiklus;
    private String jumlahhariramai;
    private String jumlahharisepi;
    private String pendapatanLainnya;
    private String pendapatanPerbulan3Periode;
    private String pendapatanRata2TigaPeriode;
    private String pendapatanPerbulanKasSehari;
    private String pendapatanPerbulanPerkiraanSehari;
    private String pendapatanPerbulanRamaiSepi;
    private String pendapatanRamai;
    private String pendapatanSepi;
    private String periode1;
    private String periode2;
    private String periode3;
    private String totalAddpendapatanLainnya;
    private String uangBelanja;
    private String uangKasBoxSekarang;
    private String uangkasBoxdiBuka;
    private String waktuKerja;
    private String waktuKerjaWithMinutes;
    private String waktukerjaSetelahbuka;

    public String getHariOperasional() {
        return hariOperasional;
    }

    public void setHariOperasional(String hariOperasional) {
        this.hariOperasional = hariOperasional;
    }

    public List<ItemCalcLainReq> getItemCalcLains() {
        return itemCalcLains;
    }

    public void setItemCalcLains(List<ItemCalcLainReq> itemCalcLains) {
        this.itemCalcLains = itemCalcLains;
    }

    public String getJamBuka() {
        return jamBuka;
    }

    public void setJamBuka(String jamBuka) {
        this.jamBuka = jamBuka;
    }

    public String getJamTutup() {
        return jamTutup;
    }

    public void setJamTutup(String jamTutup) {
        this.jamTutup = jamTutup;
    }

    public String getJenisSiklus() {
        return jenisSiklus;
    }

    public void setJenisSiklus(String jenisSiklus) {
        this.jenisSiklus = jenisSiklus;
    }

    public String getJumlahhariramai() {
        return jumlahhariramai;
    }

    public void setJumlahhariramai(String jumlahhariramai) {
        this.jumlahhariramai = jumlahhariramai;
    }

    public String getJumlahharisepi() {
        return jumlahharisepi;
    }

    public void setJumlahharisepi(String jumlahharisepi) {
        this.jumlahharisepi = jumlahharisepi;
    }

    public String getPendapatanLainnya() {
        return pendapatanLainnya;
    }

    public void setPendapatanLainnya(String pendapatanLainnya) {
        this.pendapatanLainnya = pendapatanLainnya;
    }

    public String getPendapatanPerbulan3Periode() {
        return pendapatanPerbulan3Periode;
    }

    public void setPendapatanPerbulan3Periode(String pendapatanPerbulan3Periode) {
        this.pendapatanPerbulan3Periode = pendapatanPerbulan3Periode;
    }

    public String getPendapatanPerbulanKasSehari() {
        return pendapatanPerbulanKasSehari;
    }

    public void setPendapatanPerbulanKasSehari(String pendapatanPerbulanKasSehari) {
        this.pendapatanPerbulanKasSehari = pendapatanPerbulanKasSehari;
    }

    public String getPendapatanPerbulanPerkiraanSehari() {
        return pendapatanPerbulanPerkiraanSehari;
    }

    public void setPendapatanPerbulanPerkiraanSehari(String pendapatanPerbulanPerkiraanSehari) {
        this.pendapatanPerbulanPerkiraanSehari = pendapatanPerbulanPerkiraanSehari;
    }

    public String getPendapatanPerbulanRamaiSepi() {
        return pendapatanPerbulanRamaiSepi;
    }

    public void setPendapatanPerbulanRamaiSepi(String pendapatanPerbulanRamaiSepi) {
        this.pendapatanPerbulanRamaiSepi = pendapatanPerbulanRamaiSepi;
    }

    public String getPendapatanRamai() {
        return pendapatanRamai;
    }

    public void setPendapatanRamai(String pendapatanRamai) {
        this.pendapatanRamai = pendapatanRamai;
    }

    public String getPendapatanSepi() {
        return pendapatanSepi;
    }

    public void setPendapatanSepi(String pendapatanSepi) {
        this.pendapatanSepi = pendapatanSepi;
    }

    public String getPeriode1() {
        return periode1;
    }

    public void setPeriode1(String periode1) {
        this.periode1 = periode1;
    }

    public String getPeriode2() {
        return periode2;
    }

    public void setPeriode2(String periode2) {
        this.periode2 = periode2;
    }

    public String getPeriode3() {
        return periode3;
    }

    public void setPeriode3(String periode3) {
        this.periode3 = periode3;
    }

    public String getTotalAddpendapatanLainnya() {
        return totalAddpendapatanLainnya;
    }

    public void setTotalAddpendapatanLainnya(String totalAddpendapatanLainnya) {
        this.totalAddpendapatanLainnya = totalAddpendapatanLainnya;
    }

    public String getUangBelanja() {
        return uangBelanja;
    }

    public void setUangBelanja(String uangBelanja) {
        this.uangBelanja = uangBelanja;
    }

    public String getUangKasBoxSekarang() {
        return uangKasBoxSekarang;
    }

    public void setUangKasBoxSekarang(String uangKasBoxSekarang) {
        this.uangKasBoxSekarang = uangKasBoxSekarang;
    }

    public String getUangkasBoxdiBuka() {
        return uangkasBoxdiBuka;
    }

    public void setUangkasBoxdiBuka(String uangkasBoxdiBuka) {
        this.uangkasBoxdiBuka = uangkasBoxdiBuka;
    }

    public String getWaktuKerja() {
        return waktuKerja;
    }

    public void setWaktuKerja(String waktuKerja) {
        this.waktuKerja = waktuKerja;
    }

    public String getWaktuKerjaWithMinutes() {
        return waktuKerjaWithMinutes;
    }

    public void setWaktuKerjaWithMinutes(String waktuKerjaWithMinutes) {
        this.waktuKerjaWithMinutes = waktuKerjaWithMinutes;
    }

    public String getWaktukerjaSetelahbuka() {
        return waktukerjaSetelahbuka;
    }

    public void setWaktukerjaSetelahbuka(String waktukerjaSetelahbuka) {
        this.waktukerjaSetelahbuka = waktukerjaSetelahbuka;
    }

    public String getPendapatanRata2TigaPeriode() {
        return pendapatanRata2TigaPeriode;
    }

    public void setPendapatanRata2TigaPeriode(String pendapatanRata2TigaPeriode) {
        this.pendapatanRata2TigaPeriode = pendapatanRata2TigaPeriode;
    }

}