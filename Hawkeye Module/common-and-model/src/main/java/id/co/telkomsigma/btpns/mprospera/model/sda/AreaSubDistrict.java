package id.co.telkomsigma.btpns.mprospera.model.sda;

import java.util.List;
import javax.persistence.*;

import org.hibernate.annotations.OrderBy;
import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name = "T_SDA_SUB_DISTRICT")
public class AreaSubDistrict extends GenericModel {
    private static final long serialVersionUID = 6135317091168816L;

    private String areaId;
    private String areaCategory;
    private String parentAreaId;
    private String areaName;
    private List<AreaSubDistrictDetails> areaSubDistrictDetails;

    @OneToMany(fetch = FetchType.LAZY, targetEntity = AreaSubDistrictDetails.class)
    @JoinColumn(name = "area_id")
    @OrderBy(clause = "id asc")
    public List<AreaSubDistrictDetails> getAreaSubDistrictDetails() {
        return areaSubDistrictDetails;
    }

    public void setAreaSubDistrictDetails(List<AreaSubDistrictDetails> areaSubDistrictDetails) {
        this.areaSubDistrictDetails = areaSubDistrictDetails;
    }

    @Id
    @Column(name = "id", nullable = false, unique = true)
    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    @Transient
    public String getAreaCategory() {
        return areaCategory;
    }

    public void setAreaCategory(String areaCategory) {
        this.areaCategory = areaCategory;
    }

    @Column(name = "parent_area_id", nullable = false)
    public String getParentAreaId() {
        return parentAreaId;
    }

    public void setParentAreaId(String parentAreaId) {
        this.parentAreaId = parentAreaId;
    }

    @Column(name = "area_name", nullable = false)
    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

}