package id.co.telkomsigma.btpns.mprospera.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Created by Dzulfiqar on 13/04/2017.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class SWProfileResponse {

    private String alias;
    private String birthDay;
    private String birthPlace;
    private String birthPlaceRegencyName;
    private String createdBy;
    private String dependants;
    private String education;
    private String gender;
    private String idCardExpired;
    private String isLifeTime;
    private String longName;
    private String marriedStatus;
    private String motherName;
    private String npwp;
    private String phoneNumber;
    private String religion;
    private String workStatus;

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getBirthPlaceRegencyName() {
        return birthPlaceRegencyName;
    }

    public void setBirthPlaceRegencyName(String birthPlaceRegencyName) {
        this.birthPlaceRegencyName = birthPlaceRegencyName;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getDependants() {
        return dependants;
    }

    public void setDependants(String dependants) {
        this.dependants = dependants;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIdCardExpired() {
        return idCardExpired;
    }

    public void setIdCardExpired(String idCardExpired) {
        this.idCardExpired = idCardExpired;
    }

    public String getIsLifeTime() {
        return isLifeTime;
    }

    public void setIsLifeTime(String isLifeTime) {
        this.isLifeTime = isLifeTime;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public String getMarriedStatus() {
        return marriedStatus;
    }

    public void setMarriedStatus(String marriedStatus) {
        this.marriedStatus = marriedStatus;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getNpwp() {
        return npwp;
    }

    public void setNpwp(String npwp) {
        this.npwp = npwp;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getWorkStatus() {
        return workStatus;
    }

    public void setWorkStatus(String workStatus) {
        this.workStatus = workStatus;
    }

}