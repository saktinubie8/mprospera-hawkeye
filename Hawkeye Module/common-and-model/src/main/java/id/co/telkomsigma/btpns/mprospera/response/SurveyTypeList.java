package id.co.telkomsigma.btpns.mprospera.response;

import java.util.ArrayList;
import java.util.List;

public class SurveyTypeList {

    private String surveyTypeId;
    private String surveyName;
    private String isActive;
    private String isWithPhoto;
    private List<SurveyQuestionList> surveyQuestionList;

    public String getSurveyTypeId() {
        return surveyTypeId;
    }

    public void setSurveyTypeId(String surveyTypeId) {
        this.surveyTypeId = surveyTypeId;
    }

    public String getSurveyName() {
        return surveyName;
    }

    public void setSurveyName(String surveyName) {
        this.surveyName = surveyName;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public List<SurveyQuestionList> getSurveyQuestionList() {
        if (surveyQuestionList == null)
            surveyQuestionList = new ArrayList<>();
        return surveyQuestionList;
    }

    public void setSurveyQuestionList(List<SurveyQuestionList> surveyQuestionList) {
        this.surveyQuestionList = surveyQuestionList;
    }

    public String getIsWithPhoto() {
        return isWithPhoto;
    }

    public void setIsWithPhoto(String isWithPhoto) {
        this.isWithPhoto = isWithPhoto;
    }

}