package id.co.telkomsigma.btpns.mprospera.response;

import java.math.BigDecimal;

public class HistoryList {

    private String updatedDate;
    private String description;
    private BigDecimal amount;
    private BigDecimal outstandingAmount;
    private Long outstandingCount;
    private BigDecimal clearBalance;

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getOutstandingAmount() {
        return outstandingAmount;
    }

    public void setOutstandingAmount(BigDecimal outstandingAmount) {
        this.outstandingAmount = outstandingAmount;
    }

    public Long getOutstandingCount() {
        return outstandingCount;
    }

    public void setOutstandingCount(Long outstandingCount) {
        this.outstandingCount = outstandingCount;
    }

    public BigDecimal getClearBalance() {
        return clearBalance;
    }

    public void setClearBalance(BigDecimal clearBalance) {
        this.clearBalance = clearBalance;
    }

    @Override
    public String toString() {
        return "HistoryList{" +
                "updatedDate='" + updatedDate + '\'' +
                ", description='" + description + '\'' +
                ", amount=" + amount +
                ", outstandingAmount=" + outstandingAmount +
                ", outstandingCount=" + outstandingCount +
                ", clearBalance=" + clearBalance +
                '}';
    }

}