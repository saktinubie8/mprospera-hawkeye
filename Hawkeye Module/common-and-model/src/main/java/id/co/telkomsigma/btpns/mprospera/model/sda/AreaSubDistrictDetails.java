package id.co.telkomsigma.btpns.mprospera.model.sda;

import java.util.Date;
import javax.persistence.*;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name = "T_SDA_SUB_DISTRICT_DETAIL")
public class AreaSubDistrictDetails extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = -6660052802097689240L;

    private Long areaDetailId;
    private AreaSubDistrict areaSubDistrict;
    private String countKKMiskin;
    private Date createdDate;
    private String createdBy;
    private String createdAt;
    private String pejabat;
    private String jabatan;
    private Boolean isDeleted = false;
    private String longitude;
    private String latitude;

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @Column(name = "longitude")
    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Column(name = "latitude")
    public String getLatitude() {
        return latitude;
    }

    @Column(name = "is_deleted")
    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getAreaDetailId() {
        return areaDetailId;
    }

    public void setAreaDetailId(Long areaDetailId) {
        this.areaDetailId = areaDetailId;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = AreaSubDistrict.class)
    @JoinColumn(name = "area_id", referencedColumnName = "id", nullable = true)
    public AreaSubDistrict getAreaSubDistrict() {
        return areaSubDistrict;
    }

    public void setAreaSubDistrict(AreaSubDistrict areaSubDistrict) {
        this.areaSubDistrict = areaSubDistrict;
    }

    @Column(name = "count_kk_miskin", nullable = false)
    public String getCountKKMiskin() {
        return countKKMiskin;
    }

    public void setCountKKMiskin(String countKKMiskin) {
        this.countKKMiskin = countKKMiskin;
    }

    @Column(name = "created_dt", nullable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "created_by", nullable = false)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "created_at", nullable = false)
    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @Column(name = "pejabat")
    public String getPejabat() {
        return pejabat;
    }

    public void setPejabat(String pejabat) {
        this.pejabat = pejabat;
    }

    @Column(name = "jabatan")
    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

}