package id.co.telkomsigma.btpns.mprospera.model.saf;

import java.util.Date;
import javax.persistence.*;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name = "M_SAF")
public class Saf extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Long id;
    private String method;
    private String responseCode;
    private String responseMessage;
    private Date createdDate;
    private String url;
    private byte[] rawMessage;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "raw_message", nullable = false)
    public byte[] getRawMessage() {
        return rawMessage;
    }

    public void setRawMessage(byte[] rawMessage) {
        this.rawMessage = rawMessage;
    }

    @Column(name = "method", nullable = false)
    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    @Column(name = "rc", nullable = false)
    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    @Column(name = "rm", nullable = false)
    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    @Column(name = "created_dt", nullable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "url", nullable = false)
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}