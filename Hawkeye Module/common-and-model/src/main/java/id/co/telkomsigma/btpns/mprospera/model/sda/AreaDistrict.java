package id.co.telkomsigma.btpns.mprospera.model.sda;

import java.util.List;
import javax.persistence.*;

import org.hibernate.annotations.OrderBy;
import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name = "T_SDA_DISTRICT")
public class AreaDistrict extends GenericModel {
    private static final long serialVersionUID = 6134317091168316L;

    private String areaId;
    private String areaCategory;
    private String parentAreaId;
    private String areaName;

    private List<AreaDistrictDetails> areaDistrictDetails;

    @OneToMany(fetch = FetchType.LAZY, targetEntity = AreaDistrictDetails.class)
    @JoinColumn(name = "area_id")
    @OrderBy(clause = "id asc")
    public List<AreaDistrictDetails> getAreaDistrictDetails() {
        return areaDistrictDetails;
    }

    public void setAreaDistrictDetails(List<AreaDistrictDetails> areaDistrictDetails) {
        this.areaDistrictDetails = areaDistrictDetails;
    }

    @Id
    @Column(name = "id", nullable = false, unique = true)
    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    @Transient
    public String getAreaCategory() {
        return areaCategory;
    }

    public void setAreaCategory(String areaCategory) {
        this.areaCategory = areaCategory;
    }

    @Column(name = "parent_area_id", nullable = false)
    public String getParentAreaId() {
        return parentAreaId;
    }

    public void setParentAreaId(String parentAreaId) {
        this.parentAreaId = parentAreaId;
    }

    @Column(name = "area_name", nullable = false)
    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

}