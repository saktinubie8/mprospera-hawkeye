package id.co.telkomsigma.btpns.mprospera.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by daniel on 4/10/15.
 */
public final class NumberUtil {
    private static final DecimalFormat decimalFormat = new DecimalFormat("###,###.##;-###,###.##");

    public static String formatIDNInt(Number number) {
        decimalFormat.setDecimalSeparatorAlwaysShown(false);
        decimalFormat.setMaximumFractionDigits(0);
        return decimalFormat.format(number).replace(',', '.');
    }

    public static String formatWeight(double weight) {
        decimalFormat.setDecimalSeparatorAlwaysShown(false);
        decimalFormat.setMaximumFractionDigits(2);
        return decimalFormat.format(weight).replace(",", "#").replace(".", ",").replace("#", ".");
    }

    public static String formatInputWeight(double weight) {
        decimalFormat.setDecimalSeparatorAlwaysShown(false);
        decimalFormat.setMaximumFractionDigits(2);
        return decimalFormat.format(weight).replace(",", "").replace(".", ",");
    }

    public static BigDecimal parseIDNIntStr(String string) {
        string = string.replace(".", "");
        return new BigDecimal(string);
    }

    public static double parseWeightStr(String string) {
        string = string.replace(".", "").replace(",", ".");
        return new Double(string);
    }

    public static String fetchNumberFromStr(String string) {
        StringBuffer sb = new StringBuffer();
        Matcher matcher = Pattern.compile("(\\d)+").matcher(string);
        while (matcher.find()) {
            sb.append(matcher.group());
        }
        return sb.toString();
    }

}