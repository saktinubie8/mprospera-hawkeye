package id.co.telkomsigma.btpns.mprospera.model.sentra;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name = "T_SENTRA_PHOTO")
public class SentraPhoto extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = -2462843802614081624L;
    private Long id;
    private Long sentraId;
    private byte[] sentraPhoto;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "sentra_id")
    public Long getSentraId() {
        return sentraId;
    }

    public void setSentraId(Long sentraId) {
        this.sentraId = sentraId;
    }

    @Column(name = "sentra_photo", nullable = true)
    public byte[] getSentraPhoto() {
        return sentraPhoto;
    }

    public void setSentraPhoto(byte[] sentraPhoto) {
        this.sentraPhoto = sentraPhoto;
    }

}