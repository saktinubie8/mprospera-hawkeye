package id.co.telkomsigma.btpns.mprospera.request;

import java.util.List;

public class AP3RRequest extends BaseRequest {

    private String username;
    private String sessionKey;
    private String imei;
    private String createdAt;
    private String createdBy;
    private String startLookupDate;
    private String endLookupDate;
    private String getCountData;
    private String page;
    private String swId;
    private String swProductId;
    private String haveAccount;
    private String localId;
    private String accountPurpose;
    private String fundSource;
    private String yearlyTransaction;
    private String financingPurpose;
    private String businessTypeId;
    private String disburseDate;
    private String dueDate;
    private String highRiskCustomer;
    private String highRiskBusiness;
    private List<FundedThingsReq> financedGoods;
    private String status;
    private String action;
    private String latitude;
    private String longitude;
    private String ap3rId;
    private String rejectionReason;
    private String approvalStatus;
    private String userBwmp;
    private String deviationReason;
    private String deviationCode;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getSwId() {
        return swId;
    }

    public void setSwId(String swId) {
        this.swId = swId;
    }

    public String getStartLookupDate() {
        return startLookupDate;
    }

    public void setStartLookupDate(String startLookupDate) {
        this.startLookupDate = startLookupDate;
    }

    public String getEndLookupDate() {
        return endLookupDate;
    }

    public void setEndLookupDate(String endLookupDate) {
        this.endLookupDate = endLookupDate;
    }

    public String getGetCountData() {
        return getCountData;
    }

    public void setGetCountData(String getCountData) {
        this.getCountData = getCountData;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getSwProductId() {
        return swProductId;
    }

    public void setSwProductId(String swProductId) {
        this.swProductId = swProductId;
    }

    public String getHaveAccount() {
        return haveAccount;
    }

    public void setHaveAccount(String haveAccount) {
        this.haveAccount = haveAccount;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public String getAccountPurpose() {
        return accountPurpose;
    }

    public void setAccountPurpose(String accountPurpose) {
        this.accountPurpose = accountPurpose;
    }

    public String getFundSource() {
        return fundSource;
    }

    public void setFundSource(String fundSource) {
        this.fundSource = fundSource;
    }

    public String getYearlyTransaction() {
        return yearlyTransaction;
    }

    public void setYearlyTransaction(String yearlyTransaction) {
        this.yearlyTransaction = yearlyTransaction;
    }

    public String getFinancingPurpose() {
        return financingPurpose;
    }

    public void setFinancingPurpose(String financingPurpose) {
        this.financingPurpose = financingPurpose;
    }

    public String getBusinessTypeId() {
        return businessTypeId;
    }

    public void setBusinessTypeId(String businessTypeId) {
        this.businessTypeId = businessTypeId;
    }

    public String getDisburseDate() {
        return disburseDate;
    }

    public void setDisburseDate(String disburseDate) {
        this.disburseDate = disburseDate;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getHighRiskCustomer() {
        return highRiskCustomer;
    }

    public void setHighRiskCustomer(String highRiskCustomer) {
        this.highRiskCustomer = highRiskCustomer;
    }

    public String getHighRiskBusiness() {
        return highRiskBusiness;
    }

    public void setHighRiskBusiness(String highRiskBusiness) {
        this.highRiskBusiness = highRiskBusiness;
    }

    public List<FundedThingsReq> getFinancedGoods() {
        return financedGoods;
    }

    public void setFinancedGoods(List<FundedThingsReq> financedGoods) {
        this.financedGoods = financedGoods;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAp3rId() {
        return ap3rId;
    }

    public void setAp3rId(String ap3rId) {
        this.ap3rId = ap3rId;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getRejectionReason() {
        return rejectionReason;
    }

    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }

    public String getUserBwmp() {
        return userBwmp;
    }

    public void setUserBwmp(String userBwmp) {
        this.userBwmp = userBwmp;
    }

    public String getDeviationReason() {
        return deviationReason;
    }

    public void setDeviationReason(String deviationReason) {
        this.deviationReason = deviationReason;
    }

    public String getDeviationCode() {
        return deviationCode;
    }

    public void setDeviationCode(String deviationCode) {
        this.deviationCode = deviationCode;
    }

    @Override
    public String toString() {
        return "AP3RRequest{" +
                "username='" + username + '\'' +
                ", sessionKey='" + sessionKey + '\'' +
                ", imei='" + imei + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", startLookupDate='" + startLookupDate + '\'' +
                ", endLookupDate='" + endLookupDate + '\'' +
                ", getCountData='" + getCountData + '\'' +
                ", page='" + page + '\'' +
                ", swId='" + swId + '\'' +
                ", swProductId='" + swProductId + '\'' +
                ", haveAccount='" + haveAccount + '\'' +
                ", localId='" + localId + '\'' +
                ", accountPurpose='" + accountPurpose + '\'' +
                ", fundSource='" + fundSource + '\'' +
                ", yearlyTransaction='" + yearlyTransaction + '\'' +
                ", financingPurpose='" + financingPurpose + '\'' +
                ", businessTypeId='" + businessTypeId + '\'' +
                ", disburseDate='" + disburseDate + '\'' +
                ", dueDate='" + dueDate + '\'' +
                ", highRiskCustomer='" + highRiskCustomer + '\'' +
                ", highRiskBusiness='" + highRiskBusiness + '\'' +
                ", financedGoods=" + financedGoods +
                ", status='" + status + '\'' +
                ", action='" + action + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", ap3rId='" + ap3rId + '\'' +
                ", rejectionReason='" + rejectionReason + '\'' +
                ", approvalStatus='" + approvalStatus + '\'' +
                ", userBwmp='" + userBwmp + '\'' +
                ", deviationReason='" + deviationReason + '\'' +
                ", deviationCode='" + deviationCode + '\'' +
                '}';
    }

}