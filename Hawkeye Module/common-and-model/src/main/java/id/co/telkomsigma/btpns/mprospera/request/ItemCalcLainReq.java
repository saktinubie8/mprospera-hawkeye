package id.co.telkomsigma.btpns.mprospera.request;

/**
 * Created by Dzulfiqar on 25/04/2017.
 */
public class ItemCalcLainReq {

    private String amount;
    private String name;
    private String pendapatanPerbulan;
    private String periode;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPendapatanPerbulan() {
        return pendapatanPerbulan;
    }

    public void setPendapatanPerbulan(String pendapatanPerbulan) {
        this.pendapatanPerbulan = pendapatanPerbulan;
    }

    public String getPeriode() {
        return periode;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

}