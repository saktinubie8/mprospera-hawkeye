package id.co.telkomsigma.btpns.mprospera.response;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class SentraResponse extends BaseResponse {

    private String sentraId;
    private String kodeSentra;
    private String localId;
    private String grandTotal;
    private String currentTotal;
    private String totalPage;
    private List<SentraListResponse> sentraList;
    private List<String> deletedSentraList;
    private List<String> deletedSentraGroupList;

    public String getSentraId() {
        return sentraId;
    }

    public void setSentraId(String sentraId) {
        this.sentraId = sentraId;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getCurrentTotal() {
        return currentTotal;
    }

    public void setCurrentTotal(String currentTotal) {
        this.currentTotal = currentTotal;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    public List<SentraListResponse> getSentraList() {
        if (sentraList == null)
            sentraList = new ArrayList<>();
        return sentraList;
    }

    public void setSentraList(List<SentraListResponse> sentraList) {
        this.sentraList = sentraList;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public String getKodeSentra() {
        return kodeSentra;
    }

    public void setKodeSentra(String kodeSentra) {
        this.kodeSentra = kodeSentra;
    }

    public List<String> getDeletedSentraList() {
        return deletedSentraList;
    }

    public void setDeletedSentraList(List<String> deletedSentraList) {
        this.deletedSentraList = deletedSentraList;
    }

    public List<String> getDeletedSentraGroupList() {
        return deletedSentraGroupList;
    }

    public void setDeletedSentraGroupList(List<String> deletedSentraGroupList) {
        this.deletedSentraGroupList = deletedSentraGroupList;
    }

    @Override
    public String toString() {
        return "SentraResponse{" +
                "sentraId='" + sentraId + '\'' +
                ", kodeSentra='" + kodeSentra + '\'' +
                ", localId='" + localId + '\'' +
                ", grandTotal='" + grandTotal + '\'' +
                ", currentTotal='" + currentTotal + '\'' +
                ", totalPage='" + totalPage + '\'' +
                ", sentraList=" + sentraList +
                '}';
    }

}