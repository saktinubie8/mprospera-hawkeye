package id.co.telkomsigma.btpns.mprospera.request;

public class ESBTokenRequest extends BaseRequest {

    private String username;
    private String imei;
    private String authType;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    @Override
    public String toString() {
        return "ESBTokenRequest [username=" + username + ", imei=" + imei + ", authType=" + authType
                + ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime() + ", getRetrievalReferenceNumber()="
                + getRetrievalReferenceNumber() + "]";
    }

}