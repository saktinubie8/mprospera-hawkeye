package id.co.telkomsigma.btpns.mprospera.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Created by Dzulfiqar on 13/04/2017.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class SWExpenseResponse {

    private String angsuranNonUsaha;
    private String gajiUsaha;
    private String kesehatanNonUsaha;
    private String lainlainNonUsaha;
    private String makanNonUsaha;
    private String pendidikanNonUsaha;
    private String sewaUsaha;
    private String transportNonUsaha;
    private String transportUsaha;
    private String utilitasUsaha;
    private String utitlitasNonUsaha;

    public String getAngsuranNonUsaha() {
        return angsuranNonUsaha;
    }

    public void setAngsuranNonUsaha(String angsuranNonUsaha) {
        this.angsuranNonUsaha = angsuranNonUsaha;
    }

    public String getGajiUsaha() {
        return gajiUsaha;
    }

    public void setGajiUsaha(String gajiUsaha) {
        this.gajiUsaha = gajiUsaha;
    }

    public String getKesehatanNonUsaha() {
        return kesehatanNonUsaha;
    }

    public void setKesehatanNonUsaha(String kesehatanNonUsaha) {
        this.kesehatanNonUsaha = kesehatanNonUsaha;
    }

    public String getLainlainNonUsaha() {
        return lainlainNonUsaha;
    }

    public void setLainlainNonUsaha(String lainlainNonUsaha) {
        this.lainlainNonUsaha = lainlainNonUsaha;
    }

    public String getMakanNonUsaha() {
        return makanNonUsaha;
    }

    public void setMakanNonUsaha(String makanNonUsaha) {
        this.makanNonUsaha = makanNonUsaha;
    }

    public String getPendidikanNonUsaha() {
        return pendidikanNonUsaha;
    }

    public void setPendidikanNonUsaha(String pendidikanNonUsaha) {
        this.pendidikanNonUsaha = pendidikanNonUsaha;
    }

    public String getSewaUsaha() {
        return sewaUsaha;
    }

    public void setSewaUsaha(String sewaUsaha) {
        this.sewaUsaha = sewaUsaha;
    }

    public String getTransportNonUsaha() {
        return transportNonUsaha;
    }

    public void setTransportNonUsaha(String transportNonUsaha) {
        this.transportNonUsaha = transportNonUsaha;
    }

    public String getTransportUsaha() {
        return transportUsaha;
    }

    public void setTransportUsaha(String transportUsaha) {
        this.transportUsaha = transportUsaha;
    }

    public String getUtilitasUsaha() {
        return utilitasUsaha;
    }

    public void setUtilitasUsaha(String utilitasUsaha) {
        this.utilitasUsaha = utilitasUsaha;
    }

    public String getUtitlitasNonUsaha() {
        return utitlitasNonUsaha;
    }

    public void setUtitlitasNonUsaha(String utitlitasNonUsaha) {
        this.utitlitasNonUsaha = utitlitasNonUsaha;
    }

}