package id.co.telkomsigma.btpns.mprospera.model.survey;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "T_SURVEY")
public class Survey extends GenericModel {

    private static final long serialVersionUID = 408866013256083202L;

    private Long surveyId;
    private String surveyTypeId;
    private Date surveyDate;
    private String customerName;
    private String status;
    private String appId;
    private Date disbursementDate;
    private String longitude;
    private String latitude;
    private Date createdDate;
    private String createdBy;
    private Date updatedDate;
    private String updatedBy;
    private List<SurveyDetail> surveyDetail;
    private Customer customer;

    @Column(name = "app_id")
    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    public Long getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Long surveyId) {
        this.surveyId = surveyId;
    }

    @Column(name = "survey_type")
    public String getSurveyTypeId() {
        return surveyTypeId;
    }

    public void setSurveyTypeId(String surveyTypeId) {
        this.surveyTypeId = surveyTypeId;
    }

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
    @JoinColumn(name = "customer_id", referencedColumnName = "id", nullable = false)
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customerId) {
        this.customer = customerId;
    }

    @Column(name = "longitude")
    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Column(name = "latitude")
    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @Column(name = "created_dt")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "survey_dt")
    public Date getSurveyDate() {
        return surveyDate;
    }

    public void setSurveyDate(Date surveyDate) {
        this.surveyDate = surveyDate;
    }

    @Column(name = "customer_name")
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "disbursement_dt")
    public Date getDisbursementDate() {
        return disbursementDate;
    }

    public void setDisbursementDate(Date disbursementDate) {
        this.disbursementDate = disbursementDate;
    }

    @OneToMany(fetch = FetchType.LAZY, targetEntity = SurveyDetail.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "survey_id")
    public List<SurveyDetail> getSurveyDetail() {
        return surveyDetail;
    }

    public void setSurveyDetail(List<SurveyDetail> surveyDetail) {
        this.surveyDetail = surveyDetail;
    }

    @Column(name = "updated_dt")
    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Column(name = "updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

}