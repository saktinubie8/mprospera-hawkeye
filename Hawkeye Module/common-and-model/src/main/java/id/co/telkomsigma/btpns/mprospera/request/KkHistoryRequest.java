package id.co.telkomsigma.btpns.mprospera.request;

import java.text.SimpleDateFormat;
import java.util.Date;

public class KkHistoryRequest {

    private String createdDate;
    private String countKk;

    public String getCreatedDate() {
        return this.createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        this.createdDate = formatter.format(createdDate);
    }

    public String getCountKk() {
        return countKk;
    }

    public void setCountKk(String countKk) {
        this.countKk = countKk;
    }

}