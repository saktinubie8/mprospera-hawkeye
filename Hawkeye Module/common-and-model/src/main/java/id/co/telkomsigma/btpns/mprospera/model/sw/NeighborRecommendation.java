package id.co.telkomsigma.btpns.mprospera.model.sw;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;

@Entity
@Table(name = "T_NEIGHBOR_REC")
public class NeighborRecommendation extends GenericModel {

    private Long neighborId;
    private String neighborName;
    private String neighborAddress;
    private Character neighborRelation;
    private Character neighborOutstanding;
    private Character neighborBusiness;
    private Character neighborRecomend;
    private SurveyWawancara swId;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getNeighborId() {
        return neighborId;
    }

    public void setNeighborId(Long neighborId) {
        this.neighborId = neighborId;
    }

    @Column(name = "name")
    public String getNeighborName() {
        return neighborName;
    }

    public void setNeighborName(String neighborName) {
        this.neighborName = neighborName;
    }

    @Column(name = "address")
    public String getNeighborAddress() {
        return neighborAddress;
    }

    public void setNeighborAddress(String neighborAddress) {
        this.neighborAddress = neighborAddress;
    }

    @Column(name = "good_relation")
    public Character getNeighborRelation() {
        return neighborRelation;
    }

    public void setNeighborRelation(Character neighborRelation) {
        this.neighborRelation = neighborRelation;
    }

    @Column(name = "has_outstanding")
    public Character getNeighborOutstanding() {
        return neighborOutstanding;
    }

    public void setNeighborOutstanding(Character neighborOutstanding) {
        this.neighborOutstanding = neighborOutstanding;
    }

    @Column(name = "has_business")
    public Character getNeighborBusiness() {
        return neighborBusiness;
    }

    public void setNeighborBusiness(Character neighborBusiness) {
        this.neighborBusiness = neighborBusiness;
    }

    @Column(name = "final_recommendation")
    public Character getNeighborRecomend() {
        return neighborRecomend;
    }

    public void setNeighborRecomend(Character neighborRecomend) {
        this.neighborRecomend = neighborRecomend;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = SurveyWawancara.class)
    @JoinColumn(name = "sw_id", referencedColumnName = "id")
    public SurveyWawancara getSwId() {
        return swId;
    }

    public void setSwId(SurveyWawancara swId) {
        this.swId = swId;
    }

}