package id.co.telkomsigma.btpns.mprospera.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import id.co.telkomsigma.btpns.mprospera.model.user.Role;
import id.co.telkomsigma.btpns.mprospera.model.user.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Created by daniel on 4/23/15.
 */
public final class UserUtil {

    public static List<String> checkGrantedRoles(Authentication auth) {
        List<String> grantedRoles = new ArrayList<>();
        Collection<? extends GrantedAuthority> authorities = auth.getAuthorities();
        for (GrantedAuthority grantedAuthority : authorities) {
            grantedRoles.add(grantedAuthority.getAuthority());
        }
        return grantedRoles;
    }

    public static List<String> checkGrantedRoles(User user) {
        List<String> grantedRoles = new ArrayList<>();
        Set<Role> roles = user.getRoles();
        if (roles != null) {
            for (Role role : roles) {
                if (role != null) {
                    grantedRoles.add(role.getAuthority());
                }
            }
        }
        return grantedRoles;
    }

    public static boolean isSystemUser(List<String> grantedRoles) {
        return grantedRoles.contains(Role.System.SYSTEM.getAuthority());
    }

    public static boolean isAdminUser(List<String> grantedRoles) {
        return grantedRoles.contains(Role.System.ADMIN.getAuthority());
    }

    public static boolean isCsUser(List<String> grantedRoles) {
        return grantedRoles.contains(Role.System.CS.getAuthority());
    }

    public static boolean isCustomerUser(List<String> grantedRoles) {
        return grantedRoles.contains(Role.System.CUSTOMER.getAuthority());
    }

}