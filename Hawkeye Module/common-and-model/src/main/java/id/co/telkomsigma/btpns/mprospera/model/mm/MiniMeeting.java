package id.co.telkomsigma.btpns.mprospera.model.mm;

import java.util.Date;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_MINI_MEETING")
public class MiniMeeting extends GenericModel {

    private static final long serialVersionUID = 2245862234980003931L;
    private Long mmId;
    private String areaId;
    private String mmLocationName;
    private String mmOwner;
    private String mmNumber;
    private String pmCandidate;
    private String longitude;
    private String latitude;
    private Date createdDate;
    private String createdBy;
    private String rrn;
    private Boolean isDeleted = false;
    private Long pmId;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getMmId() {
        return mmId;
    }

    public void setMmId(Long mmId) {
        this.mmId = mmId;
    }

    @Column(name = "area_id", nullable = false)
    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    @Column(name = "location_name", nullable = false)
    public String getMmLocationName() {
        return mmLocationName;
    }

    public void setMmLocationName(String mmLocationName) {
        this.mmLocationName = mmLocationName;
    }

    @Column(name = "owner", nullable = false)
    public String getMmOwner() {
        return mmOwner;
    }

    public void setMmOwner(String mmOwner) {
        this.mmOwner = mmOwner;
    }

    @Column(name = "mm_number", nullable = false)
    public String getMmNumber() {
        return mmNumber;
    }

    public void setMmNumber(String mmNumber) {
        this.mmNumber = mmNumber;
    }

    @Column(name = "longitude", nullable = false)
    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Column(name = "latitude", nullable = false)
    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @Column(name = "created_date", nullable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "created_by", nullable = false)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "is_deleted")
    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Column(name = "rrn")
    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    @Column(name = "pm_candidate_number")
    public String getPmCandidate() {
        return pmCandidate;
    }

    public void setPmCandidate(String pmCandidate) {
        this.pmCandidate = pmCandidate;
    }

    @Column(name = "pm_id")
    public Long getPmId() {
        return pmId;
    }

    public void setPmId(Long pmId) {
        this.pmId = pmId;
    }

}