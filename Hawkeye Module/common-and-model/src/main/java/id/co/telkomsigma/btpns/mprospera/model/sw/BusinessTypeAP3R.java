package id.co.telkomsigma.btpns.mprospera.model.sw;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;

/**
 * Created by Dzulfiqar on 27/04/2017.
 */
@Entity
@Table(name = "T_BUSINESS_TYPE_AP3R")
public class BusinessTypeAP3R extends GenericModel {

    private Long businessId;
    private String businessType;
    private String subBusiness;
    private String biCode;
    private String subBiCode;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    @Column(name = "business_type")
    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    @Column(name = "sub_business")
    public String getSubBusiness() {
        return subBusiness;
    }

    public void setSubBusiness(String subBusiness) {
        this.subBusiness = subBusiness;
    }

    @Column(name = "bi_code")
    public String getBiCode() {
        return biCode;
    }

    public void setBiCode(String biCode) {
        this.biCode = biCode;
    }

    @Column(name = "sub_bi_code")
    public String getSubBiCode() {
        return subBiCode;
    }

    public void setSubBiCode(String subBiCode) {
        this.subBiCode = subBiCode;
    }

}