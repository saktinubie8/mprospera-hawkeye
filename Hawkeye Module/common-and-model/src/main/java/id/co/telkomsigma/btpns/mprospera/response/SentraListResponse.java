package id.co.telkomsigma.btpns.mprospera.response;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class SentraListResponse {

    private String sentraId;
    private String areaId;
    private String sentraName;
    private String sentraOwnerName;
    private String sentraLeader;
    private String startedDate;
    private String sentraAddress;
    private String rtRw;
    private String prsDay;
    private String longitude;
    private String latitude;
    private String createdBy;
    private String createdDate;
    private String status;
    private String level;
    private String prosperaId;
    private String locationId;
    private String meetingPlace;
    private String assignedTo;
    private List<GroupListResponse> sentraGroup;
    private String userFullName;
    private String postCode;
    private String meetingTime;
    private String sentraCode;
    private String lastPrs;
    private String hasPhoto;

    public String getSentraOwnerName() {
        return sentraOwnerName;
    }

    public void setSentraOwnerName(String sentraOwnerName) {
        this.sentraOwnerName = sentraOwnerName;
    }

    public String getStartedDate() {
        return startedDate;
    }

    public void setStartedDate(String startedDate) {
        this.startedDate = startedDate;
    }

    public String getSentraAddress() {
        return sentraAddress;
    }

    public void setSentraAddress(String sentraAddress) {
        this.sentraAddress = sentraAddress;
    }

    public String getRtRw() {
        return rtRw;
    }

    public void setRtRw(String rtRw) {
        this.rtRw = rtRw;
    }

    public String getPrsDay() {
        return prsDay;
    }

    public void setPrsDay(String prsDay) {
        this.prsDay = prsDay;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getProsperaId() {
        return prosperaId;
    }

    public void setProsperaId(String prosperaId) {
        this.prosperaId = prosperaId;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getSentraId() {
        return sentraId;
    }

    public void setSentraId(String sentraId) {
        this.sentraId = sentraId;
    }

    public String getSentraName() {
        return sentraName;
    }

    public void setSentraName(String sentraName) {
        this.sentraName = sentraName;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public List<GroupListResponse> getSentraGroup() {
        if (sentraGroup == null)
            sentraGroup = new ArrayList<GroupListResponse>();
        return sentraGroup;
    }

    public void setSentraGroup(List<GroupListResponse> sentraGroup) {
        this.sentraGroup = sentraGroup;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getMeetingPlace() {
        return meetingPlace;
    }

    public void setMeetingPlace(String meetingPlace) {
        this.meetingPlace = meetingPlace;
    }

    public String getSentraLeader() {
        return sentraLeader;
    }

    public void setSentraLeader(String sentraLeader) {
        this.sentraLeader = sentraLeader;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getMeetingTime() {
        return meetingTime;
    }

    public void setMeetingTime(String meetingTime) {
        this.meetingTime = meetingTime;
    }

    public String getSentraCode() {
        return sentraCode;
    }

    public void setSentraCode(String sentraCode) {
        this.sentraCode = sentraCode;
    }

    public String getLastPrs() {
        return lastPrs;
    }

    public void setLastPrs(String lastPrs) {
        this.lastPrs = lastPrs;
    }

    public String getHasPhoto() {
        return hasPhoto;
    }

    public void setHasPhoto(String hasPhoto) {
        this.hasPhoto = hasPhoto;
    }

    @Override
    public String toString() {
        return "SentraListResponse{" +
                "sentraId='" + sentraId + '\'' +
                ", areaId='" + areaId + '\'' +
                ", sentraName='" + sentraName + '\'' +
                ", sentraOwnerName='" + sentraOwnerName + '\'' +
                ", sentraLeader='" + sentraLeader + '\'' +
                ", startedDate='" + startedDate + '\'' +
                ", sentraAddress='" + sentraAddress + '\'' +
                ", rtRw='" + rtRw + '\'' +
                ", prsDay='" + prsDay + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", status='" + status + '\'' +
                ", level='" + level + '\'' +
                ", prosperaId='" + prosperaId + '\'' +
                ", locationId='" + locationId + '\'' +
                ", meetingPlace='" + meetingPlace + '\'' +
                ", assignedTo='" + assignedTo + '\'' +
                ", sentraGroup=" + sentraGroup +
                ", userFullName='" + userFullName + '\'' +
                ", postCode='" + postCode + '\'' +
                ", meetingTime='" + meetingTime + '\'' +
                ", sentraCode='" + sentraCode + '\'' +
                ", lastPrs='" + lastPrs + '\'' +
                ", hasPhoto='" + hasPhoto + '\'' +
                '}';
    }

}