package id.co.telkomsigma.btpns.mprospera.response;

import java.util.ArrayList;
import java.util.List;

public class SurveyQuestionList {

    private String surveyQuestionId;
    private String isActive;
    private String questionType;
    private String questionText;
    private String questionOrder;
    private String isMandatory;
    private List<QuestionChoiceList> questionChoiceList;

    public String getSurveyQuestionId() {
        return surveyQuestionId;
    }

    public void setSurveyQuestionId(String surveyQuestionId) {
        this.surveyQuestionId = surveyQuestionId;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getQuestionOrder() {
        return questionOrder;
    }

    public void setQuestionOrder(String questionOrder) {
        this.questionOrder = questionOrder;
    }

    public String getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(String isMandatory) {
        this.isMandatory = isMandatory;
    }

    public List<QuestionChoiceList> getQuestionChoiceList() {
        if (questionChoiceList == null)
            questionChoiceList = new ArrayList<>();
        return questionChoiceList;
    }

    public void setQuestionChoiceList(List<QuestionChoiceList> questionChoiceList) {
        this.questionChoiceList = questionChoiceList;
    }

}