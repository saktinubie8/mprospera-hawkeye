package id.co.telkomsigma.btpns.mprospera.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class LoanRequest extends BaseRequest {

    // for add
    private String username;
    private String sessionKey;
    private String imei;
    private String swId;
    private String ap3rId;
    private String customerName;
    private String status;
    private String longitude;
    private String latitude;
    private String loanAmountRecommended;
    private String action;
    private String loanId;
    private String localId;
    private String note;
    private String tokenKey;
    private String noappid;

    // for list
    private String getCountData;
    private String page;
    private String startLookupDate;
    private String endLookupDate;

    // for prospera purpose
    private String custReffId;
    private String loanReffId;
    private String perspective;
    private String prdOfferingShortName;
    private String tipeNasabah;
    private String tipePembiayaan;
    private String applicationDate;
    private String pembiayaanKe;
    private String penambahan;
    private String namaJenisUsaha;
    private String perlengkapanWarung;
    private String hargaPerlengkapanWarung;
    private String ternak;
    private String hargaTernak;
    private String bahanBaku;
    private String hargaBahanBaku;
    private String pulsaHandphone;
    private String hargaPulsaHandphone;
    private String other;
    private String hargaOther;
    private String totalHarga;
    private String loanAmount;
    private String interestRate;
    private String noOfInstallments;
    private String disbursementDate;
    private String gracePeriodDuration;
    private String fundId;
    private String businessActivitiesId;
    private String externalId;
    private String otherInstitutionFlag;
    private String institutionName;
    private String principalResidue;
    private String installmentResidue;
    private String installmentAmount;
    private String biCheckingResult;
    private String collectAbility;
    private String businessFlag;
    private String businessType;
    private String businessTypeValue;
    private String placeStatus;
    private String placeStatusValue;
    private String age;
    private String income;
    private String expense;
    private String netIncome;
    private String incomeFamily;
    private String businessFlagExp;
    private String businessTypeExp;
    private String businessTypeValueExp;
    private String ageExp;
    private String businessTypePlan;
    private String businessTypeValuePlan;
    private String placeStatusPlan;
    private String placeStatusValuePlan;
    private String hrb;
    private String hrc;
    private String deviation;
    private String flagId;
    private String comment;
    private String revenuePerMonth;
    private String customerNumber;
    private String maxLoanAmount;
    private String minLoanAmount;
    private String maxNoOfInstall;
    private String minNoOfInstall;
    private String interest;
    private String holidayBonus;
    private String orgDueDate;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;

    // for deviation
    private String userBwmp;
    private String deviationReason;
    private String deviationCode;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getSwId() {
        return swId;
    }

    public void setSwId(String swId) {
        this.swId = swId;
    }

    public String getAp3rId() {
        return ap3rId;
    }

    public void setAp3rId(String ap3rId) {
        this.ap3rId = ap3rId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLoanAmountRecommended() {
        return loanAmountRecommended;
    }

    public void setLoanAmountRecommended(String loanAmountRecommended) {
        this.loanAmountRecommended = loanAmountRecommended;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getLoanId() {
        return loanId;
    }

    public void setLoanId(String loanId) {
        this.loanId = loanId;
    }

    public String getGetCountData() {
        return getCountData;
    }

    public void setGetCountData(String getCountData) {
        this.getCountData = getCountData;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getStartLookupDate() {
        return startLookupDate;
    }

    public void setStartLookupDate(String startLookupDate) {
        this.startLookupDate = startLookupDate;
    }

    public String getEndLookupDate() {
        return endLookupDate;
    }

    public void setEndLookupDate(String endLookupDate) {
        this.endLookupDate = endLookupDate;
    }

    public String getCustReffId() {
        return custReffId;
    }

    public void setCustReffId(String custReffId) {
        this.custReffId = custReffId;
    }

    public String getLoanReffId() {
        return loanReffId;
    }

    public void setLoanReffId(String loanReffId) {
        this.loanReffId = loanReffId;
    }

    public String getPerspective() {
        return perspective;
    }

    public void setPerspective(String perspective) {
        this.perspective = perspective;
    }

    public String getPrdOfferingShortName() {
        return prdOfferingShortName;
    }

    public void setPrdOfferingShortName(String prdOfferingShortName) {
        this.prdOfferingShortName = prdOfferingShortName;
    }

    public String getTipeNasabah() {
        return tipeNasabah;
    }

    public void setTipeNasabah(String tipeNasabah) {
        this.tipeNasabah = tipeNasabah;
    }

    public String getTipePembiayaan() {
        return tipePembiayaan;
    }

    public void setTipePembiayaan(String tipePembiayaan) {
        this.tipePembiayaan = tipePembiayaan;
    }

    public String getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(String applicationDate) {
        this.applicationDate = applicationDate;
    }

    public String getPembiayaanKe() {
        return pembiayaanKe;
    }

    public void setPembiayaanKe(String pembiayaanKe) {
        this.pembiayaanKe = pembiayaanKe;
    }

    public String getPenambahan() {
        return penambahan;
    }

    public void setPenambahan(String penambahan) {
        this.penambahan = penambahan;
    }

    public String getNamaJenisUsaha() {
        return namaJenisUsaha;
    }

    public void setNamaJenisUsaha(String namaJenisUsaha) {
        this.namaJenisUsaha = namaJenisUsaha;
    }

    public String getPerlengkapanWarung() {
        return perlengkapanWarung;
    }

    public void setPerlengkapanWarung(String perlengkapanWarung) {
        this.perlengkapanWarung = perlengkapanWarung;
    }

    public String getHargaPerlengkapanWarung() {
        return hargaPerlengkapanWarung;
    }

    public void setHargaPerlengkapanWarung(String hargaPerlengkapanWarung) {
        this.hargaPerlengkapanWarung = hargaPerlengkapanWarung;
    }

    public String getTernak() {
        return ternak;
    }

    public void setTernak(String ternak) {
        this.ternak = ternak;
    }

    public String getHargaTernak() {
        return hargaTernak;
    }

    public void setHargaTernak(String hargaTernak) {
        this.hargaTernak = hargaTernak;
    }

    public String getBahanBaku() {
        return bahanBaku;
    }

    public void setBahanBaku(String bahanBaku) {
        this.bahanBaku = bahanBaku;
    }

    public String getHargaBahanBaku() {
        return hargaBahanBaku;
    }

    public void setHargaBahanBaku(String hargaBahanBaku) {
        this.hargaBahanBaku = hargaBahanBaku;
    }

    public String getPulsaHandphone() {
        return pulsaHandphone;
    }

    public void setPulsaHandphone(String pulsaHandphone) {
        this.pulsaHandphone = pulsaHandphone;
    }

    public String getHargaPulsaHandphone() {
        return hargaPulsaHandphone;
    }

    public void setHargaPulsaHandphone(String hargaPulsaHandphone) {
        this.hargaPulsaHandphone = hargaPulsaHandphone;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getHargaOther() {
        return hargaOther;
    }

    public void setHargaOther(String hargaOther) {
        this.hargaOther = hargaOther;
    }

    public String getTotalHarga() {
        return totalHarga;
    }

    public void setTotalHarga(String totalHarga) {
        this.totalHarga = totalHarga;
    }

    public String getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(String loanAmount) {
        this.loanAmount = loanAmount;
    }

    public String getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(String interestRate) {
        this.interestRate = interestRate;
    }

    public String getNoOfInstallments() {
        return noOfInstallments;
    }

    public void setNoOfInstallments(String noOfInstallments) {
        this.noOfInstallments = noOfInstallments;
    }

    public String getDisbursementDate() {
        return disbursementDate;
    }

    public void setDisbursementDate(String disbursementDate) {
        this.disbursementDate = disbursementDate;
    }

    public String getGracePeriodDuration() {
        return gracePeriodDuration;
    }

    public void setGracePeriodDuration(String gracePeriodDuration) {
        this.gracePeriodDuration = gracePeriodDuration;
    }

    public String getFundId() {
        return fundId;
    }

    public void setFundId(String fundId) {
        this.fundId = fundId;
    }

    public String getBusinessActivitiesId() {
        return businessActivitiesId;
    }

    public void setBusinessActivitiesId(String businessActivitiesId) {
        this.businessActivitiesId = businessActivitiesId;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getOtherInstitutionFlag() {
        return otherInstitutionFlag;
    }

    public void setOtherInstitutionFlag(String otherInstitutionFlag) {
        this.otherInstitutionFlag = otherInstitutionFlag;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getPrincipalResidue() {
        return principalResidue;
    }

    public void setPrincipalResidue(String principalResidue) {
        this.principalResidue = principalResidue;
    }

    public String getInstallmentResidue() {
        return installmentResidue;
    }

    public void setInstallmentResidue(String installmentResidue) {
        this.installmentResidue = installmentResidue;
    }

    public String getInstallmentAmount() {
        return installmentAmount;
    }

    public void setInstallmentAmount(String installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    public String getBiCheckingResult() {
        return biCheckingResult;
    }

    public void setBiCheckingResult(String biCheckingResult) {
        this.biCheckingResult = biCheckingResult;
    }

    public String getCollectAbility() {
        return collectAbility;
    }

    public void setCollectAbility(String collectAbility) {
        this.collectAbility = collectAbility;
    }

    public String getBusinessFlag() {
        return businessFlag;
    }

    public void setBusinessFlag(String businessFlag) {
        this.businessFlag = businessFlag;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getBusinessTypeValue() {
        return businessTypeValue;
    }

    public void setBusinessTypeValue(String businessTypeValue) {
        this.businessTypeValue = businessTypeValue;
    }

    public String getPlaceStatus() {
        return placeStatus;
    }

    public void setPlaceStatus(String placeStatus) {
        this.placeStatus = placeStatus;
    }

    public String getPlaceStatusValue() {
        return placeStatusValue;
    }

    public void setPlaceStatusValue(String placeStatusValue) {
        this.placeStatusValue = placeStatusValue;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getExpense() {
        return expense;
    }

    public void setExpense(String expense) {
        this.expense = expense;
    }

    public String getNetIncome() {
        return netIncome;
    }

    public void setNetIncome(String netIncome) {
        this.netIncome = netIncome;
    }

    public String getIncomeFamily() {
        return incomeFamily;
    }

    public void setIncomeFamily(String incomeFamily) {
        this.incomeFamily = incomeFamily;
    }

    public String getBusinessFlagExp() {
        return businessFlagExp;
    }

    public void setBusinessFlagExp(String businessFlagExp) {
        this.businessFlagExp = businessFlagExp;
    }

    public String getBusinessTypeExp() {
        return businessTypeExp;
    }

    public void setBusinessTypeExp(String businessTypeExp) {
        this.businessTypeExp = businessTypeExp;
    }

    public String getBusinessTypeValueExp() {
        return businessTypeValueExp;
    }

    public void setBusinessTypeValueExp(String businessTypeValueExp) {
        this.businessTypeValueExp = businessTypeValueExp;
    }

    public String getAgeExp() {
        return ageExp;
    }

    public void setAgeExp(String ageExp) {
        this.ageExp = ageExp;
    }

    public String getBusinessTypePlan() {
        return businessTypePlan;
    }

    public void setBusinessTypePlan(String businessTypePlan) {
        this.businessTypePlan = businessTypePlan;
    }

    public String getBusinessTypeValuePlan() {
        return businessTypeValuePlan;
    }

    public void setBusinessTypeValuePlan(String businessTypeValuePlan) {
        this.businessTypeValuePlan = businessTypeValuePlan;
    }

    public String getPlaceStatusPlan() {
        return placeStatusPlan;
    }

    public void setPlaceStatusPlan(String placeStatusPlan) {
        this.placeStatusPlan = placeStatusPlan;
    }

    public String getPlaceStatusValuePlan() {
        return placeStatusValuePlan;
    }

    public void setPlaceStatusValuePlan(String placeStatusValuePlan) {
        this.placeStatusValuePlan = placeStatusValuePlan;
    }

    public String getHrb() {
        return hrb;
    }

    public void setHrb(String hrb) {
        this.hrb = hrb;
    }

    public String getHrc() {
        return hrc;
    }

    public void setHrc(String hrc) {
        this.hrc = hrc;
    }

    public String getDeviation() {
        return deviation;
    }

    public void setDeviation(String deviation) {
        this.deviation = deviation;
    }

    public String getFlagId() {
        return flagId;
    }

    public void setFlagId(String flagId) {
        this.flagId = flagId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getRevenuePerMonth() {
        return revenuePerMonth;
    }

    public void setRevenuePerMonth(String revenuePerMonth) {
        this.revenuePerMonth = revenuePerMonth;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getMaxLoanAmount() {
        return maxLoanAmount;
    }

    public void setMaxLoanAmount(String maxLoanAmount) {
        this.maxLoanAmount = maxLoanAmount;
    }

    public String getMinLoanAmount() {
        return minLoanAmount;
    }

    public void setMinLoanAmount(String minLoanAmount) {
        this.minLoanAmount = minLoanAmount;
    }

    public String getMaxNoOfInstall() {
        return maxNoOfInstall;
    }

    public void setMaxNoOfInstall(String maxNoOfInstall) {
        this.maxNoOfInstall = maxNoOfInstall;
    }

    public String getMinNoOfInstall() {
        return minNoOfInstall;
    }

    public void setMinNoOfInstall(String minNoOfInstall) {
        this.minNoOfInstall = minNoOfInstall;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getHolidayBonus() {
        return holidayBonus;
    }

    public void setHolidayBonus(String holidayBonus) {
        this.holidayBonus = holidayBonus;
    }

    public String getOrgDueDate() {
        return orgDueDate;
    }

    public void setOrgDueDate(String orgDueDate) {
        this.orgDueDate = orgDueDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getDeviationReason() {
        return deviationReason;
    }

    public void setDeviationReason(String deviationReason) {
        this.deviationReason = deviationReason;
    }

    public String getDeviationCode() {
        return deviationCode;
    }

    public void setDeviationCode(String deviationCode) {
        this.deviationCode = deviationCode;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public String getUserBwmp() {
        return userBwmp;
    }

    public void setUserBwmp(String userBwmp) {
        this.userBwmp = userBwmp;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(String tokenKey) {
        this.tokenKey = tokenKey;
    }

    public String getNoappid() {
        return noappid;
    }

    public void setNoappid(String noappid) {
        this.noappid = noappid;
    }

    @Override
    public String toString() {
        return "LoanRequest{" +
                "username='" + username + '\'' +
                ", sessionKey='" + sessionKey + '\'' +
                ", imei='" + imei + '\'' +
                ", swId='" + swId + '\'' +
                ", ap3rId='" + ap3rId + '\'' +
                ", customerName='" + customerName + '\'' +
                ", status='" + status + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", loanAmountRecommended='" + loanAmountRecommended + '\'' +
                ", action='" + action + '\'' +
                ", loanId='" + loanId + '\'' +
                ", localId='" + localId + '\'' +
                ", note='" + note + '\'' +
                ", tokenKey='" + tokenKey + '\'' +
                ", noappid='" + noappid + '\'' +
                ", getCountData='" + getCountData + '\'' +
                ", page='" + page + '\'' +
                ", startLookupDate='" + startLookupDate + '\'' +
                ", endLookupDate='" + endLookupDate + '\'' +
                ", custReffId='" + custReffId + '\'' +
                ", loanReffId='" + loanReffId + '\'' +
                ", perspective='" + perspective + '\'' +
                ", prdOfferingShortName='" + prdOfferingShortName + '\'' +
                ", tipeNasabah='" + tipeNasabah + '\'' +
                ", tipePembiayaan='" + tipePembiayaan + '\'' +
                ", applicationDate='" + applicationDate + '\'' +
                ", pembiayaanKe='" + pembiayaanKe + '\'' +
                ", penambahan='" + penambahan + '\'' +
                ", namaJenisUsaha='" + namaJenisUsaha + '\'' +
                ", perlengkapanWarung='" + perlengkapanWarung + '\'' +
                ", hargaPerlengkapanWarung='" + hargaPerlengkapanWarung + '\'' +
                ", ternak='" + ternak + '\'' +
                ", hargaTernak='" + hargaTernak + '\'' +
                ", bahanBaku='" + bahanBaku + '\'' +
                ", hargaBahanBaku='" + hargaBahanBaku + '\'' +
                ", pulsaHandphone='" + pulsaHandphone + '\'' +
                ", hargaPulsaHandphone='" + hargaPulsaHandphone + '\'' +
                ", other='" + other + '\'' +
                ", hargaOther='" + hargaOther + '\'' +
                ", totalHarga='" + totalHarga + '\'' +
                ", loanAmount='" + loanAmount + '\'' +
                ", interestRate='" + interestRate + '\'' +
                ", noOfInstallments='" + noOfInstallments + '\'' +
                ", disbursementDate='" + disbursementDate + '\'' +
                ", gracePeriodDuration='" + gracePeriodDuration + '\'' +
                ", fundId='" + fundId + '\'' +
                ", businessActivitiesId='" + businessActivitiesId + '\'' +
                ", externalId='" + externalId + '\'' +
                ", otherInstitutionFlag='" + otherInstitutionFlag + '\'' +
                ", institutionName='" + institutionName + '\'' +
                ", principalResidue='" + principalResidue + '\'' +
                ", installmentResidue='" + installmentResidue + '\'' +
                ", installmentAmount='" + installmentAmount + '\'' +
                ", biCheckingResult='" + biCheckingResult + '\'' +
                ", collectAbility='" + collectAbility + '\'' +
                ", businessFlag='" + businessFlag + '\'' +
                ", businessType='" + businessType + '\'' +
                ", businessTypeValue='" + businessTypeValue + '\'' +
                ", placeStatus='" + placeStatus + '\'' +
                ", placeStatusValue='" + placeStatusValue + '\'' +
                ", age='" + age + '\'' +
                ", income='" + income + '\'' +
                ", expense='" + expense + '\'' +
                ", netIncome='" + netIncome + '\'' +
                ", incomeFamily='" + incomeFamily + '\'' +
                ", businessFlagExp='" + businessFlagExp + '\'' +
                ", businessTypeExp='" + businessTypeExp + '\'' +
                ", businessTypeValueExp='" + businessTypeValueExp + '\'' +
                ", ageExp='" + ageExp + '\'' +
                ", businessTypePlan='" + businessTypePlan + '\'' +
                ", businessTypeValuePlan='" + businessTypeValuePlan + '\'' +
                ", placeStatusPlan='" + placeStatusPlan + '\'' +
                ", placeStatusValuePlan='" + placeStatusValuePlan + '\'' +
                ", hrb='" + hrb + '\'' +
                ", hrc='" + hrc + '\'' +
                ", deviation='" + deviation + '\'' +
                ", flagId='" + flagId + '\'' +
                ", comment='" + comment + '\'' +
                ", revenuePerMonth='" + revenuePerMonth + '\'' +
                ", customerNumber='" + customerNumber + '\'' +
                ", maxLoanAmount='" + maxLoanAmount + '\'' +
                ", minLoanAmount='" + minLoanAmount + '\'' +
                ", maxNoOfInstall='" + maxNoOfInstall + '\'' +
                ", minNoOfInstall='" + minNoOfInstall + '\'' +
                ", interest='" + interest + '\'' +
                ", holidayBonus='" + holidayBonus + '\'' +
                ", orgDueDate='" + orgDueDate + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", modifiedBy='" + modifiedBy + '\'' +
                ", modifiedDate='" + modifiedDate + '\'' +
                ", userBwmp='" + userBwmp + '\'' +
                ", deviationReason='" + deviationReason + '\'' +
                ", deviationCode='" + deviationCode + '\'' +
                '}';
    }

}