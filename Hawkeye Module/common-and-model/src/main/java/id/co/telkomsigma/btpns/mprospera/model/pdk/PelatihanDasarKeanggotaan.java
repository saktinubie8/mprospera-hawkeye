package id.co.telkomsigma.btpns.mprospera.model.pdk;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name = "T_PDK")
public class PelatihanDasarKeanggotaan extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = 324826063966211848L;
    private Long pdkId;
    private String pdkLocation;
    private String phoneNumber;
    private Date pdkDate;
    private String latitude;
    private String longitude;
    private Integer totalParticipant;
    private Integer graduateParticipant;
    private String createdBy;
    private Date createdDate;
    private String rrn;
    private List<PDKDetail> swList;
    private Boolean isDeleted = false;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getPdkId() {
        return pdkId;
    }

    public void setPdkId(Long pdkId) {
        this.pdkId = pdkId;
    }

    @Column(name = "is_deleted")
    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Column(name = "pdk_location", nullable = false)
    public String getPdkLocation() {
        return pdkLocation;
    }

    public void setPdkLocation(String pdkLocation) {
        this.pdkLocation = pdkLocation;
    }

    @Column(name = "phone_number")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Column(name = "latitude", nullable = false)
    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @Column(name = "longitude", nullable = false)
    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @OneToMany(fetch = FetchType.EAGER, targetEntity = PDKDetail.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "pdk_id")
    public List<PDKDetail> getSwList() {
        return swList;
    }

    public void setSwList(List<PDKDetail> swList) {
        this.swList = swList;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    @Column(name = "pdk_date", nullable = false)
    public Date getPdkDate() {
        return pdkDate;
    }

    public void setPdkDate(Date pdkDate) {
        this.pdkDate = pdkDate;
    }

    @Column(name = "total_participant")
    public Integer getTotalParticipant() {
        return totalParticipant;
    }

    public void setTotalParticipant(Integer totalParticipant) {
        this.totalParticipant = totalParticipant;
    }

    @Column(name = "graduate_participant")
    public Integer getGraduateParticipant() {
        return graduateParticipant;
    }

    public void setGraduateParticipant(Integer graduateParticipant) {
        this.graduateParticipant = graduateParticipant;
    }

    @Column(name = "created_by", nullable = false)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "created_date", nullable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "rrn")
    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

}