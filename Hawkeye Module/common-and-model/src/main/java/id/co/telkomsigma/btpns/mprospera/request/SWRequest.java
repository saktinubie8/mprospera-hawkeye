package id.co.telkomsigma.btpns.mprospera.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class SWRequest extends BaseRequest {

    /* untuk add SW */
    private String username;
    private String sessionKey;
    private String imei;
    private String areaId;
    private String customerRegistrationType;
    private String customerCifNumber;
    private String customerName;
    private String customerIdNumber;
    private String idPhoto;
    private String surveyPhoto;
    private String workType;
    private String surveyDate;
    private String houseType;
    private String neighborhoodRecommendation;
    private String workIncome;
    private String totalPurchase;
    private String workExpense;
    private String iir;
    private String loanAmountRequested;
    private String loanAmountRecommended;
    private String finalRecommendation;
    private String longitude;
    private String latitude;
    private String action;
    private String swId;

    /* untuk list SW */
    private String getCountData;
    private String page;
    private String startLookupDate;
    private String endLookupDate;
    private String appId;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getCustomerRegistrationType() {
        return customerRegistrationType;
    }

    public void setCustomerRegistrationType(String customerRegistrationType) {
        this.customerRegistrationType = customerRegistrationType;
    }

    public String getCustomerCifNumber() {
        return customerCifNumber;
    }

    public void setCustomerCifNumber(String customerCifNumber) {
        this.customerCifNumber = customerCifNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerIdNumber() {
        return customerIdNumber;
    }

    public void setCustomerIdNumber(String customerIdNumber) {
        this.customerIdNumber = customerIdNumber;
    }

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public String getSurveyDate() {
        return surveyDate;
    }

    public void setSurveyDate(String surveyDate) {
        this.surveyDate = surveyDate;
    }

    public String getHouseType() {
        return houseType;
    }

    public void setHouseType(String houseType) {
        this.houseType = houseType;
    }

    public String getNeighborhoodRecommendation() {
        return neighborhoodRecommendation;
    }

    public void setNeighborhoodRecommendation(String neighborhoodRecommendation) {
        this.neighborhoodRecommendation = neighborhoodRecommendation;
    }

    public String getWorkIncome() {
        return workIncome;
    }

    public void setWorkIncome(String workIncome) {
        this.workIncome = workIncome;
    }

    public String getTotalPurchase() {
        return totalPurchase;
    }

    public void setTotalPurchase(String totalPurchase) {
        this.totalPurchase = totalPurchase;
    }

    public String getWorkExpense() {
        return workExpense;
    }

    public void setWorkExpense(String workExpense) {
        this.workExpense = workExpense;
    }

    public String getIir() {
        return iir;
    }

    public void setIir(String iir) {
        this.iir = iir;
    }

    public String getLoanAmountRequested() {
        return loanAmountRequested;
    }

    public void setLoanAmountRequested(String loanAmountRequested) {
        this.loanAmountRequested = loanAmountRequested;
    }

    public String getFinalRecommendation() {
        return finalRecommendation;
    }

    public void setFinalRecommendation(String finalRecommendation) {
        this.finalRecommendation = finalRecommendation;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLattitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getSwId() {
        return swId;
    }

    public void setSwId(String swId) {
        this.swId = swId;
    }

    public String getGetCountData() {
        return getCountData;
    }

    public void setGetCountData(String getCountData) {
        this.getCountData = getCountData;
    }

    public String getStartLookupDate() {
        return startLookupDate;
    }

    public void setStartLookupDate(String startLookupDate) {
        this.startLookupDate = startLookupDate;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getEndLookupDate() {
        return endLookupDate;
    }

    public void setEndLookupDate(String endLookupDate) {
        this.endLookupDate = endLookupDate;
    }

    public String getIdPhoto() {
        return idPhoto;
    }

    public void setIdPhoto(String idPhoto) {
        this.idPhoto = idPhoto;
    }

    public String getSurveyPhoto() {
        return surveyPhoto;
    }

    public void setSurveyPhoto(String surveyPhoto) {
        this.surveyPhoto = surveyPhoto;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLoanAmountRecommended() {
        return loanAmountRecommended;
    }

    public void setLoanAmountRecommended(String loanAmountRecommended) {
        this.loanAmountRecommended = loanAmountRecommended;
    }

    @Override
    public String toString() {
        return "SWRequest [username=" + username + ", sessionKey=" + sessionKey + ", imei=" + imei + ", areaId="
                + areaId + ", customerRegistrationType=" + customerRegistrationType + ", customerCifNumber="
                + customerCifNumber + ", customerName=" + customerName + ", customerIdNumber=" + customerIdNumber
                + ", workType=" + workType + ", surveyDate=" + surveyDate + ", houseType=" + houseType
                + ", neighborhoodRecommendation=" + neighborhoodRecommendation + ", workIncome=" + workIncome
                + ", totalPurchase=" + totalPurchase + ", workExpense=" + workExpense + ", iir=" + iir
                + ", loanAmountRequested=" + loanAmountRequested + ", loanAmountRecommended=" + loanAmountRecommended
                + ", finalRecommendation=" + finalRecommendation + ", longitude=" + longitude + ", latitude="
                + latitude + ", action=" + action + ", swId=" + swId + ", getCountData=" + getCountData + ", page="
                + page + ", startLookupDate=" + startLookupDate + ", endLookupDate=" + endLookupDate + ", appId="
                + appId + ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime()
                + ", getRetrievalReferenceNumber()=" + getRetrievalReferenceNumber() + "]";
    }

}