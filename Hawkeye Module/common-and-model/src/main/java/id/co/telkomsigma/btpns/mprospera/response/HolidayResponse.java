package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class HolidayResponse extends BaseResponse {

    private List<String> listOfHolidayDate;

    public List<String> getListOfHolidayDate() {
        return listOfHolidayDate;
    }

    public void setListOfHolidayDate(List<String> listOfHolidayDate) {
        this.listOfHolidayDate = listOfHolidayDate;
    }

    @Override
    public String toString() {
        return "HolidayResponse [listOfHolidayDate=" + listOfHolidayDate + "]";
    }

}