package id.co.telkomsigma.btpns.mprospera.model.sw;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "T_FUNDED_THINGS")
public class FundedThings extends GenericModel {

    private Long productId;
    private String productName;
    private BigDecimal price;
    private AP3R ap3rId;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    @Column(name = "product_name")
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Column(name = "price")
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = AP3R.class)
    @JoinColumn(name = "ap3r_id", referencedColumnName = "id", nullable = true)
    public AP3R getAp3rId() {
        return ap3rId;
    }

    public void setAp3rId(AP3R ap3rId) {
        this.ap3rId = ap3rId;
    }

}