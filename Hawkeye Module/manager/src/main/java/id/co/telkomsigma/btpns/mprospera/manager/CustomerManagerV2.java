package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerWithAbsence;
import id.co.telkomsigma.btpns.mprospera.model.customer.CycleNumberAndCustomer;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface CustomerManagerV2 {

	List<CycleNumberAndCustomer> getAllCustomerByLoc(String loc,int startPosition,int maxResult);

    List<CycleNumberAndCustomer> getAllCustomerByLoc(String loc);

    List<CycleNumberAndCustomer> getAllCustomerByLocAndCreatedDate(String loc, String parse, String parse2);

    List<CycleNumberAndCustomer> getAllCustomerByLocAndCreatedDate(String loc, String start, String end, int startPosition,int maxResult);

    Integer countCustomerByUsername(String username);
    
    String getGrandTotal(String loc);
    
    String getGrandTotal(String loc,String start, String end);

    CustomerWithAbsence findByCustomerId(Long customerId);

    Boolean isValidCustomer(String customerId);

    void clearCache();

    void save(Customer customer);

    Customer getBySwId(Long swId);

    Customer getByRrn(String rrn);

    Customer findById(long parseLong);

    void delete(Customer customer);

    List<Customer> getNewCustomerLoanDeviation(Long userId);

    List<Customer> getExistCustomerLoanDeviation(Long userId);

    List<Customer> getNewCustomerLoanDeviationAndWisma(List<String> userIdList);

    List<Customer> getExistCustomerLoanDeviationAndWisma(List<String> userIdList);

    List<Customer> getNewCustomerLoanDeviationWithBatch(Long userId, String batch);

    List<Customer> getExistCustomerLoanDeviationWithBatch(Long userId, String batch);

    List<Customer> getNewCustomerLoanDeviationAndWismaWithBatch(List<String> userIdList, String batch);

    List<Customer> getExistCustomerLoanDeviationAndWismaWithBatch(List<String> userIdList, String batch);

    List<Customer> findIsDeletedCustomerList();
    
    List<Long> findProductIdByCycleNumber(int cycleNumber);
    
    

}