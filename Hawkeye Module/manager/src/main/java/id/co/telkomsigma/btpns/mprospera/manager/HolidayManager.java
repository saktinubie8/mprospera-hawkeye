package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.parameter.Holiday;

public interface HolidayManager {

    Holiday findById(Long id);

    List<Holiday> findAll();
}