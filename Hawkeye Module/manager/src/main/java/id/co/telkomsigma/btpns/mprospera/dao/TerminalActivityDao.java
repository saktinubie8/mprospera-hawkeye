package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Dzulfiqar on 11/12/15.
 */
public interface TerminalActivityDao extends JpaRepository<TerminalActivity, String> {

}