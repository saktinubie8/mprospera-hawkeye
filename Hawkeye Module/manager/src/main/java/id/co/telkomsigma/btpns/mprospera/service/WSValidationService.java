package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.feign.VisionInterface;
import id.co.telkomsigma.btpns.mprospera.manager.ManageApkManager;
import id.co.telkomsigma.btpns.mprospera.manager.TerminalManager;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import org.apache.commons.validator.routines.checkdigit.LuhnCheckDigit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service("wsValidationService")
public class WSValidationService extends GenericService {

    @Autowired
    private VisionInterface visionInterface;

    @Autowired
    private TerminalManager terminalManager;

    @Autowired
    private ManageApkManager manageApkManager;

    @Autowired
    private UserManager userManager;

    public String wsValidation(String username, String imei, String sessionKey, String apkVersion) {

        log.info("Validating REST Message from User : " + username + " and Device : " + imei);
        String responseCode = WebGuiConstant.RC_SUCCESS;
        //autovalid for monitoring purpose
        String tmp = "lIiwiaWF0IjoxNTE2MjM5MDIyfQ";
        if (tmp.equals(imei) && tmp.equals(sessionKey)) return responseCode;
        if (username == null) {
            log.error("Username is blank,rejecting messages");
            responseCode = WebGuiConstant.RC_USERNAME_NOT_EXISTS;
        } else if (imei == null) {
            log.error("IMEI is blank,rejecting messages");
            responseCode = WebGuiConstant.RC_IMEI_NOT_EXISTS;
        } else if (sessionKey == null) {
            log.error("Session key is blank,rejecting messages");
            responseCode = WebGuiConstant.RC_SESSION_KEY_NOT_EXISTS;
        } else if (!new LuhnCheckDigit().isValid(imei)) {
            log.error("IMEI is not a valid format,rejecting messages");
            responseCode = WebGuiConstant.RC_IMEI_NOT_VALID_FORMAT;
        } else {
            Terminal terminalData = terminalManager.getTerminalByImei(imei);
            if (username.equals("")) {
                log.error("Username is blank,rejecting messages");
                responseCode = WebGuiConstant.RC_USERNAME_NOT_EXISTS;
                return responseCode;
            }
            String lastSessionKey = userManager.getSessionKeyByUsername(username);
            log.debug("Session Key : " + lastSessionKey);
            if (terminalData == null) {
                log.error("No terminal data found based on the IMEI,rejecting messages");
                responseCode = WebGuiConstant.RC_TERMINAL_NOT_FOUND;
            } else {
                boolean retVal = false;
                if ("".equals(sessionKey))
                    retVal = false;
                if (sessionKey.equals(lastSessionKey))
                    retVal = true;
                if (!retVal) {
                    log.error("Session Key is not valid,rejecting messages");
                    responseCode = WebGuiConstant.RC_INVALID_SESSION_KEY;
                }
            }
        }
        log.info("Finished validationg REST messages from Android, Result:" + responseCode);
        if (!manageApkManager.isApkValid(apkVersion))
            responseCode = WebGuiConstant.RC_INVALID_APK;
        return responseCode;
        //return visionInterface.wsValidation(username, imei, sessionKey, apkVersion);
    }

}