package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.sw.*;
import java.util.Date;
import java.util.List;

public interface SWManager {

    BusinessTypeAP3R findBusinessTypeAP3RById(Long businessId);
    
    BusinessTypeAP3R findBySubBiCode(String subBiCode);

    List<SurveyWawancara> findByUserMS(List<String> userIdList);

    List<SurveyWawancara> findByUserMSWithDate(List<String> userIdList, Date startDate, Date endDate);

    Boolean isValidSw(String swId);

    void save(SurveyWawancara surveyWawancara);

    SwIdPhoto getSwIdPhoto(String swId);

    SwSurveyPhoto getSwSurveyPhoto(String swId);

    SurveyWawancara getSWById(String swId);

    SurveyWawancara findByLocalId(String localId);

    LoanProduct findByProductId(String productId);

    SWProductMapping findProductMapById(Long id);

    List<DirectBuyThings> findProductBySwId(SurveyWawancara sw);

    List<AWGMBuyThings> findAwgmProductBySwId(SurveyWawancara sw);

    List<NeighborRecommendation> findNeighborBySwId(SurveyWawancara sw);

    List<SurveyWawancara> findSwIdInLoanDeviation(Long userId);

    List<SurveyWawancara> findSwIdInLoanDeviationWithBatch(Long userId, String batch);

    void clearCache();

    List<SWProductMapping> findProductMapBySwId(Long swId);

    List<OtherItemCalculation> findOtherItemCalcBySwId(Long swId);

    List<SWUserBWMPMapping> findBySw(Long sw);

    List<SurveyWawancara> findIsDeletedSwList();

}