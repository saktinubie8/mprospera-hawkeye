package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.AuditLogDao;
import id.co.telkomsigma.btpns.mprospera.manager.AuditLogManager;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

@Service("auditLogManager")
public class AuditLogManagerImpl implements AuditLogManager {

    @Autowired
    private AuditLogDao auditLogDao;

    @Override
    @Caching(evict = {@CacheEvict(value = "gro.audit.getAll", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "gro.audit.searchAuditLog", allEntries = true, beforeInvocation = true)})
    public AuditLog insertAuditLog(AuditLog auditLog) {
//		auditLog = auditLogDao.save(auditLog);
//		return auditLog;
        return null;
    }

}