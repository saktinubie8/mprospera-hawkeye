package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("cacheService")
public class CacheService extends GenericService {

    @Autowired
    AreaManager areaManager;
    @Autowired
    ParamManager paramManager;
    @Autowired
    PDKManager pdkManager;
    @Autowired
    SentraManager sentraManager;
    @Autowired
    SWManager swManager;
    @Autowired
    TerminalManager terminalManager;
    @Autowired
    UserManager userManager;
    @Autowired
    LocationManager locationManager;
    @Autowired
    CustomerManager customerManager;
    @Autowired
    LoanManager loanManager;
    @Autowired
    SurveyManager surveyManager;
    @Autowired
    AP3RManager ap3RManager;

    public void clearCache() {
        areaManager.clearCache();
        ap3RManager.clearCache();
        customerManager.clearCache();
        loanManager.clearCache();
        paramManager.clearCache();
        pdkManager.clearCache();
        sentraManager.clearCache();
        swManager.clearCache();
        terminalManager.clearCache();
        userManager.clearCache();
        locationManager.clearCache();
        customerManager.clearCache();
        loanManager.clearCache();
        surveyManager.clearCache();
    }

}