package id.co.telkomsigma.btpns.mprospera.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import id.co.telkomsigma.btpns.mprospera.model.sw.ProductPlafond;

public interface ProductPlafondDao extends JpaRepository<ProductPlafond, Long> {

}