package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.ParamDao;
import id.co.telkomsigma.btpns.mprospera.manager.ParamManager;
import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service("paramManager")
public class ParamManagerImpl implements ParamManager {

    @Autowired
    private ParamDao paramDao;

    @Override
    @Cacheable(value = "hwk.param.paramByParamName", unless = "#result == null", key = "#paramName")
    public SystemParameter getParamByParamName(String paramName) {
        SystemParameter param = paramDao.findByParamName(paramName);
        if (param != null)
            param.getCreatedBy().setOfficeCode(null);
        return param;
    }

    @Override
    @CacheEvict(value = {"hwk.param.paramByParamName"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {
    }

}