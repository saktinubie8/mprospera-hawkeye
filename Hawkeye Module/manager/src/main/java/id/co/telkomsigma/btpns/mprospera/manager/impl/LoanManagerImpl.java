package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.dao.*;
import id.co.telkomsigma.btpns.mprospera.model.loan.*;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.manager.LoanManager;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

@Service("loanManager")
public class LoanManagerImpl implements LoanManager {

    protected final Log log = LogFactory.getLog(getClass());

    @Autowired
    private LoanDao loanDao;

    @Autowired
    private LoanDeviationDao loanDeviationDao;

    @Autowired
    private LoanWithEmergencyFundDao loanWithEmergencyFundDao;

    @Autowired
    private DeviationOfficerMapDao deviationOfficerMapDao;

    @Autowired
    private DeviationCodeDao deviationCodeDao;

    @Autowired
    private DeviationPhotoDao deviationPhotoDao;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    DataSource dataSource;

    @Override
    @Cacheable(value = "hwk.loan.findByLoanId", unless = "#result == null")
    public Loan findById(long parseLong) {
        // TODO Auto-generated method stub
        return loanDao.findOne(parseLong);
    }

    @Override
    @Cacheable(value = "hwk.loan.getLoanByRrn", unless = "#result == null")
    public Loan getLoanByRrn(String rrn) {
        // TODO Auto-generated method stub
        return loanDao.findByRrnAndIsDeletedFalse(rrn);
    }

    @Override
    @Cacheable(value = "hwk.loan.getLoanByAp3rId", unless = "#result == null")
    public Loan getLoanByAp3rId(Long ap3rId) {
        return loanDao.findByAp3rIdAndIsDeletedFalse(ap3rId);
    }

    @Override
    @Cacheable(value = "hwk.loan.findLoanByCreatedDate", unless = "#result == null")
    public Page<Loan> findByCreatedDate(Date startDate, Date endDate, String officeId) {
        // TODO Auto-generated method stub
        return loanDao.findByCreatedDate(startDate, endDate, new PageRequest(0, Integer.MAX_VALUE), officeId);
    }

    @Override
    @Cacheable(value = "hwk.loan.findLoanByCreatedDatePageable", unless = "#result == null")
    public Page<Loan> findByCreatedDatePageable(Date startDate, Date endDate, PageRequest pageRequest,
                                                String officeId) {
        // TODO Auto-generated method stub
        return loanDao.findByCreatedDate(startDate, endDate, pageRequest, officeId);
    }

    @Override
    @Cacheable(value = "hwk.loan.isValidLoan", unless = "#result == null")
    public Boolean isValidLoan(Long loanId) {
        Integer count = loanDao.countByLoanId(loanId);
        if (count > 0)
            return true;
        else
            return false;
    }

    @Override
    @Cacheable(value = "hwk.loan.findIsDeletedLoanList", unless = "#result == null")
    public List<Loan> findIsDeletedLoanList() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -7);
        Date dateBefore7Days = cal.getTime();
        return loanDao.findIsDeletedLoanId(dateBefore7Days, new Date());
    }

    @Override
    @Cacheable(value = "hwk.loan.findByCustomerAndStatus", unless = "#result == null")
    public List<Loan> findByCustomerAndStatus(Customer customer, String status) {
        // TODO Auto-generated method stub
        return loanDao.findByCustomerAndStatus(customer, status);
    }

    @Override
    @Cacheable(value = "hwk.loan.findByAp3rAndCustomer", unless = "#result == null")
    public Loan findByAp3rAndCustomer(Long ap3rId, Customer customer) {
        List<String> statusList = new ArrayList<>();
        statusList.add(WebGuiConstant.STATUS_ACTIVE);
        statusList.add(WebGuiConstant.STATUS_APPROVED);
        statusList.add(WebGuiConstant.STATUS_DRAFT);
        return loanDao.findByAp3rIdAndCustomerAndAp3rIdIsNotNullAndIsDeletedFalseAndStatusIn
                (ap3rId, customer, statusList);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "hwk.loan.findLoanByCreatedDate", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.loan.findLoanByCreatedDatePageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.loan.getLoanByRrn", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.loan.findByLoanId", key = "#loan.loanId", beforeInvocation = true),
            @CacheEvict(value = "hwk.loan.getLoanByAp3rId", key = "#loan.ap3rId", beforeInvocation = true),
            @CacheEvict(value = "hwk.loan.isValidLoan", key = "#loan.loanId", beforeInvocation = true),
            @CacheEvict(value = "hwk.loan.findIsDeletedLoanList", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.loan.findByCustomerAndStatus", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.loan.findByAp3rAndCustomer", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "irn.loan.findByLoanId", key = "#loan.loanId", beforeInvocation = true),
            @CacheEvict(value = "irn.loan.findById", key = "#loan.loanId", beforeInvocation = true),
            @CacheEvict(value = "irn.loan.getLoanByAppId", key = "#loan.appId", beforeInvocation = true),
            @CacheEvict(value = "irn.loan.getLoanCairBySenta", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "irn.loan.findByCustomer", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.loan.findByCreatedDate", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.loan.findByCreatedDatePageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.loan.findById", key = "#loan.loanId", beforeInvocation = true),
            @CacheEvict(value = "wln.loan.findByAp3rAndCustomer", allEntries = true, beforeInvocation = true)})
    public void save(Loan loan) {
        loanDao.save(loan);
    }

    @Override
    @Cacheable(value = "hwk.deviation.findDeviationByLoanId", unless = "#result == null")
    public LoanDeviation findByLoanId(Long loanId) {
        return loanDeviationDao.findByLoanIdAndIsDeletedFalse(loanId);
    }

    @Override
    @Cacheable(value = "hwk.deviation.findByAp3rId", unless = "#result == null")
    public LoanDeviation findByAp3rId(Long ap3rId, Boolean isDeleted) {
        return loanDeviationDao.findByAp3rIdAndIsDeleted(ap3rId, isDeleted);
    }

    @Override
    @Cacheable(value = "hwk.deviation.findById", unless = "#result == null")
    public LoanDeviation findById(Long deviationId) {
        return loanDeviationDao.findOne(deviationId);
    }

    @Override
    @Cacheable(value = "hwk.deviation.findAll", unless = "#result == null")
    public List<LoanDeviation> findAll() {
        return loanDeviationDao.findAll();
    }

    @Override
    @Cacheable(value = "hwk.deviation.findByBatch", unless = "#result == null")
    public List<LoanDeviation> findByBatch(String batch) {
        // TODO Auto-generated method stub
        return loanDeviationDao.findByBatchAndIsDeletedFalse(batch);
    }

    @Override
    @Cacheable(value = "hwk.deviation.findIsDeletedDeviationList", unless = "#result == null")
    public List<LoanDeviation> findIsDeletedDeviationList() {
        // TODO Auto-generated method stub
        return loanDeviationDao.findByIsDeleted(true);
    }

    @Override
    @Cacheable(value = "hwk.deviation.findAllDeviationIsNotApproved", unless = "#result == null")
    public List<LoanDeviation> findAllDeviationIsNotApproved() {
        // TODO Auto-generated method stub
        return loanDeviationDao.findByStatusNot(WebGuiConstant.STATUS_APPROVED);
    }

    @Override
    @Cacheable(value = "hwk.deviation.findDeviationByMs", unless = "#result == null")
    public List<LoanDeviation> findDeviationByMs(Location locationId) {
        return loanDeviationDao.findDeviationByMs(locationId);
    }

    @Override
    @Cacheable(value = "hwk.deviation.findDeviationByUserNonMs", unless = "#result == null")
    public List<LoanDeviation> findDeviationByUserNonMs(Long officer) {
        return loanDeviationDao.findByUserApprovalNonMS(officer, WebGuiConstant.STATUS_WAITING);
    }

    @Override
    @Cacheable(value = "hwk.deviation.findDeviationByAp3rId", unless = "#result == null")
    public LoanDeviation findDeviationByAp3rId(Long ap3rId) {
        // TODO Auto-generated method stub
        return loanDeviationDao.findByAp3rIdAndIsDeletedFalse(ap3rId);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "hwk.deviation.findDeviationByLoanId", key = "#loanDeviation.loanId", beforeInvocation = true),
            @CacheEvict(value = "hwk.deviation.findByAp3rId", key = "#loanDeviation.ap3rId", beforeInvocation = true),
            @CacheEvict(value = "hwk.deviation.findById", key = "#loanDeviation.deviationId", beforeInvocation = true),
            @CacheEvict(value = "hwk.deviation.findAll", key = "#loanDeviation.deviationId", beforeInvocation = true),
            @CacheEvict(value = "hwk.deviation.findByBatch", key = "#loanDeviation.batch", beforeInvocation = true),
            @CacheEvict(value = "hwk.deviation.findIsDeletedDeviationList", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.deviation.findAllDeviationIsNotApproved", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.deviation.findDeviationByMs", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.deviation.findDeviationByUserNonMs", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.deviation.findDeviationByAp3rId", key = "#loanDeviation.ap3rId", beforeInvocation = true),
            @CacheEvict(value = "wln.deviation.findDeviationByAp3rId", allEntries = true, beforeInvocation = true)})
    public void save(LoanDeviation loanDeviation) {
        loanDeviationDao.save(loanDeviation);
    }

    @Override
    @Cacheable(value = "hwk.deviation.photo.findByDeviationId", unless = "#result == null")
    public DeviationPhoto findByDeviationId(Long deviationId) {
        return deviationPhotoDao.findByDeviationId(deviationId);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "hwk.deviation.photo.findByDeviationId", allEntries = true, beforeInvocation = true)})
    public void save(DeviationPhoto deviationPhoto) {
        deviationPhotoDao.save(deviationPhoto);
    }

    @Override
    @Cacheable(value = "hwk.deviation.history.findByDeviationAndLevel", unless = "#result == null")
    public DeviationOfficerMapping findByDeviationAndLevel(Long deviationId, String level) {
        return deviationOfficerMapDao.findByDeviationIdAndLevel(deviationId, level);
    }

    @Override
    @Cacheable(value = "hwk.deviation.history.findTopByDeviationId", unless = "#result == null")
    public DeviationOfficerMapping findTopByDeviationId(Long deviationId) {
        return deviationOfficerMapDao.findTopByDeviationIdOrderByUpdatedDateAsc(deviationId);
    }

    @Override
    @Cacheable(value = "hwk.deviation.history.findTopByDeviationIdAndStatus", unless = "#result == null")
    public DeviationOfficerMapping findTopByDeviationIdAndStatus(Long deviationId) {
        return deviationOfficerMapDao.findTopByDeviationIdAndDateNotOrderByUpdatedDateDesc(deviationId, WebGuiConstant.STATUS_WAITING);
    }

    @Override
    @Cacheable(value = "hwk.deviation.history.countDeviationWithStatus", unless = "#result == null")
    public int countDeviationWithStatus(Long id) {
        return deviationOfficerMapDao.countDeviation(id, WebGuiConstant.STATUS_WAITING);
    }

    @Override
    @Cacheable(value = "hwk.deviation.history.countDeviationWaiting", unless = "#result == null")
    public int countDeviationWaiting(Long id) {
        return deviationOfficerMapDao.countDeviationWaiting(id, WebGuiConstant.STATUS_WAITING);
    }

    @Override
    @Cacheable(value = "hwk.deviation.history.findByLoanDeviationId", unless = "#result == null")
    public List<DeviationOfficerMapping> findByLoanDeviationId(String deviationId) {
        return deviationOfficerMapDao.findByDeviationId(Long.parseLong(deviationId));
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "hwk.deviation.history.findByDeviationAndLevel", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.deviation.history.findTopByDeviationId", key = "#deviationOfficerMapping.deviationId", beforeInvocation = true),
            @CacheEvict(value = "hwk.deviation.history.findTopByDeviationIdAndStatus", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.deviation.history.countDeviationWaiting", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.deviation.history.findByLoanDeviationId", key = "#deviationOfficerMapping.deviationId", beforeInvocation = true)})
    public void save(DeviationOfficerMapping deviationOfficerMapping) {
        deviationOfficerMapDao.save(deviationOfficerMapping);
    }

    @Override
    @Cacheable(value = "hwk.deviation.code.findDeviationCode", unless = "#result == null")
    public List<DeviationCode> findDeviationCode() {
        return deviationCodeDao.findAll();
    }

    @Override
    @CacheEvict(value = {"hwk.loan.findLoanByCreatedDate", "hwk.loan.findLoanByCreatedDatePageable",
            "hwk.loan.findByLoanId", "hwk.loan.getLoanByRrn", "hwk.loan.fund.findLoanWithEmergencyFundByLoanId",
            "hwk.deviation.findDeviationByLoanId", "hwk.deviation.photo.findByDeviationId", "hwk.loan.getLoanByAp3rId",
            "hwk.loan.isValidLoan", "hwk.loan.findIsDeletedLoanList", "hwk.loan.findByCustomerAndStatus", "hwk.loan.findByAp3rAndCustomer",
            "hwk.deviation.findByAp3rId", "hwk.deviation.findById", "hwk.deviation.findAll",
            "hwk.deviation.findByBatch", "hwk.deviation.findIsDeletedDeviationList", "hwk.deviation.findAllDeviationIsNotApproved",
            "hwk.deviation.findDeviationByMs", "hwk.deviation.findDeviationByUserNonMs", "hwk.deviation.findDeviationByAp3rId",
            "hwk.deviation.history.findByDeviationAndLevel", "hwk.deviation.history.findTopByDeviationId", "hwk.deviation.history.findTopByDeviationIdAndStatus",
            "hwk.deviation.history.countDeviationWaiting", "hwk.deviation.history.findByLoanDeviationId", "hwk.deviation.code.findDeviationCode"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub
    }

    @Override
    @Cacheable(value = "hwk.loan.fund.findLoanWithEmergencyFundByLoanId", unless = "#result == null")
    public LoanWithEmergencyFund findLoanWithEmergencyFundByLoanId(Long loanId) {
        // TODO Auto-generated method stub
        return loanWithEmergencyFundDao.findByLoanId(loanId);
    }

    @Override
    public List<LoanWithRemainingPrincipal> getAllLoanWithRemainingPrincipal(String username) {
        List<LoanWithRemainingPrincipal> loanList = new ArrayList<>();
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            PreparedStatement statementforCount = conn.prepareStatement("SELECT count(a.id) totalE\n" +
                    " FROM t_loan_account a LEFT OUTER JOIN \n" +
                    " t_loan_prs b ON a.id=b.loan_id \n" +
                    " LEFT JOIN t_loan_prs c \n" +
                    " ON b.loan_id=c.loan_id AND b.id<c.id \n" +
                    "INNER JOIN t_customer cust ON cust.id = a.customer_id \n" +
                    "INNER JOIN m_location loc ON loc.id = cust.sentra_location \n" +
                    "INNER JOIN m_users usr ON usr.location_id = loc.id \n" +
                    "WHERE c.id IS NULL AND usr.user_login = ?");
            statementforCount.setString(1, username);
            ResultSet rsCount = statementforCount.executeQuery();
            LoanWithRemainingPrincipal loan2 = new LoanWithRemainingPrincipal();
            while (rsCount.next()) {
                loan2.setTotalElements(rsCount.getInt(1));
            }
            PreparedStatement stmt = conn.prepareStatement(getAllLoanWithRemainingPrincipal);
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                LoanWithRemainingPrincipal loan = new LoanWithRemainingPrincipal();
                loan.setLoanId(rs.getLong("id"));
                loan.setAccountNumber(rs.getString("account_number"));
                loan.setAppId(rs.getString("application_id"));
                //loan.setApprovedBy(rs.getString("approved_by"));
                loan.setCreatedBy(rs.getString("created_by"));
                loan.setCreatedDate(rs.getDate("created_date"));
                loan.setCustomerName(rs.getString("customer_name"));
                loan.setDisbursementDate(rs.getDate("disbursement_date"));
                //loan.setDeleted(rs.getBoolean("is_deleted"));
                //loan.setLatitude(rs.getString("latitude"));
                // loan.setLoanAmountRecommended(rs.getBigDecimal("loan_amt_rec"));
                //loan.setLongitude(rs.getString("longitude"));
                loan.setProsperaId(rs.getString("prospera_id"));
                //loan.setRrn(rs.getString("rrn"));
                loan.setStatus(rs.getString("status"));
                loan.setSwId(rs.getLong("sw_id"));
                loan.setCustomer(rs.getLong("customer_id"));
                loan.setProductId(rs.getLong("product_id"));
                loan.setPlafond(rs.getBigDecimal("plafond"));
                loan.setAp3rId(rs.getLong("ap3r_id"));
                //loan.setFirstInstallmentDate(rs.getDate("first_installment_date"));
                //loan.setLastInstallmentDate(rs.getDate("last_installment_date"));
                //loan.setNote(rs.getString("note"));
                //loan.setUpdatedBy(rs.getString("updated_by"));
                //loan.setUpdatedDate(rs.getDate("updated_dt"));
                //loan.setApprovedDate(rs.getDate("approved_dt"));
                loan.setRemainingPrincipal(rs.getBigDecimal("remaining_principal"));
                loan.setDueDate(rs.getDate("due_date"));
                loan.setCycleNumber(rs.getInt("cycle_number"));
                loan.setTotalElements(loan2.getTotalElements());
                loan.setTotalPages(1);
                loanList.add(loan);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("listLoan error: " + e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (Exception e) {
                log.error("Error : " + e);
            }
        }
        return loanList;
    }

    @Override
    public List<LoanWithRemainingPrincipal> getAllLoanWithRemainingPrincipalPageable(String username, String page, String getCountData) {
        List<LoanWithRemainingPrincipal> loanList = new ArrayList<>();
        Connection conn = null;
        try {
            Integer pageInt = Integer.parseInt(page);
            Integer countDataInt = Integer.parseInt(getCountData);
            Integer borderRowFirst = countDataInt * (pageInt - 1) + 1;
            Integer borderRowLast = countDataInt * pageInt;

            conn = dataSource.getConnection();
            PreparedStatement statementforCount = conn.prepareStatement("SELECT count(a.id) totalE\n" +
                    " FROM t_loan_account a LEFT OUTER JOIN \n" +
                    " t_loan_prs b ON a.id=b.loan_id \n" +
                    " LEFT JOIN t_loan_prs c \n" +
                    " ON b.loan_id=c.loan_id AND b.id<c.id \n" +
                    "INNER JOIN t_customer cust ON cust.id = a.customer_id \n" +
                    "INNER JOIN m_location loc ON loc.id = cust.sentra_location \n" +
                    "INNER JOIN m_users usr ON usr.location_id = loc.id \n" +
                    "WHERE c.id IS NULL AND usr.user_login = ?");
            statementforCount.setString(1, username);
            ResultSet rsCount = statementforCount.executeQuery();
            LoanWithRemainingPrincipal loan2 = new LoanWithRemainingPrincipal();
            while (rsCount.next()) {
                int totalE = rsCount.getInt(1);
                log.info("TotalE : " + totalE);
                int totalP = (totalE / countDataInt);
                log.info("totalP : " + totalP);
                loan2.setContent(countDataInt);
                loan2.setTotalElements(rsCount.getInt(1));
                if (countDataInt > totalE) {
                    loan2.setTotalPages(1);
                } else if (totalE % countDataInt == 0) {
                    loan2.setTotalPages(totalP);
                } else {
                    loan2.setTotalPages(totalP + 1);
                }
            }
            PreparedStatement stmt = conn.prepareStatement(getAllLoanWithRemainingPrincipalPageable);
            stmt.setString(1, username);
            stmt.setInt(2, borderRowFirst);
            stmt.setInt(3, borderRowLast);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                LoanWithRemainingPrincipal loan = new LoanWithRemainingPrincipal();
                loan.setLoanId(rs.getLong("id"));
                loan.setAccountNumber(rs.getString("account_number"));
                loan.setAppId(rs.getString("application_id"));
                //loan.setApprovedBy(rs.getString("approved_by"));
                loan.setCreatedBy(rs.getString("created_by"));
                loan.setCreatedDate(rs.getDate("created_date"));
                loan.setCustomerName(rs.getString("customer_name"));
                loan.setDisbursementDate(rs.getDate("disbursement_date"));
                //loan.setDeleted(rs.getBoolean("is_deleted"));
                //loan.setLatitude(rs.getString("latitude"));
                //loan.setLoanAmountRecommended(rs.getBigDecimal("loan_amt_rec"));
                //loan.setLongitude(rs.getString("longitude"));
                loan.setProsperaId(rs.getString("prospera_id"));
                //loan.setRrn(rs.getString("rrn"));
                loan.setStatus(rs.getString("status"));
                loan.setSwId(rs.getLong("sw_id"));
                loan.setCustomer(rs.getLong("customer_id"));
                loan.setProductId(rs.getLong("product_id"));
                loan.setPlafond(rs.getBigDecimal("plafond"));
                loan.setAp3rId(rs.getLong("ap3r_id"));
                //loan.setFirstInstallmentDate(rs.getDate("first_installment_date"));
                //loan.setLastInstallmentDate(rs.getDate("last_installment_date"));
                //loan.setNote(rs.getString("note"));
                //loan.setUpdatedBy(rs.getString("updated_by"));
                //loan.setUpdatedDate(rs.getDate("updated_dt"));
                //loan.setApprovedDate(rs.getDate("approved_dt"));
                loan.setRemainingPrincipal(rs.getBigDecimal("remaining_principal"));
                loan.setDueDate(rs.getDate("due_date"));
                loan.setCycleNumber(rs.getInt("cycle_number"));
                loan.setTotalElements(loan2.getTotalElements());
                loan.setTotalPages(loan2.getTotalPages());
                loanList.add(loan);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("listLoan error: " + e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (Exception e) {
                log.error("Error : " + e);
            }
        }
        return loanList;
    }

    @Override
    public List<LoanWithRemainingPrincipal> findAllLoanWithRemainingPrincipalByCreatedDate(String username, Date startDate, Date endDate) {
        List<LoanWithRemainingPrincipal> loanList = new ArrayList<>();
        Connection conn = null;
        try {
            java.sql.Date startDateSQL = new java.sql.Date(startDate.getTime());
            java.sql.Date endDateSQL = new java.sql.Date(endDate.getTime());

            conn = dataSource.getConnection();
            PreparedStatement statementforCount = conn.prepareStatement("SELECT count(a.id) totalE\n" +
                    " FROM t_loan_account a LEFT OUTER JOIN \n" +
                    " t_loan_prs b ON a.id=b.loan_id \n" +
                    " LEFT JOIN t_loan_prs c \n" +
                    " ON b.loan_id=c.loan_id AND b.id<c.id \n" +
                    "INNER JOIN t_customer cust ON cust.id = a.customer_id \n" +
                    "INNER JOIN m_location loc ON loc.id = cust.sentra_location \n" +
                    "INNER JOIN m_users usr ON usr.location_id = loc.id\n" +
                    "WHERE c.id IS NULL AND usr.user_login = ? AND a.created_date>= ? AND a.created_date< ? AND a.is_deleted = 0");
            statementforCount.setString(1, username);
            statementforCount.setDate(2, startDateSQL);
            statementforCount.setDate(3, endDateSQL);
            ResultSet rsCount = statementforCount.executeQuery();
            LoanWithRemainingPrincipal loan2 = new LoanWithRemainingPrincipal();
            while (rsCount.next()) {
                loan2.setTotalElements(rsCount.getInt(1));
            }
            PreparedStatement stmt = conn.prepareStatement(findAllLoanWithRemainingPrincipalByCreatedDate);
            stmt.setString(1, username);
            stmt.setDate(2, startDateSQL);
            stmt.setDate(3, endDateSQL);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                LoanWithRemainingPrincipal loan = new LoanWithRemainingPrincipal();
                loan.setLoanId(rs.getLong("id"));
                loan.setAccountNumber(rs.getString("account_number"));
                loan.setAppId(rs.getString("application_id"));
                // loan.setApprovedBy(rs.getString("approved_by"));
                loan.setCreatedBy(rs.getString("created_by"));
                loan.setCreatedDate(rs.getDate("created_date"));
                loan.setCustomerName(rs.getString("customer_name"));
                loan.setDisbursementDate(rs.getDate("disbursement_date"));
                //loan.setDeleted(rs.getBoolean("is_deleted"));
                //loan.setLatitude(rs.getString("latitude"));
                //loan.setLoanAmountRecommended(rs.getBigDecimal("loan_amt_rec"));
                //loan.setLongitude(rs.getString("longitude"));
                loan.setProsperaId(rs.getString("prospera_id"));
                //loan.setRrn(rs.getString("rrn"));
                loan.setStatus(rs.getString("status"));
                loan.setSwId(rs.getLong("sw_id"));
                loan.setCustomer(rs.getLong("customer_id"));
                loan.setProductId(rs.getLong("product_id"));
                loan.setPlafond(rs.getBigDecimal("plafond"));
                loan.setAp3rId(rs.getLong("ap3r_id"));
                //loan.setFirstInstallmentDate(rs.getDate("first_installment_date"));
                //loan.setLastInstallmentDate(rs.getDate("last_installment_date"));
                //loan.setNote(rs.getString("note"));
                //loan.setUpdatedBy(rs.getString("updated_by"));
                //loan.setUpdatedDate(rs.getDate("updated_dt"));
                //loan.setApprovedDate(rs.getDate("approved_dt"));
                loan.setRemainingPrincipal(rs.getBigDecimal("remaining_principal"));
                loan.setDueDate(rs.getDate("due_date"));
                loan.setCycleNumber(rs.getInt("cycle_number"));
                loan.setTotalElements(loan2.getTotalElements());
                loan.setTotalPages(loan2.getTotalPages());
                loanList.add(loan);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("listLoan error: " + e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (Exception e) {
                log.error("Error : " + e);
            }
        }
        return loanList;
    }

    @Override
    public List<LoanWithRemainingPrincipal> findAllLoanWithRemainingPrincipalByCreatedDatePageable(String username, String page, String getCountData, Date startDate, Date endDate) {
        List<LoanWithRemainingPrincipal> loanList = new ArrayList<>();
        Connection conn = null;
        try {
            java.sql.Date startDateSQL = new java.sql.Date(startDate.getTime());
            java.sql.Date endDateSQL = new java.sql.Date(endDate.getTime());
            Integer pageInt = Integer.parseInt(page);
            Integer countDataInt = Integer.parseInt(getCountData);
            Integer borderRowFirst = countDataInt * (pageInt - 1) + 1;
            Integer borderRowLast = countDataInt * pageInt;
            conn = dataSource.getConnection();

            PreparedStatement statementforCount = conn.prepareStatement("SELECT count(a.id) totalE\n" +
                    " FROM t_loan_account a LEFT OUTER JOIN \n" +
                    " t_loan_prs b ON a.id=b.loan_id \n" +
                    " LEFT JOIN t_loan_prs c \n" +
                    " ON b.loan_id=c.loan_id AND b.id<c.id \n" +
                    "INNER JOIN t_customer cust ON cust.id = a.customer_id \n" +
                    "INNER JOIN m_location loc ON loc.id = cust.sentra_location \n" +
                    "INNER JOIN m_users usr ON usr.location_id = loc.id \n" +
                    "WHERE c.id IS NULL AND usr.user_login = ? AND a.created_date>= ? AND a.created_date< ? AND a.is_deleted = 0 \n");
            statementforCount.setString(1, username);
            statementforCount.setDate(2, startDateSQL);
            statementforCount.setDate(3, endDateSQL);
            ResultSet rsCount = statementforCount.executeQuery();
            LoanWithRemainingPrincipal loan2 = new LoanWithRemainingPrincipal();
            while (rsCount.next()) {
                int totalE = rsCount.getInt(1);
                log.info("TotalE : " + totalE);
                int totalP = (totalE / countDataInt);
                log.info("totalP : " + totalP);
                loan2.setContent(countDataInt);
                loan2.setTotalElements(rsCount.getInt(1));
                if (countDataInt > totalE) {
                    loan2.setTotalPages(1);
                } else if (totalE % countDataInt == 0) {
                    loan2.setTotalPages(totalP);
                } else {
                    loan2.setTotalPages(totalP + 1);
                }
            }
            PreparedStatement stmt = conn.prepareStatement(findAllLoanWithRemainingPrincipalByCreatedDatePageable);
            stmt.setString(1, username);
            stmt.setDate(2, startDateSQL);
            stmt.setDate(3, endDateSQL);
            stmt.setInt(4, borderRowFirst);
            stmt.setInt(5, borderRowLast);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                LoanWithRemainingPrincipal loan = new LoanWithRemainingPrincipal();
                loan.setLoanId(rs.getLong("id"));
                loan.setAccountNumber(rs.getString("account_number"));
                loan.setAppId(rs.getString("application_id"));
                //loan.setApprovedBy(rs.getString("approved_by"));
                loan.setCreatedBy(rs.getString("created_by"));
                loan.setCreatedDate(rs.getDate("created_date"));
                loan.setCustomerName(rs.getString("customer_name"));
                loan.setDisbursementDate(rs.getDate("disbursement_date"));
                //loan.setDeleted(rs.getBoolean("is_deleted"));
                //loan.setLatitude(rs.getString("latitude"));
                //loan.setLoanAmountRecommended(rs.getBigDecimal("loan_amt_rec"));
                //loan.setLongitude(rs.getString("longitude"));
                loan.setProsperaId(rs.getString("prospera_id"));
                //loan.setRrn(rs.getString("rrn"));
                loan.setStatus(rs.getString("status"));
                loan.setSwId(rs.getLong("sw_id"));
                loan.setCustomer(rs.getLong("customer_id"));
                loan.setProductId(rs.getLong("product_id"));
                loan.setPlafond(rs.getBigDecimal("plafond"));
                loan.setAp3rId(rs.getLong("ap3r_id"));
                //loan.setFirstInstallmentDate(rs.getDate("first_installment_date"));
                //loan.setLastInstallmentDate(rs.getDate("last_installment_date"));
                //loan.setNote(rs.getString("note"));
                //loan.setUpdatedBy(rs.getString("updated_by"));
                //loan.setUpdatedDate(rs.getDate("updated_dt"));
                //loan.setApprovedDate(rs.getDate("approved_dt"));
                loan.setRemainingPrincipal(rs.getBigDecimal("remaining_principal"));
                loan.setDueDate(rs.getDate("due_date"));
                loan.setCycleNumber(rs.getInt("cycle_number"));
                loan.setTotalElements(loan2.getTotalElements());
                loan.setTotalPages(loan2.getTotalPages());
                loanList.add(loan);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("listLoan error: " + e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (Exception e) {
                log.error("Error : " + e);
            }
        }
        return loanList;
    }

    private String getAllLoanWithRemainingPrincipal = "EXEC getAllLoanWithRemainingPrincipal @username=?";


    private String getAllLoanWithRemainingPrincipalPageable = "EXEC getAllLoanWithRemainingPrincipalPageable @username=?,@startRow=?,@endRow=?";


    private String findAllLoanWithRemainingPrincipalByCreatedDate = "EXEC findAllLoanWithRemainingPrincipalByCreatedDate @username=?,@startDateSQL=?,@endDateSQL=?";


    private String findAllLoanWithRemainingPrincipalByCreatedDatePageable = "EXEC findAllLoanWithRemainingPrincipalByCreatedDatePageable @username=?,@startDateSQL=?,@endDateSQL=?,@borderRowFirst=?,@borderRowLast=?";

}