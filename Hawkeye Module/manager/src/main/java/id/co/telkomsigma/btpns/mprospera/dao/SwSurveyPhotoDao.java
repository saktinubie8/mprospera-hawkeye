package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.sw.SwSurveyPhoto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SwSurveyPhotoDao extends JpaRepository<SwSurveyPhoto, Long> {

    @Query("Select o FROM SwSurveyPhoto o where o.swId.swId = :swId")
    SwSurveyPhoto getSwSurveyPhoto(@Param("swId") Long swId);

}