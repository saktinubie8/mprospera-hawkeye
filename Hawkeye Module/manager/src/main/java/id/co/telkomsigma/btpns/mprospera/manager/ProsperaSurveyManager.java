package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.survey.ProsperaSurveyQuestion;
import id.co.telkomsigma.btpns.mprospera.model.survey.ProsperaSurveyType;

import java.util.List;

public interface ProsperaSurveyManager {

    ProsperaSurveyType findByProsperaSurveyTypeId(long id);

    List<ProsperaSurveyType> findAllSurveyType();

    void clearCache();

    List<ProsperaSurveyQuestion> findProsperaSurveyQuestionByType(ProsperaSurveyType type);

}