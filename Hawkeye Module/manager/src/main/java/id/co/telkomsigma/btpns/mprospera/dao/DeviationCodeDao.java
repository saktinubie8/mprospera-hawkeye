package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.loan.DeviationCode;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Dzulfiqar on 6/05/2017.
 */
public interface DeviationCodeDao extends JpaRepository<DeviationCode, Long> {
}