package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SWDao extends JpaRepository<SurveyWawancara, String> {

    @Query("SELECT m FROM SurveyWawancara m WHERE m.isDeleted = false AND m.localId is not null ORDER BY m.createdBy ASC")
    Page<SurveyWawancara> findAll(Pageable pageable);

    @Query("SELECT m FROM SurveyWawancara m WHERE m.createdDate>=:startDate AND m.createdDate<:endDate AND m.isDeleted = false AND areaId in :areaList AND m.localId is not null ORDER BY m.createdBy ASC")
    Page<SurveyWawancara> findByCreatedDate(@Param("areaList") List<String> kelIdList,
                                            @Param("startDate") Date startDate, @Param("endDate") Date endDate, Pageable pageable);

    @Query("SELECT COUNT(m) FROM SurveyWawancara m WHERE m.swId = :swId AND m.isDeleted = false AND m.localId is not null")
    Integer countBySwId(@Param("swId") Long swId);

    @Query("SELECT a FROM SurveyWawancara a WHERE a.swId  = :swId AND a.isDeleted = false AND a.localId is not null")
    SurveyWawancara findSwBySwId(@Param("swId") Long swId);

    SurveyWawancara findSwByLocalIdAndIsDeletedFalse(String localId);

    @Query("select sw from SurveyWawancara sw where sw.createdBy in :userIdList and sw.localId is not null and sw.isDeleted = false AND sw.createdDate>=:startDate AND sw.createdDate<:endDate")
    List<SurveyWawancara> findByCreatedByInAndLocalIdIsNotNullAndIsDeletedFalseAndCreatedDate(@Param("userIdList") List<String> userIdList, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

    List<SurveyWawancara> findByCreatedByInAndLocalIdIsNotNullAndIsDeletedFalse(List<String> userIdList);

    @Query("select s from SurveyWawancara s , AP3R a " +
            " , LoanDeviation ld " +
            " , DeviationOfficerMapping dom " +
            " where a.swId = s.swId and ld.ap3rId = a.ap3rId" +
            " and dom.deviationId = ld.deviationId and dom.officer = :userNonMs and s.isDeleted = false")
    List<SurveyWawancara> findSwIdInLoanDeviation(@Param("userNonMs") Long userNonMs);

    @Query("select s from SurveyWawancara s , AP3R a " +
            " , LoanDeviation ld " +
            " , DeviationOfficerMapping dom " +
            " where a.swId = s.swId and ld.ap3rId = a.ap3rId" +
            " and dom.deviationId = ld.deviationId and ld.batch = :batch and dom.officer = :userNonMs and s.isDeleted = false")
    List<SurveyWawancara> findSwIdInLoanDeviationWithBatch(@Param("userNonMs") Long userNonMs, @Param("batch") String batch);

    @Query("SELECT m FROM SurveyWawancara m WHERE m.createdDate>=:startDate AND m.createdDate<:endDate and m.isDeleted = true")
    List<SurveyWawancara> findIsDeletedSwId(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

}