package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.SWManager;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.*;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Service("swService")
public class SWService extends GenericService {

    @Autowired
    @Qualifier("swManager")
    private SWManager swManager;

    @Autowired
    private UserManager userManager;

    private SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

    public List<SurveyWawancara> getSWForMS(String username, String startDate,
                                            String endDate) throws ParseException {
        User userSW = userManager.getUserByUsername(username);
        List<User> userListWisma = userManager.getUserByLocId(userSW.getOfficeCode());
        List<String> userIdList = new ArrayList<>();
        for (User user : userListWisma) {
            userIdList.add(user.getUsername());
        }
        if ((startDate == null || startDate.equals("")) && endDate != null)
            startDate = endDate;
        if (endDate == null || endDate.equals("") && startDate != null)
            endDate = startDate;
        if (startDate == null || startDate.equals(""))
            return swManager.findByUserMS(userIdList);
        else
            return swManager.findByUserMSWithDate(userIdList, formatter.parse(startDate), formatter.parse(endDate));
    }

    public BusinessTypeAP3R getBusinessTypeAP3RById(Long businessId) {
        return swManager.findBusinessTypeAP3RById(businessId);
    }
    
    public BusinessTypeAP3R findBySubBiCode(String subBiCode) {
    	return swManager.findBySubBiCode(subBiCode);
    }

    public Boolean isValidSw(String swId) {
        return swManager.isValidSw(swId);
    }

    public void save(SurveyWawancara surveyWawancara) {
        swManager.save(surveyWawancara);
    }

    public SwIdPhoto getSwIdPhoto(String swId) {
        return swManager.getSwIdPhoto(swId);
    }

    public SwSurveyPhoto getSwSurveyPhoto(String swId) {
        return swManager.getSwSurveyPhoto(swId);
    }

    public SurveyWawancara getSWById(String swId) {
        return swManager.getSWById(swId);
    }

    public LoanProduct findByProductId(String productId) {
        return swManager.findByProductId(productId);
    }

    public SWProductMapping findProductMapById(Long id) {
        return swManager.findProductMapById(id);
    }

    public List<DirectBuyThings> findProductBySwId(SurveyWawancara sw) {
        return swManager.findProductBySwId(sw);
    }

    public List<AWGMBuyThings> findAwgmProductBySwId(SurveyWawancara sw) {
        return swManager.findAwgmProductBySwId(sw);
    }

    public List<NeighborRecommendation> findNeighborBySwId(SurveyWawancara sw) {
        return swManager.findNeighborBySwId(sw);
    }

    public List<SurveyWawancara> findSwIdInLoanDeviation(String userName) {
        User userNonMs = userManager.getUserByUsername(userName);
        return swManager.findSwIdInLoanDeviation(userNonMs.getUserId());
    }

    public List<SurveyWawancara> findSwIdInLoanDeviationWithBatch(String userName, String batch) {
        User userNonMs = userManager.getUserByUsername(userName);
        return swManager.findSwIdInLoanDeviationWithBatch(userNonMs.getUserId(), batch);
    }

    public List<SWProductMapping> findProductMapBySwId(Long swId) {
        return swManager.findProductMapBySwId(swId);
    }

    public List<OtherItemCalculation> findOtherItemCalcBySwId(Long swId) {
        return swManager.findOtherItemCalcBySwId(swId);
    }

    public List<SWUserBWMPMapping> findBySw(Long sw) {
        return swManager.findBySw(sw);
    }

    public SurveyWawancara findSwByLocalId(String localId) {
        return swManager.findByLocalId(localId);
    }

    public List<SurveyWawancara> findIsDeletedSwList() {
        return swManager.findIsDeletedSwList();
    }

}