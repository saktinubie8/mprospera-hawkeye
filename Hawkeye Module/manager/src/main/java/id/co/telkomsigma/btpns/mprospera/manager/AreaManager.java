package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.sda.*;

public interface AreaManager {

    AreaSubDistrict findSubDistrictById(String areaId);

    AreaKelurahan findKelurahanById(String areaId);

    AreaKelurahan findById(String id);

    void clearCache();

    AreaDistrict findDistrictById(String districtCode);

    AreaSubDistrict findKecamatanById(String id);

    AreaProvince findProvinceById(String id);

    AreaKelurahan findKelurahanByName(String areaName);

    AreaKelurahan findKelurahanByNameAndParent(String areaName, String parentAreaId);

    AreaSubDistrict findKecamatanByName(String areaName);

    AreaSubDistrict findKecamatanByNameAndParent(String areaName, String parentId);

    AreaDistrict findKotaByName(String areaName);

    AreaProvince findProvinceByName(String areaName);

    AreaDistrict findKotaById(String id);

    AreaDistrict findByAreaName(String name);

}