package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

import javax.persistence.QueryHint;

public interface LoanDao extends JpaRepository<Loan, Long> {

    @Query("Select count(a) from Loan a where a.loanId = :loanId and a.isDeleted = false")
    Integer countByLoanId(@Param("loanId") Long loanId);

    @Query("SELECT s FROM Loan s INNER JOIN s.customer c " + "INNER JOIN s.customer.sentraLocation l "
            + " WHERE s.createdDate>=:startDate AND s.createdDate<:endDate AND l.locationId = :officeId AND s.isDeleted = false ORDER BY s.createdDate")
    @QueryHints(@QueryHint(name = "org.hibernate.fetchSize" , value = "100"))
    Page<Loan> findByCreatedDate(@Param("startDate") Date startDate, @Param("endDate") Date endDate,
                                 Pageable pageRequest, @Param("officeId") String officeId);

    Loan findByRrnAndIsDeletedFalse(String rrn);

    Loan findByAp3rIdAndIsDeletedFalse(Long ap3rId);

    @Query("SELECT l FROM Loan l WHERE l.createdDate>=:startDate AND l.createdDate<:endDate and l.isDeleted = true")
    @QueryHints(@QueryHint(name = "org.hibernate.fetchSize" , value = "100"))
    List<Loan> findIsDeletedLoanId(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

    List<Loan> findByCustomerAndStatus(Customer customer, String status);

    Loan findByAp3rIdAndCustomerAndAp3rIdIsNotNullAndIsDeletedFalseAndStatusIn
            (Long ap3rId, Customer customer, List<String> statusList);

}