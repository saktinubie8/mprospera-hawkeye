package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.MessageLogsDao;
import id.co.telkomsigma.btpns.mprospera.manager.MessageLogsManager;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("messageLogsManager")
public class MessageLogsManagerImpl implements MessageLogsManager {

    @Autowired
    MessageLogsDao messageLogsDao;

    @Override
    public void insert(MessageLogs messageLogs) {
        messageLogsDao.save(messageLogs);
    }

}