package id.co.telkomsigma.btpns.mprospera.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.manager.FreedayManager;

@Service("freedayService")
public class FreedayService extends GenericService {
	@Autowired
	private FreedayManager freedayManager;
	
	  public int countHoliday(String tglAwal, String tglAkhir)  throws ParseException{
		    SimpleDateFormat formatterDate = new SimpleDateFormat("yyyy-MM-dd");
		    Integer count = freedayManager.countHoliday(formatterDate.parse(tglAwal),formatterDate.parse(tglAkhir));
		    return count;
	    }
}
