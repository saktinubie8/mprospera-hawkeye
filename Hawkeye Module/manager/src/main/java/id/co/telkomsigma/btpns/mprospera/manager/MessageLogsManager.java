package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;

public interface MessageLogsManager {

    void insert(MessageLogs messageLogs);

}