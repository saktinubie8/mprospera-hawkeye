package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.survey.ProsperaQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProsperaQuestionDao extends JpaRepository<ProsperaQuestion, Long> {

}