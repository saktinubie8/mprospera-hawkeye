package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.sw.SwIdPhoto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SwIdPhotoDao extends JpaRepository<SwIdPhoto, Long> {

    @Query("Select o FROM SwIdPhoto o where o.swId.swId = :swId")
    SwIdPhoto getSwIdPhoto(@Param("swId") Long swId);

}