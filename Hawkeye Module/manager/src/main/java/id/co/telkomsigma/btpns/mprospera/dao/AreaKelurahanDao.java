package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.sda.AreaKelurahan;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AreaKelurahanDao extends JpaRepository<AreaKelurahan, String> {

    @Query("SELECT a FROM AreaKelurahan a WHERE a.areaId  = :areaId ")
    AreaKelurahan findKelurahanById(@Param("areaId") String areaId);

    @Cacheable("findBy1")
    AreaKelurahan findTopByAreaNameLikeIgnoreCaseAndParentAreaId(String areaName, String areaId);

    @Cacheable("findBy2")
    AreaKelurahan findTopByAreaNameLikeIgnoreCase(String areaName);

}