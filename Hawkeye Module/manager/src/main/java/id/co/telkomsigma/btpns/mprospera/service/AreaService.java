package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.AreaManager;
import id.co.telkomsigma.btpns.mprospera.manager.LocationManager;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaDistrict;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaKelurahan;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaProvince;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaSubDistrict;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("areaService")
public class AreaService extends GenericService {

    @Autowired
    private AreaManager areaManager;

    @Autowired
    private LocationManager locationManager;

    public AreaKelurahan getAreaKelurahanByAreaId(String areaId) {
        return areaManager.findById(areaId);
    }

    public String findLocationCodeByLocationId(String id) {
        Location loc = locationManager.findById(id);
        if (loc != null)
            return loc.getLocationCode();
        return id;
    }

    public Location findLocationById(String id) {
        return locationManager.findByLocationId(id);
    }

    public AreaDistrict findById(String id) {
        return areaManager.findDistrictById(id);
    }

    public AreaSubDistrict findKecamatanById(String id) {
        return areaManager.findKecamatanById(id);
    }

    public AreaProvince findProvById(String id) {
        return areaManager.findProvinceById(id);
    }

    public AreaKelurahan findKelurahanByAreaName(String name) {
        return areaManager.findKelurahanByName(name);
    }

    public AreaKelurahan findKelurahanByAreaNameAndParent(String name, String parentId) {
        return areaManager.findKelurahanByNameAndParent(name, parentId);
    }

    public AreaSubDistrict findKecamatanByAreaName(String name) {
        return areaManager.findKecamatanByName(name);
    }

    public AreaSubDistrict findKecamatanByAreaNameAndParent(String name, String parentId) {
        return areaManager.findKecamatanByNameAndParent(name, parentId);
    }

    public AreaDistrict findKotaByAreaName(String name) {
        return areaManager.findKotaByName(name);
    }

    public AreaDistrict findKotaByAreaNameAndParent(String name, String parentId) {
        return areaManager.findKotaByName(name);
    }

    public AreaProvince findProvinceByAreaName(String name) {
        return areaManager.findProvinceByName(name);
    }

    public AreaDistrict findKotaById(String id) {
        return areaManager.findKotaById(id);
    }

    public AreaDistrict findByAreaName(String name) {
        return areaManager.findByAreaName(name);
    }

    public AreaProvince findProvinceById(String areaId) {
        return areaManager.findProvinceById(areaId);
    }

    public AreaDistrict findDistrictById(String areaId) {
        return areaManager.findDistrictById(areaId);
    }

    public AreaSubDistrict findSubDistrictById(String areaId) {
        return areaManager.findSubDistrictById(areaId);
    }

    public AreaKelurahan findKelurahanById(String areaId) {
        return areaManager.findKelurahanById(areaId);
    }

}