package id.co.telkomsigma.btpns.mprospera.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import id.co.telkomsigma.btpns.mprospera.model.sw.AP3R;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AP3RDao extends JpaRepository<AP3R, Long> {

    @Query("select a from AP3R a , LoanDeviation ld " +
            " , DeviationOfficerMapping dom " +
            " where ld.ap3rId = a.ap3rId and dom.deviationId = ld.deviationId " +
            " and dom.officer = :userNonMs and a.isDeleted = false and ld.isDeleted = false")
    List<AP3R> findByAp3rIdInLoanDeviation(@Param("userNonMs") Long userNonMs);

    @Query("select a from AP3R a where a.ap3rId " +
            "in (select ap3rId from LoanDeviation l where l.isDeleted = false) and a.isDeleted = false and a.createdBy in :userIdList")
    List<AP3R> findByAp3rIdInLoanDeviationAndWisma(@Param("userIdList") List<String> userIdList);

    @Query("select a from AP3R a , LoanDeviation ld " +
            " , DeviationOfficerMapping dom " +
            " where ld.ap3rId = a.ap3rId and dom.deviationId = ld.deviationId " +
            " and dom.officer = :userNonMs and a.isDeleted = false and ld.isDeleted = false and ld.batch = :batch")
    List<AP3R> findByAp3rIdInLoanDeviationWithBatch(@Param("userNonMs") Long userNonMs, @Param("batch") String batch);

    @Query("select a from AP3R a where a.ap3rId " +
            "in (select ap3rId from LoanDeviation l where l.isDeleted = false and l.batch = :batch) and a.isDeleted = false and a.createdBy in :userIdList")
    List<AP3R> findByAp3rIdInLoanDeviationAndWismaWithBatch(@Param("userIdList") List<String> userIdList, @Param("batch") String batch);

    AP3R findTopBySwIdAndLocalIdIsNotNullAndIsDeletedFalse(Long sw);

    AP3R findByAp3rIdAndLocalIdIsNotNullAndIsDeletedFalse(Long id);

    AP3R findByLocalIdAndIsDeletedFalse(String localId);

    List<AP3R> findBySwIdAndIsDeleted(Long sw, Boolean deleted);

}