package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import id.co.telkomsigma.btpns.mprospera.model.sw.NeighborRecommendation;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;

public interface NeighborDao extends JpaRepository<NeighborRecommendation, Long> {

    List<NeighborRecommendation> findBySwId(SurveyWawancara sw);

}