package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.MessageLogsManager;
import id.co.telkomsigma.btpns.mprospera.manager.TerminalActivityManager;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Dzulfiqar on 11/12/15.
 */
@Service("terminalActivityService")
public class TerminalActivityService extends GenericService {

    @Autowired
    private TerminalActivityManager terminalActivityManager;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private HazelcastClientService hazelcastService;

    @Autowired
    private MessageLogsManager messageLogsManager;

    public void saveTerminalActivityAndMessageLogs(final TerminalActivity terminalActivity, final List<MessageLogs> messageLogs) {
        if (terminalActivity.getTerminalActivityId() == null)
            terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
        terminalActivityManager.insertTerminalActivity(terminalActivity);
    }

    public void insertRrn(String map, String key) {
        hazelcastService.insertKeyValueToMap(map, key, new Integer(1));
    }

}