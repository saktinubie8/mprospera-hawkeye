package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.Date;

public interface FreedayManager {
	
    int countHoliday(Date tglAwal, Date tglAkhir);

}
