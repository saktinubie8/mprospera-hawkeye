package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import id.co.telkomsigma.btpns.mprospera.model.sw.AP3R;
import id.co.telkomsigma.btpns.mprospera.model.sw.FundedThings;

public interface FundedThingsDao extends JpaRepository<FundedThings, Long> {

    List<FundedThings> findByAp3rId(AP3R ap3r);

}