package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;

public interface LoanProductDao extends JpaRepository<LoanProduct, Long> {

    LoanProduct findByProductId(Long productId);

    @Query("SELECT p FROM LoanProduct p")
    Page<LoanProduct> findAll(Pageable pageable);
    
    @Query("SELECT p.productId FROM LoanProduct p WHERE p.cycleNumber=:cycleNumber")
    List<Long> findLoanProductByCycleNumber(@Param("cycleNumber") int cycleNumber);

}