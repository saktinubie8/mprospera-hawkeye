package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.dao.AP3RDao;
import id.co.telkomsigma.btpns.mprospera.dao.FundedThingsDao;
import id.co.telkomsigma.btpns.mprospera.manager.AP3RManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.AP3R;
import id.co.telkomsigma.btpns.mprospera.model.sw.FundedThings;

@Service("ap3rManager")
public class AP3RManagerImpl implements AP3RManager {

    @Autowired
    private FundedThingsDao fundedThingsDao;

    @Autowired
    private AP3RDao ap3rDao;

    @Override
    @Cacheable(value = "hwk.ap3r.findTopBySwId", unless = "#result == null")
    public AP3R findTopBySwId(Long sw) {
        // TODO Auto-generated method stub
        return ap3rDao.findTopBySwIdAndLocalIdIsNotNullAndIsDeletedFalse(sw);
    }

    @Override
    @Cacheable(value = "hwk.things.findByAp3rId", unless = "#result == null")
    public List<FundedThings> findByAp3rId(AP3R ap3r) {
        // TODO Auto-generated method stub
        return fundedThingsDao.findByAp3rId(ap3r);
    }

    @Override
    @Cacheable(value = "hwk.ap3r.findAp3rById", unless = "#result == null")
    public AP3R findById(Long id) {
        // TODO Auto-generated method stub
        return ap3rDao.findByAp3rIdAndLocalIdIsNotNullAndIsDeletedFalse(id);
    }

    @Override
    public BigDecimal sumFunds(AP3R ap3r) {
        BigDecimal total = new BigDecimal(0);
        for (FundedThings fundedThing : fundedThingsDao.findByAp3rId(ap3r)) {
            total = total.add(fundedThing.getPrice());
        }
        return total;
    }

    @Override
    @Cacheable(value = "hwk.ap3r.findAp3rListBySwId", unless = "#result == null")
    public List<AP3R> findAp3rListBySwId(Long swId) {
        return ap3rDao.findBySwIdAndIsDeleted(swId, false);
    }

    @Override
    @Cacheable(value = "hwk.ap3r.findByAp3rIdInLoanDeviation", unless = "#result == null")
    public List<AP3R> findByAp3rIdInLoanDeviation(Long userId) {
        // TODO Auto-generated method stub
        return ap3rDao.findByAp3rIdInLoanDeviation(userId);
    }

    @Override
    @Cacheable(value = "hwk.ap3r.findByAp3rIdInLoanDeviationAndWisma", unless = "#result == null")
    public List<AP3R> findByAp3rIdInLoanDeviationAndWisma(List<String> userIdList) {
        // TODO Auto-generated method stub
        return ap3rDao.findByAp3rIdInLoanDeviationAndWisma(userIdList);
    }

    @Override
    @Cacheable(value = "hwk.ap3r.findByAp3rIdInLoanDeviationWithBatch", unless = "#result == null")
    public List<AP3R> findByAp3rIdInLoanDeviationWithBatch(Long userId, String batch) {
        // TODO Auto-generated method stub
        return ap3rDao.findByAp3rIdInLoanDeviationWithBatch(userId, batch);
    }

    @Override
    @Cacheable(value = "hwk.ap3r.findByAp3rIdInLoanDeviationAndWismaWithBatch", unless = "#result == null")
    public List<AP3R> findByAp3rIdInLoanDeviationAndWismaWithBatch(List<String> userIdList, String batch) {
        // TODO Auto-generated method stub
        return ap3rDao.findByAp3rIdInLoanDeviationAndWismaWithBatch(userIdList, batch);
    }
    
    /*
 	 * @author : Ilham
 	 * @Since  : 20191128
 	 * @category patch 
 	 *    - set disbursment_date ap3r disama kan dengan disbursmentdate loan   
 	 */ 
    @Override
	public AP3R updatedisbursmendate(AP3R ap3r) {
		// TODO Auto-generated method stub				
		return ap3rDao.save(ap3r);
	}


    @Override
    @Cacheable(value = "hwk.ap3r.findByLocalId", unless = "#result == null")
    public AP3R findByLocalId(String localId) {
        return ap3rDao.findByLocalIdAndIsDeletedFalse(localId);
    }

    @Override
    @CacheEvict(allEntries = true, cacheNames = {"hwk.ap3r.findTopBySwId", "hwk.things.findByAp3rId", "hwk.ap3r.findAp3rById",
            "hwk.ap3r.findAp3rListBySwId", "hwk.ap3r.findByAp3rIdInLoanDeviation", "hwk.ap3r.findByAp3rIdInLoanDeviationAndWisma",
            "hwk.ap3r.findByAp3rIdInLoanDeviationWithBatch", "hwk.ap3r.findByAp3rIdInLoanDeviationAndWismaWithBatch",
            "hwk.ap3r.findByLocalId"})
    public void clearCache() {

    }

}