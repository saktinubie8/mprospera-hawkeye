package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.Date;

import id.co.telkomsigma.btpns.mprospera.dao.SurveyPhotoDao;
import id.co.telkomsigma.btpns.mprospera.model.survey.SurveyPhoto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.dao.SurveyDetailDao;
import id.co.telkomsigma.btpns.mprospera.dao.SurveyDao;
import id.co.telkomsigma.btpns.mprospera.manager.SurveyManager;
import id.co.telkomsigma.btpns.mprospera.model.survey.Survey;

@Service("surveyManager")
public class SurveyManagerImpl implements SurveyManager {

    @Autowired
    private SurveyDao surveyDao;

    @Autowired
    private SurveyDetailDao surveyDetailDao;

    @Autowired
    private SurveyPhotoDao surveyPhotoDao;

    protected final Log log = LogFactory.getLog(getClass());

    @Override
    @Cacheable(value = "hwk.survey.findBySurveyId", unless = "#result == null")
    public Survey findBySurveyId(Long surveyId) {
        return surveyDao.findBySurveyId(surveyId);
    }

    @Override
    @Cacheable(value = "hwk.survey.getAllSurvey", unless = "#result == null")
    public Page<Survey> getAllSurvey(String officeId) {
        return surveyDao.getAllSurvey(new PageRequest(0, Integer.MAX_VALUE), officeId);
    }

    @Override
    @Cacheable(value = "hwk.survey.getAllSurveyPageable", unless = "#result == null")
    public Page<Survey> getAllSurveyPageable(PageRequest pageRequest, String officeId) {
        Page<Survey> surveyPage = surveyDao.getAllSurvey(pageRequest, officeId);
        return surveyPage;
    }

    @Override
    @Cacheable(value = "hwk.survey.findSurveyByCreatedDate", unless = "#result == null")
    public Page<Survey> findByCreatedDate(Date startDate, Date endDate, String officeId) {
        return surveyDao.findByCreatedDate(startDate, endDate, officeId, new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    @Cacheable(value = "hwk.survey.findSurveyByCreatedDatePageable", unless = "#result == null")
    public Page<Survey> findByCreatedDatePageable(Date startDate, Date endDate, PageRequest pageRequest,
                                                  String officeId) {
        return surveyDao.findByCreatedDate(startDate, endDate, officeId, pageRequest);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "hwk.survey.findBySurveyId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.survey.getAllSurvey", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "hwk.survey.getAllSurveyPageable", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "hwk.survey.findSurveyByCreatedDate", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "hwk.survey.findSurveyByCreatedDatePageable", beforeInvocation = true, allEntries = true)})
    public void save(Survey survey) {
        surveyDao.save(survey);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "hwk.survey.photo.findPhotoBySurveyId", allEntries = true, beforeInvocation = true)})
    public void save(SurveyPhoto surveyPhoto) {
        surveyPhotoDao.save(surveyPhoto);
    }

    @Override
    @Cacheable(value = "hwk.survey.photo.findPhotoBySurveyId", unless = "#result == null")
    public SurveyPhoto findPhotoBySurveyId(Long surveyId) {
        return surveyPhotoDao.findBySurveyId(surveyId);
    }

    @Override
    @CacheEvict(value = {"hwk.survey.findBySurveyId", "hwk.survey.getAllSurvey", "hwk.survey.getAllSurveyPageable",
            "hwk.survey.findSurveyByCreatedDate", "hwk.survey.findSurveyByCreatedDatePageable"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {
    }

}