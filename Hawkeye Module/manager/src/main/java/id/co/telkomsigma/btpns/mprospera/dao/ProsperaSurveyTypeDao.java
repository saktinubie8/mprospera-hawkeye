package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.survey.ProsperaSurveyType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProsperaSurveyTypeDao extends JpaRepository<ProsperaSurveyType, Long> {

}