package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.CustomerManager;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerWithAbsence;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service("syncCustomerService")
public class SyncCustomerService extends GenericService {

    @Autowired
    private CustomerManager customerManager;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

    public Integer countAll(String username) {
        // TODO Auto-generated method stub
        return customerManager.countCustomerByUsername(username);
    }

    public CustomerWithAbsence findByCustomerId(Long customerId) {
        return customerManager.findByCustomerId(customerId);
    }

    public Page<Customer> getCustomerByLoc(String page, String getCountData, String startDate, String endDate,
                                           String loc) throws Exception {

        if (getCountData == null)
            page = null;
        else if (page == null || "".equals(page))
            page = "1";
        if ((startDate == null || startDate.equals("")) && endDate != null)
            startDate = endDate;
        if ((endDate == null || endDate.equals("")) && startDate != null)
            endDate = startDate;
        if (endDate != null && !endDate.equals("")) {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            cal.setTime(formatter.parse(endDate));
            cal.add(Calendar.DATE, 1);
            endDate = formatter.format(cal.getTime());
        }
        if (startDate != null && !startDate.equals("")) {
            Calendar calStart = Calendar.getInstance();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            calStart.setTime(formatter.parse(startDate));
            calStart.add(Calendar.DATE, -7);
            startDate = formatter.format(calStart.getTime());
        }
        if (startDate == null || startDate.equals(""))
            if (page == null || page.equals(""))
                return customerManager.getAllCustomerByLoc(loc);
            else
                return customerManager.getAllCustomerByLoc(loc,
                        new PageRequest(Integer.parseInt(page) - 1, Integer.parseInt(getCountData)));
        else if (page == null || page.equals(""))
            return customerManager.getAllCustomerByLocAndCreatedDate(loc, formatter.parse(startDate),
                    formatter.parse(endDate));
        else
            return customerManager.getAllCustomerByLocAndCreatedDate(loc, formatter.parse(startDate),
                    formatter.parse(endDate),
                    new PageRequest(Integer.parseInt(page) - 1, Integer.parseInt(getCountData)));
    }

    public List<Customer> findIsDeletedCustomerList() {
        return customerManager.findIsDeletedCustomerList();
    }

}