package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.sentra.SentraPhoto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SentraPhotoDao extends JpaRepository<SentraPhoto, Long> {

    @Query("Select o FROM SentraPhoto o where o.sentraId.sentraId = :sentraId")
    SentraPhoto getSentraPhoto(@Param("sentraId") Long sentraId);

}