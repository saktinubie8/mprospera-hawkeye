package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.survey.SurveyPhoto;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Dzulfiqar on 12/05/2017.
 */
public interface SurveyPhotoDao extends JpaRepository<SurveyPhoto, Long> {

    SurveyPhoto findBySurveyId(Long id);

}