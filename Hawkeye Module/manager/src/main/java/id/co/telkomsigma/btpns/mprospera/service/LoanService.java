package id.co.telkomsigma.btpns.mprospera.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.loan.*;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.manager.LoanManager;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;

@Service("loanService")
public class LoanService extends GenericService {

    @Autowired
    private LoanManager loanManager;

    @Autowired
    private UserManager userManager;

    public void save(Loan loan) {
        loanManager.save(loan);
    }

    public void save(LoanDeviation loanDeviation) {
        loanManager.save(loanDeviation);
    }

    public void save(DeviationPhoto deviationPhoto) {
        loanManager.save(deviationPhoto);
    }

    public DeviationPhoto findByDeviationId(Long deviationId) {
        return loanManager.findByDeviationId(deviationId);
    }

    public void save(DeviationOfficerMapping deviationOfficerMapping) {
        loanManager.save(deviationOfficerMapping);
    }

    public LoanDeviation findLoanDeviationByLoanId(Long loanId) {
        return loanManager.findByLoanId(loanId);
    }

    public LoanDeviation findLoanDeviationByAp3rId(Long ap3rId, Boolean isDeleted) {
        return loanManager.findByAp3rId(ap3rId, isDeleted);
    }

    public List<LoanDeviation> findByBatch(String batch) {
        return loanManager.findByBatch(batch);
    }

    public DeviationOfficerMapping findByDeviationAndLevel(Long deviationId, String level) {
        return loanManager.findByDeviationAndLevel(deviationId, level);
    }

    public DeviationOfficerMapping findTopByDeviationId(Long deviationId) {
        return loanManager.findTopByDeviationId(deviationId);
    }

    public DeviationOfficerMapping findTopByDeviationIdAndStatus(Long deviationId) {
        return loanManager.findTopByDeviationIdAndStatus(deviationId);
    }

    public int countDeviationWithStatus(Long id) {
        return loanManager.countDeviationWithStatus(id);
    }

    public int countDeviationWaiting(Long id) {
        return loanManager.countDeviationWaiting(id);
    }

    public LoanDeviation findById(Long deviationId) {
        return loanManager.findById(deviationId);
    }

    public List<DeviationOfficerMapping> findMappingByLoanDeviationId(String deviationId) {
        return loanManager.findByLoanDeviationId(deviationId);
    }

    public Boolean isValidLoan(Long loanId) {
        return loanManager.isValidLoan(loanId);
    }

    public Loan getLoanByRrn(String rrn) {
        return loanManager.getLoanByRrn(rrn);
    }

    public Loan findById(String loanId) {
        if (loanId == null || "".equals(loanId))
            return null;
        return loanManager.findById(Long.parseLong(loanId));
    }

    public Loan getLoanByAp3rId(Long ap3rId) {
        return loanManager.getLoanByAp3rId(ap3rId);
    }

    public LoanWithEmergencyFund findLoanWithEmergencyFundByLoanId(Long loanId) {
        // TODO Auto-generated method stub
        return loanManager.findLoanWithEmergencyFundByLoanId(loanId);
    }

    public List<DeviationCode> findDeviationCode() {
        return loanManager.findDeviationCode();
    }

    public List<LoanDeviation> findIsDeletedDeviationList() {
        return loanManager.findIsDeletedDeviationList();
    }

    public List<Loan> findIsDeletedLoanList() {
        return loanManager.findIsDeletedLoanList();
    }

    public List<Loan> findLoanByCustomerAndStatus(Customer customer, String status) {
        // TODO Auto-generated method stub
        return loanManager.findByCustomerAndStatus(customer, status);
    }

    public List<LoanDeviation> findAllDeviationIsNotApproved() {
        return loanManager.findAllDeviationIsNotApproved();
    }

    public List<LoanDeviation> findDeviationByMs(Location locationId) {
        return loanManager.findDeviationByMs(locationId);
    }

    public LoanDeviation findDeviationByAp3rId(Long ap3rId) {
        return loanManager.findDeviationByAp3rId(ap3rId);
    }

    public Loan findByAp3rAndCustomer(Long ap3rId, Customer customer) {
        return loanManager.findByAp3rAndCustomer(ap3rId, customer);
    }

    public List<LoanDeviation> findByUserNonMs(Long officer) {
        return loanManager.findDeviationByUserNonMs(officer);
    }

    public List<LoanWithRemainingPrincipal> getLoanWithRemainingPrincipalByCreatedDate(String username, String page, String getCountData, String startDate,
                                                                                       String endDate) throws Exception {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        Calendar c = Calendar.getInstance();
        Calendar cStart = Calendar.getInstance();
        if (getCountData == null)
            page = null;
        else if (page == null || "".equals(page))
            page = "1";
        if (endDate == null || "".equals(endDate)) {
            endDate = startDate;
        }
        if (startDate == null || startDate.equals("")) {
            if (page == null || page.equals(""))
                return loanManager.getAllLoanWithRemainingPrincipal(username);
            else
                return loanManager.getAllLoanWithRemainingPrincipalPageable(username, page, getCountData);
        } else {
            c.setTime(formatter.parse(endDate));
            cStart.setTime(formatter.parse(startDate));
            c.add(Calendar.DATE, 1);
            cStart.add(Calendar.DATE, -7);
            endDate = formatter.format(c.getTime());
            startDate = formatter.format(cStart.getTime());
            if (page == null || page.equals(""))
                return loanManager.findAllLoanWithRemainingPrincipalByCreatedDate(username, formatter.parse(startDate), formatter.parse(endDate));
            else
                return loanManager.findAllLoanWithRemainingPrincipalByCreatedDatePageable(username, page, getCountData, formatter.parse(startDate), formatter.parse(endDate));
        }
    }

}