package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;


import id.co.telkomsigma.btpns.mprospera.dao.CustomerAbsenceDao;
import id.co.telkomsigma.btpns.mprospera.dao.CustomerDaoV2;
import id.co.telkomsigma.btpns.mprospera.dao.LoanProductDao;
import id.co.telkomsigma.btpns.mprospera.manager.CustomerManagerV2;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerWithAbsence;
import id.co.telkomsigma.btpns.mprospera.model.customer.CycleNumberAndCustomer;

@Service("customerManagerV2")
public class CustomerManagerImplV2 implements CustomerManagerV2 {

    @Autowired
    private CustomerDaoV2 customerDao;
    
    @Autowired
    private LoanProductDao loanProductDao;

    @Autowired
    private CustomerAbsenceDao customerAbsenceDao;
    
	@PersistenceContext
	private EntityManager em;

    @Override
    @Cacheable(value = "hwk.customer.countCustomerByUsername", unless = "#result == null")
    public Integer countCustomerByUsername(String assignedUsername) {
        return customerDao.countByAssignedUsernameAndIsDeletedFalse(assignedUsername);
    }

    @Override
    @Cacheable(value = "hwk.customer.absence.findCustomerAbsenceByCustomerId", unless = "#result == null")
    public CustomerWithAbsence findByCustomerId(Long customerId) {
        return customerAbsenceDao.findByCustomerId(customerId);
    }

    @Override
    @Cacheable(value = "hwk.customer.isValidCustomer", unless = "#result == null")
    public Boolean isValidCustomer(String customerId) {
        // TODO Auto-generated method stub
        Integer count = customerDao.countByCustomerIdAndIsDeletedFalse(Long.parseLong(customerId));
        if (count > 0)
            return true;
        else
            return false;
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "hwk.customer.getCustomerByRrn", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.countCustomerByUsername", key = "#customer.assignedUsername", beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getAllCustomerByLocPageable", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "hwk.customer.getAllCustomerByLoc", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "hwk.customer.getAllCustomerByLocAndCreatedDate", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "hwk.customer.getAllCustomerByLocAndCreatedDatePagable", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "hwk.customer.findIsDeletedCustomerList", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "hwk.customer.findByCustomerId", key = "#customer.customerId", beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.isValidCustomer", key = "#customer.customerId", beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getBySwId", key = "#customer.swId", beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getNewCustomerLoanDeviation", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getExistCustomerLoanDeviation", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getNewCustomerLoanDeviationAndWisma", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getExistCustomerLoanDeviationAndWisma", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getNewCustomerLoanDeviationWithBatch", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getExistCustomerLoanDeviationWithBatch", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getNewCustomerLoanDeviationAndWismaWithBatch", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getExistCustomerLoanDeviationAndWismaWithBatch", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "irn.customer.getBySentraId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "irn.customer.findById", key = "#customer.customerId", beforeInvocation = true),
            @CacheEvict(value = "irn.customer.getCustomerByCifId", key = "#customer.customerCifNumber", beforeInvocation = true),
            @CacheEvict(value = "wln.customer.getBySwId", key = "#customer.swId", beforeInvocation = true),
            @CacheEvict(value = "wln.customer.findById", key = "#customer.customerId", beforeInvocation = true),
            @CacheEvict(value = "wln.customer.getByPdkId", key = "#customer.pdkId", beforeInvocation = true)})
    public void save(Customer customer) {
        // TODO Auto-generated method stub
        customerDao.save(customer);
    }

    @Override
    @CacheEvict(allEntries = true, value = {"hwk.customer.findByCustomerId", "hwk.customer.countCustomerByUsername",
            "hwk.customer.getAllCustomerByLocPageable", "hwk.customer.getAllCustomerByLoc",
            "hwk.customer.getAllCustomerByLocAndCreatedDate", "hwk.customer.getAllCustomerByLocAndCreatedDatePagable",
            "hwk.customer.absence.findCustomerAbsenceByCustomerId", "hwk.customer.findIsDeletedCustomerList",
            "hwk.customer.getCustomerByRrn", "hwk.customer.isValidCustomer", "hwk.customer.getBySwId",
            "hwk.customer.getNewCustomerLoanDeviation", "hwk.customer.getExistCustomerLoanDeviation",
            "hwk.customer.getNewCustomerLoanDeviationAndWisma", "hwk.customer.getExistCustomerLoanDeviationAndWisma",
            "hwk.customer.getNewCustomerLoanDeviationWithBatch", "hwk.customer.getExistCustomerLoanDeviationWithBatch",
            "hwk.customer.getNewCustomerLoanDeviationAndWismaWithBatch", "hwk.customer.getExistCustomerLoanDeviationAndWismaWithBatch"}, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub

    }

    @Override
    @Cacheable(value = "hwk.customer.getBySwId", unless = "#result == null")
    public Customer getBySwId(Long swId) {
        // TODO Auto-generated method stub
        return customerDao.findTopBySwIdAndIsDeletedFalse(swId);
    }

    @Override
    @Cacheable(value = "hwk.customer.findByCustomerId", unless = "#result == null")
    public Customer findById(long parseLong) {
        // TODO Auto-generated method stub
        return customerDao.findOne(parseLong);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "hwk.customer.getCustomerByRrn", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.countCustomerByUsername", key = "#customer.assignedUsername", beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getAllCustomerByLocPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getAllCustomerByLoc", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getAllCustomerByLocAndCreatedDate", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getAllCustomerByLocAndCreatedDatePagable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.findIsDeletedCustomerList", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.findByCustomerId", key = "#customer.customerId", beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.isValidCustomer", key = "#customer.customerId", beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getBySwId", key = "#customer.swId", beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getNewCustomerLoanDeviation", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getExistCustomerLoanDeviation", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getNewCustomerLoanDeviationAndWisma", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getExistCustomerLoanDeviationAndWisma", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getNewCustomerLoanDeviationWithBatch", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getExistCustomerLoanDeviationWithBatch", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getNewCustomerLoanDeviationAndWismaWithBatch", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.customer.getExistCustomerLoanDeviationAndWismaWithBatch", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "irn.customer.getBySentraId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "irn.customer.findById", key = "#customer.customerId", beforeInvocation = true),
            @CacheEvict(value = "irn.customer.getCustomerByCifId", key = "#customer.customerCifNumber", beforeInvocation = true),
            @CacheEvict(value = "wln.customer.getBySwId", key = "#customer.swId", beforeInvocation = true),
            @CacheEvict(value = "wln.customer.findById", key = "#customer.customerId", beforeInvocation = true),
            @CacheEvict(value = "wln.customer.getByPdkId", key = "#customer.pdkId", beforeInvocation = true)})
    public void delete(Customer customer) {
        // TODO Auto-generated method stub
        customerDao.delete(customer);
    }

    @Override
    @Cacheable(value = "hwk.customer.getCustomerByRrn", unless = "#result == null")
    public Customer getByRrn(String rrn) {
        return customerDao.findByRrnAndIsDeletedFalse(rrn);
    }

    @Override
    //@Cacheable(value = "hwk.customer.getAllCustomerByLocPageable", unless = "#result == null")
    public List<CycleNumberAndCustomer> getAllCustomerByLoc(String loc,int startPosition,int maxResult) {
  	 
  	Query query = this.em.createNativeQuery(CycleNumberAndCustomer, "CustomerCycleNumberMapping");
  	query.setParameter(1, loc);
  	query.setFirstResult(startPosition);
  	query.setMaxResults(maxResult);
		List<CycleNumberAndCustomer> results = query.getResultList();
		em.close();
    	return results;
    }

    @Override
    //@Cacheable(value = "hwk.customer.getAllCustomerByLoc", unless = "#result == null")
    public List<CycleNumberAndCustomer> getAllCustomerByLoc(String loc) {
    	Query query = this.em.createNativeQuery(CycleNumberAndCustomer, "CustomerCycleNumberMapping");
    	query.setParameter(1, loc);
		List<CycleNumberAndCustomer> results = query.getResultList();
		em.close();
		
        return results;
    }

    @Override
    //@Cacheable(value = "hwk.customer.getAllCustomerByLocAndCreatedDate", unless = "#result == null")
    public List<CycleNumberAndCustomer> getAllCustomerByLocAndCreatedDate(String loc, String start, String end) {
    	Query query = this.em.createNativeQuery(CycleNumberAndCustomerWithCreatedDate, "CustomerCycleNumberMapping");
      	query.setParameter(1, loc);
      	query.setParameter("2", start);
      	query.setParameter("3", end);
    		List<CycleNumberAndCustomer> results = query.getResultList();
    		em.close();
        	return results;
    }

    @Override
    //@Cacheable(value = "hwk.customer.getAllCustomerByLocAndCreatedDatePagable", unless = "#result == null")
    public List<CycleNumberAndCustomer> getAllCustomerByLocAndCreatedDate(String loc, String start, String end, int startPosition,int maxResult) {
    	Query query = this.em.createNativeQuery(CycleNumberAndCustomerWithCreatedDate, "CustomerCycleNumberMapping");
      	query.setParameter(1, loc);
      	query.setParameter("2", start);
      	query.setParameter("3", end);
      	query.setFirstResult(startPosition);
      	query.setMaxResults(maxResult);
    		List<CycleNumberAndCustomer> results = query.getResultList();
    		em.close();
        	return results;
    }

    @Override
    @Cacheable(value = "hwk.customer.getNewCustomerLoanDeviation", unless = "#result == null")
    public List<Customer> getNewCustomerLoanDeviation(Long userId) {
        // TODO Auto-generated method stub
        return customerDao.findNewCustomerByLoanDeviation(userId);
    }

    @Override
    @Cacheable(value = "hwk.customer.getExistCustomerLoanDeviation", unless = "#result == null")
    public List<Customer> getExistCustomerLoanDeviation(Long userId) {
        // TODO Auto-generated method stub
        return customerDao.findExistCustomerByLoanDeviation(userId);
    }

    @Override
    @Cacheable(value = "hwk.customer.getNewCustomerLoanDeviationAndWisma", unless = "#result == null")
    public List<Customer> getNewCustomerLoanDeviationAndWisma(List<String> userIdList) {
        // TODO Auto-generated method stub
        return customerDao.findNewCustomerByLoanDeviationAndWisma(userIdList);
    }

    @Override
    @Cacheable(value = "hwk.customer.getExistCustomerLoanDeviationAndWisma", unless = "#result == null")
    public List<Customer> getExistCustomerLoanDeviationAndWisma(List<String> userIdList) {
        // TODO Auto-generated method stub
        return customerDao.findExistCustomerByLoanDeviationAndWisma(userIdList);
    }

    @Override
    @Cacheable(value = "hwk.customer.getNewCustomerLoanDeviationWithBatch", unless = "#result == null")
    public List<Customer> getNewCustomerLoanDeviationWithBatch(Long userId, String batch) {
        // TODO Auto-generated method stub
        return customerDao.findNewCustomerByLoanDeviationWithBatch(userId, batch);
    }

    @Override
    @Cacheable(value = "hwk.customer.getExistCustomerLoanDeviationWithBatch", unless = "#result == null")
    public List<Customer> getExistCustomerLoanDeviationWithBatch(Long userId, String batch) {
        // TODO Auto-generated method stub
        return customerDao.findExistCustomerByLoanDeviationWithBatch(userId, batch);
    }

    @Override
    @Cacheable(value = "hwk.customer.getNewCustomerLoanDeviationAndWismaWithBatch", unless = "#result == null")
    public List<Customer> getNewCustomerLoanDeviationAndWismaWithBatch(List<String> userIdList, String batch) {
        // TODO Auto-generated method stub
        return customerDao.findNewCustomerByLoanDeviationAndWismaWithBatch(userIdList, batch);
    }

    @Override
    @Cacheable(value = "hwk.customer.getExistCustomerLoanDeviationAndWismaWithBatch", unless = "#result == null")
    public List<Customer> getExistCustomerLoanDeviationAndWismaWithBatch(List<String> userIdList, String batch) {
        // TODO Auto-generated method stub
        return customerDao.findExistCustomerByLoanDeviationAndWismaWithBatch(userIdList, batch);
    }

    @Override
    @Cacheable(value = "hwk.customer.findIsDeletedCustomerList", unless = "#result == null")
    public List<Customer> findIsDeletedCustomerList() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -7);
        Date dateBefore7Days = cal.getTime();
        return customerDao.findIsDeletedCustomerId(dateBefore7Days, new Date());
    }
    
	@Override
	public List<Long> findProductIdByCycleNumber(int cycleNumber) {
		return loanProductDao.findLoanProductByCycleNumber(cycleNumber);
	}
	
	@Override
	public String getGrandTotal(String loc) {
		Query query = this.em.createNativeQuery(CountCycleNumberAndCustomer);
		query.setParameter(1, loc);
    		Object resultsObject = query.getSingleResult();
    		String results = resultsObject.toString();
    		em.close();
        	return results;
	}
	
	@Override
	public String getGrandTotal(String loc, String start, String end) {
		Query query = this.em.createNativeQuery(CountCycleNumberAndCustomerWithCreatedDate);
      	query.setParameter(1, loc);
      	query.setParameter("2", start);
      	query.setParameter("3", end);
      		Object resultsObject = query.getSingleResult();
    		String results = resultsObject.toString();
    		em.close();
        	return results;
	}

	String CycleNumberAndCustomer = "select outCus.*,cus.* from t_customer cus "
			+ "left outer join "
				+ "( select x.id as cus_id, x.cycle_number from "
					+ "( select cus.id,lprod.cycle_number,la.id as loan_id ,"
					+ "RANK() OVER (PARTITION BY cus.cif_number ORDER BY la.disbursement_date DESC, la.created_date DESC) as n "
					+ "from t_customer cus "
					+ "inner join m_location loc on cus.sentra_location=loc.id "
					+ "left join t_loan_account la on la.customer_id=cus.id "
					+ "left join t_loan_product lprod on la.product_id=lprod.id "
					+ "where loc.id=?1 and la.status in ('ACTIVE','CLOSED') "
					+ "and lprod.loan_type='TipePembiayaan-PembiayaanBaru' "
					+ "and lprod.tujuan_pembiayaan='Modal Kerja' "
					+ "and lprod.status='ProductStatus-Active' ) "
					+ "as x "
				+ "where n=1 ) as outCus "
			+ "on cus.id=outCus.cus_id where cus.sentra_location=?1";
	
	String CycleNumberAndCustomerWithCreatedDate = "select outCus.*,cus.* from t_customer cus "
			+ "left outer join "
			+ "( select x.id as cus_id, x.cycle_number from "
				+ "( select cus.id,lprod.cycle_number,la.id as loan_id ,"
				+ "RANK() OVER (PARTITION BY cus.cif_number ORDER BY la.disbursement_date DESC, la.created_date DESC) as n "
				+ "from t_customer cus "
				+ "inner join m_location loc on cus.sentra_location=loc.id "
				+ "left join t_loan_account la on la.customer_id=cus.id "
				+ "left join t_loan_product lprod on la.product_id=lprod.id "
				+ "where loc.id=?1 and la.status in ('ACTIVE','CLOSED') "
				+ "and lprod.loan_type='TipePembiayaan-PembiayaanBaru' "
				+ "and lprod.tujuan_pembiayaan='Modal Kerja' "
				+ "and lprod.status='ProductStatus-Active' ) "
				+ "as x "
			+ "where n=1 ) as outCus "
		+ "on cus.id=outCus.cus_id where cus.sentra_location=?1 "
		+ "and cus.created_dt BETWEEN :2 and :3";
	
	String CountCycleNumberAndCustomer = "select COUNT(cus.id) from t_customer cus "
			+ "left outer join "
			+ "( select x.id as cus_id, x.cycle_number from "
				+ "( select cus.id,lprod.cycle_number,la.id as loan_id ,"
				+ "RANK() OVER (PARTITION BY cus.cif_number ORDER BY la.disbursement_date DESC, la.created_date DESC) as n "
				+ "from t_customer cus "
				+ "inner join m_location loc on cus.sentra_location=loc.id "
				+ "left join t_loan_account la on la.customer_id=cus.id "
				+ "left join t_loan_product lprod on la.product_id=lprod.id "
				+ "where loc.id=?1 and la.status in ('ACTIVE','CLOSED') "
				+ "and lprod.loan_type='TipePembiayaan-PembiayaanBaru' "
				+ "and lprod.tujuan_pembiayaan='Modal Kerja' "
				+ "and lprod.status='ProductStatus-Active' ) "
				+ "as x "
			+ "where n=1 ) as outCus "
		+ "on cus.id=outCus.cus_id where cus.sentra_location=?1";

	String CountCycleNumberAndCustomerWithCreatedDate = "select COUNT(cus.id) from t_customer cus "
			+ "left outer join "
			+ "( select x.id as cus_id, x.cycle_number from "
				+ "( select cus.id,lprod.cycle_number,la.id as loan_id ,"
				+ "RANK() OVER (PARTITION BY cus.cif_number ORDER BY la.disbursement_date DESC, la.created_date DESC) as n "
				+ "from t_customer cus "
				+ "inner join m_location loc on cus.sentra_location=loc.id "
				+ "left join t_loan_account la on la.customer_id=cus.id "
				+ "left join t_loan_product lprod on la.product_id=lprod.id "
				+ "where loc.id=?1 and la.status in ('ACTIVE','CLOSED') "
				+ "and lprod.loan_type='TipePembiayaan-PembiayaanBaru' "
				+ "and lprod.tujuan_pembiayaan='Modal Kerja' "
				+ "and lprod.status='ProductStatus-Active' ) "
				+ "as x "
			+ "where n=1 ) as outCus "
		+ "on cus.id=outCus.cus_id where cus.sentra_location=?1 "
		+ "and cus.created_dt BETWEEN :2 and :3";
}