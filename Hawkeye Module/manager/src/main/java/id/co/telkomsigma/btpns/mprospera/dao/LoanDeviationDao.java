package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.loan.LoanDeviation;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;

import javax.persistence.QueryHint;
import java.util.List;

/**
 * Created by Dzulfiqar on 25/04/2017.
 */
public interface LoanDeviationDao extends JpaRepository<LoanDeviation, Long> {

    LoanDeviation findByLoanIdAndIsDeletedFalse(Long loanId);

    LoanDeviation findByAp3rIdAndIsDeletedFalse(Long ap3rId);

    LoanDeviation findByAp3rIdAndIsDeleted(Long ap3rId, Boolean isDeleted);

    List<LoanDeviation> findByBatchAndIsDeletedFalse(String batch);

    List<LoanDeviation> findByIsDeleted(Boolean deleted);

    List<LoanDeviation> findByStatusNot(String statusApproved);

    @Query("SELECT ld FROM LoanDeviation ld , AP3R a, SurveyWawancara sw, Customer c " +
            "WHERE ld.ap3rId = a.ap3rId AND a.swId = sw.swId AND sw.customerId = c.customerId AND c.sentraLocation = :locationId")
    @QueryHints(@QueryHint(name = "org.hibernate.fetchSize", value = "100"))
    List<LoanDeviation> findDeviationByMs(@Param("locationId") Location locationId);

    @Query("SELECT ld FROM LoanDeviation ld, DeviationOfficerMapping dom WHERE ld.id = dom.deviationId" +
            " AND dom.officer = :userNonMs AND dom.date = :status AND ld.isDeleted = false")
    @QueryHints(@QueryHint(name = "org.hibernate.fetchSize", value = "100"))
    List<LoanDeviation> findByUserApprovalNonMS(@Param("userNonMs") Long officer, @Param("status") String status);

}