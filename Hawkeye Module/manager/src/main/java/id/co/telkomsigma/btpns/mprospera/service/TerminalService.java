package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.TerminalManager;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by Dzulfiqar on 11/11/15.
 */
@Service("terminalService")
public class TerminalService extends GenericService {

    @Autowired
    private TerminalManager terminalManager;

    @Autowired
    private HazelcastClientService hazelcastService;

    public Terminal loadTerminalByImei(final String imei) throws UsernameNotFoundException {
        return terminalManager.getTerminalByImei(imei);
    }

    public Terminal updateTerminal(final Terminal terminal) {
        terminalManager.updateTerminal(terminal);
        return terminal;
    }

    public boolean isDuplicate(String rrn, String classMethod) {
        try {
            return hazelcastService.checkExist(classMethod, rrn);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}