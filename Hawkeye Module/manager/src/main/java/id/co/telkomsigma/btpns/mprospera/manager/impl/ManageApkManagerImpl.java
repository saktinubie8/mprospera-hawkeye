package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.ManageApkDao;
import id.co.telkomsigma.btpns.mprospera.manager.ManageApkManager;
import id.co.telkomsigma.btpns.mprospera.model.manageApk.ManageApk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service("manageApkManager")
public class ManageApkManagerImpl implements ManageApkManager {

    @Autowired
    private ManageApkDao manageApkDao;

    @Override
    @CacheEvict(allEntries = true, beforeInvocation = true, cacheNames = {"hwk.apk.isApkValid"})
    public void clearCache() {
        // TODO Auto-generated method stub
    }

    @Override
    @Cacheable(value = "hwk.apk.isApkValid", unless = "#result == null")
    public Boolean isApkValid(String version) {
        // TODO Auto-generated method stub
        ManageApk apk = manageApkDao.findByApkVersion(version);
        if (apk == null)
            return false;
        return apk.isAllowedVersion();
    }

}