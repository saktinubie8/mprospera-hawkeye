package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.CustomerManagerV2;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerWithAbsence;
import id.co.telkomsigma.btpns.mprospera.model.customer.CycleNumberAndCustomer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service("syncCustomerServiceV2")
public class SyncCustomerServiceV2 extends GenericService {

    @Autowired
    private CustomerManagerV2 customerManagerV2;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

    public Integer countAll(String username) {
        // TODO Auto-generated method stub
        return customerManagerV2.countCustomerByUsername(username);
    }

    public CustomerWithAbsence findByCustomerId(Long customerId) {
        return customerManagerV2.findByCustomerId(customerId);
    }

    public List<CycleNumberAndCustomer> getCustomerByLoc(String page, String getCountData, String startDate, String endDate,
                                           String loc) throws Exception {

        if (getCountData == null)
            page = null;
        else if (page == null || "".equals(page))
            page = "1";
        if ((startDate == null || startDate.equals("")) && endDate != null)
            startDate = endDate;
        if ((endDate == null || endDate.equals("")) && startDate != null)
            endDate = startDate;
        if (endDate != null && !endDate.equals("")) {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            cal.setTime(formatter.parse(endDate));
            cal.add(Calendar.DATE, 1);
            endDate = formatter.format(cal.getTime());
        }
        if (startDate != null && !startDate.equals("")) {
            Calendar calStart = Calendar.getInstance();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            calStart.setTime(formatter.parse(startDate));
            calStart.add(Calendar.DATE, -7);
            startDate = formatter.format(calStart.getTime());
        }
        if (startDate == null || startDate.equals(""))
            if (page == null || page.equals(""))
                return customerManagerV2.getAllCustomerByLoc(loc);
            else
                return customerManagerV2.getAllCustomerByLoc(loc,(Integer.parseInt(page) - 1)*Integer.parseInt(getCountData), Integer.parseInt(getCountData));
        else if (page == null || page.equals(""))
            return customerManagerV2.getAllCustomerByLocAndCreatedDate(loc, startDate,
                    endDate);
        else
            return customerManagerV2.getAllCustomerByLocAndCreatedDate(loc, startDate,
                    endDate,
                    (Integer.parseInt(page) - 1)*Integer.parseInt(getCountData), Integer.parseInt(getCountData));
    }

    public List<Customer> findIsDeletedCustomerList() {
        return customerManagerV2.findIsDeletedCustomerList();
    }
    
	public List<String> findProductIdByCycleNumber(int cycleNumber) {
		List<Long> listProductId = customerManagerV2.findProductIdByCycleNumber(cycleNumber+1);
		ArrayList<String> strHasil = new ArrayList<>();
		if (listProductId != null) {
			for (Long i : listProductId) {
				strHasil.add(i.toString());
			}
		}else {
			strHasil.add(null);
		}
		return strHasil;
	}
	
	public String getGrandTotal(String loc,String startDate, String endDate) throws ParseException{
		if (endDate != null && !endDate.equals("") || startDate != null && !startDate.equals("")) {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            cal.setTime(formatter.parse(endDate));
            cal.add(Calendar.DATE, 1);
            endDate = formatter.format(cal.getTime());
            Calendar calStart = Calendar.getInstance();
            SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMdd");
            calStart.setTime(formatter2.parse(startDate));
            calStart.add(Calendar.DATE, -7);
            startDate = formatter2.format(calStart.getTime());
            return customerManagerV2.getGrandTotal(loc,startDate,endDate);
        }else {
        	return customerManagerV2.getGrandTotal(loc);
        }
	}
}