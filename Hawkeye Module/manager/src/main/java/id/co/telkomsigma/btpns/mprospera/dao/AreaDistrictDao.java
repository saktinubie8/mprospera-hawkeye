package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.sda.AreaDistrict;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AreaDistrictDao extends JpaRepository<AreaDistrict, String> {

    AreaDistrict findTopByAreaNameLikeIgnoreCase(String kabupaten);

    AreaDistrict findTopByAreaId(String id);

    AreaDistrict findByAreaName(String name);

}