package id.co.telkomsigma.btpns.mprospera.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.survey.SurveyPhoto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.manager.ProsperaSurveyManager;
import id.co.telkomsigma.btpns.mprospera.manager.SurveyManager;
import id.co.telkomsigma.btpns.mprospera.model.survey.ProsperaSurveyQuestion;
import id.co.telkomsigma.btpns.mprospera.model.survey.ProsperaSurveyType;
import id.co.telkomsigma.btpns.mprospera.model.survey.Survey;

@Service("surveyService")
public class SurveyService extends GenericService {

    @Autowired
    private SurveyManager surveyManager;

    public void save(Survey survey) {
        surveyManager.save(survey);
    }

    public void save(SurveyPhoto surveyPhoto) {
        surveyManager.save(surveyPhoto);
    }

    public SurveyPhoto findBySurveyId(Long id) {
        return surveyManager.findPhotoBySurveyId(id);
    }

    public Survey findBySurveyId(String surveyId) {
        if (surveyId == null || "".equals(surveyId))
            return null;
        return surveyManager.findBySurveyId(Long.parseLong(surveyId));
    }

    public Page<Survey> getSurveyByCreatedDate(String page, String getCountData, String startDate, String endDate,
                                               String officeId) throws Exception {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

        Calendar c = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();

        if (getCountData == null)
            page = null;
        if (endDate == null || "".equals(endDate)) {
            endDate = startDate;
        }
        if (startDate == null || startDate.equals("")) {
            if (page == null || page.equals(""))
                return surveyManager.getAllSurvey(officeId);
            else
                return surveyManager.getAllSurveyPageable(
                        new PageRequest(Integer.parseInt(page) - 1, Integer.parseInt(getCountData)), officeId);
        } else {
            c.setTime(formatter.parse(endDate));
            c.add(Calendar.DATE, 1);
            endDate = formatter.format(c.getTime());
            calStart.setTime(formatter.parse(startDate));
            calStart.add(Calendar.DATE, -7);
            startDate = formatter.format(calStart.getTime());
            if (page == null || page.equals(""))
                return surveyManager.findByCreatedDate(formatter.parse(startDate), formatter.parse(endDate), officeId);
            else
                return surveyManager.findByCreatedDatePageable(formatter.parse(startDate), formatter.parse(endDate),
                        new PageRequest(Integer.parseInt(page) - 1, Integer.parseInt(getCountData)), officeId);
        }
    }

    @Autowired
    private ProsperaSurveyManager prosperaSurveyManager;

    public List<ProsperaSurveyType> findAllSurveyType() {
        // TODO Auto-generated method stub
        return prosperaSurveyManager.findAllSurveyType();
    }

    public ProsperaSurveyType findSurveyType(String surveyType) {
        // TODO Auto-generated method stub
        return prosperaSurveyManager.findByProsperaSurveyTypeId(Long.parseLong(surveyType));
    }

    public List<ProsperaSurveyQuestion> findProsperaSurveyQuestionByType(ProsperaSurveyType type) {
        return prosperaSurveyManager.findProsperaSurveyQuestionByType(type);
    }

}