package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.AreaDistrictDao;
import id.co.telkomsigma.btpns.mprospera.dao.AreaKelurahanDao;
import id.co.telkomsigma.btpns.mprospera.dao.AreaProvinceDao;
import id.co.telkomsigma.btpns.mprospera.dao.AreaSubDistrictDao;
import id.co.telkomsigma.btpns.mprospera.manager.AreaManager;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaDistrict;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaKelurahan;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaProvince;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaSubDistrict;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service("areaManager")
public class AreaManagerImpl implements AreaManager {

    @Autowired
    AreaProvinceDao areaProvinceDao;

    @Autowired
    AreaDistrictDao areaDistrictDao;

    @Autowired
    AreaSubDistrictDao areaSubDistrictDao;

    @Autowired
    AreaKelurahanDao areaKelurahanDao;

    @Override
    @Cacheable(value = "hwk.area.findSubDistrictById", unless = "#result == null")
    public AreaSubDistrict findSubDistrictById(String areaId) {
        // TODO Auto-generated method stub
        return areaSubDistrictDao.findSubDistrictById(areaId);
    }

    @Override
    @Cacheable(value = "hwk.area.findKelurahanById", unless = "#result == null")
    public AreaKelurahan findKelurahanById(String areaId) {
        // TODO Auto-generated method stub
        return areaKelurahanDao.findKelurahanById(areaId);
    }

    @Override
    @Cacheable(value = "hwk.area.findById", unless = "#result == null")
    public AreaKelurahan findById(String id) {
        return areaKelurahanDao.findOne(id);
    }

    @Override
    @CacheEvict(allEntries = true, cacheNames = {"hwk.area.findSubDistrictById","hwk.area.findKelurahanById",
            "hwk.area.findById","hwk.area.findDistrictById","hwk.area.findKecamatanById","hwk.area.findProvinceById",
            "hwk.area.findKelurahanByName","hwk.area.findKelurahanByNameAndParent","hwk.area.findKecamatanByName",
            "hwk.area.findKecamatanByNameAndParent","hwk.area.findKotaByName","hwk.area.findProvinceByName",
            "hwk.area.findKotaById","hwk.area.findByAreaName"})
    public void clearCache() {
        // TODO Auto-generated method stub
    }

    @Override
    @Cacheable(value = "hwk.area.findDistrictById", unless = "#result == null")
    public AreaDistrict findDistrictById(String districtCode) {
        return areaDistrictDao.findOne(districtCode);
    }

    @Override
    @Cacheable(value = "hwk.area.findKecamatanById", unless = "#result == null")
    public AreaSubDistrict findKecamatanById(String id) {
        // TODO Auto-generated method stub
        return areaSubDistrictDao.findSubDistrictById(id);
    }

    @Override
    @Cacheable(value = "hwk.area.findProvinceById", unless = "#result == null")
    public AreaProvince findProvinceById(String id) {
        // TODO Auto-generated method stub
        return areaProvinceDao.findProvinceById(id);
    }

    @Override
    @Cacheable(value = "hwk.area.findKelurahanByName", unless = "#result == null")
    public AreaKelurahan findKelurahanByName(String areaName) {
        return areaKelurahanDao.findTopByAreaNameLikeIgnoreCase(areaName);
    }

    @Override
    @Cacheable(value = "hwk.area.findKelurahanByNameAndParent", unless = "#result == null")
    public AreaKelurahan findKelurahanByNameAndParent(String areaName, String parentAreaId) {
        return areaKelurahanDao.findTopByAreaNameLikeIgnoreCaseAndParentAreaId(areaName, parentAreaId);
    }

    @Override
    @Cacheable(value = "hwk.area.findKecamatanByName", unless = "#result == null")
    public AreaSubDistrict findKecamatanByName(String areaName) {
        return areaSubDistrictDao.findTopByAreaNameLikeIgnoreCase(areaName);
    }

    @Override
    @Cacheable(value = "hwk.area.findKecamatanByNameAndParent", unless = "#result == null")
    public AreaSubDistrict findKecamatanByNameAndParent(String areaName, String parentId) {
        return areaSubDistrictDao.findTopByAreaNameLikeIgnoreCaseAndParentAreaId(areaName, parentId);
    }

    @Override
    @Cacheable(value = "hwk.area.findKotaByName", unless = "#result == null")
    public AreaDistrict findKotaByName(String areaName) {
        return areaDistrictDao.findTopByAreaNameLikeIgnoreCase(areaName);
    }

    @Override
    @Cacheable(value = "hwk.area.findProvinceByName", unless = "#result == null")
    public AreaProvince findProvinceByName(String areaName) {
        return areaProvinceDao.findTopByAreaNameLikeIgnoreCase(areaName);
    }

    @Override
    @Cacheable(value = "hwk.area.findKotaById", unless = "#result == null")
    public AreaDistrict findKotaById(String id) {
        // TODO Auto-generated method stub
        return areaDistrictDao.findTopByAreaId(id);
    }

    @Override
    @Cacheable(value = "hwk.area.findByAreaName", unless = "#result == null")
    public AreaDistrict findByAreaName(String name) {
        // TODO Auto-generated method stub
        return areaDistrictDao.findByAreaName(name);
    }

}