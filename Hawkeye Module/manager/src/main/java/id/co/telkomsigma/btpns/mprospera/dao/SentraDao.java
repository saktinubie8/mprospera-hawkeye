package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;

public interface SentraDao extends JpaRepository<Sentra, Long> {

    Integer countBySentraId(long sentraId);

    Sentra findBySentraId(Long sentraId);

    Sentra findByRrn(String rrn);

    @Query("SELECT s FROM Sentra s where s.sentraId in "
            + "(SELECT g.sentra.sentraId FROM Group g where g.groupId= :groupId)")
    Sentra getSentraByGroupId(@Param("groupId") Long groupId);

    @Query("SELECT s FROM Sentra s WHERE s.isDeleted=false and s.locationId=:loc AND s.status in :paramList")
    Page<Sentra> getAllSentraWithLoc(@Param("loc") String loc, Pageable pageRequest,
                                     @Param("paramList") List<String> paramList);

    @Query("SELECT s FROM Sentra s WHERE s.createdDate>=:startDate AND s.createdDate<:endDate and s.isDeleted=false and s.assignedTo=:username AND s.status in :paramList")
    Page<Sentra> findByCreatedDate(@Param("username") String username, @Param("startDate") Date startDate,
                                   @Param("endDate") Date endDate, Pageable pageRequest, @Param("paramList") List<String> paramList);

    @Query("SELECT s FROM Sentra s WHERE s.createdDate>=:startDate AND s.createdDate<:endDate and s.isDeleted=false and s.locationId=:loc AND s.status in :paramList")
    Page<Sentra> findByCreatedDateWithLoc(@Param("loc") String loc, @Param("startDate") Date startDate,
                                          @Param("endDate") Date endDate, Pageable pageRequest, @Param("paramList") List<String> paramList);

    @Query("SELECT s FROM Sentra s WHERE s.updatedDate>=:startDate AND s.updatedDate<:endDate and (s.isDeleted = true or s.status not in :paramList)")
    List<Sentra> findIsDeletedSentraId(@Param("startDate") Date startDate, @Param("endDate") Date endDate,
                                       @Param("paramList") List<String> paramList);

}