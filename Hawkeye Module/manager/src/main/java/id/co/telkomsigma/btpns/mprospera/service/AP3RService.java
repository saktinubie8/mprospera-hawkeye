package id.co.telkomsigma.btpns.mprospera.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.manager.AP3RManager;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.AP3R;
import id.co.telkomsigma.btpns.mprospera.model.sw.FundedThings;

@Service("ap3rService")
public class AP3RService extends GenericService {

    @Autowired
    private UserManager userManager;

    @Autowired
    private AP3RManager ap3rManager;

    public AP3R findTopBySwId(Long sw) {
        return ap3rManager.findTopBySwId(sw);
    }

    public AP3R findById(Long ap3r) {
        return ap3rManager.findById(ap3r);
    }

    public List<FundedThings> findByAp3rId(AP3R ap3r) {
        return ap3rManager.findByAp3rId(ap3r);
    }

    public List<AP3R> findByAp3rIdInLoanDeviation(String username) {
        User userNonMs = userManager.getUserByUsername(username);
        return ap3rManager.findByAp3rIdInLoanDeviation(userNonMs.getUserId());
    }

    public List<AP3R> findByAp3rIdInLoanDeviationAndWisma(String username) {
        User userSW = userManager.getUserByUsername(username);
        List<User> userListWisma = userManager.getUserByLocId(userSW.getOfficeCode());
        List<String> userIdList = new ArrayList<>();
        for (User user : userListWisma) {
            userIdList.add(user.getUsername());
        }
        return ap3rManager.findByAp3rIdInLoanDeviationAndWisma(userIdList);
    }

    public List<AP3R> findByAp3rIdInLoanDeviationWithBatch(String username, String batch) {
        User userNonMs = userManager.getUserByUsername(username);
        return ap3rManager.findByAp3rIdInLoanDeviationWithBatch(userNonMs.getUserId(), batch);
    }

    public List<AP3R> findByAp3rIdInLoanDeviationAndWismaWithBatch(String username, String batch) {
        User userSW = userManager.getUserByUsername(username);
        List<User> userListWisma = userManager.getUserByLocId(userSW.getOfficeCode());
        List<String> userIdList = new ArrayList<>();
        for (User user : userListWisma) {
            userIdList.add(user.getUsername());
        }
        return ap3rManager.findByAp3rIdInLoanDeviationAndWismaWithBatch(userIdList, batch);
    }

    public BigDecimal sumFunds(AP3R ap3r) {
        return ap3rManager.sumFunds(ap3r);
    }

    public AP3R findByLocalId(String localId) {
        return ap3rManager.findByLocalId(localId);
    }

    public void updatedisbursmendate(AP3R ap3r) {
        ap3rManager.updatedisbursmendate(ap3r);
    }
    
    public List<AP3R> findAp3rListBySwId(Long swId) {
        return ap3rManager.findAp3rListBySwId(swId);
    }

}