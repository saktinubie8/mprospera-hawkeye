package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.*;
import id.co.telkomsigma.btpns.mprospera.manager.SWManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service("swManager")
public class SWManagerImpl implements SWManager {

    @Autowired
    SWDao swDao;

    @Autowired
    SwIdPhotoDao swIdPhotoDao;

    @Autowired
    SwSurveyPhotoDao swSurveyPhotoDao;

    @Autowired
    private LoanProductDao loanProductDao;

    @Autowired
    private BusinessTypeDao businessTypeDao;

    @Autowired
    private ProductPlafondDao productPlafondDao;

    @Autowired
    private DirectBuyDao directBuyDao;

    @Autowired
    private AWGMDao awgmDao;

    @Autowired
    private NeighborDao neighborDao;

    @Autowired
    private SwProductMapDao swProductMapDao;

    @Autowired
    private BusinessTypeAp3rDao businessTypeAp3rDao;

    @PersistenceContext
    public EntityManager em;

    @Autowired
    private OtherItemCalculationDao otherItemCalculationDao;

    @Autowired
    SwUserBwmpMapDao swUserBwmpMapDao;

    @Override
    @Cacheable(value = "hwk.sw.isValidSw", unless = "#result == null")
    public Boolean isValidSw(String swId) {
        Integer count = swDao.countBySwId(Long.parseLong(swId));
        if (count > 0)
            return true;
        else
            return false;
    }

    @Override
    @Cacheable(value = "hwk.sw.getSWById", unless = "#result == null")
    public SurveyWawancara getSWById(String swId) {
        // TODO Auto-generated method stub
        return swDao.findSwBySwId(Long.parseLong(swId));
    }

    @Override
    @Cacheable(value = "hwk.sw.findByUserMS", unless = "#result == null")
    public List<SurveyWawancara> findByUserMS(List<String> userIdList) {
        // TODO Auto-generated method stub
        return swDao.findByCreatedByInAndLocalIdIsNotNullAndIsDeletedFalse(userIdList);
    }

    @Override
    @Cacheable(value = "hwk.sw.findByUserMSWithDate", unless = "#result == null")
    public List<SurveyWawancara> findByUserMSWithDate(List<String> userIdList, Date startDate, Date endDate) {
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return swDao.findByCreatedByInAndLocalIdIsNotNullAndIsDeletedFalseAndCreatedDate(userIdList, calStart.getTime(), cal.getTime());
    }

    @Override
    @Cacheable(value = "hwk.sw.findByLocalId", unless = "#result == null")
    public SurveyWawancara findByLocalId(String localId) {
        return swDao.findSwByLocalIdAndIsDeletedFalse(localId);
    }

    @Override
    @Cacheable(value = "hwk.sw.findIsDeletedSwList", unless = "#result == null")
    public List<SurveyWawancara> findIsDeletedSwList() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -7);
        Date dateBefore7Days = cal.getTime();
        return swDao.findIsDeletedSwId(dateBefore7Days, new Date());
    }

    @Override
    @Cacheable(value = "hwk.sw.findSwIdInLoanDeviationWithBatch", unless = "#result == null")
    public List<SurveyWawancara> findSwIdInLoanDeviationWithBatch(Long userId, String batch) {
        // TODO Auto-generated method stub
        return swDao.findSwIdInLoanDeviationWithBatch(userId, batch);
    }

    @Override
    @Cacheable(value = "hwk.sw.findSwIdInLoanDeviation", unless = "#result == null")
    public List<SurveyWawancara> findSwIdInLoanDeviation(Long userId) {
        // TODO Auto-generated method stub
        return swDao.findSwIdInLoanDeviation(userId);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "hwk.sw.findIsDeletedSwList", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sw.findByUserMSWithDate", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sw.isValidSw", key = "#surveyWawancara.swId", beforeInvocation = true),
            @CacheEvict(value = "hwk.sw.getSWById", key = "#surveyWawancara.swId", beforeInvocation = true),
            @CacheEvict(value = "hwk.sw.findByUserMS", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sw.findByLocalId", key = "#surveyWawancara.localId", beforeInvocation = true),
            @CacheEvict(value = "hwk.sw.findSwIdInLoanDeviationWithBatch", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hwk.sw.findSwIdInLoanDeviation", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.countAllSW", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.countAllSWByStatus", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findAll", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findByCreatedDate", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findByCreatedDatePageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.isValidSw", key = "#surveyWawancara.swId", beforeInvocation = true),
            @CacheEvict(value = "wln.sw.getSWById", key = "#surveyWawancara.swId", beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findByRrn", key = "#surveyWawancara.rrn", beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findAllByUser", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findAllByUserPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findByCreatedDateAndUser", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findByCreatedDateAndUserPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findAllByUserMS", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findAllByUserMSPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findByCreatedDateAndUserMS", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findByCreatedDateAndUserMSPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findAllNonMs", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findAllNonMsPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findAllNonMsWithUserId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findAllNonMsWithUserIdPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findByCreatedDateNonMs", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findByCreatedDateNonMsPageable", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findByLocalId", key = "#surveyWawancara.localId", beforeInvocation = true),
            @CacheEvict(value = "wln.sw.findIsDeletedSwList", allEntries = true, beforeInvocation = true),})
    public void save(SurveyWawancara surveyWawancara) {
        // TODO Auto-generated method stub
        swDao.save(surveyWawancara);

    }

    @Override
    @Cacheable(value = "hwk.idPhoto.getSwIdPhoto", unless = "#result == null")
    public SwIdPhoto getSwIdPhoto(String swId) {
        // TODO Auto-generated method stub
        return swIdPhotoDao.getSwIdPhoto(Long.parseLong(swId));
    }

    @Override
    @Cacheable(value = "hwk.businessPlacePhoto.getSwSurveyPhoto", unless = "#result == null")
    public SwSurveyPhoto getSwSurveyPhoto(String swId) {
        // TODO Auto-generated method stub
        return swSurveyPhotoDao.getSwSurveyPhoto(Long.parseLong(swId));
    }

    @Override
    @CacheEvict(value = {"hwk.sw.countAllSW", "hwk.sw.findIsDeletedSwList", "hwk.sw.findByUserMSWithDate",
            "hwk.direct.findProductBySwId", "hwk.awgm.findAwgmProductBySwId", "hwk.neighbor.findNeighborBySwId",
            "hwk.swProductMap.findProductMapBySwId", "hwk.other.item.findOtherItemCalcBySwId",
            "hwk.swUserBwmpMap.findSWUserBWMPMappingBySW", "hwk.sw.isValidSw", "hwk.sw.getSWById", "hwk.sw.findByUserMS",
            "hwk.sw.findByLocalId", "hwk.sw.findSwIdInLoanDeviationWithBatch", "hwk.sw.findSwIdInLoanDeviation",
            "hwk.idPhoto.getSwIdPhoto", "hwk.businessPlacePhoto.getSwSurveyPhoto", "hwk.swProductMap.findProductMapById",
            "hwk.loan.product.findByProductId", "hwk.businessTypeAp3r.findBusinessTypeAP3RById"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub

    }

    @Override
    @Cacheable(value = "hwk.swProductMap.findProductMapBySwId", unless = "#result == null")
    public List<SWProductMapping> findProductMapBySwId(Long swId) {
        // TODO Auto-generated method stub
        return swProductMapDao.findSwProductMapBySwId(swId);
    }

    @Override
    @Cacheable(value = "hwk.swProductMap.findProductMapById", unless = "#result == null")
    public SWProductMapping findProductMapById(Long id) {
        return swProductMapDao.findOne(id);
    }

    @Override
    @Cacheable(value = "hwk.direct.findProductBySwId", unless = "#result == null")
    public List<DirectBuyThings> findProductBySwId(SurveyWawancara sw) {
        // TODO Auto-generated method stub
        return directBuyDao.findBySwId(sw);
    }

    @Override
    @Cacheable(value = "hwk.awgm.findAwgmProductBySwId", unless = "#result == null")
    public List<AWGMBuyThings> findAwgmProductBySwId(SurveyWawancara sw) {
        // TODO Auto-generated method stub
        return awgmDao.findBySwId(sw);
    }

    @Override
    @Cacheable(value = "hwk.neighbor.findNeighborBySwId", unless = "#result == null")
    public List<NeighborRecommendation> findNeighborBySwId(SurveyWawancara sw) {
        // TODO Auto-generated method stub
        return neighborDao.findBySwId(sw);
    }

    @Override
    @Cacheable(value = "hwk.loan.product.findByProductId", unless = "#result == null")
    public LoanProduct findByProductId(String productId) {
        // TODO Auto-generated method stub
        return loanProductDao.findByProductId(Long.parseLong(productId));
    }


    @Override
    @Cacheable(value = "hwk.businessTypeAp3r.findBusinessTypeAP3RById", unless = "#result == null")
    public BusinessTypeAP3R findBusinessTypeAP3RById(Long businessId) {
        // TODO Auto-generated method stub
        return businessTypeAp3rDao.findByBusinessId(businessId);
    }

    @Override
    @Cacheable(value = "hwk.other.item.findOtherItemCalcBySwId", unless = "#result == null")
    public List<OtherItemCalculation> findOtherItemCalcBySwId(Long swId) {
        return otherItemCalculationDao.findBySwId(swId);
    }

    @Override
    @Cacheable(value = "hwk.swUserBwmpMap.findSWUserBWMPMappingBySW", unless = "#result == null")
    public List<SWUserBWMPMapping> findBySw(Long sw) {
        return swUserBwmpMapDao.findBySwId(sw);
    }

	@Override
	public BusinessTypeAP3R findBySubBiCode(String subBiCode) {
		return businessTypeAp3rDao.findBySubBiCode(subBiCode);
	}

}