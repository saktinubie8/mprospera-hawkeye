package id.co.telkomsigma.btpns.mprospera.manager;

import java.math.BigDecimal;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.sw.AP3R;
import id.co.telkomsigma.btpns.mprospera.model.sw.FundedThings;

public interface AP3RManager {

    AP3R findByLocalId(String localId);
    
    AP3R updatedisbursmendate(AP3R ap3r);

    AP3R findTopBySwId(Long sw);

    List<FundedThings> findByAp3rId(AP3R ap3r);

    List<AP3R> findByAp3rIdInLoanDeviation(Long userId);

    List<AP3R> findByAp3rIdInLoanDeviationAndWisma(List<String> userIdList);

    List<AP3R> findByAp3rIdInLoanDeviationWithBatch(Long userId, String batch);

    List<AP3R> findByAp3rIdInLoanDeviationAndWismaWithBatch(List<String> userIdList, String batch);

    AP3R findById(Long id);

    BigDecimal sumFunds(AP3R ap3r);

    List<AP3R> findAp3rListBySwId(Long swId);

    void clearCache();

}