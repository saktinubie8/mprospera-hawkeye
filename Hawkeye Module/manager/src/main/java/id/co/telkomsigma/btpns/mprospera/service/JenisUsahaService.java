package id.co.telkomsigma.btpns.mprospera.service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.manager.JenisUsahaManager;
import id.co.telkomsigma.btpns.mprospera.model.jenisusaha.JenisUsaha;
import org.springframework.stereotype.Service;

@Service("jenisUsahaService")
public class JenisUsahaService extends GenericService {
	

	@Autowired
	private JenisUsahaManager jenisUsahaManager;

	public List<JenisUsaha> getAllJenisUsaha() {
		return jenisUsahaManager.getAllJenisUsaha();
	}
	
	public JenisUsaha getJenisUsahaById(Long id) {
		return jenisUsahaManager.findById(id);
	}

	public String getVersionByDate(Date date, Long timestamp) throws ParseException {
		long epoch = 0;
		JenisUsaha jenisUsaha = jenisUsahaManager.getJenisUsahaByDate(date);
		if (jenisUsaha != null) {
			log.info("jenisUsaha" + jenisUsaha);

			// long epoch = new java.text.SimpleDateFormat("yyyy/mm/dd
			// HH:mm:ss").parse(jenisUsaha.getCreatedDate().toString()).getTime() / 1000;
			epoch = jenisUsaha.getCreatedDate().getTime();

			log.info("epoch : " + epoch);

			// FIND TOP ONE

			JenisUsaha jenisUsahaNewTop = jenisUsahaManager.findTop1ByCreateDateMoreThanOrderByCreatedDateDesc(date);
			log.info("jenisUsahaNewTop" + jenisUsahaNewTop);
			if (jenisUsahaNewTop != null) {

				long epochNewTop = jenisUsahaNewTop.getCreatedDate().getTime();

				log.info("epochNewTop : " + epochNewTop);

				if (epoch != 0 && epoch < epochNewTop) {
					return WebGuiConstant.JENIS_USAHA_STATUS_TRUE;
				} else {
					return WebGuiConstant.JENIS_USAHA_STATUS_FALSE;
				}
			} else {
				return WebGuiConstant.JENIS_USAHA_STATUS_FALSE;
			}
		} else {
			return WebGuiConstant.JENIS_USAHA_STATUS_NOT_FOUND;
		}
	}
	
	public Date getLastData() {
		JenisUsaha jenisUsahaLastDateData = jenisUsahaManager.findTop1ByCreateDateOrderByCreatedDateDesc();
		log.info("jenisUsahaLastDateData : "+jenisUsahaLastDateData);
		return jenisUsahaLastDateData.getCreatedDate();
	}

}
