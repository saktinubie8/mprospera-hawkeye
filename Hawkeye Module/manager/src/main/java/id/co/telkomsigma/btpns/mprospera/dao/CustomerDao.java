package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import javax.persistence.QueryHint;

public interface CustomerDao extends JpaRepository<Customer, Long> {

    Integer countByAssignedUsernameAndIsDeletedFalse(String assignedUsername);

    Integer countByCustomerIdAndIsDeletedFalse(long customerId);

    Customer findTopBySwIdAndIsDeletedFalse(Long swId);

    Customer findByRrnAndIsDeletedFalse(String rrn);

    @Query("SELECT c FROM Customer c INNER JOIN c.sentraLocation l WHERE l.locationId = (:loc) AND c.isDeleted = false")
    @QueryHints(@QueryHint(name = "org.hibernate.fetchSize" , value = "100"))
    Page<Customer> findByLocId(@Param("loc") String loc, Pageable pageRequest);

    @Query("SELECT c FROM Customer c INNER JOIN c.sentraLocation l WHERE l.locationId = :loc AND c.approvedDate between :startDate AND :endDate AND c.isDeleted = false")
    @QueryHints(@QueryHint(name = "org.hibernate.fetchSize" , value = "100"))
    Page<Customer> findByLocIdAndCreatedDateBetween(@Param("loc") String loc, @Param("startDate") Date start,
                                                    @Param("endDate") Date end, Pageable pageable);

    @Query("SELECT c FROM Customer c " + "INNER JOIN c.group g " + ",Sentra st "
            + "WHERE st.sentraId = g.sentra AND st.sentraId = (:sentraId) AND c.isDeleted = false")
    @QueryHints(@QueryHint(name = "org.hibernate.fetchSize" , value = "100"))
    List<Customer> findBySentraId(@Param("sentraId") Long sentraId);

    @Query("select c from Loan la inner join la.customer c " +
            " , AP3R a" +
            " , LoanDeviation ld " +
            " , DeviationOfficerMapping dom " +
            " where a.ap3rId = la.ap3rId and ld.ap3rId = a.ap3rId " +
            " and dom.deviationId = ld.deviationId " +
            " and dom.officer = :userNonMs and c.isDeleted = false")
    @QueryHints(@QueryHint(name = "org.hibernate.fetchSize" , value = "100"))
    List<Customer> findNewCustomerByLoanDeviation(@Param("userNonMs") Long userNonMs);

    @Query("select c from Customer c , SurveyWawancara sw " +
            " , AP3R a " +
            " , LoanDeviation ld " +
            " , DeviationOfficerMapping dom " +
            " where c.customerId = sw.customerId and a.swId = sw.swId " +
            " and ld.ap3rId = a.ap3rId and dom.deviationId = ld.deviationId " +
            " and dom.officer = :userNonMs and c.isDeleted = false")
    @QueryHints(@QueryHint(name = "org.hibernate.fetchSize" , value = "100"))
    List<Customer> findExistCustomerByLoanDeviation(@Param("userNonMs") Long userNonMs);

    @Query("select c from Customer c where c.customerId in " +
            "(select l.customer.customerId from Loan l where l.ap3rId in " +
            "(select a.ap3rId from AP3R a where a.ap3rId in " +
            "(select ld.ap3rId from LoanDeviation ld))) and c.createdBy in :userIdList AND c.isDeleted = false")
    @QueryHints(@QueryHint(name = "org.hibernate.fetchSize" , value = "100"))
    List<Customer> findNewCustomerByLoanDeviationAndWisma(@Param("userIdList") List<String> userIdList);

    @Query("select c from Customer c where c.customerId in " +
            "(select sw.customerId from SurveyWawancara sw where sw.swId in " +
            "(select a.swId from AP3R a where a.ap3rId in " +
            "(select ld.ap3rId from LoanDeviation ld))) and c.createdBy in :userIdList AND c.isDeleted = false")
    @QueryHints(@QueryHint(name = "org.hibernate.fetchSize" , value = "100"))
    List<Customer> findExistCustomerByLoanDeviationAndWisma(@Param("userIdList") List<String> userIdList);

    @Query("select c from Loan la inner join la.customer c " +
            " , AP3R a" +
            " , LoanDeviation ld " +
            " , DeviationOfficerMapping dom " +
            " where a.ap3rId = la.ap3rId and ld.ap3rId = a.ap3rId " +
            " and dom.deviationId = ld.deviationId " +
            " and dom.officer = :userNonMs and c.isDeleted = false and ld.batch = :batch")
    @QueryHints(@QueryHint(name = "org.hibernate.fetchSize" , value = "100"))
    List<Customer> findNewCustomerByLoanDeviationWithBatch(@Param("userNonMs") Long userNonMs, @Param("batch") String batch);

    @Query("select c from Customer c , SurveyWawancara sw " +
            " , AP3R a " +
            " , LoanDeviation ld " +
            " , DeviationOfficerMapping dom " +
            " where c.customerId = sw.customerId and a.swId = sw.swId " +
            " and ld.ap3rId = a.ap3rId and dom.deviationId = ld.deviationId " +
            " and dom.officer = :userNonMs and c.isDeleted = false and ld.batch = :batch")
    @QueryHints(@QueryHint(name = "org.hibernate.fetchSize" , value = "100"))
    List<Customer> findExistCustomerByLoanDeviationWithBatch(@Param("userNonMs") Long userNonMs, @Param("batch") String batch);

    @Query("select c from Customer c where c.customerId in " +
            "(select l.customer.customerId from Loan l where l.ap3rId in " +
            "(select a.ap3rId from AP3R a where a.ap3rId in " +
            "(select ld.ap3rId from LoanDeviation ld where ld.batch = :batch))) and c.createdBy in :userIdList AND c.isDeleted = false")
    @QueryHints(@QueryHint(name = "org.hibernate.fetchSize" , value = "100"))
    List<Customer> findNewCustomerByLoanDeviationAndWismaWithBatch(@Param("userIdList") List<String> userIdList, @Param("batch") String batch);

    @Query("select c from Customer c where c.customerId in " +
            "(select sw.customerId from SurveyWawancara sw where sw.swId in " +
            "(select a.swId from AP3R a where a.ap3rId in " +
            "(select ld.ap3rId from LoanDeviation ld where ld.batch = :batch))) and c.createdBy in :userIdList AND c.isDeleted = false")
    @QueryHints(@QueryHint(name = "org.hibernate.fetchSize" , value = "100"))
    List<Customer> findExistCustomerByLoanDeviationAndWismaWithBatch(@Param("userIdList") List<String> userIdList, @Param("batch") String batch);

    @Query("SELECT c FROM Customer c WHERE c.createdDate>=:startDate AND c.createdDate<:endDate and c.isDeleted = true")
    @QueryHints(@QueryHint(name = "org.hibernate.fetchSize" , value = "100"))
    List<Customer> findIsDeletedCustomerId(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

}