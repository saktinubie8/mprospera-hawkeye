package id.co.telkomsigma.btpns.mprospera.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.manager.SentraManager;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import id.co.telkomsigma.btpns.mprospera.model.sentra.SentraPhoto;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service("sentraService")
public class SentraService extends GenericService {

    @Autowired
    private SentraManager sentraManager;

    @Autowired
    private UserManager userManager;

    public void save(Sentra sentra) {
        sentraManager.save(sentra);
    }

    public void savePhoto(SentraPhoto sentraPhoto) {
        sentraManager.savePhoto(sentraPhoto);
    }

    public void saveGroup(Group group) {
        sentraManager.saveGroup(group);
    }

    public Boolean isValidSentra(Long sentraId) {
        return sentraManager.isValidSentra(sentraId);
    }

    public SentraPhoto getSentraPhoto(Long sentraId) {
        return sentraManager.getSentraPhoto(sentraId);
    }

    public Sentra findBySentraId(String sentraId) {
        if (sentraId == null || "".equals(sentraId))
            return null;
        return sentraManager.findBySentraId(Long.parseLong(sentraId));
    }

    public Boolean isValidGroup(Long groupId) {
        return sentraManager.isValidGroup(groupId);
    }

    public Page<Sentra> getSentraByCreatedDate(String username, String page, String getCountData, String startDate, String endDate) throws Exception {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

        Calendar c = Calendar.getInstance();
        Calendar cStart = Calendar.getInstance();

        if (getCountData == null)
            page = null;
        if (endDate == null || "".equals(endDate)) {
            endDate = startDate;
        }
        User user = userManager.getUserByUsername(username);
        String loc = user.getOfficeCode();
        if (user.getRoleUser().equals(null)) {
            user.setRoleUser("");
        }

        if (startDate == null || startDate.equals("")) {
            if (page == null || page.equals(""))
                return sentraManager.getAllSentra(username, loc);
            else
                return sentraManager.getAllSentraPageable(username, loc, new PageRequest(Integer.parseInt(page) - 1, Integer.parseInt(getCountData)));
        } else {
            c.setTime(formatter.parse(endDate));
            cStart.setTime(formatter.parse(startDate));
            c.add(Calendar.DATE, 1);
            cStart.add(Calendar.DATE, -7);
            endDate = formatter.format(c.getTime());
            startDate = formatter.format(cStart.getTime());

            if (page == null || page.equals(""))
                return sentraManager.findByCreatedDate(username, loc, formatter.parse(startDate), formatter.parse(endDate));
            else
                return sentraManager.findByCreatedDatePageable(username, loc, formatter.parse(startDate), formatter.parse(endDate), new PageRequest(Integer.parseInt(page) - 1, Integer.parseInt(getCountData)));
        }
    }

    public List<Group> getGroupBySentraId(Long sentraId) {
        return sentraManager.getGroupBySentraId(sentraId);
    }

    public Group getGroupByGroupId(String groupId) {
        if (groupId == null || "".equals(groupId))
            return null;
        return sentraManager.getGroupByGroupId(Long.parseLong(groupId));
    }

    public Group getGroupByCustomerId(Long customerId) {
        return sentraManager.getGroupByCustomerId(customerId);
    }

    public Sentra getSentraByRrn(String rrn) {
        return sentraManager.findByRrn(rrn);
    }

    public Sentra getSentraByGroupId(Long groupId) {
        return sentraManager.getSentraByGroupId(groupId);
    }

    public Group getGroupByRrn(String rrn) {
        return sentraManager.findGroupByRrn(rrn);
    }

    public List<Sentra> findAll() {
        // TODO Auto-generated method stub
        return sentraManager.findAll();
    }

    public List<Sentra> findIsDeletedSentraList() {
        return sentraManager.findIsDeletedSentraList();
    }

    public List<Group> findIsDeletedSentraGroupList() {
        return sentraManager.findIsDeletedSentraGroupList();
    }

}