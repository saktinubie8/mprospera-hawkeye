package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.loan.LoanWithEmergencyFund;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Agung on 03/05/2017.
 */
public interface LoanWithEmergencyFundDao extends JpaRepository<LoanWithEmergencyFund, Long> {

    LoanWithEmergencyFund findByLoanId(Long loanId);

}