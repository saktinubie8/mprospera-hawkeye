package id.co.telkomsigma.btpns.mprospera.service;

import java.util.ArrayList;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.manager.CustomerManager;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;

@Service("customerService")
public class CustomerService extends GenericService {

    @Autowired
    @Qualifier("customerManager")
    private CustomerManager customerManager;

    @Autowired
    private UserManager userManager;

    public Boolean isValidCustomer(String customerId) {
        return customerManager.isValidCustomer(customerId);
    }

    public void save(Customer customer) {
        customerManager.save(customer);
    }

    public Customer getBySwId(Long swId) {
        return customerManager.getBySwId(swId);
    }

    public Customer findById(String customerId) {
        if (customerId == null || "".equals(customerId))
            return null;
        return customerManager.findById(Long.parseLong(customerId));
    }

    public void delete(Customer customer) {
        // TODO Auto-generated method stub
        customerManager.delete(customer);
    }

    public Customer getCustomerByRrn(String rrn) {
        return customerManager.getByRrn(rrn);
    }

    public List<Customer> getNewCustomerLoanDeviation(String username) {
        User userNonMS = userManager.getUserByUsername(username);
        return customerManager.getNewCustomerLoanDeviation(userNonMS.getUserId());
    }

    public List<Customer> getExistCustomerLoanDeviation(String username) {
        User userNonMS = userManager.getUserByUsername(username);
        return customerManager.getExistCustomerLoanDeviation(userNonMS.getUserId());
    }

    public List<Customer> getNewCustomerLoanDeviationAndWisma(String username) {
        User userSW = userManager.getUserByUsername(username);
        List<User> userListWisma = userManager.getUserByLocId(userSW.getOfficeCode());
        List<String> userIdList = new ArrayList<>();
        for (User user : userListWisma) {
            userIdList.add(user.getUsername());
        }
        return customerManager.getNewCustomerLoanDeviationAndWisma(userIdList);
    }

    public List<Customer> getExistCustomerLoanDeviationAndWisma(String username) {
        User userSW = userManager.getUserByUsername(username);
        List<User> userListWisma = userManager.getUserByLocId(userSW.getOfficeCode());
        List<String> userIdList = new ArrayList<>();
        for (User user : userListWisma) {
            userIdList.add(user.getUsername());
        }
        return customerManager.getExistCustomerLoanDeviationAndWisma(userIdList);
    }

    public List<Customer> getNewCustomerLoanDeviationWithBatch(String username, String batch) {
        User userNonMS = userManager.getUserByUsername(username);
        return customerManager.getNewCustomerLoanDeviationWithBatch(userNonMS.getUserId(), batch);
    }

    public List<Customer> getExistCustomerLoanDeviationWithBatch(String username, String batch) {
        User userNonMS = userManager.getUserByUsername(username);
        return customerManager.getExistCustomerLoanDeviationWithBatch(userNonMS.getUserId(), batch);
    }

    public List<Customer> getNewCustomerLoanDeviationAndWismaWithBatch(String username, String batch) {
        User userSW = userManager.getUserByUsername(username);
        List<User> userListWisma = userManager.getUserByLocId(userSW.getOfficeCode());
        List<String> userIdList = new ArrayList<>();
        for (User user : userListWisma) {
            userIdList.add(user.getUsername());
        }
        return customerManager.getNewCustomerLoanDeviationAndWismaWithBatch(userIdList, batch);
    }

    public List<Customer> getExistCustomerLoanDeviationAndWismaWithBatch(String username, String batch) {
        User userSW = userManager.getUserByUsername(username);
        List<User> userListWisma = userManager.getUserByLocId(userSW.getOfficeCode());
        List<String> userIdList = new ArrayList<>();
        for (User user : userListWisma) {
            userIdList.add(user.getUsername());
        }
        return customerManager.getExistCustomerLoanDeviationAndWismaWithBatch(userIdList, batch);
    }

}