package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.dao.JenisUsahaDao;
import id.co.telkomsigma.btpns.mprospera.manager.JenisUsahaManager;
import id.co.telkomsigma.btpns.mprospera.model.jenisusaha.JenisUsaha;

@Service("jenisUsahaManager")
public class JenisUsahaManagerImpl implements JenisUsahaManager {
	
	private static final Logger log = Logger.getLogger(JenisUsahaManagerImpl.class.getName());
	
	private final Log loging = LogFactory.getLog(getClass());

	@Autowired
	private JenisUsahaDao jUDao;


	@Override
	public JenisUsaha findById(Long id) {
		return jUDao.findOne(id);
	}

	@Override
	public JenisUsaha findByKdSectorEkonomi(Long kdSectorEkonomi) {
		return jUDao.findByKdSectorEkonomi(kdSectorEkonomi);
	}

	@Override
	@Transactional
	public JenisUsaha doSave(JenisUsaha jenisUsaha) {
		JenisUsaha dbData = new JenisUsaha();
		log.info("Jenis Usaha Manager ============");
		loging.info("JenisUsahaManagerImpl accessed");
		log.info("jenisUsaha.getId() : "+jenisUsaha.getId());
		/*if (jenisUsaha.getId() != null) {
			dbData = jUDao.findOne(jenisUsaha.getId());
			ILock lock = hazelcastInstance.getLock("Jenis Usaha - " + jenisUsaha.getId());
			try {
			lock.lock();
			loging.info("Hazelcast Lock accessed");*/
				dbData.setTujuanPembiayaan(jenisUsaha.getTujuanPembiayaan());log.info(dbData.getTujuanPembiayaan());
				dbData.setKatBidangUsaha(jenisUsaha.getKatBidangUsaha());log.info(dbData.getKatBidangUsaha());
				dbData.setDescUsaha(jenisUsaha.getDescUsaha());log.info(dbData.getDescUsaha());
				dbData.setKdSectorEkonomi(jenisUsaha.getKdSectorEkonomi());log.info(dbData.getKdSectorEkonomi());
				dbData.setSandiBaruDesc(jenisUsaha.getSandiBaruDesc());log.info(dbData.getSandiBaruDesc());
				jUDao.save(dbData);
			/*}catch(Exception e){
			log.error(e.getMessage());
			} finally {
				lock.unlock();
			}
		}*/
		return dbData;
	}

	@Override
	public List<JenisUsaha> getAllJenisUsaha() {
		return jUDao.findAll(sortByCreatedDate());
	}

	@Override
	public JenisUsaha getJenisUsahaByDate(Date date) {
		return jUDao.findTop1ByCreatedDateOrderByIdDesc(date);
	}

	@Override
	public JenisUsaha findTop1ByCreateDateMoreThanOrderByCreatedDateDesc(Date date) {
		return jUDao.findTop1ByCreatedDateGreaterThanOrderByCreatedDateDesc(date);
	}

	@Override
	public JenisUsaha findTop1ByCreateDateOrderByCreatedDateDesc() {
		return jUDao.findTop1ByOrderByCreatedDateDesc();
	}
	
	private Sort sortByCreatedDate() {
		return new Sort(Sort.Direction.ASC,"createdDate");
	}

}
