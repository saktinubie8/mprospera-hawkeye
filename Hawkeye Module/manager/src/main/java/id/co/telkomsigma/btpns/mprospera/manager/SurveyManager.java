package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.survey.Survey;
import id.co.telkomsigma.btpns.mprospera.model.survey.SurveyPhoto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Date;

public interface SurveyManager {

    void save(Survey sentra);

    void save(SurveyPhoto surveyPhoto);

    SurveyPhoto findPhotoBySurveyId(Long surveyId);

    Survey findBySurveyId(Long surveyId);

    Page<Survey> getAllSurvey(String officeId);

    Page<Survey> getAllSurveyPageable(PageRequest pageRequest, String officeId);

    Page<Survey> findByCreatedDate(Date startDate, Date endDate, String officeId);

    Page<Survey> findByCreatedDatePageable(Date startDate, Date endDate, PageRequest pageRequest, String officeId);

    void clearCache();

}