package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import id.co.telkomsigma.btpns.mprospera.model.sentra.SentraPhoto;

public interface SentraManager {

    void save(Sentra sentra);

    void save(List sentra);

    void savePhoto(SentraPhoto sentraPhoto);

    void saveGroup(Group group);

    boolean isValidSentra(Long sentraId);

    SentraPhoto getSentraPhoto(Long sentraId);

    Sentra findBySentraId(Long sentraId);

    boolean isValidGroup(Long groupId);

    Page<Sentra> getAllSentra(String username, String loc);

    Page<Sentra> getAllSentraPageable(String username, String loc, PageRequest pageRequest);

    Page<Sentra> findByCreatedDate(String username, String loc, Date startDate, Date endDate);

    Page<Sentra> findByCreatedDatePageable(String username, String loc, Date startDate, Date endDate, PageRequest pageRequest);

    List<Group> getGroupBySentraId(Long sentraId);

    Group getGroupByGroupId(Long groupId);

    Group getGroupByCustomerId(Long customerId);

    Sentra getSentraByGroupId(Long groupId);

    void clearCache();

    Sentra findByRrn(String rrn);

    Group findGroupByRrn(String rrn);

    List<Sentra> findAll();

    List<Sentra> findIsDeletedSentraList();

    List<Group> findIsDeletedSentraGroupList();

}