package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.survey.ProsperaSurveyQuestion;
import id.co.telkomsigma.btpns.mprospera.model.survey.ProsperaSurveyType;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProsperaSurveyQuestionDao extends JpaRepository<ProsperaSurveyQuestion, Long> {

    List<ProsperaSurveyQuestion> findBySurveyType(ProsperaSurveyType type);

}