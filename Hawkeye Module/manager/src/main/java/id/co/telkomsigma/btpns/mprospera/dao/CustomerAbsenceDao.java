package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerWithAbsence;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Dzulfiqar on 29/04/2017.
 */
public interface CustomerAbsenceDao extends JpaRepository<CustomerWithAbsence, Long> {

    CustomerWithAbsence findByCustomerId(Long id);

}