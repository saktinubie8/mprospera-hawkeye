package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.loan.*;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Date;
import java.util.List;

public interface LoanManager {

    void save(Loan loan);

    void save(LoanDeviation loanDeviation);

    void save(DeviationPhoto deviationPhoto);

    void save(DeviationOfficerMapping deviationOfficerMapping);

    DeviationOfficerMapping findByDeviationAndLevel(Long deviationId, String level);

    DeviationOfficerMapping findTopByDeviationId(Long deviationId);

    DeviationOfficerMapping findTopByDeviationIdAndStatus(Long deviationId);

    int countDeviationWithStatus(Long id);

    int countDeviationWaiting(Long id);

    DeviationPhoto findByDeviationId(Long deviationId);

    LoanDeviation findByLoanId(Long loanId);

    LoanDeviation findByAp3rId(Long ap3rId, Boolean isDeleted);

    LoanDeviation findById(Long deviationId);

    List<LoanDeviation> findByBatch(String batch);

    List<LoanDeviation> findAll();

    List<DeviationOfficerMapping> findByLoanDeviationId(String deviationId);

    Boolean isValidLoan(Long loanId);

    Page<Loan> findByCreatedDate(Date startDate, Date endDate, String officeId);

    Page<Loan> findByCreatedDatePageable(Date startDate, Date endDate, PageRequest pageRequest, String officeId);

    List<LoanWithRemainingPrincipal> getAllLoanWithRemainingPrincipal(String username);

    List<LoanWithRemainingPrincipal> getAllLoanWithRemainingPrincipalPageable(String username, String page, String getCountData);

    List<LoanWithRemainingPrincipal> findAllLoanWithRemainingPrincipalByCreatedDate(String username, Date startDate, Date endDate);

    List<LoanWithRemainingPrincipal> findAllLoanWithRemainingPrincipalByCreatedDatePageable(String username, String page, String getCountData, Date startDate, Date endDate);

    void clearCache();

    Loan getLoanByRrn(String rrn);

    Loan getLoanByAp3rId(Long ap3rId);

    Loan findById(long parseLong);

    LoanWithEmergencyFund findLoanWithEmergencyFundByLoanId(Long loanId);

    List<DeviationCode> findDeviationCode();

    List<LoanDeviation> findIsDeletedDeviationList();

    List<Loan> findIsDeletedLoanList();

    List<Loan> findByCustomerAndStatus(Customer customer, String status);

    List<LoanDeviation> findAllDeviationIsNotApproved();

    List<LoanDeviation> findDeviationByMs(Location locationId);

    List<LoanDeviation> findDeviationByUserNonMs(Long officer);

    LoanDeviation findDeviationByAp3rId(Long ap3rId);

    Loan findByAp3rAndCustomer(Long ap3rId, Customer customer);

}