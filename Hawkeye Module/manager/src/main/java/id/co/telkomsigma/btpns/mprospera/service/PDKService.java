package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.PDKManager;
import id.co.telkomsigma.btpns.mprospera.model.pdk.PelatihanDasarKeanggotaan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("pdkService")
public class PDKService extends GenericService {

    @Autowired
    @Qualifier("pdkManager")
    private PDKManager pdkManager;

    public Boolean isValidPdk(String pdkId) {
        return pdkManager.isValidpdk(pdkId);
    }

    public PelatihanDasarKeanggotaan findById(String id) {
        return pdkManager.findById(id);
    }

}