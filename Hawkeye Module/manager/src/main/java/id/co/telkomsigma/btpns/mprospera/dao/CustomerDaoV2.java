package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.customer.CycleNumberAndCustomer;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public interface CustomerDaoV2 extends JpaRepository<Customer, Long> {

    Integer countByAssignedUsernameAndIsDeletedFalse(String assignedUsername);

    Integer countByCustomerIdAndIsDeletedFalse(long customerId);

    Customer findTopBySwIdAndIsDeletedFalse(Long swId);

    Customer findByRrnAndIsDeletedFalse(String rrn);
    
    
    @Query("SELECT c FROM Customer c INNER JOIN c.sentraLocation l WHERE l.locationId = (:loc) AND c.isDeleted = false")
    List<CycleNumberAndCustomer> findByLocId(@Param("loc") String loc, Pageable pageRequest);

    @Query("SELECT c FROM Customer c INNER JOIN c.sentraLocation l WHERE l.locationId = :loc AND c.approvedDate between :startDate AND :endDate AND c.isDeleted = false")
    List<CycleNumberAndCustomer> findByLocIdAndCreatedDateBetween(@Param("loc") String loc, @Param("startDate") Date start,
                                                    @Param("endDate") Date end, Pageable pageable);

    @Query("SELECT c FROM Customer c " + "INNER JOIN c.group g " + ",Sentra st "
            + "WHERE st.sentraId = g.sentra AND st.sentraId = (:sentraId) AND c.isDeleted = false")
    List<Customer> findBySentraId(@Param("sentraId") Long sentraId);

    @Query("select c from Loan la inner join la.customer c " +
            " , AP3R a" +
            " , LoanDeviation ld " +
            " , DeviationOfficerMapping dom " +
            " where a.ap3rId = la.ap3rId and ld.ap3rId = a.ap3rId " +
            " and dom.deviationId = ld.deviationId " +
            " and dom.officer = :userNonMs and c.isDeleted = false")
    List<Customer> findNewCustomerByLoanDeviation(@Param("userNonMs") Long userNonMs);

    @Query("select c from Customer c , SurveyWawancara sw " +
            " , AP3R a " +
            " , LoanDeviation ld " +
            " , DeviationOfficerMapping dom " +
            " where c.customerId = sw.customerId and a.swId = sw.swId " +
            " and ld.ap3rId = a.ap3rId and dom.deviationId = ld.deviationId " +
            " and dom.officer = :userNonMs and c.isDeleted = false")
    List<Customer> findExistCustomerByLoanDeviation(@Param("userNonMs") Long userNonMs);

    @Query("select c from Customer c where c.customerId in " +
            "(select l.customer.customerId from Loan l where l.ap3rId in " +
            "(select a.ap3rId from AP3R a where a.ap3rId in " +
            "(select ld.ap3rId from LoanDeviation ld))) and c.createdBy in :userIdList AND c.isDeleted = false")
    List<Customer> findNewCustomerByLoanDeviationAndWisma(@Param("userIdList") List<String> userIdList);

    @Query("select c from Customer c where c.customerId in " +
            "(select sw.customerId from SurveyWawancara sw where sw.swId in " +
            "(select a.swId from AP3R a where a.ap3rId in " +
            "(select ld.ap3rId from LoanDeviation ld))) and c.createdBy in :userIdList AND c.isDeleted = false")
    List<Customer> findExistCustomerByLoanDeviationAndWisma(@Param("userIdList") List<String> userIdList);

    @Query("select c from Loan la inner join la.customer c " +
            " , AP3R a" +
            " , LoanDeviation ld " +
            " , DeviationOfficerMapping dom " +
            " where a.ap3rId = la.ap3rId and ld.ap3rId = a.ap3rId " +
            " and dom.deviationId = ld.deviationId " +
            " and dom.officer = :userNonMs and c.isDeleted = false and ld.batch = :batch")
    List<Customer> findNewCustomerByLoanDeviationWithBatch(@Param("userNonMs") Long userNonMs, @Param("batch") String batch);

    @Query("select c from Customer c , SurveyWawancara sw " +
            " , AP3R a " +
            " , LoanDeviation ld " +
            " , DeviationOfficerMapping dom " +
            " where c.customerId = sw.customerId and a.swId = sw.swId " +
            " and ld.ap3rId = a.ap3rId and dom.deviationId = ld.deviationId " +
            " and dom.officer = :userNonMs and c.isDeleted = false and ld.batch = :batch")
    List<Customer> findExistCustomerByLoanDeviationWithBatch(@Param("userNonMs") Long userNonMs, @Param("batch") String batch);

    @Query("select c from Customer c where c.customerId in " +
            "(select l.customer.customerId from Loan l where l.ap3rId in " +
            "(select a.ap3rId from AP3R a where a.ap3rId in " +
            "(select ld.ap3rId from LoanDeviation ld where ld.batch = :batch))) and c.createdBy in :userIdList AND c.isDeleted = false")
    List<Customer> findNewCustomerByLoanDeviationAndWismaWithBatch(@Param("userIdList") List<String> userIdList, @Param("batch") String batch);

    @Query("select c from Customer c where c.customerId in " +
            "(select sw.customerId from SurveyWawancara sw where sw.swId in " +
            "(select a.swId from AP3R a where a.ap3rId in " +
            "(select ld.ap3rId from LoanDeviation ld where ld.batch = :batch))) and c.createdBy in :userIdList AND c.isDeleted = false")
    List<Customer> findExistCustomerByLoanDeviationAndWismaWithBatch(@Param("userIdList") List<String> userIdList, @Param("batch") String batch);

    @Query("SELECT c FROM Customer c WHERE c.createdDate>=:startDate AND c.createdDate<:endDate and c.isDeleted = true")
    List<Customer> findIsDeletedCustomerId(@Param("startDate") Date startDate, @Param("endDate") Date endDate);
    
	/*
	 * String a =
	 * "select cus.* from t_customer as cus inner join m_location as loc " +
	 * "on cus.sentra_location=loc.id where cus.sentra_location=?1 and cus.is_deleted is not null"
	 * ;
	 * 
	 * @Query(nativeQuery=true,value=a) List<Object> objectCustomer(String loc);
	 * 
	 * @Query(nativeQuery=true,value=a) ArrayList<Object> objectCustomer1(String
	 * loc);
	 * 
	 * String CycleNumberAndCustomer =
	 * "select  c.cycle_number,a.* from t_customer a inner join m_location d " +
	 * "on a.sentra_location=d.id left outer join " +
	 * "(t_loan_account b inner join t_loan_product c " +
	 * "on b.product_id=c.id and b.status='ACTIVE' " +
	 * "and c.loan_type='TipePembiayaan-PembiayaanBaru' " +
	 * "and c.tujuan_pembiayaan='Modal Kerja' " +
	 * "and c.status='ProductStatus-Active') " + "on a.id=b.customer_id " +
	 * "WHERE d.id=?1";
	 * 
	 * @Query(nativeQuery=true,value=CycleNumberAndCustomer,name=
	 * "CustomerCycleNumberMapping")
	 * List<id.co.telkomsigma.btpns.mprospera.model.customer.CycleNumberAndCustomer>
	 * objectCustomer2(String loc);
	 */

}