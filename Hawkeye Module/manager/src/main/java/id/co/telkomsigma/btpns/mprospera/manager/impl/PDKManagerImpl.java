package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.PDKDao;
import id.co.telkomsigma.btpns.mprospera.dao.PDKDetailDao;
import id.co.telkomsigma.btpns.mprospera.manager.PDKManager;
import id.co.telkomsigma.btpns.mprospera.model.pdk.PelatihanDasarKeanggotaan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service("pdkManager")
public class PDKManagerImpl implements PDKManager {

    @Autowired
    PDKDao pdkDao;

    @Override
    @Cacheable(value = "hwk.pdk.isValidpdk", unless = "#result == null")
    public Boolean isValidpdk(String pdkId) {
        // TODO Auto-generated method stub
        Integer count = pdkDao.countByPdkId(Long.parseLong(pdkId));
        if (count > 0)
            return true;
        else
            return false;
    }

    @Override
    @CacheEvict(value = {"hwk.pdk.isValidpdk", "hwk.pdk.findById"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub
    }

    @Override
    @Cacheable(value = "hwk.pdk.findById", unless = "#result == null")
    public PelatihanDasarKeanggotaan findById(String id) {
        // TODO Auto-generated method stub
        return pdkDao.findByPdkId(Long.parseLong(id));
    }

}