package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.loan.DeviationPhoto;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Dzulfiqar on 10/05/2017.
 */
public interface DeviationPhotoDao extends JpaRepository<DeviationPhoto, Long> {

    DeviationPhoto findByDeviationId(Long deviationId);

}