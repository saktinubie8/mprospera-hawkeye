package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.pdk.PelatihanDasarKeanggotaan;

public interface PDKManager {

    Boolean isValidpdk(String pdkId);

    PelatihanDasarKeanggotaan findById(String id);

    void clearCache();

}