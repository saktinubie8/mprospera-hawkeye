package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParamDao extends JpaRepository<SystemParameter, String> {

    SystemParameter findByParamName(String paramName);

}