package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.sda.AreaProvince;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AreaProvinceDao extends JpaRepository<AreaProvince, String> {

    @Query("SELECT a FROM AreaProvince a WHERE a.areaId  = :areaId ")
    AreaProvince findProvinceById(@Param("areaId") String areaId);

    AreaProvince findTopByAreaNameLikeIgnoreCase(String areaName);

}