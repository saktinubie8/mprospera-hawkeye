package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import id.co.telkomsigma.btpns.mprospera.model.sw.SWProductMapping;

public interface SwProductMapDao extends JpaRepository<SWProductMapping, Long> {

    @Query("SELECT s FROM SWProductMapping s WHERE s.swId = :swId AND s.deleted = false")
    List<SWProductMapping> findSwProductMapBySwId(@Param("swId") Long sw);

    SWProductMapping findByLocalId(String localId);

}