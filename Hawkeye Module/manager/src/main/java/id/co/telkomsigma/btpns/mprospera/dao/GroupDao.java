package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface GroupDao extends JpaRepository<Group, Long> {

    Integer countByGroupId(long groupId);

    @Query("SELECT g FROM Group g WHERE g.isDeleted = false and g.sentra.sentraId = :sentraId")
    List<Group> getGroupBySentraId(@Param("sentraId") Long sentraId);

    @Query("SELECT g FROM Group g WHERE g.isDeleted = false and g.id = :groupId")
    Group getGroupByGroupId(@Param("groupId") Long groupId);

    @Query("SELECT g FROM Group g where g.groupId in "
            + "(SELECT c.group.groupId FROM Customer c where c.customerId= :customerId)")
    Group getGroupByCustomerId(@Param("customerId") Long customerId);

    Group findByRrn(String Rrn);

    @Query("SELECT g FROM Group g WHERE g.createdDate>=:startDate AND g.createdDate<:endDate and g.isDeleted = true")
    List<Group> findIsDeletedSentraGroupId(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

}