package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerWithAbsence;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface CustomerManager {

    Page<Customer> getAllCustomerByLoc(String loc, PageRequest pageRequest);

    Page<Customer> getAllCustomerByLoc(String loc);

    Page<Customer> getAllCustomerByLocAndCreatedDate(String loc, Date parse, Date parse2);

    Page<Customer> getAllCustomerByLocAndCreatedDate(String loc, Date parse, Date parse2, PageRequest pageRequest);

    Integer countCustomerByUsername(String username);

    CustomerWithAbsence findByCustomerId(Long customerId);

    Boolean isValidCustomer(String customerId);

    void clearCache();

    void save(Customer customer);

    Customer getBySwId(Long swId);

    Customer getByRrn(String rrn);

    Customer findById(long parseLong);

    void delete(Customer customer);

    List<Customer> getNewCustomerLoanDeviation(Long userId);

    List<Customer> getExistCustomerLoanDeviation(Long userId);

    List<Customer> getNewCustomerLoanDeviationAndWisma(List<String> userIdList);

    List<Customer> getExistCustomerLoanDeviationAndWisma(List<String> userIdList);

    List<Customer> getNewCustomerLoanDeviationWithBatch(Long userId, String batch);

    List<Customer> getExistCustomerLoanDeviationWithBatch(Long userId, String batch);

    List<Customer> getNewCustomerLoanDeviationAndWismaWithBatch(List<String> userIdList, String batch);

    List<Customer> getExistCustomerLoanDeviationAndWismaWithBatch(List<String> userIdList, String batch);

    List<Customer> findIsDeletedCustomerList();

}