package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.dao.GroupDao;
import id.co.telkomsigma.btpns.mprospera.dao.SentraDao;
import id.co.telkomsigma.btpns.mprospera.dao.SentraPhotoDao;
import id.co.telkomsigma.btpns.mprospera.manager.SentraManager;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import id.co.telkomsigma.btpns.mprospera.model.sentra.SentraPhoto;

@Service("sentraManager")
public class SentraManagerImpl implements SentraManager {

    @Autowired
    private SentraDao sentraDao;

    @Autowired
    private SentraPhotoDao sentraPhotoDao;

    @Autowired
    private GroupDao groupDao;

    @Override
    @Cacheable(value = "hwk.sentra.isValidSentra", unless = "#result == null")
    public boolean isValidSentra(Long sentraId) {
        // TODO Auto-generated method stub
        Integer count = sentraDao.countBySentraId(sentraId);
        if (count > 0)
            return true;
        else
            return false;
    }

    @Override
    @Cacheable(value = "hwk.sentra.findBySentraId", unless = "#result == null")
    public Sentra findBySentraId(Long sentraId) {
        // TODO Auto-generated method stub
        return sentraDao.findBySentraId(sentraId);
    }

    @Override
    @Cacheable(value = "hwk.sentra.getAllSentra", unless = "#result == null")
    public Page<Sentra> getAllSentra(String username, String loc) {
        // TODO Auto-generated method stub
        List<String> paramList = new ArrayList<>();
        paramList.add(WebGuiConstant.STATUS_APPROVED);
        paramList.add(WebGuiConstant.STATUS_DRAFT);
        return sentraDao.getAllSentraWithLoc(loc, new PageRequest(0, Integer.MAX_VALUE), paramList);
    }

    @Override
    @Cacheable(value = "hwk.sentra.getAllSentraPageable", unless = "#result == null")
    public Page<Sentra> getAllSentraPageable(String username, String loc, PageRequest pageRequest) {
        // TODO Auto-generated method stub
        List<String> paramList = new ArrayList<>();
        paramList.add(WebGuiConstant.STATUS_APPROVED);
        paramList.add(WebGuiConstant.STATUS_DRAFT);
        return sentraDao.getAllSentraWithLoc(loc, pageRequest, paramList);
    }

    @Override
    @Cacheable(value = "hwk.sentra.findSentraByCreatedDate", unless = "#result == null")
    public Page<Sentra> findByCreatedDate(String username, String loc, Date startDate, Date endDate) {
        // TODO Auto-generated method stub
        List<String> paramList = new ArrayList<>();
        paramList.add(WebGuiConstant.STATUS_APPROVED);
        paramList.add(WebGuiConstant.STATUS_DRAFT);
        return sentraDao.findByCreatedDateWithLoc(loc, startDate, endDate, new PageRequest(0, Integer.MAX_VALUE), paramList);
    }

    @Override
    @Cacheable(value = "hwk.sentra.findSentraByCreatedDatePageable", unless = "#result == null")
    public Page<Sentra> findByCreatedDatePageable(String username, String loc, Date startDate, Date endDate, PageRequest pageRequest) {
        // TODO Auto-generated method stub
        List<String> paramList = new ArrayList<>();
        paramList.add(WebGuiConstant.STATUS_APPROVED);
        paramList.add(WebGuiConstant.STATUS_DRAFT);
        return sentraDao.findByCreatedDateWithLoc(loc, startDate, endDate, pageRequest, paramList);
    }

    @Override
    @Cacheable(value = "hwk.sentra.getSentraByGroupId", unless = "#result == null")
    public Sentra getSentraByGroupId(Long groupId) {
        // TODO Auto-generated method stub
        return sentraDao.getSentraByGroupId(groupId);
    }

    @Override
    @Cacheable(value = "hwk.sentra.getSentraByRrn", unless = "#result == null")
    public Sentra findByRrn(String rrn) {
        // TODO Auto-generated method stub
        return sentraDao.findByRrn(rrn);
    }

    @Override
    @Cacheable(value = "hwk.sentra.findAllSentraList", unless = "#result == null")
    public List<Sentra> findAll() {
        // TODO Auto-generated method stub
        return sentraDao.findAll();
    }

    @Override
    @Cacheable(value = "hwk.sentra.findDeletedSentraList", unless = "#result == null")
    public List<Sentra> findIsDeletedSentraList() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -7);
        Date dateBefore7Days = cal.getTime();
        List<String> paramList = new ArrayList<>();
        paramList.add(WebGuiConstant.STATUS_APPROVED);
        paramList.add(WebGuiConstant.STATUS_DRAFT);
        return sentraDao.findIsDeletedSentraId(dateBefore7Days, new Date(), paramList);
    }

    @Override
    @Caching(
            evict = {
                    @CacheEvict(value = "hwk.sentra.findBySentraId", key = "#sentra.sentraId", beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.getAllSentra", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.getAllSentraPageable", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.findSentraByCreatedDate", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.findSentraByCreatedDatePageable", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.isValidSentra", key = "#sentra.sentraId", beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.getSentraByGroupId", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.findAllSentraList", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.getSentraByRrn", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.findDeletedSentraList", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "vsn.sentra.findByProsperaId", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "irn.sentra.findBySentraId", key = "#sentra.sentraId", beforeInvocation = true),
                    @CacheEvict(value = "irn.sentra.findByProsperaId", key = "#sentra.prosperaId", beforeInvocation = true),
                    @CacheEvict(value = "irn.sentra.getSentraByWismaId", key = "#sentra.locationId", beforeInvocation = true),
                    @CacheEvict(value = "irn.sentra.getSentraByWismaIdPaging", key = "#sentra.locationId", beforeInvocation = true),
                    @CacheEvict(value = "wln.sentra.findByCreatedDate", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "wln.sentra.findByCreatedDatePageable", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "wln.sentra.findByRrn", key = "#sentra.rrn", beforeInvocation = true),
                    @CacheEvict(value = "wln.sentra.findAll", allEntries = true, beforeInvocation = true)
            }
    )
    public void save(Sentra sentra) {
        sentraDao.save(sentra);
    }

    @Override
    @Caching(
            evict = {
                    @CacheEvict(value = "hwk.sentra.findBySentraId", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.getAllSentra", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.getAllSentraPageable", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.findSentraByCreatedDate", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.findSentraByCreatedDatePageable", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.isValidSentra", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.getSentraByGroupId", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.findAllSentraList", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.getSentraByRrn", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.sentra.findDeletedSentraList", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "vsn.sentra.findByProsperaId", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "irn.sentra.findBySentraId", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "irn.sentra.findByProsperaId", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "irn.sentra.getSentraByWismaId", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "irn.sentra.getSentraByWismaIdPaging", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "wln.sentra.findByCreatedDate", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "wln.sentra.findByCreatedDatePageable", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "wln.sentra.findByRrn", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "wln.sentra.findAll", allEntries = true, beforeInvocation = true)
            }
    )
    public void save(List sentra) {
        // TODO Auto-generated method stub
        sentraDao.save(sentra);
    }

    @Override
    @Cacheable(value = "hwk.sentra.photo.getSentraPhoto", unless = "#result == null")
    public SentraPhoto getSentraPhoto(Long sentraId) {
        // TODO Auto-generated method stub
        return sentraPhotoDao.getSentraPhoto(sentraId);
    }

    @Override
    @Caching(
            evict = {
                    @CacheEvict(value = "hwk.sentra.photo.getSentraPhoto", allEntries = true, beforeInvocation = true)
            }
    )
    public void savePhoto(SentraPhoto sentraPhoto) {
        // TODO Auto-generated method stub
        sentraPhotoDao.save(sentraPhoto);
    }

    @Override
    @Cacheable(value = "hwk.group.isValidGroup", unless = "#result == null")
    public boolean isValidGroup(Long groupId) {
        // TODO Auto-generated method stub
        Integer count = groupDao.countByGroupId(groupId);
        if (count > 0)
            return true;
        else
            return false;
    }

    @Override
    @Cacheable(value = "hwk.group.getGroupBySentraId", unless = "#result == null")
    public List<Group> getGroupBySentraId(Long sentraId) {
        // TODO Auto-generated method stub
        return groupDao.getGroupBySentraId(sentraId);
    }

    @Override
    @Cacheable(value = "hwk.group.getGroupByGroupId", unless = "#result == null")
    public Group getGroupByGroupId(Long groupId) {
        // TODO Auto-generated method stub
        return groupDao.getGroupByGroupId(groupId);
    }

    @Override
    @Cacheable(value = "hwk.group.getGroupByCustomerId", unless = "#result == null")
    public Group getGroupByCustomerId(Long customerId) {
        // TODO Auto-generated method stub
        return groupDao.getGroupByCustomerId(customerId);
    }

    @Override
    @Cacheable(value = "hwk.group.getGroupByRrn", unless = "#result == null")
    public Group findGroupByRrn(String rrn) {
        // TODO Auto-generated method stub
        return groupDao.findByRrn(rrn);
    }

    @Override
    @Cacheable(value = "hwk.group.findDeletedGroupList", unless = "#result == null")
    public List<Group> findIsDeletedSentraGroupList() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -7);
        Date dateBefore7Days = cal.getTime();
        return groupDao.findIsDeletedSentraGroupId(dateBefore7Days, new Date());
    }

    @Override
    @Caching(
            evict = {
                    @CacheEvict(value = "hwk.group.getGroupBySentraId", key = "#group.sentra.sentraId", beforeInvocation = true),
                    @CacheEvict(value = "hwk.group.getGroupByGroupId", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.group.getGroupByCustomerId", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.group.getGroupByRrn", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.group.isValidGroup", allEntries = true, beforeInvocation = true),
                    @CacheEvict(value = "hwk.group.findDeletedGroupList", allEntries = true, beforeInvocation = true)
            }
    )
    public void saveGroup(Group group) {
        // TODO Auto-generated method stub
        groupDao.save(group);
    }

    @Override
    @CacheEvict(value = {"hwk.sentra.findBySentraId", "hwk.sentra.getAllSentra", "hwk.sentra.getAllSentraPageable",
            "hwk.sentra.findSentraByCreatedDate", "hwk.sentra.findSentraByCreatedDatePageable",
            "hwk.sentra.isValidSentra", "hwk.sentra.getSentraByGroupId", "hwk.sentra.findAllSentraList",
            "hwk.sentra.getSentraByRrn", "hwk.sentra.findDeletedSentraList", "hwk.group.getGroupBySentraId",
            "hwk.group.getGroupByGroupId", "hwk.group.getGroupByCustomerId", "hwk.group.getGroupByRrn",
            "hwk.group.isValidGroup", "hwk.group.findDeletedGroupList", "hwk.sentra.photo.getSentraPhoto"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub
    }

}