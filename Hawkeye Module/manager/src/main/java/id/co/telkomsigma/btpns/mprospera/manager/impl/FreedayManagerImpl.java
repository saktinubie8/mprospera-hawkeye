package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.dao.FreedayDao;
import id.co.telkomsigma.btpns.mprospera.manager.FreedayManager;

@Service("freedayManager")
public class FreedayManagerImpl implements FreedayManager {

    @Autowired
    private FreedayDao dao;
    
	@Override
	public int countHoliday(Date tglAwal, Date tglAkhir) {
		return dao.getHoliday(tglAwal, tglAkhir);
	}

}
