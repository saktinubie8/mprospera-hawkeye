package id.co.telkomsigma.btpns.mprospera.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by daniel on 3/31/15.
 */
public interface UserDao extends JpaRepository<User, String> {

    /**
     * Get single user by username
     *
     * @param username
     * @return single User object
     */
    User findByUsername(String username);

    User findByUserId(Long userId);

    List<User> findByOfficeCode(String locId);

    @Query("SELECT u.sessionKey FROM User u WHERE username = :username")
    String findSessionKeyByUsername(@Param("username") String username);

}