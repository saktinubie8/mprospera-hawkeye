package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.HolidayDao;
import id.co.telkomsigma.btpns.mprospera.manager.HolidayManager;
import id.co.telkomsigma.btpns.mprospera.model.parameter.Holiday;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service("wowibManager")
public class HolidayManagerImpl implements HolidayManager {

    @Autowired
    private HolidayDao dao;

    @Override
    @Cacheable(value = "hwk.holiday.findById", unless = "#result == null")
    public Holiday findById(Long id) {
        return dao.findOne(id);
    }

    @Override
    @Cacheable(value = "hwk.holiday.findAll", unless = "#result == null")
    public List<Holiday> findAll() {
        return dao.findAll();
    }
}