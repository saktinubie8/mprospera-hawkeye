package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.pdk.PDKDetail;
import id.co.telkomsigma.btpns.mprospera.model.pdk.PDKDetailId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PDKDetailDao extends JpaRepository<PDKDetail, PDKDetailId> {

}