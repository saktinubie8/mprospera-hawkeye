package id.co.telkomsigma.btpns.mprospera.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import id.co.telkomsigma.btpns.mprospera.model.sw.BusinessTypeAP3R;

public interface BusinessTypeAp3rDao extends JpaRepository<BusinessTypeAP3R, Long> {

    BusinessTypeAP3R findByBusinessId(Long businessId);
    BusinessTypeAP3R findBySubBiCode(String subBiCode);

}