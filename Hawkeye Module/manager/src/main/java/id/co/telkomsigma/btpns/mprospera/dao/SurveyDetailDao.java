package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.survey.SurveyDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SurveyDetailDao extends JpaRepository<SurveyDetail, Long> {

}