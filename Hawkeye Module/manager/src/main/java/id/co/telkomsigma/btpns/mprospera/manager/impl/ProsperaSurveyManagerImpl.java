package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.dao.ProsperaQuestionDao;
import id.co.telkomsigma.btpns.mprospera.dao.ProsperaSurveyQuestionDao;
import id.co.telkomsigma.btpns.mprospera.dao.ProsperaSurveyTypeDao;
import id.co.telkomsigma.btpns.mprospera.manager.ProsperaSurveyManager;
import id.co.telkomsigma.btpns.mprospera.model.survey.ProsperaSurveyQuestion;
import id.co.telkomsigma.btpns.mprospera.model.survey.ProsperaSurveyType;

@Service("prospearSurveyManager")
public class ProsperaSurveyManagerImpl implements ProsperaSurveyManager {

    @Autowired
    private ProsperaSurveyQuestionDao prosperaSurveyQuestionDao;

    @Autowired
    private ProsperaQuestionDao prosperaQuestionDao;

    @Autowired
    private ProsperaSurveyTypeDao prosperaSurveyTypeDao;

    @Override
    @Cacheable(value = "hwk.survey.findByProsperaSurveyTypeId", unless = "#result == null")
    public ProsperaSurveyType findByProsperaSurveyTypeId(long id) {
        // TODO Auto-generated method stub
        return prosperaSurveyTypeDao.findOne(id);
    }

    @Override
    @Cacheable(value = "hwk.survey.findAllSurveyType", unless = "#result == null")
    public List<ProsperaSurveyType> findAllSurveyType() {
        // TODO Auto-generated method stub
        return prosperaSurveyTypeDao.findAll();
    }

    @Override
    @CacheEvict(value = {"hwk.survey.findAllSurveyType", "hwk.survey.findByProsperaSurveyTypeId", "hwk.survey.findProsperaSurveyQuestionByType"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub

    }

    @Override
    @Cacheable(value = "hwk.survey.findProsperaSurveyQuestionByType", unless = "#result == null")
    public List<ProsperaSurveyQuestion> findProsperaSurveyQuestionByType(ProsperaSurveyType type) {
        // TODO Auto-generated method stub
        return prosperaSurveyQuestionDao.findBySurveyType(type);
    }

}