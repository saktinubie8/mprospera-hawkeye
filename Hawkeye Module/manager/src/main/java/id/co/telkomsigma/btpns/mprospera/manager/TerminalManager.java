package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;

public interface TerminalManager {

    Terminal getTerminalByImei(String imei);

    Terminal updateTerminal(Terminal terminal);

    void clearCache();

}