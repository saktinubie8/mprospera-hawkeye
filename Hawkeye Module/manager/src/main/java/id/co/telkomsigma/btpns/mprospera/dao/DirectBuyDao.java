package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import id.co.telkomsigma.btpns.mprospera.model.sw.DirectBuyThings;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;

public interface DirectBuyDao extends JpaRepository<DirectBuyThings, Long> {

    List<DirectBuyThings> findBySwId(SurveyWawancara sw);

}