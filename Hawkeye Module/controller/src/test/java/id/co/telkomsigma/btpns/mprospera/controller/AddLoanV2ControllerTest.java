package id.co.telkomsigma.btpns.mprospera.controller;

import id.co.telkomsigma.btpns.mprospera.controller.webservice.RESTClient;
import id.co.telkomsigma.btpns.mprospera.controller.webservice.WebServiceAddLoanV2Controller;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.jenisusaha.JenisUsaha;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import id.co.telkomsigma.btpns.mprospera.model.sw.AP3R;
import id.co.telkomsigma.btpns.mprospera.model.sw.BusinessTypeAP3R;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.request.ESBLoanRequest;
import id.co.telkomsigma.btpns.mprospera.request.LoanRequest;
import id.co.telkomsigma.btpns.mprospera.response.AddLoanResponse;
import id.co.telkomsigma.btpns.mprospera.response.LoanResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

@RunWith(SpringRunner.class)
public class AddLoanV2ControllerTest {

    @TestConfiguration
    static class AddLoanV2ControllerTestContextConfiguration {
        @Bean
        public WebServiceAddLoanV2Controller webServiceAddLoanV2Controller() {
            return new WebServiceAddLoanV2Controller();
        }

        @Bean
        protected Locale locale() {
            return new Locale("id", "ID");
        }
    }

    @Autowired
    private WebServiceAddLoanV2Controller webServiceAddLoanV2Controller;

    @MockBean
    private WSValidationService wsValidationService;
    
    @MockBean
    private FreedayService FreedayService;

    @MockBean
    private TerminalActivityService terminalActivityService;

    @MockBean
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @MockBean
    private TerminalService terminalService;

    @MockBean
    private RESTClient restClient;

    @MockBean
    private SWService swService;

    @MockBean
    private PDKService pdkService;

    @MockBean
    private AreaService areaService;

    @MockBean
    private CustomerService customerService;

    @MockBean
    private AP3RService ap3RService;

    @MockBean
    private AuditLogService auditLogService;

    @MockBean
    private SentraService sentraService;

    @MockBean
    private LoanService loanService;

    @MockBean
    private UserService userService;
    
    @MockBean
    private JenisUsahaService jenisUsahaService;

    String username = "w0211f";
    String imei = "865800023782128";
    String apkVersion = "3.70";
    String sessionKey = "e2a0bfc8-7598-4a64-8310-d91b0f910a76";
    LoanResponse loanResponse = new LoanResponse();
    String swId = "15268717227705181584904541220020";
    String ap3rId = "15283479458505181584904541220192";
    Customer customer = new Customer();
    SurveyWawancara sw = new SurveyWawancara();
    AP3R ap3r = new AP3R();
    LoanRequest loanRequest = new LoanRequest();
    AddLoanResponse addLoanResponse = new AddLoanResponse();
    BusinessTypeAP3R businessTypeAP3R = new BusinessTypeAP3R();
    Loan loan = new Loan();
    Group group = new Group();
    Sentra sentra = new Sentra();
    Location location = new Location();
    User user = new User();
    JenisUsaha jenisUsaha = new JenisUsaha();

    @Before
    public void setUp() {
        loanResponse.setResponseCode("00");
        loanResponse.setLoanId("12345");
        customer.setCustomerId(Long.parseLong("12345"));
        customer.setCustomerCifNumber("13215325");
        loan.setCustomer(customer);
        loan.setLoanId(Long.parseLong("132142"));
        sentra.setSentraId(Long.parseLong("1241351"));
        sentra.setLocationId("335");
        group.setSentra(sentra);
        user.setUserId(Long.parseLong("131353"));
        user.setRoleUser("2");
        businessTypeAP3R.setBusinessId(Long.parseLong("2243"));
        sw.setSwId(Long.parseLong("321434"));
        sw.setGender("0");
        sw.setMaritalStatus("1");
        sw.setWorkType("1");
        sw.setReligion("1");
        sw.setHouseType("1");
        sw.setEducation("1");
        sw.setCustomerRegistrationType('1');
        sw.setBusinessField("1");
        sw.setBusinessAgeMonth(1);
        sw.setBusinessOwnerStatus('1');
        sw.setBusinessAgeYear(1);
        sw.setBusinessAgeMonth(1);
        sw.setTotalIncome(new BigDecimal(1000000));
        sw.setExpenditure(new BigDecimal(100000));
        sw.setNettIncome(new BigDecimal(900000));
        sw.setStatus("APPROVED");
        ap3r.setAp3rId(Long.parseLong("213535"));
        ap3r.setBusinessField("1");
        ap3r.setDueDate(new Date());
        Mockito.when(wsValidationService.wsValidation(username, imei, sessionKey, apkVersion)).thenReturn("00");
        Mockito.when(swService.findSwByLocalId(swId)).thenReturn(sw);
        Mockito.when(userService.findUserByUsername(username)).thenReturn(user);
        Mockito.when(ap3RService.findByLocalId(ap3rId)).thenReturn(ap3r);
        Mockito.when(customerService.getBySwId(sw.getSwId())).thenReturn(customer);
        Mockito.when(ap3RService.sumFunds(ap3r)).thenReturn(new BigDecimal(120000));
        Mockito.when(swService.getBusinessTypeAP3RById(ap3r.getAp3rId())).thenReturn(businessTypeAP3R);
        Mockito.when(swService.isValidSw(sw.getSwId().toString())).thenReturn(true);
        group.setGroupId(Long.parseLong("54264"));
        customer.setGroup(group);
        Mockito.when(sentraService.isValidGroup(group.getGroupId())).thenReturn(true);
        Mockito.when(sentraService.getGroupByGroupId(group.getGroupId().toString())).thenReturn(group);
        Mockito.when(areaService.findLocationById(sentra.getLocationId())).thenReturn(location);
        Mockito.when(jenisUsahaService.getJenisUsahaById(Long.parseLong(ap3r.getBusinessField()))).thenReturn(jenisUsaha);
        try {
            Mockito.when(restClient.addLoan(any(ESBLoanRequest.class))).thenReturn(addLoanResponse);
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        addLoanResponse.setResponseCode("00");
        addLoanResponse.setProspectId("3135235");
    }

    @Test
    public void WhenAddLoan_ThenReturnSuccess() {
        String request = "{\n" +
                "  \"ap3rId\": \"15283479458505181584904541220192\",\n" +
                "  \"customerName\": \"Indun\",\n" +
                "  \"disbursementDate\": \"20180814 00:00:00\",\n" +
                "  \"loanId\": \"\",\n" +
                "  \"note\": \"\",\n" +
                "  \"status\": \"APPROVED\",\n" +
                "  \"swId\": \"15268717227705181584904541220020\",\n" +
                "  \"action\": \"insert\",\n" +
                "  \"latitude\": \"0.0\",\n" +
                "  \"longitude\": \"0.0\",\n" +
                "  \"imei\": \"865800023782128\",\n" +
                "  \"retrievalReferenceNumber\": \"86580002378212815306829667910093\",\n" +
                "  \"sessionKey\": \"e2a0bfc8-7598-4a64-8310-d91b0f910a76\",\n" +
                "  \"tokenKey\": \"\",\n" +
                "  \"transmissionDateAndTime\": \"20180704124246\",\n" +
                "  \"username\": \"w0211f\"\n" +
                "}";
        try {
            LoanResponse result = webServiceAddLoanV2Controller.doAdd(request, apkVersion);
            assertEquals(result.getResponseCode(), loanResponse.getResponseCode());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}