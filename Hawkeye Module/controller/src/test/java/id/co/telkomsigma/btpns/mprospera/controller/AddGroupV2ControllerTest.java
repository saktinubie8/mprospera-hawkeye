package id.co.telkomsigma.btpns.mprospera.controller;

import id.co.telkomsigma.btpns.mprospera.controller.webservice.RESTClient;
import id.co.telkomsigma.btpns.mprospera.controller.webservice.WebServiceAddGroupV2Controller;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import id.co.telkomsigma.btpns.mprospera.request.GroupRequest;
import id.co.telkomsigma.btpns.mprospera.response.GroupResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;

@RunWith(SpringRunner.class)
public class AddGroupV2ControllerTest {

    @TestConfiguration
    static class AddGroupV2ControllerTestContextConfiguration {

        @Bean
        public WebServiceAddGroupV2Controller webServiceAddGroupV2Controller() {
            return new WebServiceAddGroupV2Controller();
        }

        @Bean
        protected Locale locale() {
            return new Locale("id", "ID");
        }

    }

    @Autowired
    private WebServiceAddGroupV2Controller webServiceAddGroupV2Controller;

    @MockBean
    private WSValidationService wsValidationService;

    @MockBean
    private TerminalActivityService terminalActivityService;

    @MockBean
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @MockBean
    private TerminalService terminalService;

    @MockBean
    private RESTClient restClient;

    @MockBean
    private SentraService sentraService;

    @MockBean
    private AuditLogService auditLogService;

    String username = "w0211f";
    String imei = "358525070497940";
    String apkVersion = "3.70";
    String sessionKey = "0beba6ac-2b04-4889-866f-b36dea07aff6";
    String action = "add";
    GroupResponse groupResponse = new GroupResponse();
    Group group = new Group();
    String sentraId = "106100";
    Sentra sentra = new Sentra();

    @Before
    public void setUp() {
        groupResponse.setResponseCode("00");
        groupResponse.setGroupId("12345");
        group.setGroupId(Long.parseLong("12345"));
        Mockito.when(wsValidationService.wsValidation(username, imei, sessionKey, apkVersion)).thenReturn("00");
        try {
            Mockito.when(restClient.processGroup(any(GroupRequest.class), eq(action))).thenReturn(groupResponse);
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        sentra.setSentraId(Long.parseLong(sentraId));
        Mockito.when(sentraService.findBySentraId(sentraId)).thenReturn(sentra);
    }

    @Test
    public void WhenAddGroup_ThenReturnSuccess() {
        String request = "{\"groupId\":\"\",\"groupLeader\":\"\",\"groupName\":\"grup 1\",\"localId\":0,\"sentraId\":\"106100\",\"status\":\"APPROVED\",\"action\":\"insert\",\"latitude\":\"-6.2296838\",\"longitude\":\"106.8275577\",\"imei\":\"358525070497940\",\"retrievalReferenceNumber\":\"86580002378212815306154620360197\",\"sessionKey\":\"0beba6ac-2b04-4889-866f-b36dea07aff6\",\"tokenKey\":\"\",\"transmissionDateAndTime\":\"20180703175742\",\"username\":\"w0211f\"}";
        try {
            GroupResponse result = webServiceAddGroupV2Controller.doAddGroup(request, apkVersion);
            assertEquals(result.getResponseCode(), groupResponse.getResponseCode());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}