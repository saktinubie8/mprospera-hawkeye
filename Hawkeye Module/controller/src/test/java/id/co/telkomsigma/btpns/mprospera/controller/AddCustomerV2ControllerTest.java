package id.co.telkomsigma.btpns.mprospera.controller;

import id.co.telkomsigma.btpns.mprospera.controller.webservice.RESTClient;
import id.co.telkomsigma.btpns.mprospera.controller.webservice.WebServiceAddCustomerV2Controller;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.request.AddCustomerRequest;
import id.co.telkomsigma.btpns.mprospera.response.AddCustomerResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

@RunWith(SpringRunner.class)
public class AddCustomerV2ControllerTest {

    @TestConfiguration
    static class AddCustomerV2ControllerTestContextConfiguration {
        @Bean
        public WebServiceAddCustomerV2Controller webServiceAddCustomerV2Controller() {
            return new WebServiceAddCustomerV2Controller();
        }

        @Bean
        protected Locale locale() {
            return new Locale("id", "ID");
        }
    }

    @Autowired
    private WebServiceAddCustomerV2Controller webServiceAddCustomerV2Controller;

    @MockBean
    private WSValidationService wsValidationService;

    @MockBean
    private TerminalActivityService terminalActivityService;

    @MockBean
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @MockBean
    private TerminalService terminalService;

    @MockBean
    private RESTClient restClient;

    @MockBean
    private SWService swService;

    @MockBean
    private PDKService pdkService;

    @MockBean
    private AreaService areaService;

    @MockBean
    private CustomerService customerService;

    @MockBean
    private AP3RService ap3RService;

    @MockBean
    private AuditLogService auditLogService;

    @MockBean
    private SentraService sentraService;

    String username = "w0211f";
    String imei = "358525070497940";
    String apkVersion = "3.70";
    String sessionKey = "0beba6ac-2b04-4889-866f-b36dea07aff6";
    AddCustomerResponse addCustomerResponse = new AddCustomerResponse();
    String swId = "15266344509735181584904541220194";
    Customer customer = new Customer();
    SurveyWawancara sw = new SurveyWawancara();
    AddCustomerRequest addCustomerRequest = new AddCustomerRequest();
    Group group = new Group();
    Sentra sentra = new Sentra();
    Location location = new Location();

    @Before
    public void setUp() {
        addCustomerResponse.setResponseCode("00");
        addCustomerResponse.setCustomerId("12345");
        customer.setCustomerId(Long.parseLong("12345"));
        addCustomerRequest.setPdkId("50371");
        addCustomerRequest.setGroupId("54264");
        sentra.setSentraId(Long.parseLong("1241351"));
        sentra.setLocationId("335");
        group.setSentra(sentra);
        sw.setSwId(Long.parseLong("321434"));
        sw.setGender("0");
        sw.setMaritalStatus("1");
        sw.setWorkType("1");
        sw.setReligion("1");
        sw.setHouseType("1");
        sw.setEducation("1");
        Mockito.when(wsValidationService.wsValidation(username, imei, sessionKey, apkVersion)).thenReturn("00");
        Mockito.when(swService.findSwByLocalId(swId)).thenReturn(sw);
        Mockito.when(pdkService.isValidPdk(addCustomerRequest.getPdkId())).thenReturn(true);
        Mockito.when(swService.isValidSw(sw.getSwId().toString())).thenReturn(true);
        group.setGroupId(Long.parseLong("54264"));
        customer.setGroup(group);
        Mockito.when(sentraService.isValidGroup(group.getGroupId())).thenReturn(true);
        Mockito.when(sentraService.getGroupByGroupId(group.getGroupId().toString())).thenReturn(group);
        Mockito.when(areaService.findLocationById(sentra.getLocationId())).thenReturn(location);
        try {
            Mockito.when(restClient.addCustomer(any(AddCustomerRequest.class))).thenReturn(addCustomerResponse);
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void WhenAddCustomer_ThenReturnSuccess() {
        String request = "{\n" +
                "  \"customerId\": \"\",\n" +
                "  \"groupId\": \"54264\",\n" +
                "  \"pdkId\": \"50371\",\n" +
                "  \"status\": \"APPROVED\",\n" +
                "  \"swId\": \"15266344509735181584904541220194\",\n" +
                "  \"action\": \"insert\",\n" +
                "  \"latitude\": \"-6.2297979\",\n" +
                "  \"longitude\": \"106.8276486\",\n" +
                "  \"imei\": \"358525070497940\",\n" +
                "  \"retrievalReferenceNumber\": \"86580002378212815306863636090135\",\n" +
                "  \"sessionKey\": \"0beba6ac-2b04-4889-866f-b36dea07aff6\",\n" +
                "  \"tokenKey\": \"\",\n" +
                "  \"transmissionDateAndTime\": \"20180704133923\",\n" +
                "  \"username\": \"w0211f\"\n" +
                "}";
        try {
            AddCustomerResponse result = webServiceAddCustomerV2Controller.doAdd(request, apkVersion);
            assertEquals(result.getResponseCode(), addCustomerResponse.getResponseCode());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}