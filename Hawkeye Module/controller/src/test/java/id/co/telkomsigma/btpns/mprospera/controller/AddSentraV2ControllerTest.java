package id.co.telkomsigma.btpns.mprospera.controller;

import id.co.telkomsigma.btpns.mprospera.controller.webservice.RESTClient;
import id.co.telkomsigma.btpns.mprospera.controller.webservice.WebServiceAddSentraV2Controller;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaDistrict;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaKelurahan;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaProvince;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaSubDistrict;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import id.co.telkomsigma.btpns.mprospera.request.SentraRequest;
import id.co.telkomsigma.btpns.mprospera.response.SentraResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

@RunWith(SpringRunner.class)
public class AddSentraV2ControllerTest {

    @TestConfiguration
    static class AddSentraV2ControllerTestContextConfiguration {
        @Bean
        public WebServiceAddSentraV2Controller webServiceAddSentraV2Controller() {
            return new WebServiceAddSentraV2Controller();
        }

        @Bean
        protected Locale locale() {
            return new Locale("id", "ID");
        }
    }

    @Autowired
    private WebServiceAddSentraV2Controller webServiceAddSentraV2Controller;

    @MockBean
    private WSValidationService wsValidationService;

    @MockBean
    private TerminalActivityService terminalActivityService;

    @MockBean
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @MockBean
    private TerminalService terminalService;

    @MockBean
    private RESTClient restClient;

    @MockBean
    private SentraService sentraService;

    @MockBean
    private AreaService areaService;

    @MockBean
    private AuditLogService auditLogService;

    String username = "w0211f";
    String imei = "358525070497940";
    String apkVersion = "3.70";
    String sessionKey = "0beba6ac-2b04-4889-866f-b36dea07aff6";
    String kelurahanId = "2413513";
    String kecamatanId = "1351351";
    String kotaId = "35235213";
    String provId = "13526362";
    SentraResponse sentraResponse = new SentraResponse();
    Sentra sentra = new Sentra();
    AreaKelurahan kel = new AreaKelurahan();
    AreaSubDistrict kec = new AreaSubDistrict();
    AreaDistrict kota = new AreaDistrict();
    AreaProvince prov = new AreaProvince();

    @Before
    public void setUp() {
        sentraResponse.setResponseCode("00");
        sentraResponse.setSentraId("12345");
        sentra.setSentraId(Long.parseLong("12345"));
        Mockito.when(wsValidationService.wsValidation(username, imei, sessionKey, apkVersion)).thenReturn("00");
        try {
            Mockito.when(restClient.addSentra(any(SentraRequest.class))).thenReturn(sentraResponse);
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        Mockito.when(areaService.findKelurahanById(kelurahanId)).thenReturn(kel);
        kel.setAreaId("2413513");
        kel.setParentAreaId("1351351");
        Mockito.when(areaService.findSubDistrictById(kecamatanId)).thenReturn(kec);
        kec.setAreaId("1351351");
        kec.setParentAreaId("35235213");
        Mockito.when(areaService.findDistrictById(kotaId)).thenReturn(kota);
        kota.setAreaId("35235213");
        kota.setParentAreaId("13526362");
        Mockito.when(areaService.findProvinceById(provId)).thenReturn(prov);
    }

    @Test
    public void WhenAddSentra_ThenReturnSuccess() {
        String request = "{\"areaId\":\"3327080001\",\"localId\":0,\"meetingPlace\":\"Puskesmas Pemalang\",\"meetingTime\":\"09:00\",\"officeCode\":\"334\",\"postcode\":\"52319\",\"prsDay\":1,\"rtRw\":\"04/14\",\"sentraAddress\":\"Pemalang\",\"sentraId\":\"\",\"sentraLeader\":\"\",\"sentraName\":\"Pemalang\",\"sentraOwnerName\":\"Dimas Mardika\",\"startedDate\":\"20180820\",\"status\":\"APPROVED\",\"action\":\"insert\",\"latitude\":\"-6.229458\",\"longitude\":\"106.8279778\",\"imei\":\"358525070497940\",\"retrievalReferenceNumber\":\"35852507049736115344027145270075\",\"sessionKey\":\"0beba6ac-2b04-4889-866f-b36dea07aff6\",\"tokenKey\":\"\",\"transmissionDateAndTime\":\"20180816135834\",\"username\":\"w0211f\"}";
        try {
            SentraResponse result = webServiceAddSentraV2Controller.doAdd(request, apkVersion);
            assertEquals(result.getResponseCode(), sentraResponse.getResponseCode());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}