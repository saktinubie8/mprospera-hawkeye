package id.co.telkomsigma.btpns.mprospera.controller;

import id.co.telkomsigma.btpns.mprospera.controller.webservice.WebServiceSyncDeviationV2Controller;
import id.co.telkomsigma.btpns.mprospera.model.loan.DeviationOfficerMapping;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanDeviation;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.sw.AP3R;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.request.DeviationRequest;
import id.co.telkomsigma.btpns.mprospera.response.ListDeviationResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class SyncDeviasiV2ControllerTest {

    @TestConfiguration
    static class SyncDeviasiV2ControllerTestContextConfiguration {

        @Bean
        public WebServiceSyncDeviationV2Controller webServiceSyncDeviationV2Controller() {
            return new WebServiceSyncDeviationV2Controller();
        }

        @Bean
        protected Locale locale() {
            return new Locale("id", "ID");
        }
    }

    @Autowired
    private WebServiceSyncDeviationV2Controller webServiceSyncDeviationV2Controller;

    @MockBean
    private WSValidationService wsValidationService;

    @MockBean
    private TerminalActivityService terminalActivityService;

    @MockBean
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @MockBean
    private TerminalService terminalService;

    @MockBean
    private UserService userService;

    @MockBean
    private AreaService areaService;

    @MockBean
    private LoanService loanService;

    @MockBean
    private AP3RService ap3RService;

    @MockBean
    private SWService swService;

    @MockBean
    private CustomerService customerService;

    @MockBean
    private SyncCustomerService syncCustomerService;

    @MockBean
    private SentraService sentraService;

    String username = "w0211f";
    String imei = "358525070497940";
    String apkVersion = "3.70";
    String sessionKey = "0beba6ac-2b04-4889-866f-b36dea07aff6";
    ListDeviationResponse listDeviationResponse = new ListDeviationResponse();
    Terminal terminal = new Terminal();
    User user = new User();
    Location location = new Location();
    List<LoanDeviation> deviationPage = new ArrayList<>();
    LoanDeviation loanDeviation = new LoanDeviation();
    AP3R ap3r = new AP3R();
    List<DeviationOfficerMapping> deviationOfficerMappings = new ArrayList<>();
    DeviationOfficerMapping deviationOfficerMapping = new DeviationOfficerMapping();

    @Before
    public void setUp() {
        listDeviationResponse.setResponseCode("00");
        loanDeviation.setDeviationId(Long.parseLong("43253"));
        loanDeviation.setAp3rId(Long.parseLong("1234"));
        user.setUserId(Long.parseLong("24235"));
        user.setRoleUser("2");
        user.setOfficeCode("334");
        location.setLocationId("334");
        terminal.setTerminalId(Long.parseLong("2462"));
        Mockito.when(wsValidationService.wsValidation(username, imei, sessionKey, apkVersion)).thenReturn("00");
        Mockito.when(userService.findUserByUsername(username)).thenReturn(user);
        Mockito.when(areaService.findLocationById(user.getOfficeCode())).thenReturn(location);
        Mockito.when(loanService.findDeviationByMs(location)).thenReturn(deviationPage);
        Mockito.when(ap3RService.findById(loanDeviation.getAp3rId())).thenReturn(ap3r);
        Mockito.when(loanService.findMappingByLoanDeviationId(loanDeviation.getDeviationId().toString())).thenReturn(deviationOfficerMappings);
        Mockito.when(userService.loadUserByUserId(deviationOfficerMapping.getOfficer())).thenReturn(user);
        Mockito.when(loanService.findIsDeletedDeviationList()).thenReturn(deviationPage);
        Mockito.when(terminalService.loadTerminalByImei(imei)).thenReturn(terminal);
    }

    @Test
    public void WhenSyncDeviasiV2_ThenReturnSuccess() {
        DeviationRequest deviationRequest = new DeviationRequest();
        deviationRequest.setImei("358525070497940");
        deviationRequest.setUsername("w0211f");
        deviationRequest.setStartLookupDate("");
        deviationRequest.setEndLookupDate("");
        deviationRequest.setSessionKey("0beba6ac-2b04-4889-866f-b36dea07aff6");
        try {
            ListDeviationResponse result = webServiceSyncDeviationV2Controller.doListNonMs(deviationRequest, apkVersion);
            assertEquals(result.getResponseCode(), listDeviationResponse.getResponseCode());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}