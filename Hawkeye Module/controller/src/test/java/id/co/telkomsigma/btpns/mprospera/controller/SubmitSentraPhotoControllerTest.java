package id.co.telkomsigma.btpns.mprospera.controller;

import id.co.telkomsigma.btpns.mprospera.controller.webservice.RESTClient;
import id.co.telkomsigma.btpns.mprospera.controller.webservice.WebServiceSentraController;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import id.co.telkomsigma.btpns.mprospera.request.PhotoRequest;
import id.co.telkomsigma.btpns.mprospera.response.PhotoResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Locale;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class SubmitSentraPhotoControllerTest {

    @TestConfiguration
    static class SubmitSentraPhotoControllerTestContextConfiguration {
        @Bean
        public WebServiceSentraController webServiceSentraController() {
            return new WebServiceSentraController();
        }

        @Bean
        protected Locale locale() {
            return new Locale("id", "ID");
        }
    }

    @Autowired
    private WebServiceSentraController webServiceSentraController;

    @MockBean
    private WSValidationService wsValidationService;

    @MockBean
    private TerminalActivityService terminalActivityService;

    @MockBean
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @MockBean
    private TerminalService terminalService;

    @MockBean
    private RESTClient restClient;

    @MockBean
    private SentraService sentraService;

    @MockBean
    private AreaService areaService;

    @MockBean
    private AuditLogService auditLogService;

    @MockBean
    private UserService userService;

    @MockBean
    private HazelcastClientService hazelcastClientService;

    String username = "w0211f";
    String imei = "358525070497940";
    String apkVersion = "3.70";
    String sessionKey = "0beba6ac-2b04-4889-866f-b36dea07aff6";
    PhotoResponse photoResponse = new PhotoResponse();
    Sentra sentra = new Sentra();

    @Before
    public void setUp() {
        photoResponse.setResponseCode("00");
        sentra.setSentraId(Long.parseLong("12345"));
        Mockito.when(wsValidationService.wsValidation(username, imei, sessionKey, apkVersion)).thenReturn("00");
        Mockito.when(sentraService.findBySentraId(sentra.getSentraId().toString())).thenReturn(sentra);
    }

    @Test
    public void WhenSubmitSentraPhoto_ThenReturnSuccess() {
        PhotoRequest photoRequest = new PhotoRequest();
        photoRequest.setImei("358525070497940");
        photoRequest.setUsername("w0211f");
        photoRequest.setSentraId("12345");
        photoRequest.setSessionKey("0beba6ac-2b04-4889-866f-b36dea07aff6");
        try {
            PhotoResponse result = webServiceSentraController.submitSentraPhoto(photoRequest, apkVersion);
            assertEquals(result.getResponseCode(), photoResponse.getResponseCode());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}