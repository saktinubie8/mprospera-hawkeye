package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.manager.HolidayManager;
import id.co.telkomsigma.btpns.mprospera.model.parameter.Holiday;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.HolidayRequest;
import id.co.telkomsigma.btpns.mprospera.response.HolidayResponse;
import id.co.telkomsigma.btpns.mprospera.service.TerminalActivityService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalService;
import id.co.telkomsigma.btpns.mprospera.service.WSValidationService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("webServiceHolidayController")
public class WebServiceHolidayController extends GenericController {

    @Autowired
    private HolidayManager holidayManager;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private WSValidationService wsValidationService;

    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_HOLIDAY_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    HolidayResponse doList(@RequestBody final HolidayRequest request, @PathVariable("apkVersion") String apkVersion) throws Exception {

        final HolidayResponse response = new HolidayResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("listHoliday INCOMING GET HOLIDAY REQUEST " + jsonUtils.toJson(request));
            // validating
            log.info("TRY VALIDATING REQUEST");
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(), request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei() + " ,sessionKey : " + request.getSessionKey());
            } else {
                List<String> holidayList = new ArrayList<String>();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
                for (Holiday holiday : holidayManager.findAll()) {
                    holidayList.add(formatter.format(holiday.getDate()));
                }
                response.setListOfHolidayDate(holidayList);
            }
            String label = getMessage("webservice.rc.label." + response.getResponseCode());
            response.setResponseMessage(label);
        } catch (Exception e) {
            log.error("listHoliday error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("listHoliday error: " + e.getMessage());
        } finally {
            try {
                // insert MessageLogs
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_GET_HOLIDAY);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
            } catch (Exception e) {
                log.error("listHoliday saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            log.info("listHoliday RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            return response;
        }
    }

}