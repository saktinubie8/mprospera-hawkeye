package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "logResponse")
public class LogResponse {
    private String responseCode;
    private String responseMessage;
    private String log;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

}