package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.LoanConstant;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.jenisusaha.JenisUsaha;
import id.co.telkomsigma.btpns.mprospera.model.loan.DeviationOfficerMapping;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanDeviation;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.sw.*;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.request.DeviationRequest;
import id.co.telkomsigma.btpns.mprospera.request.ESBLoanRequest;
import id.co.telkomsigma.btpns.mprospera.request.LoanRequest;
import id.co.telkomsigma.btpns.mprospera.response.AddLoanResponse;
import id.co.telkomsigma.btpns.mprospera.response.DeviationResponse;
import id.co.telkomsigma.btpns.mprospera.response.LoanResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Controller("webServiceAddLoanV2Controller")
public class WebServiceAddLoanV2Controller extends GenericController {

    @Autowired
    private SWService swService;
    
    @Autowired
    private FreedayService freedayService;
    
    @Autowired
    private AP3RService ap3rService;

    @Autowired
    private LoanService loanService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private AuditLogService auditLogService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private UserService userService;

    @Autowired
    private RESTClient restClient;
    
    @Autowired
    private JenisUsahaService jenisUsahaService;

    JsonUtils jsonUtils = new JsonUtils();

    /*
 	 * @author : Ilham
 	 * @Since  : 20191128
 	 * @category patch 
 	 *    - set disbursment_date ap3r and sw disama kan dengan disbursmentdate loan   
 	 */ 
    @RequestMapping(value = WebGuiConstant.ADD_LOAN_V2_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    LoanResponse doAdd(@RequestBody String requestString,
                       @PathVariable("apkVersion") String apkVersion) throws Exception {

        log.info("addLoanV2 Loan add request received, payload : " + requestString);
        SimpleDateFormat formatDateTime = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        Object obj = new JsonUtils().fromJson(requestString, LoanRequest.class);
        final LoanRequest request = (LoanRequest) obj;
        final LoanResponse response = new LoanResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        response.setResponseMessage("SUKSES");
        String loanId = "";
        log.info("TRY VALIDATING MESSAGE");
        try {
            // validasi
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(), request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei() + " ,sessionKey : " + request.getSessionKey());
            } else if (!"APPROVED".equalsIgnoreCase(request.getStatus())) {
                log.error("UNKNOWN STATUS");
                response.setResponseCode(WebGuiConstant.RC_UNKNOWN_STATUS);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
            } else {
                AP3R ap3r = new AP3R();
                Loan loan = new Loan();
                LoanDeviation deviation = new LoanDeviation();
                if (request.getSwId() == null || request.getSwId().equals("")) {
                    log.error("UNKNOWN SW ID");
                    response.setResponseCode(WebGuiConstant.RC_UNKNOWN_SW_ID);
                    String label = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(label);
                    log.info("RESPONSE CODE : " + response);
                    return response;
                }
                log.info("Check Valid SW");
                SurveyWawancara sw = swService.findSwByLocalId(request.getSwId());
                if (sw == null) {
                    log.error("UNKNOWN SW ID");
                    response.setResponseCode(WebGuiConstant.RC_UNKNOWN_SW_ID);
                    String label = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(label);
                    log.info("RESPONSE CODE : " + response);
                    return response;
                }
                if (swService.isValidSw(sw.getSwId().toString()).equals(true)) {
                    log.info("SW Valid");
                    log.info("Start create Loan");
                    loan.setSwId(sw.getSwId());
                    User userMs = userService.findUserByUsername(request.getUsername());
                    if (!userMs.getRoleUser().equals("2")) {
                        loan.setCreatedBy(request.getUsername());
                    }
                    loan.setCreatedDate(new Date());
                    loan.setCustomerName(request.getCustomerName());
                    loan.setStatus(request.getStatus());
                    loan.setLatitude(request.getLatitude());
                    loan.setLongitude(request.getLongitude());
                    loan.setRrn(request.getRetrievalReferenceNumber());
                    loan.setNote(request.getNote());
                    if (request.getAp3rId() == null || request.getAp3rId().equals("")) {
                        log.error("AP3R NULL");
                        response.setResponseCode(WebGuiConstant.RC_AP3R_NULL);
                        String label = getMessage("webservice.rc.label." + response.getResponseCode());
                        response.setResponseMessage(label);
                        log.info("RESPONSE MESSAGE : " + response);
                        return response;
                    }
                    ap3r = ap3rService.findByLocalId(request.getAp3rId());
                    if (ap3r == null) {
                        log.error("AP3R NULL");
                        response.setResponseCode(WebGuiConstant.RC_AP3R_NULL);
                        String label = getMessage("webservice.rc.label." + response.getResponseCode());
                        response.setResponseMessage(label);
                        log.info("RESPONSE MESSAGE : " + response);
                        return response;
                    }
                    SWProductMapping swProductMap = swService.findProductMapById(ap3r.getSwProductId());
                    if (swProductMap != null) {
                        if (request.getLoanAmountRecommended() != null)
                            loan.setLoanAmountRecommended(new BigDecimal(request.getLoanAmountRecommended()));
                        loan.setCustomer(customerService.getBySwId(sw.getSwId()));
                        if (loan.getAp3rId() != null && loan.getStatus().equals(WebGuiConstant.STATUS_DRAFT)
                                && request.getAction().equals("insert")) {
                            response.setResponseCode(WebGuiConstant.RC_SUCCESS);
                            String label = getMessage("webservice.rc.label." + WebGuiConstant.RC_LOAN_ALREADY_EXIST);
                            response.setResponseMessage(label);
                            return response;
                        }
                        loan.setAp3rId(ap3r.getAp3rId());
                        loan.setProductId(swProductMap.getRecommendedProductId());
                        loan.setPlafond(swProductMap.getRecommendedPlafon());

                        if (request.getDisbursementDate() != null) {
                           if (!request.getDisbursementDate().equals("")) {
                                SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm:ss");
                                Date disburseDate = formatDateTime.parse(request.getDisbursementDate());
                                if (formatTime.format(disburseDate).equals("23:00:00")) {
                                    Calendar cal = Calendar.getInstance();
                                    cal.setTime(disburseDate);
                                    cal.add(Calendar.HOUR_OF_DAY, 1);
                                    disburseDate = cal.getTime();
                                    loan.setDisbursementDate(disburseDate);
                                } else {
                                    loan.setDisbursementDate(formatDateTime.parse(request.getDisbursementDate()));
                               }                              
                            }
                        } else {
                            if (ap3r.getDisbursementDate() != null)
                                loan.setDisbursementDate(ap3r.getDisbursementDate());
                            else
                            	loan.setDisbursementDate(formatDateTime.parse(request.getDisbursementDate()));
                        }
                    }
                }
                if (!terminalService.isDuplicate(request.getRetrievalReferenceNumber(), "addLoan")) {
                    Loan loanWithAp3r = loanService.getLoanByAp3rId(ap3r.getAp3rId());
                    if (loanWithAp3r != null) {
                        log.error("DUPLICATE AP3R");
                        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
                        String label = getMessage("webservice.rc.label." + WebGuiConstant.RC_LOAN_ALREADY_EXIST);
                        response.setResponseMessage(label);
                        log.info("RESPONSE MESSAGE : " + response);
                        response.setLoanId(loanWithAp3r.getLoanId().toString());
                        response.setNoappid(loanWithAp3r.getAppId());
                        return response;
                    }
                    Customer customer = customerService.getBySwId(sw.getSwId());
                    if (customer != null) {
                        request.setCustReffId(customer.getCustomerId().toString());
                    } else {
                        log.error("CUSTOMER NOT FOUND");
                        response.setResponseCode(WebGuiConstant.RC_UNKNOWN_CUSTOMER_ID);
                        String label = getMessage("webservice.rc.label." + WebGuiConstant.RC_UNKNOWN_CUSTOMER_ID);
                        response.setResponseMessage(label);
                        log.info("RESPONSE MESSAGE : " + response);
                        return response;
                    }
                    SimpleDateFormat esbDateTimeFormatter = new SimpleDateFormat("yyyyMMddHHmmss");
                    SimpleDateFormat applicationDateFormatter = new SimpleDateFormat("dd/MM/yyyy");
                    SimpleDateFormat disbursementDateFormatter = new SimpleDateFormat("yyyy-MM-dd");
                    LoanProduct loanProduct = null;
                    if (loan.getProductId() != null) {
                        loanProduct = swService.findByProductId(String.valueOf(loan.getProductId()));
                    }
                    //for ESB
                    log.info("START MAPPING DATA FOR ESB");
                    request.setSwId(sw.getSwId().toString());
                    request.setSessionKey(null);
                    request.setLoanReffId("");
                    request.setLoanId("");
                    if (loanProduct != null) {
                        request.setPrdOfferingShortName(loanProduct.getProductCode());
                        if (loanProduct.getLoanType().toUpperCase().contains("TOPUP"))
                            request.setPenambahan("1");
                        else
                            request.setPenambahan("0");
                        if (loanProduct.getMargin() != null) {
                            request.setInterestRate(loanProduct.getMargin().toString() + "");
                        }
                        request.setNoOfInstallments(loanProduct.getInstallmentCount() + "");
                        if (loanProduct.getLoanType().toUpperCase().contains("BARU")) {
                            request.setTipePembiayaan(LoanConstant.TIPE_PEMBIAYAAN_BARU);
                        } else if (loanProduct.getLoanType().toUpperCase().contains("TOPUP")) {
                            request.setTipePembiayaan(LoanConstant.TIPE_PEMBIAYAAN_TOPUP);
                        } else {
                            request.setTipePembiayaan(LoanConstant.TIPE_PEMBIAYAAN_RESTRUKTUR);
                        }
                    }
                    request.setPerspective(null);
                    if (sw.getCustomerRegistrationType() == '1' || sw.getCustomerRegistrationType().equals(new Character('1')))
                        request.setTipeNasabah(LoanConstant.TIPE_NASABAH_BARU);
                    else if (sw.getCustomerRegistrationType() == '2' || sw.getCustomerRegistrationType().equals(new Character('2')))
                        request.setTipeNasabah(LoanConstant.TIPE_NASABAH_LAMA);
                    request.setApplicationDate(applicationDateFormatter.format(esbDateTimeFormatter.parse(request.getTransmissionDateAndTime())));
                    request.setPembiayaanKe(null);
                    request.setPerlengkapanWarung(null);
                    request.setHargaPerlengkapanWarung(null);
                    request.setTernak(null);
                    request.setHargaTernak(null);
                    request.setBahanBaku(null);
                    request.setHargaBahanBaku(null);
                    request.setPulsaHandphone(null);
                    request.setHargaPulsaHandphone(null);
                    request.setOther("Lainnya");
                    request.setHargaOther(ap3rService.sumFunds(ap3r).toString());
                    request.setTotalHarga(ap3rService.sumFunds(ap3r).toString());
                    request.setLoanAmount(loan.getPlafond() + "");
                    if (loan.getDisbursementDate() != null)
                        request.setDisbursementDate(disbursementDateFormatter.format(loan.getDisbursementDate()));
                    request.setGracePeriodDuration("0");
                    request.setFundId(null);
                    request.setBusinessActivitiesId(null);
                    request.setExternalId(null);
                    request.setOtherInstitutionFlag("0");
                    request.setInstitutionName(null);
                    request.setPrincipalResidue(null);
                    request.setInstallmentResidue(null);
                    request.setInstallmentAmount(null);
                    request.setBiCheckingResult(null);
                    request.setCollectAbility(null);
                    if (sw.getBusinessField().equals("0") || sw.getBusinessField() == null) {
                        request.setBusinessFlag("0");
                        request.setBusinessFlagExp("0");
                    } else {
                        request.setBusinessFlag("1");
                        request.setBusinessFlagExp("1");
                    }
                    if (sw.getBusinessField().equals("43")) {
                        request.setBusinessType(LoanConstant.JENIS_USAHA_PERTANIAN);
                        request.setBusinessTypeExp(LoanConstant.JENIS_USAHA_PERTANIAN);
                    } else if (sw.getBusinessField().equals("44")) {
                        request.setBusinessType(LoanConstant.JENIS_USAHA_PERKEBUNAN);
                        request.setBusinessTypeExp(LoanConstant.JENIS_USAHA_PERKEBUNAN);
                    } else if (sw.getBusinessField().equals("45")) {
                        request.setBusinessType(LoanConstant.JENIS_USAHA_PETERNAKAN);
                        request.setBusinessTypeExp(LoanConstant.JENIS_USAHA_PETERNAKAN);
                    } else if (sw.getBusinessField().equals("46")) {
                        request.setBusinessType(LoanConstant.JENIS_USAHA_PERDAGANGAN);
                        request.setBusinessTypeExp(LoanConstant.JENIS_USAHA_PERDAGANGAN);
                    } else if (sw.getBusinessField().equals("47")) {
                        request.setBusinessType(LoanConstant.JENIS_USAHA_JASA);
                        request.setBusinessTypeExp(LoanConstant.JENIS_USAHA_JASA);
                    } else if (sw.getBusinessField().equals("48")) {
                        request.setBusinessType(LoanConstant.JENIS_USAHA_LAINNYA);
                        request.setBusinessTypeExp(LoanConstant.JENIS_USAHA_LAINNYA);
                    } else if (sw.getBusinessField().equals("49")) {
                        request.setBusinessType(LoanConstant.JENIS_USAHA_INDUSTRI);
                        request.setBusinessTypeExp(LoanConstant.JENIS_USAHA_INDUSTRI);
                    } else {
                        request.setBusinessType(null);
                        request.setBusinessTypeExp(null);
                    }
                    request.setAgeExp(sw.getBusinessAgeMonth().toString());
                    try {
                        request.setNamaJenisUsaha(sw.getBusinessDescription());
                    } catch (NullPointerException e) {
                        request.setNamaJenisUsaha("");
                    }
                    request.setBusinessTypeValue(null);
                    if (sw.getBusinessOwnerStatus() == '1' || sw.getBusinessOwnerStatus().equals(new Character('1')))
                        request.setPlaceStatus(LoanConstant.STATUS_TEMPAT_USAHA_PRIBADI);
                    else if (sw.getBusinessOwnerStatus() == '2' || sw.getBusinessOwnerStatus().equals(new Character('2')))
                        request.setPlaceStatus(LoanConstant.STATUS_TEMPAT_USAHA_ORANG_TUA);
                    else if (sw.getBusinessOwnerStatus() == '3' || sw.getBusinessOwnerStatus().equals(new Character('3')))
                        request.setPlaceStatus(LoanConstant.STATUS_TEMPAT_USAHA_PIHAK_KETIGA);
                    else
                        request.setPlaceStatus(LoanConstant.STATUS_TEMPAT_USAHA_LAINNYA);
                    request.setPlaceStatusValue(null);
                    Integer yearInMonth = sw.getBusinessAgeYear() * 12;
                    Integer totalMonth = yearInMonth + sw.getBusinessAgeMonth();
                    request.setAge(totalMonth + "");
                    if (sw.getTotalIncome() == null)
                        request.setIncome("0");
                    else
                        request.setIncome(sw.getTotalIncome().toString());
                    if (sw.getExpenditure() == null)
                        request.setExpense("0");
                    else
                        request.setExpense(sw.getExpenditure().toString());
                    if (sw.getNettIncome() == null)
                        request.setNetIncome("0");
                    else
                        request.setNetIncome(sw.getNettIncome().toString());
                    request.setIncomeFamily("0");
                    if (ap3r.getBusinessField() != null) {
                        JenisUsaha ju = jenisUsahaService.getJenisUsahaById(Long.parseLong(ap3r.getBusinessField()));
                        BusinessTypeAP3R bt;
                        if(ju==null){
                            bt = swService.getBusinessTypeAP3RById(Long.parseLong(ap3r.getBusinessField()));
                            if (bt != null) {
                                request.setBusinessTypePlan(bt.getBiCode());
                                request.setBusinessTypeValuePlan(String.valueOf(bt.getSubBiCode()));
                            }
                        }else{
                            bt = swService.findBySubBiCode(String.valueOf(ju.getKdSectorEkonomi()));
                            if (bt != null) {
                                request.setBusinessTypePlan(bt.getBiCode());
                                request.setBusinessTypeValuePlan(String.valueOf(ju.getKdSectorEkonomi()));
                            }
                        }
                    }
                    if (sw.getBusinessOwnerStatus() == '1' || sw.getBusinessOwnerStatus().equals(new Character('1')))
                        request.setPlaceStatusPlan(LoanConstant.STATUS_TEMPAT_USAHA_PRIBADI);
                    else if (sw.getBusinessOwnerStatus() == '2' || sw.getBusinessOwnerStatus().equals(new Character('2')))
                        request.setPlaceStatusPlan(LoanConstant.STATUS_TEMPAT_USAHA_ORANG_TUA);
                    else if (sw.getBusinessOwnerStatus() == '3' || sw.getBusinessOwnerStatus().equals(new Character('3')))
                        request.setPlaceStatusPlan(LoanConstant.STATUS_TEMPAT_USAHA_PIHAK_KETIGA);
                    else
                        request.setPlaceStatusPlan(null);
                    request.setPlaceStatusValuePlan(null);
                    request.setHrb(null);
                    request.setHrc(null);
                    request.setDeviation(null);
                    request.setFlagId(null);
                    if (sw.getTotalIncome() == null)
                        request.setRevenuePerMonth("0");
                    else
                        request.setRevenuePerMonth(sw.getTotalIncome().toString());
                    if (loan.getCustomer() != null) {
                        request.setCustomerNumber(loan.getCustomer().getCustomerCifNumber());
                    }
                    request.setMaxLoanAmount(null);
                    request.setMinLoanAmount(null);
                    request.setMaxNoOfInstall(null);
                    request.setMinNoOfInstall(null);
                    request.setStatus("2");
                    request.setInterest(null);
                    request.setHolidayBonus(null);
                    if(ap3r.getDueDate() != null) {
                    	request.setOrgDueDate(disbursementDateFormatter.format(ap3r.getDueDate()));	
                    }
                    else {
                    	 if (loan.getProductId() != null) {               		 
                             loanProduct = swService.findByProductId(String.valueOf(loan.getProductId()));
                             Integer installmentCount = loanProduct.getInstallmentCount();
                             String  dates ="";
                             String finalDate = "";
                             Calendar calendar = Calendar.getInstance();
                             calendar.setTime(disbursementDateFormatter.parse((request.getDisbursementDate())));
                             calendar.set(Calendar.HOUR_OF_DAY, 0);
                             calendar.set(Calendar.MINUTE, 0);
                             calendar.set(Calendar.SECOND, 0);
                             calendar.set(Calendar.MILLISECOND, 0);
                             calendar.add(Calendar.DAY_OF_YEAR,14 * installmentCount); 
                             dates =  disbursementDateFormatter.format(calendar.getTime());
                             /*
                              * Set with Holiday
                              */                          
                             Integer countHoliday = freedayService.countHoliday(request.getDisbursementDate(), dates);
                             countHoliday = countHoliday == null ? 0 : countHoliday;
                             Integer totInstallmentCount = installmentCount + countHoliday;
                             Calendar calendarFinal = Calendar.getInstance();
                             calendarFinal.setTime(disbursementDateFormatter.parse((request.getDisbursementDate())));
                             calendarFinal.set(Calendar.HOUR_OF_DAY, 0);
                             calendarFinal.set(Calendar.MINUTE, 0);
                             calendarFinal.set(Calendar.SECOND, 0);
                             calendarFinal.set(Calendar.MILLISECOND, 0);
                             calendarFinal.add(Calendar.DAY_OF_YEAR,(14 * totInstallmentCount)); 
                             finalDate =  disbursementDateFormatter.format(calendarFinal.getTime());
                             AP3R updateDueDisDateAp3r = ap3rService.findByLocalId(ap3r.getLocalId());
                             request.setOrgDueDate(finalDate);
                             log.info("SET DUEDATE : "+ request.getOrgDueDate());   
                             updateDueDisDateAp3r.setDueDate(disbursementDateFormatter.parse(request.getOrgDueDate()));
                             updateDueDisDateAp3r.setDisbursementDate(disbursementDateFormatter.parse(request.getDisbursementDate()));
                             ap3rService.updatedisbursmendate(updateDueDisDateAp3r); //save dueDate Ap3r
                             log.info("SET DUEDATE : "+ request.getOrgDueDate());
                    	 }	
                    }                    request.setCreatedBy(loan.getCreatedBy());
                    request.setCreatedDate(disbursementDateFormatter.format(loan.getCreatedDate()));
                    request.setModifiedBy(request.getUsername());
                    request.setModifiedDate(disbursementDateFormatter.format(new Date()));
                    if (!sw.getStatus().equals(WebGuiConstant.STATUS_APPROVED)) {
                        if (sw.getStatus().equals(WebGuiConstant.STATUS_REJECTED)) {
                            log.error("LOAN ALREADY REJECTED");
                            response.setResponseCode(WebGuiConstant.RC_ALREADY_REJECTED);
                            String label = getMessage("webservice.rc.label." + response.getResponseCode());
                            response.setResponseMessage(label);
                            log.info("RESPONSE MESSAGE : " + response);
                            return response;
                        } else {
                            log.error("DEVIATION HAS NOT BEEN APPROVED");
                            response.setResponseCode(WebGuiConstant.RC_DEVIATION_NOT_APPROVED);
                            String label = getMessage("webservice.rc.label." + response.getResponseCode());
                            response.setResponseMessage(label);
                            log.info("RESPONSE MESSAGE : " + response);
                            return response;
                        }
                    }
                    LoanDeviation loanDeviation = loanService.findLoanDeviationByAp3rId(ap3r.getAp3rId(), false);
                    if (loanDeviation != null) {
                        if (loanDeviation.getStatus().equals(WebGuiConstant.STATUS_REJECTED)) {
                            log.error("LOAN DEVIATION ALREADY REJECTED");
                            response.setResponseCode(WebGuiConstant.RC_ALREADY_REJECTED);
                            String label = getMessage("webservice.rc.label." + response.getResponseCode());
                            response.setResponseMessage(label);
                            log.info("RESPONSE MESSAGE : " + response);
                            return response;
                        } else if (loanService.countDeviationWithStatus(loanDeviation.getDeviationId()) < 1) {
                            log.error("DEVIATION HAS NOT BEEN APPROVED");
                            response.setResponseCode(WebGuiConstant.RC_DEVIATION_NOT_APPROVED);
                            String label = getMessage("webservice.rc.label." + response.getResponseCode());
                            response.setResponseMessage(label);
                            log.info("RESPONSE MESSAGE : " + response);
                            return response;
                        } else if (loanService.countDeviationWaiting(loanDeviation.getDeviationId()) > 0) {
                            log.error("DEVIATION HAS NOT BEEN APPROVED");
                            response.setResponseCode(WebGuiConstant.RC_DEVIATION_NOT_APPROVED);
                            String label = getMessage("webservice.rc.label." + response.getResponseCode());
                            response.setResponseMessage(label);
                            log.info("RESPONSE MESSAGE : " + response);
                            return response;
                        }
                    }
                    if (loanDeviation != null) {
                        if (loanDeviation.getDeviationCode() != null) {
                            log.info("Kode deviasi : " + loanDeviation.getDeviationCode());
                            request.setComment(loan.getNote() + ";" + loanDeviation.getDeviationCode());
                        } else {
                            request.setComment(loan.getNote());
                        }
                    } else {
                        request.setComment(loan.getNote());
                    }
                    ESBLoanRequest esbRequest = new ESBLoanRequest(request);
                    log.info("Send to ESB, payload : " + esbRequest);
                    AddLoanResponse restResponse = restClient.addLoan(esbRequest);
                    if (restResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)
                            || restResponse.getResponseCode().equals(WebGuiConstant.RC_LOAN_ALREADY_EXIST)) {
                        if (restResponse.getProspectId() == null) {
                            log.error("UNKNOWN LOAN ID");
                            response.setResponseCode(WebGuiConstant.RC_UNKNOWN_LOAN_ID);
                            String label = getMessage("webservice.rc.label." + response.getResponseCode());
                            response.setResponseMessage(label);
                            log.info("RESPONSE MESSAGE : " + response);
                            return response;
                        }
                        loan.setCustomer(loan.getCustomer());
                        loan.setProsperaId(restResponse.getProspectId());
                        loan.setAppId(restResponse.getNoappid());
                        loan.setApprovedBy(request.getUsername());
                        loan.setApprovedDate(new Date());
                        if (restResponse.getFirstInstallmentDate() != null && !"".equals(restResponse.getFirstInstallmentDate()))
                            loan.setFirstInstallmentDate(disbursementDateFormatter.parse(restResponse.getFirstInstallmentDate()));
                        if (restResponse.getLastInstallmentDate() != null && !"".equals(restResponse.getLastInstallmentDate()))
                            loan.setLastInstallmentDate(disbursementDateFormatter.parse(restResponse.getLastInstallmentDate()));
                        loan.setStatus(WebGuiConstant.STATUS_APPROVED);
                        log.info("SAVING APPROVED LOAN");
                        loanService.save(loan);
                        swService.save(sw);
                        if (loan.getLoanId() != null) {
                            loanId = loan.getLoanId().toString();
                        } else {
                            log.info("SUCCESS SUBMIT LOAN TO PROSPERA, BUT FAILED IN MPROSPERA");
                        }
                        DeviationRequest esbDeviationRequest = new DeviationRequest();
                        esbDeviationRequest.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
                        esbDeviationRequest.setTransmissionDateAndTime(request.getTransmissionDateAndTime());
                        esbDeviationRequest.setImei(request.getImei());
                        esbDeviationRequest.setUsername(request.getUsername());
                        esbDeviationRequest.setNoappid(restResponse.getNoappid());
                        if (loanDeviation != null) {
                            DeviationOfficerMapping deviationOfficerMapping = loanService.findTopByDeviationIdAndStatus(loanDeviation.getDeviationId());
                            if (deviationOfficerMapping != null) {
                                User user = userService.loadUserByUserId(deviationOfficerMapping.getOfficer());
                                esbDeviationRequest.setUserApproval(user.getUsername());
                                esbDeviationRequest.setRole(user.getProsperaRoleId());
                            }
                            log.info("SEND DEVIATION DATA TO PROSPERA VIA ESB");
                            DeviationResponse esbResponse = restClient.addDeviation(esbDeviationRequest);
                            if (esbResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)) {
                                List<DeviationOfficerMapping> deviationMapApproved = loanService.findMappingByLoanDeviationId(loanDeviation.getDeviationId().toString());
                                for (DeviationOfficerMapping deviationMap : deviationMapApproved) {
                                    deviationMap.setStatus(WebGuiConstant.STATUS_APPROVED);
                                    deviationMap.setUpdatedDate(new Date());
                                    loanService.save(deviationMap);
                                }
                            } else {
                                response.setResponseCode(WebGuiConstant.RC_CREATE_LOAN_FAILED);
                            }
                        }
                    } else if (restResponse.getResponseCode().equals("36")) {
                        response.setResponseCode(WebGuiConstant.RC_LOAN_ALREADY_EXIST);
                        String label = getMessage("webservice.rc.label." + response.getResponseCode());
                        response.setResponseMessage(label);
                        return response;
                    }
                    response.setResponseCode(restResponse.getResponseCode());
                    response.setResponseMessage(restResponse.getResponseMessage());
                    response.setProspectId(restResponse.getProspectId());
                    response.setNoappid(restResponse.getNoappid());
                    Boolean isDeleted = false;
                    deviation = loanService.findLoanDeviationByAp3rId(ap3r.getAp3rId(), isDeleted);
                    if (deviation != null) {
                        deviation.setAp3rId(deviation.getAp3rId());
                        deviation.setDeviationReason(deviation.getDeviationReason());
                        deviation.setDeviationCode(deviation.getDeviationCode());
                        deviation.setDeviationId(deviation.getDeviationId());
                        deviation.setUserBwmp(deviation.getUserBwmp());
                        deviation.setCreatedBy(deviation.getCreatedBy());
                        deviation.setStatus(deviation.getStatus());
                        deviation.setBatch(deviation.getBatch());
                        deviation.setLoanId(loan.getLoanId());
                        loanService.save(deviation);
                    }
                } else {
                    Loan loanExist = loanService.getLoanByRrn(request.getRetrievalReferenceNumber());
                    if (loanExist != null) {
                        loanId = loanExist.getLoanId().toString();
                    }
                    log.error("LOAN ALREADY APPROVED");
                    response.setResponseCode(WebGuiConstant.RC_LOAN_ALREADY_EXIST);
                    String label = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(label);
                }
            }
        } catch (Exception e) {
            log.error("addLoanV2 error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            String label = getMessage("webservice.rc.label." + response.getResponseCode());
            response.setResponseMessage("addLoanV2 error: " + label + " - " + e.getMessage());
        } finally {
            response.setLoanId(loanId);
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_MODY_LOAN);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.insertRrn("addLoan", request.getRetrievalReferenceNumber().trim());
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        AuditLog auditLog = new AuditLog();
                        auditLog.setActivityType(AuditLog.MODIF_LOAN);
                        auditLog.setCreatedBy(request.getUsername());
                        auditLog.setCreatedDate(new Date());
                        auditLog.setDescription(request.toString() + " | " + response.toString());
                        auditLog.setReffNo(request.getRetrievalReferenceNumber());
                        auditLogService.insertAuditLog(auditLog);
                    }
                });
            } catch (Exception e) {
                log.error("addLoanV2 saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            log.info("addLoanV2 RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            return response;
        }
    }

}