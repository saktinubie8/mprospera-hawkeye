package id.co.telkomsigma.btpns.mprospera.controller.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * Created by daniel on 4/20/15.
 */
@Configuration
public class ThreadPoolConfig {

    @Value("${thread-pool.core.pool.size}")
    private Integer threadPoolCorePoolSize;
    @Value("${thread-pool.max.pool.size}")
    private Integer threadPoolMaxPoolSize;
    @Value("${thread-pool.queue.capacity}")
    private Integer threadPoolQueueCapacity;

    @Bean(name = "threadPoolTaskExecutor")
    public ThreadPoolTaskExecutor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setCorePoolSize(threadPoolCorePoolSize);
        threadPoolTaskExecutor.setMaxPoolSize(threadPoolMaxPoolSize);
        threadPoolTaskExecutor.setKeepAliveSeconds(300);
        threadPoolTaskExecutor.setQueueCapacity(Integer.MAX_VALUE);
        threadPoolTaskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        return threadPoolTaskExecutor;
    }

}