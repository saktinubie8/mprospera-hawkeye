package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

@Component
public class Timer {

    private static List<String> information = new ArrayList<String>();
    private final Log LOG = LogFactory.getLog(getClass());
    private Long startTime;
    private String info;

    public void start(String info) {
        startTime = System.currentTimeMillis();
        this.info = info;
    }

    public void stop() {
        if (startTime != null) {
            Long endTime = System.currentTimeMillis();
            Long elapsedTime = endTime - startTime;
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            synchronized (information) {
                String temp = formatter.format(new Date()) + " EXECUTING " + info
                        + " ========================================= time : " + elapsedTime + " ms";
                information.add(temp);
            }
        }
    }

    public void printLog() {
        for (String info : information) {
            LOG.info(info);
        }
    }

}