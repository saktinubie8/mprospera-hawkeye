package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.request.EchoRequest;
import id.co.telkomsigma.btpns.mprospera.response.EchoResponse;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
public class EchoTerminalController extends GenericController {

    @RequestMapping(value = WebGuiConstant.TERMINAL_ECHO_PATH, method = RequestMethod.POST)
    public @ResponseBody
    EchoResponse echo(@RequestBody EchoRequest request) throws IOException {
        EchoResponse response = new EchoResponse();
        if (request.getRequestCode().equals("ECHO")) {
            response.setResponseCode(WebGuiConstant.RC_SUCCESS);
            response.setResponseMessage("SUCCESS");
        } else {
            response.setResponseCode(WebGuiConstant.RC_GLOBAL_EXCEPTION);
            response.setResponseMessage("FAILED");
        }
        return response;
    }

}