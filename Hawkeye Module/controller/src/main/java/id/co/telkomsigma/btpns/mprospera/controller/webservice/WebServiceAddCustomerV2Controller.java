package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.CustomerConstant;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.pdk.PelatihanDasarKeanggotaan;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaDistrict;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaKelurahan;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaProvince;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaSubDistrict;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import id.co.telkomsigma.btpns.mprospera.model.sw.AP3R;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.AddCustomerRequest;
import id.co.telkomsigma.btpns.mprospera.response.AddCustomerResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("webServiceAddCustomerV2Controller")
public class WebServiceAddCustomerV2Controller extends GenericController {

    @Autowired
    private SWService swService;

    @Autowired
    private PDKService pdkService;

    @Autowired
    private AreaService areaService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private AP3RService ap3rService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private AuditLogService auditLogService;

    @Autowired
    private SentraService sentraService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private RESTClient restClient;

    @Autowired
    private WSValidationService wsValidationService;

    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.ADD_CUSTOMER_V2_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    AddCustomerResponse doAdd(@RequestBody String requestString,
                              @PathVariable("apkVersion") String apkVersion) throws Exception {

        log.info("addCustomerV2 INCOMING ADD CUSTOMER REQUEST MESSAGE : " + requestString);
        Object obj = new JsonUtils().fromJson(requestString, AddCustomerRequest.class);
        final AddCustomerRequest request = (AddCustomerRequest) obj;
        final AddCustomerResponse response = new AddCustomerResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        response.setResponseMessage("SUKSES");
        String customerId = "";
        String prospectId = "";
        String cifNumber = "";
        try {
            log.info("Try to validating request");
            // validation
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(), request.getSessionKey(), apkVersion);
            log.info("Get validating result" + validation);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei() + " ,sessionKey : " + request.getSessionKey());
            } else if (!"APPROVED".equalsIgnoreCase(request.getStatus())) {
                log.error("UNKNOWN STATUS");
                response.setResponseCode(WebGuiConstant.RC_UNKNOWN_STATUS);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
            } else {
                // insert ke tabel Customer
                Customer customer = new Customer();
                SurveyWawancara sw = new SurveyWawancara();
                Group sentraGroup = new Group();
                if (request.getSwId() != null) {
                    if (!"".equals(request.getSwId())) {
                        sw = swService.findSwByLocalId(request.getSwId());
                        if (sw != null) {
                            customer.setSwId(sw.getSwId());
                            Customer customerExist = customerService.getBySwId(sw.getSwId());
                            if (customerExist != null) {
                                log.error("DUPLICATE CUSTOMER");
                                response.setResponseCode(WebGuiConstant.RC_CUSTOMER_ALREADY_EXIST);
                                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                                response.setResponseMessage(label);
                                log.info("RESPONSE MESSAGE : " + response);
                                return response;
                            }
                        } else {
                            log.error("UNKNOWN SW ID");
                            response.setResponseCode(WebGuiConstant.RC_UNKNOWN_SW_ID);
                            String label = getMessage("webservice.rc.label." + response.getResponseCode());
                            response.setResponseMessage(label);
                            log.info("RESPONSE MESSAGE : " + response);
                            return response;
                        }
                    } else {
                        log.error("UNKNOWN SW ID");
                        response.setResponseCode(WebGuiConstant.RC_UNKNOWN_SW_ID);
                        String label = getMessage("webservice.rc.label." + response.getResponseCode());
                        response.setResponseMessage(label);
                        log.info("RESPONSE MESSAGE : " + response);
                        return response;
                    }
                } else {
                    log.error("UNKNOWN SW ID");
                    response.setResponseCode(WebGuiConstant.RC_UNKNOWN_SW_ID);
                    String label = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(label);
                    log.info("RESPONSE MESSAGE : " + response);
                    return response;
                }
                log.info("NEW CUSTOMER");
                log.info("PDK ID : " + request.getPdkId());
                if (request.getPdkId() != null) {
                    customer.setPdkId(Long.parseLong(request.getPdkId()));
                } else {
                    log.error("UNKNOWN PDK ID");
                    response.setResponseCode(WebGuiConstant.RC_UNKNOWN_PDK_ID);
                    String label = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(label);
                    log.info("RESPONSE MESSAGE : " + response);
                    return response;
                }
                if (pdkService.isValidPdk("" + customer.getPdkId()).equals(true)) {
                    customer.setPdkId(Long.parseLong(request.getPdkId()));
                } else {
                    log.error("UNKNOWN PDK ID");
                    response.setResponseCode(WebGuiConstant.RC_UNKNOWN_PDK_ID);
                    String label = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(label);
                }
                log.info("SW ID : " + customer.getSwId());
                if (swService.isValidSw("" + customer.getSwId()).equals(true)) {
                    customer.setCustomerName(sw.getCustomerName());
                    customer.setCustomerIdNumber(sw.getCustomerIdNumber());
                    customer.setCustomerIdName(sw.getCustomerIdName());
                    customer.setCustomerIdAddress(sw.getAddress() + ", " + sw.getRtrw());
                    customer.setOtherAddress(sw.getDomisiliAddress());
                    if (sw.getKelurahan() != null) {
                        AreaKelurahan kel = areaService.getAreaKelurahanByAreaId(sw.getKelurahan());
                        if (kel != null)
                            customer.setKelurahan(kel.getAreaName());
                    }
                    if (sw.getKecamatan() != null) {
                        AreaSubDistrict kec = areaService.findKecamatanById(sw.getKecamatan());
                        if (kec != null)
                            customer.setKecamatan(kec.getAreaName());
                    }
                    if (sw.getProvince() != null) {
                        AreaProvince prov = areaService.findProvById(sw.getProvince());
                        if (prov != null) {
                            AreaDistrict city = areaService.findById(sw.getCity());
                            if (city != null) {
                                customer.setProvince(prov.getAreaName());
                                customer.setCity(city.getAreaName());
                            }
                        }
                    }
                    customer.setPostalCode(sw.getPostalCode());
                    customer.setPlaceOfBirth(sw.getBirthPlace());
                    customer.setDateOfBirth(sw.getBirthDate());
                    customer.setPhoneNumber(sw.getPhoneNumber());
                    customer.setSpouseName(sw.getCoupleName());
                    customer.setSpousePlaceOfBirth(sw.getCoupleBirthPlace());
                    customer.setSpouseDateOfBirth(sw.getCoupleBirthDate());
                    customer.setReligion(sw.getReligion());
                    if (sw.getMaritalStatus() != null) {
                        if (sw.getMaritalStatus().equals("1")) {
                            customer.setMaritalStatus(CustomerConstant.Marital_Desc_Menikah);
                        } else if (sw.getMaritalStatus().equals("2")) {
                            customer.setMaritalStatus(CustomerConstant.Marital_Desc_Single);
                        } else {
                            customer.setMaritalStatus(CustomerConstant.Marital_Desc_Tidak_Menikah);
                        }
                    }
                    customer.setGender(sw.getGender());
                    customer.setAssignedUsername(request.getUsername());
                    customer.setApprovedDate(new Date());
                } else {
                    log.error("UNKNOWN SW ID");
                    response.setResponseCode(WebGuiConstant.RC_UNKNOWN_SW_ID);
                    String label = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(label);
                }
                if (sentraService.isValidGroup(Long.parseLong(request.getGroupId())).equals(true)) {
                    sentraGroup = sentraService.getGroupByGroupId((request.getGroupId()));
                    customer.setGroup(sentraGroup);
                    Sentra sentra = sentraGroup.getSentra();
                    Location l = areaService.findLocationById(sentra.getLocationId());
                    if (sentra != null) {
                        customer.setSentraLocation(l);
                    }
                }
                customer.setCreatedDate(new Date());
                customer.setCustomerStatus(request.getStatus());
                customer.setLatitude(request.getLatitude());
                customer.setLongitude(request.getLongitude());
                customer.setIsDeleted(false);
                customer.setCreatedBy(request.getUsername());
                if (!terminalService.isDuplicate(request.getRetrievalReferenceNumber(), "addCustomer")) {
                    log.info("SAVE CUSTOMER AS APPROVE");
                    if (response.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)
                            && request.getStatus().equals(WebGuiConstant.STATUS_APPROVED)) {
                        log.info("MAPPING TO ESB REQUEST MESSAGE");
                        request.setSessionKey(null);
                        request.setReffId(request.getCustomerId());
                        request.setGovernmentId(customer.getCustomerIdNumber());
                        request.setFullName(customer.getCustomerName());
                        if (customer.getGroup().getGroupId() != null) {
                            request.setCustomerNumberGroup(sentraGroup.getProsperaId());
                            if (sentraGroup.getSentra().getSentraId() != null) {
                                Sentra sentra = sentraService.findBySentraId(sentraGroup.getSentra().getSentraId().toString());
                                if (sentra != null) {
                                    request.setCustomerNumberCenter(sentra.getProsperaId());
                                }
                            } else {
                                log.error("SENTRA NOT FOUND");
                                response.setResponseCode(WebGuiConstant.RC_SENTRA_NOT_APPROVED);
                                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                                response.setResponseMessage(label);
                                log.info("RESPONSE MESSAGE : " + response);
                                return response;
                            }
                        } else {
                            log.error("GROUP NOT FOUND");
                            response.setResponseCode(WebGuiConstant.RC_GROUP_ID_NOT_FOUND);
                            String label = getMessage("webservice.rc.label." + response.getResponseCode());
                            response.setResponseMessage(label);
                            log.info("RESPONSE MESSAGE : " + response);
                            return response;
                        }
                        request.setGroupFlag("1");
                        request.setActionFlag(request.getAction());
                        request.setUsername(customer.getCreatedBy());
                        request.setFirstName(sw.getCustomerIdName());
                        request.setFullName(sw.getCustomerName());
                        request.setNickName(sw.getCustomerAliasName());
                        if (sw.getIdExpiryDate() != null) {
                            request.setDateOfGovernmentId(formatter2.format(sw.getIdExpiryDate()));
                        } else {
                            request.setDateOfGovernmentId("2100-12-31");
                        }
                        if (sw.getBirthDate() != null) {
                            request.setDateOfBirth(formatter2.format(sw.getBirthDate()));
                        } else {
                            request.setDateOfBirth("");
                        }
                        AreaDistrict kab = areaService.findById(sw.getBirthPlace());
                        if (kab != null) {
                            request.setPlaceOfBirth(kab.getAreaName());
                        } else {
                            request.setPlaceOfBirth("");
                        }
                        request.setTelephone(sw.getPhoneNumber());
                        request.setMotherName(sw.getMotherName());
                        if (sw.getGender().equals("0")) {
                            request.setGender(CustomerConstant.Gender_Perempuan);
                        } else {
                            request.setGender(CustomerConstant.Gender_Laki_Laki);
                        }
                        if (sw.getMaritalStatus().equals("1")) {
                            request.setMaritalStatus(CustomerConstant.Marital_Menikah);
                        } else if (sw.getMaritalStatus().equals("2")) {
                            request.setMaritalStatus(CustomerConstant.Marital_Single);
                        } else {
                            request.setMaritalStatus(CustomerConstant.Marital_Tidak_Menikah);
                        }
                        if (sw.getWorkType().equals("1")) {
                            request.setOccupation("Wiraswasta");
                        } else {
                            request.setOccupation("Ibu Rumah Tangga");
                        }
                        if (sw.getDependentCount() != null) {
                            request.setNumberOfChildren(sw.getDependentCount().toString());
                        } else {
                            request.setNumberOfChildren("");
                        }
                        if (sw.getReligion().equals("3")) {
                            request.setReligion(CustomerConstant.Religion_Islam);
                        } else if (sw.getReligion().equals("5")) {
                            request.setReligion(CustomerConstant.Religion_Kristen);
                        } else if (sw.getReligion().equals("4")) {
                            request.setReligion(CustomerConstant.Religion_Katolik);
                        } else if (sw.getReligion().equals("2")) {
                            request.setReligion(CustomerConstant.Religion_Hindu);
                        } else if (sw.getReligion().equals("1")) {
                            request.setReligion(CustomerConstant.Religion_Budha);
                        } else {
                            request.setReligion(CustomerConstant.Religion_Lainnya);
                        }
                        if (sw.getHouseType().equals("1")) {
                            request.setResidenceStatus(CustomerConstant.Residence_Status_Milik_Sendiri);
                        } else if (sw.getHouseType().equals("2")) {
                            request.setResidenceStatus(CustomerConstant.Residence_Status_Orang_Tua);
                        } else {
                            request.setResidenceStatus(CustomerConstant.Residence_Status_Mengontrak);
                        }
                        if (sw.getEducation().equals("1")) {
                            request.setLatestEducation(CustomerConstant.Edu_SD);
                        } else if (sw.getEducation().equals("2")) {
                            request.setLatestEducation(CustomerConstant.Edu_SLTP);
                        } else if (sw.getEducation().equals("3")) {
                            request.setLatestEducation(CustomerConstant.Edu_SLTA);
                        } else {
                            request.setLatestEducation(CustomerConstant.Edu_S1);
                        }
                        request.setHusbandName(sw.getCoupleName());
                        if (sw.getCoupleBirthPlace() != null) {
                            AreaDistrict kab1 = areaService.findById(sw.getCoupleBirthPlace());
                            if (kab1 != null) {
                                request.setHusbandPlaceOfBirth(kab1.getAreaName());
                            } else {
                                request.setHusbandPlaceOfBirth("");
                            }
                        } else {
                            request.setHusbandPlaceOfBirth("");
                        }
                        if (sw.getCoupleBirthDate() != null) {
                            request.setHusbandDateOfBirth(formatter2.format(sw.getCoupleBirthDate()));
                        } else {
                            request.setHusbandDateOfBirth("");
                        }
                        request.setAddress(sw.getAddress());
                        request.setRtRw(sw.getRtrw());
                        AreaKelurahan kel = areaService.getAreaKelurahanByAreaId(sw.getKelurahan());
                        if (kel != null) {
                            request.setKelurahan(kel.getAreaName());
                        } else {
                            request.setKelurahan("");
                        }
                        if (sw.getKecamatan() != null) {
                            AreaSubDistrict kec = areaService.findKecamatanById(sw.getKecamatan());
                            if (kec != null) {
                                request.setKecamatan(kec.getAreaName());
                            } else {
                                request.setKecamatan("");
                            }
                        }
                        AreaProvince prov = areaService.findProvById(sw.getProvince());
                        if (prov != null) {
                            AreaDistrict city = areaService.findById(sw.getCity());
                            if (city != null) {
                                request.setState(city.getAreaName());
                                request.setCountry(city.getAreaId());
                            } else {
                                request.setState("");
                            }
                        } else {
                            request.setState("");
                        }
                        if (sw.getPostalCode() != null) {
                            request.setPostalCode(sw.getPostalCode());
                        } else {
                            request.setPostalCode("");
                        }
                        AP3R ap3r = ap3rService.findTopBySwId(sw.getSwId());
                        if (ap3r != null) {
                            if (!ap3r.getHasSaving().equals("")) {
                                if (ap3r.getHasSaving().equals("t")) {
                                    request.setSavingAccountFlag("1");
                                } else {
                                    request.setSavingAccountFlag("0");
                                }
                            } else {
                                request.setSavingAccountFlag("0");
                            }
                            if (ap3r.getOpenSavingReason() != null) {
                                if (!ap3r.getOpenSavingReason().equals("")) {
                                    if (ap3r.getOpenSavingReason().equals("0")) {
                                        request.setReasonOpenAccount(CustomerConstant.Reason_Open_Account_Tabungan);
                                    } else {
                                        request.setReasonOpenAccount(CustomerConstant.Reason_Open_Transaksi_Usaha);
                                    }
                                } else {
                                    request.setReasonOpenAccount(CustomerConstant.Reason_Open_Account_Lainnya);
                                }
                            } else {
                                request.setReasonOpenAccount(CustomerConstant.Reason_Open_Account_Lainnya);
                            }
                            if (ap3r.getFundSource() != null) {
                                if (!ap3r.getFundSource().equals("")) {
                                    if (ap3r.getFundSource().equals("0")) {
                                        request.setFundSource(CustomerConstant.Fund_Source_Hasil_Usaha);
                                    } else {
                                        request.setFundSource(CustomerConstant.Fund_Source_Gaji);
                                    }
                                } else {
                                    request.setFundSource(CustomerConstant.Fund_Source_Lainnya);
                                }
                            } else {
                                request.setFundSource(CustomerConstant.Fund_Source_Lainnya);
                            }
                        } else {
                            request.setSavingAccountFlag("0");
                            request.setReasonOpenAccount(CustomerConstant.Reason_Open_Account_Lainnya);
                            request.setFundSource(CustomerConstant.Fund_Source_Lainnya);
                        }
                        PelatihanDasarKeanggotaan pdk = pdkService.findById(customer.getPdkId().toString());
                        if (pdk != null) {
                            request.setTrainedDate(formatter2.format(pdk.getPdkDate()));
                            request.setTrained("1");
                        } else {
                            request.setTrainedDate(formatter2.format(new Date()));
                            request.setTrained("0");
                        }
                        request.setSwId(null);
                        request.setPdkId(null);
                        request.setStatus(null);
                        request.setLongitude(null);
                        request.setLatitude(null);
                        request.setAction(null);
                        request.setCustomerId(null);
                        request.setGroupId(null);
                        request.setBusinessIncomeFamily("");
                        request.setCitizenShip("712");
                        request.setCitizenType("722");
                        request.setHouseCapacious("");
                        request.setHouseCondition("");
                        request.setHouseElectricity("");
                        request.setHouseFloor("");
                        request.setHouseIndexTotalScore("");
                        request.setHouseRoofType("");
                        request.setHouseSourceWater("");
                        request.setHouseWall("");
                        request.setLoanAset("");
                        request.setNetoAset("");
                        request.setEthinicity("");
                        request.setTotalAset("");
                        request.setCreatedDate(formatter2.format(new Date()));
                        request.setCreatedBy(request.getUsername());
                        request.setFormedByPersonnel(sw.getCreatedBy());
                        log.info("SEND REQUEST TO ESB");
                        AddCustomerResponse restResponse = restClient.addCustomer(request);
                        if (restResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)
                                || restResponse.getResponseCode().equals(WebGuiConstant.RC_CUSTOMER_ALREADY_EXIST)
                                || restResponse.getResponseCode().equals("31")) {
                            if (restResponse.getCustomerId() == null) {
                                log.error("FAILED CREATE CUSTOMER");
                                response.setResponseCode(restResponse.getResponseCode());
                                if (restResponse.getResponseCode().equals("31")) {
                                    response.setResponseCode(WebGuiConstant.RC_CUSTOMER_ALREADY_EXIST);
                                }
                                response.setResponseMessage(restResponse.getResponseMessage());
                                log.info("RESPONSE MESSAGE : " + response);
                                return response;
                            }
                            log.info("GET SUCCESS RESPONSE FROM ESB");
                            customer.setProsperaId(restResponse.getCustomerId());
                            customer.setCustomerCifNumber(restResponse.getCustomerNumber());
                            customer.setApprovedDate(new Date());
                            customer.setCustomerStatus(WebGuiConstant.STATUS_APPROVED);
                            log.info("UPDATE STATUS CUSTOMER FROM DRAFT TO APPROVED");
                            customerService.save(customer);
                            prospectId = customer.getProsperaId();
                            cifNumber = customer.getCustomerCifNumber();
                            if (customer.getCustomerId() != null) {
                                customerId = customer.getCustomerId().toString();
                                sw.setCustomerId(Long.parseLong(customerId));
                            }
                            swService.save(sw);
                        }
                        response.setResponseCode(restResponse.getResponseCode());
                        response.setResponseMessage(restResponse.getResponseMessage());
                    }
                } else {
                    Customer customerExist = customerService
                            .getCustomerByRrn(request.getRetrievalReferenceNumber());
                    if (customerExist != null)
                        customerId = customerExist.getCustomerId().toString();
                    log.error("INVALID RRN");
                    response.setResponseCode(WebGuiConstant.RC_INVALID_RRN);
                    String label = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(label);
                }
            }
        } catch (Exception e) {
            log.error("addCustomerV2 error: " + e.getMessage(), e);
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("addCustomerV2 error: " + e.getMessage());
        } finally {
            response.setCustomerId(customerId);
            response.setProspectId(prospectId);
            response.setCifNumber(cifNumber);
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_MODY_CUSTOMER);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        if (WebGuiConstant.STATUS_DRAFT.equals(request.getStatus())) {
                            terminalActivityService.insertRrn("addCustomer", request.getRetrievalReferenceNumber().trim());
                        } else if (WebGuiConstant.STATUS_APPROVED.equals(request.getStatus())) {
                            terminalActivityService.insertRrn("approveCustomer", request.getRetrievalReferenceNumber().trim());
                        }
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        AuditLog auditLog = new AuditLog();
                        auditLog.setActivityType(AuditLog.MODIF_CUSTOMER);
                        auditLog.setCreatedBy(request.getUsername());
                        auditLog.setCreatedDate(new Date());
                        auditLog.setDescription(request.toString() + " | " + response.toString());
                        auditLog.setReffNo(request.getRetrievalReferenceNumber());
                        auditLogService.insertAuditLog(auditLog);
                    }
                });
            } catch (Exception e) {
                log.error("addCustomerV2 saveTerminalActivityAndMessageLogs error: " + e.getMessage(), e);
            }
            log.info("addCustomerV2 RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            return response;
        }
    }

}