package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.loan.DeviationPhoto;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.sentra.SentraPhoto;
import id.co.telkomsigma.btpns.mprospera.model.survey.SurveyPhoto;
import id.co.telkomsigma.btpns.mprospera.model.sw.SwSurveyPhoto;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.PhotoRequest;
import id.co.telkomsigma.btpns.mprospera.response.PhotoResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Controller("webServiceGetPhotoController")
public class WebServiceGetPhotoController extends GenericController {

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private LoanService loanService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private SurveyService surveyService;

    @Autowired
    private SWService swService;

    @Autowired
    private SentraService sentraService;

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_DEVIATION_PHOTO_LINK_REQUEST, method = {RequestMethod.POST})
    public void getIdPhoto(final HttpServletRequest request, final HttpServletResponse response,
                           @PathVariable("apkVersion") String apkVersion, @PathVariable("id") String id,
                           @RequestHeader(value = "transmissionDateAndTime", required = true) String transmissionDateAndTime,
                           @RequestHeader(value = "retrievalReferenceNumber", required = true) final String retrievalReferenceNumber,
                           @RequestHeader(value = "username", required = true) final String username,
                           @RequestHeader(value = "sessionKey", required = true) final String sessionKey,
                           @RequestHeader(value = "imei", required = true) final String imei) {

        final PhotoRequest requestMapping = new PhotoRequest();
        requestMapping.setTransmissionDateAndTime(transmissionDateAndTime);
        requestMapping.setRetrievalReferenceNumber(retrievalReferenceNumber);
        requestMapping.setUsername(username);
        requestMapping.setSessionKey(sessionKey);
        requestMapping.setImei(imei);
        final PhotoResponse responseCode = new PhotoResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("deviationPhoto Try to processing get deviation photo request: " + request.getReader().lines().collect(Collectors.joining(System.lineSeparator())));
            log.info("Validating Request");
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
                try {
                    response.getWriter().write((new JsonUtils().toJson(responseCode)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.info("Get Deviation Photo");
                DeviationPhoto photo = loanService.findByDeviationId(Long.parseLong(id));
                if (photo != null) {
                    responseCode.setPhoto(photo.getDeviationPhoto());
                    try {
                        response.setContentType("image/jpg;charset=utf-8");
                        response.getOutputStream().write(Base64.decodeBase64(responseCode.getPhoto()));
                        response.getOutputStream().flush();
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                } else {
                    responseCode.setResponseCode(WebGuiConstant.RC_ID_PHOTO_NULL);
                    String labelError = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(labelError);
                    try {
                        response.getWriter().write((new JsonUtils().toJson(responseCode)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            log.error("deviationPhoto error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("deviationPhoto error: " + e.getMessage());
            try {
                response.getWriter().write((new JsonUtils().toJson(responseCode)));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(imei);
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_GET_ID_PHOTO);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(retrievalReferenceNumber.trim());
                        terminalActivity.setSessionKey(sessionKey);
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(username.trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
            } catch (Exception e) {
                log.error("deviationPhoto saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
        }

    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_SURVEY_PHOTO_LINK_REQUEST, method = {RequestMethod.POST})
    public void getSurveyPhoto(final HttpServletRequest request, final HttpServletResponse response,
                               @PathVariable("apkVersion") String apkVersion, @PathVariable("id") String id,
                               @RequestHeader(value = "transmissionDateAndTime", required = true) String transmissionDateAndTime,
                               @RequestHeader(value = "retrievalReferenceNumber", required = true) final String retrievalReferenceNumber,
                               @RequestHeader(value = "username", required = true) final String username,
                               @RequestHeader(value = "sessionKey", required = true) final String sessionKey,
                               @RequestHeader(value = "imei", required = true) final String imei) {

        final PhotoRequest requestMapping = new PhotoRequest();
        requestMapping.setTransmissionDateAndTime(transmissionDateAndTime);
        requestMapping.setRetrievalReferenceNumber(retrievalReferenceNumber);
        requestMapping.setUsername(username);
        requestMapping.setSessionKey(sessionKey);
        requestMapping.setImei(imei);
        final PhotoResponse responseCode = new PhotoResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("surveyPhoto Try to processing get survey photo request: " + request.getReader().lines().collect(Collectors.joining(System.lineSeparator())));
            log.info("Validating Request");
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
                try {
                    response.getWriter().write((new JsonUtils().toJson(responseCode)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.info("Get Survey Photo");
                SurveyPhoto photo = surveyService.findBySurveyId(Long.parseLong(id));
                if (photo != null) {
                    responseCode.setPhoto(photo.getSurveyPhoto());
                    try {
                        response.setContentType("image/jpg;charset=utf-8");
                        response.getOutputStream().write(Base64.decodeBase64(responseCode.getPhoto()));
                        response.getOutputStream().flush();
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                } else {
                    responseCode.setResponseCode(WebGuiConstant.RC_ID_PHOTO_NULL);
                    String labelError = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(labelError);
                    try {
                        response.getWriter().write((new JsonUtils().toJson(responseCode)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            log.error("surveyPhoto error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("surveyPhoto error: " + e.getMessage());
            try {
                response.getWriter().write((new JsonUtils().toJson(responseCode)));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(imei);
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_GET_ID_PHOTO);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(retrievalReferenceNumber.trim());
                        terminalActivity.setSessionKey(sessionKey);
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(username.trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
            } catch (Exception e) {
                log.error("surveyPhoto saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
        }

    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_BUSINESS_PLACE_PHOTO_LINK_REQUEST, method = {RequestMethod.POST})
    public void getBusinessPlacePhoto(final HttpServletRequest request, final HttpServletResponse response,
                                      @PathVariable("apkVersion") String apkVersion, @PathVariable("id") String id,
                                      @RequestHeader(value = "transmissionDateAndTime", required = true) String transmissionDateAndTime,
                                      @RequestHeader(value = "retrievalReferenceNumber", required = true) final String retrievalReferenceNumber,
                                      @RequestHeader(value = "username", required = true) final String username,
                                      @RequestHeader(value = "sessionKey", required = true) final String sessionKey,
                                      @RequestHeader(value = "imei", required = true) final String imei) {

        final PhotoRequest requestMapping = new PhotoRequest();
        requestMapping.setTransmissionDateAndTime(transmissionDateAndTime);
        requestMapping.setRetrievalReferenceNumber(retrievalReferenceNumber);
        requestMapping.setUsername(username);
        requestMapping.setSessionKey(sessionKey);
        requestMapping.setImei(imei);
        final PhotoResponse responseCode = new PhotoResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("businessPlacePhoto Try to processing get business place photo request: " + request.getReader().lines().collect(Collectors.joining(System.lineSeparator())));
            log.info("Validating Request");
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
                try {
                    response.getWriter().write((new JsonUtils().toJson(responseCode)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.info("Get Business Place Photo");
                SwSurveyPhoto photo = swService.getSwSurveyPhoto(id);
                if (photo != null) {
                    responseCode.setPhoto(photo.getSurveyPhoto());
                    try {
                        response.setContentType("image/jpg;charset=utf-8");
                        response.getOutputStream().write(Base64.decodeBase64(responseCode.getPhoto()));
                        response.getOutputStream().flush();
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                } else {
                    responseCode.setResponseCode(WebGuiConstant.RC_SURVEY_PHOTO_NULL);
                    String labelError = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(labelError);
                    try {
                        response.getWriter().write((new JsonUtils().toJson(responseCode)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            log.error("businessPlacePhoto error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("businessPlacePhoto error: " + e.getMessage());
            try {
                response.getWriter().write((new JsonUtils().toJson(responseCode)));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(imei);
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_GET_SURVEY_PHOTO);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(retrievalReferenceNumber.trim());
                        terminalActivity.setSessionKey(sessionKey);
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(username.trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
            } catch (Exception e) {
                log.error("businessPlacePhoto saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
        }

    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_SENTRA_PHOTO_LINK_REQUEST, method = {RequestMethod.POST})
    public void getSentraPhoto(final HttpServletRequest request, final HttpServletResponse response,
                               @PathVariable("apkVersion") String apkVersion, @PathVariable("id") String id,
                               @RequestHeader(value = "transmissionDateAndTime", required = true) String transmissionDateAndTime,
                               @RequestHeader(value = "retrievalReferenceNumber", required = true) final String retrievalReferenceNumber,
                               @RequestHeader(value = "username", required = true) final String username,
                               @RequestHeader(value = "sessionKey", required = true) final String sessionKey,
                               @RequestHeader(value = "imei", required = true) final String imei) {

        final PhotoRequest requestMapping = new PhotoRequest();
        requestMapping.setTransmissionDateAndTime(transmissionDateAndTime);
        requestMapping.setRetrievalReferenceNumber(retrievalReferenceNumber);
        requestMapping.setUsername(username);
        requestMapping.setSessionKey(sessionKey);
        requestMapping.setImei(imei);
        final PhotoResponse responseCode = new PhotoResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("getSentraPhoto Try to processing get sentra photo request: " + request.getReader().lines().collect(Collectors.joining(System.lineSeparator())));
            log.info("Validating Request");
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
                try {
                    response.getWriter().write((new JsonUtils().toJson(responseCode)));
                } catch (Exception e) {
                    log.error("getSentraPhoto error: " + e.getMessage());
                }
            } else {
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.info("Get Business Place Photo");
                SentraPhoto photo = sentraService.getSentraPhoto(Long.parseLong(id));
                if (photo != null) {
                    responseCode.setPhoto(photo.getSentraPhoto());
                    try {
                        response.setContentType("image/jpg;charset=utf-8");
                        response.getOutputStream().write(Base64.decodeBase64(responseCode.getPhoto()));
                        response.getOutputStream().flush();
                    } catch (Exception e) {
                        log.error("getSentraPhoto error: " + e.getMessage());
                    }
                } else {
                    responseCode.setResponseCode(WebGuiConstant.RC_SENTRA_PHOTO_NULL);
                    String labelError = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(labelError);
                    try {
                        response.getWriter().write((new JsonUtils().toJson(responseCode)));
                    } catch (Exception e) {
                        log.error("getSentraPhoto error: " + e.getMessage());
                    }
                }
            }
        } catch (Exception e) {
            log.error("getSentraPhoto error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("getSentraPhoto error: " + e.getMessage());
            try {
                response.getWriter().write((new JsonUtils().toJson(responseCode)));
            } catch (Exception ex) {
                log.error("getSentraPhoto error: " + ex.getMessage());
            }
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(imei);
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_GET_SURVEY_PHOTO);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(retrievalReferenceNumber.trim());
                        terminalActivity.setSessionKey(sessionKey);
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(username.trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
            } catch (Exception e) {
                log.error("getSentraPhoto saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
        }
    }

}