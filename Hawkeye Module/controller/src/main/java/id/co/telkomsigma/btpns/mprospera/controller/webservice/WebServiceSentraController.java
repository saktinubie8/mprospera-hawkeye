package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaDistrict;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaKelurahan;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaProvince;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaSubDistrict;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import id.co.telkomsigma.btpns.mprospera.model.sentra.SentraPhoto;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.GroupRequest;
import id.co.telkomsigma.btpns.mprospera.request.PhotoRequest;
import id.co.telkomsigma.btpns.mprospera.request.SentraRequest;
import id.co.telkomsigma.btpns.mprospera.response.*;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.PercentageSynchronizer;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("webserviceSentraController")
public class WebServiceSentraController extends GenericController {

    @Autowired
    private AreaService areaService;

    @Autowired
    private SentraService sentraService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private AuditLogService auditLogService;

    @Autowired
    private UserService userService;

    @Autowired
    private RESTClient restClient;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private HazelcastClientService hazelcastService;

    @Autowired
    private WSValidationService wsValidationService;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
    SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");

    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.SENTRA_SUBMIT_DATA_REQUEST, method = {RequestMethod.POST})
    public
    @ResponseBody
    SentraResponse doAdd(@RequestBody String requestString,
                         @PathVariable("apkVersion") String apkVersion) throws Exception {

        log.info("addSentra INCOMING SUBMIT SENTRA REQUEST : " + requestString);
        Object obj = new JsonUtils().fromJson(requestString, SentraRequest.class);
        final SentraRequest request = (SentraRequest) obj;
        final SentraResponse response = new SentraResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        String sentraId = "";
        try {
            log.debug("TRY VALIDATING REQUEST");
            // validation imei, session key
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(),
                    request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei()
                        + " ,sessionKey : " + request.getSessionKey());
            } else if (!"REJECTED".equalsIgnoreCase(request.getStatus())
                    && !"DRAFT".equalsIgnoreCase(request.getStatus())
                    && !"APPROVED".equalsIgnoreCase(request.getStatus())) {
                log.error("UNKNOWN STATUS");
                response.setResponseCode(WebGuiConstant.RC_UNKNOWN_STATUS);
            } else {
                Timer timer = new Timer();
                timer.start("[" + WebGuiConstant.SENTRA_SUBMIT_DATA_REQUEST + "] [sentraService.findBySentraId]");
                Sentra sentra = sentraService.findBySentraId(request.getSentraId());
                timer.stop();
                if (sentra == null) {
                    log.debug("NEW SENTRA");
                    sentra = new Sentra();
                }
                if (WebGuiConstant.STATUS_APPROVED.equals(sentra.getStatus()) && !"update".equals(request.getAction())) {
                    SentraResponse sentraResponse = new SentraResponse();
                    log.error("SENTRA ALREADY APPROVED");
                    sentraResponse.setResponseCode(WebGuiConstant.RC_SENTRA_ALREADY_APPROVED);
                    String label = getMessage("webservice.rc.label." + sentraResponse.getResponseCode());
                    sentraResponse.setResponseMessage(label);
                    log.info("RESPONSE MESSAGE : " + sentraResponse);
                    return sentraResponse;
                } else if (!WebGuiConstant.STATUS_APPROVED.equals(request.getStatus())) {
                    // insert ke Sentra
                    sentra.setAreaId(request.getAreaId());
                    if (!request.getStatus().equals(WebGuiConstant.STATUS_REJECTED)) {
                        sentra.setCreatedBy(request.getUsername());
                        sentra.setCreatedDate(new Date());
                        sentra.setAssignedTo(request.getUsername());
                    }
                    if ("delete".equals(request.getAction()))
                        sentra.setIsDeleted(true);
                    sentra.setLatitude(request.getLatitude());
                    sentra.setLongitude(request.getLongitude());
                    sentra.setPrsDay(request.getPrsDay().charAt(0));
                    sentra.setRtRw(request.getRtRw());
                    sentra.setSentraAddress(request.getSentraAddress());
                    sentra.setMeetingPlace(request.getMeetingPlace());
                    sentra.setSentraName(request.getSentraName());
                    sentra.setSentraOwnerName(request.getSentraOwnerName());
                    sentra.setSentraLeader(request.getSentraLeader());
                    if (request.getStartedDate() != null || request.getStartedDate() != "")
                        sentra.setStartedDate(formatter.parse(request.getStartedDate()));
                    sentra.setLocationId(request.getOfficeCode());
                    sentra.setPostCode(request.getPostcode());
                    if (request.getMeetingTime() != null) {
                        if (!request.getMeetingTime().equals(""))
                            sentra.setMeetingTime(new java.sql.Time(formatTime.parse(request.getMeetingTime()).getTime()));
                    }
                }
                if ("delete".equals(request.getAction()) || "update".equals(request.getAction())) {
                    if (null != request.getSentraId()) {
                        if (!request.getSentraId().equals("")) {
                            sentra.setSentraId(Long.parseLong(request.getSentraId()));
                            sentra.setUpdatedDate(new Date());
                            sentraId = request.getSentraId();
                        } else {
                            log.error("UNKONWN SENTRA ID");
                            response.setResponseCode(WebGuiConstant.RC_UNKNOWN_SENTRA_ID);
                        }
                    } else {
                        log.error("UNKNOWN SENTRA ID");
                        response.setResponseCode(WebGuiConstant.RC_UNKNOWN_SENTRA_ID);
                        String label = getMessage("webservice.rc.label." + response.getResponseCode());
                        response.setResponseMessage(label);
                        log.info("RESPONSE MESSAGE : " + response);
                        return response;
                    }
                }
                if (("delete".equals(request.getAction()) || "update".equals(request.getAction()))
                        && !sentraService.isValidSentra(Long.parseLong(request.getSentraId()))) {
                    log.error("UNKNOWN SENTRA ID");
                    response.setResponseCode(WebGuiConstant.RC_UNKNOWN_SENTRA_ID);
                } else {
                    if (!WebGuiConstant.STATUS_APPROVED.equals(request.getStatus())) {
                        if (!terminalService.isDuplicate(request.getRetrievalReferenceNumber(), "addSentra")) {
                            sentra.setStatus(request.getStatus());
                            log.debug("SAVING SENTRA DATA");
                            sentraService.save(sentra);
                            sentraId = sentra.getSentraId().toString();
                        } else {
                            Timer timer2 = new Timer();
                            timer2.start("[" + WebGuiConstant.SENTRA_SUBMIT_DATA_REQUEST
                                    + "] [sentraService.getSentraByRrn]");
                            Sentra sentraExist = sentraService.getSentraByRrn(request.getRetrievalReferenceNumber());
                            timer2.stop();
                            if (sentraExist != null) {
                                sentraId = sentraExist.getSentraId().toString();
                            }
                        }
                    }
                }
                // save sentraPhoto
                if (request.getSentraPicture() != null && !"".equals(request.getSentraPicture())) {
                    SentraPhoto sentraPhoto = new SentraPhoto();
                    sentraPhoto.setSentraId(sentra.getSentraId());
                    sentraPhoto.setSentraPhoto(request.getSentraPicture().getBytes());
                    // cek actionnya , insert apa update atau delete
                    if ("delete".equals(request.getAction()) || "update".equals(request.getAction())) {
                        SentraPhoto sentraPhotoExisting = new SentraPhoto();
                        Timer timer2 = new Timer();
                        timer2.start("[" + WebGuiConstant.SENTRA_SUBMIT_DATA_REQUEST
                                + "] [sentraService.getSentraPhoto]");
                        sentraPhotoExisting = sentraService.getSentraPhoto(Long.parseLong(request.getSentraId()));
                        timer2.stop();
                        if (null != sentraPhotoExisting) {
                            sentraPhoto.setId(sentraPhotoExisting.getId());
                        } else {
                            log.debug("UNKNOWN SENTRA ID");
                            response.setResponseCode(WebGuiConstant.RC_UNKNOWN_SENTRA_ID);
                        }
                    }
                    log.debug("SAVING SENTRA PHOTO");
                    sentraService.savePhoto(sentraPhoto);
                }
                log.debug("START MAPPING DATA TO ESB");
                // insert ke prospera
                if (response.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)
                        && (WebGuiConstant.STATUS_APPROVED).equals(request.getStatus())) {
                    AreaKelurahan kel = new AreaKelurahan();
                    AreaSubDistrict kec = new AreaSubDistrict();
                    AreaDistrict kab = new AreaDistrict();
                    AreaProvince prov = new AreaProvince();
                    sentra.setApprovedBy(request.getUsername());
                    Timer timer3 = new Timer();
                    timer3.start("[" + WebGuiConstant.SENTRA_SUBMIT_DATA_REQUEST
                            + "] [addSdaService.findKelurahanById]");
                    kel = areaService.findKelurahanById(sentra.getAreaId());
                    timer3.stop();
                    Timer timer2 = new Timer();
                    timer2.start("[" + WebGuiConstant.SENTRA_SUBMIT_DATA_REQUEST
                            + "] [addSdaService.findSubDistrictById]");
                    kec = areaService.findSubDistrictById(kel.getParentAreaId());
                    timer2.stop();
                    kab = areaService.findDistrictById(kec.getParentAreaId());
                    prov = areaService.findProvinceById(kab.getParentAreaId());
                    String action = null;
                    if (request.getStatus().equals(WebGuiConstant.STATUS_APPROVED)
                            && sentra.getStatus().equals(WebGuiConstant.STATUS_DRAFT)) {
                        action = "add";
                    } else if (request.getStatus().equals(WebGuiConstant.STATUS_APPROVED)
                            && sentra.getStatus().equals(WebGuiConstant.STATUS_APPROVED)) {
                        action = "update";
                    }
                    request.setReffId(String.valueOf(sentra.getProsperaId()));
                    if (prov.getAreaName() == null) {
                        request.setProvinsi("");
                    } else {
                        request.setProvinsi(prov.getAreaName());
                    }
                    if (kab.getAreaName() == null) {
                        request.setKabupaten("");
                    } else {
                        request.setKabupaten(kab.getAreaName());
                    }
                    if (kec.getAreaName() == null) {
                        request.setKecamatan("");
                    } else {
                        request.setKecamatan(kec.getAreaName());
                    }
                    if (kel.getAreaName() == null) {
                        request.setKelurahan("");
                    } else {
                        request.setKelurahan(kel.getAreaName());
                    }
                    request.setDayNumber(new SimpleDateFormat("dd").format(sentra.getStartedDate()));
                    request.setGetCountData(null);
                    request.setSentraPicture(null);
                    request.setSentraOwnerName(null);
                    if (request.getSentraLeader() != null) {
                        request.setCustomerNumberLeader(request.getSentraLeader());
                    } else {
                        request.setCustomerNumberLeader("");
                    }
                    if (action.equals("update")) {
                        if (request.getSentraLeader() != null) {
                            request.setCustomerNumberLeader(request.getSentraLeader());
                        } else {
                            request.setCustomerNumberLeader("");
                        }
                    }
                    if (sentra.getPostCode() == null) {
                        request.setPostcode("");
                    } else {
                        request.setPostcode(sentra.getPostCode());
                    }
                    request.setAreaId(null);
                    request.setStatus(null);
                    if (sentra.getRtRw() == null) {
                        request.setRtRw("");
                    } else {
                        request.setRtRw(sentra.getRtRw());
                    }
                    if (sentra.getPrsDay() == '1') {
                        request.setPrsDay("2");
                    } else if (sentra.getPrsDay() == '2') {
                        request.setPrsDay("3");
                    } else if (sentra.getPrsDay() == '3') {
                        request.setPrsDay("4");
                    } else if (sentra.getPrsDay() == '4') {
                        request.setPrsDay("5");
                    }
                    request.setSentraAddress(sentra.getSentraAddress());
                    request.setMeetingPlace(sentra.getMeetingPlace());
                    request.setSessionKey(null);
                    request.setStartedDate(new SimpleDateFormat("yyyyMMdd").format(sentra.getStartedDate()));
                    request.setUsername(sentra.getCreatedBy());
                    if (request.getMeetingTime() == null) {
                        request.setActionTime("00:00:00");
                    } else {
                        if (request.getMeetingTime().equals("")) {
                            request.setActionTime("00:00:00");
                        } else {
                            request.setActionTime(request.getMeetingTime() + ":00");
                        }
                    }
                    request.setRecurAfter("2");
                    request.setMeetingTime(null);
                    request.setLocalId(null);
                    request.setTokenKey(null);
                    Timer timer4 = new Timer();
                    timer4.start("[" + WebGuiConstant.SENTRA_SUBMIT_DATA_REQUEST
                            + "] [areaService.findLocationCodeByLocationId]");
                    request.setOfficeCode(areaService.findLocationCodeByLocationId(sentra.getLocationId()));
                    timer4.stop();
                    if (request.getAction() != "delete") {
                        SentraResponse restResponse = restClient.processSentra(request, action.toLowerCase());
                        if (restResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)) {
                            if (restResponse.getSentraId() == null) {
                                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                                response.setResponseMessage(label);
                                return response;
                            }
                            sentra.setProsperaId(restResponse.getSentraId());
                            sentra.setSentraCode(restResponse.getKodeSentra());
                            sentra.setApprovedDate(new Date());
                            sentra.setStatus(WebGuiConstant.STATUS_APPROVED);
                            if (request.getSentraLeader() != null) {
                                sentra.setSentraLeader(request.getSentraLeader());
                            }
                            sentra.setCreatedDate(sentra.getCreatedDate());
                            sentraService.save(sentra);
                        } else if (restResponse.getResponseCode().equals("21")) {
                            response.setResponseCode(WebGuiConstant.RC_SUCCESS);
                            response.setResponseMessage(WebGuiConstant.RC_SENTRA_ALREADY_EXIST);
                            return response;
                        }
                        response.setResponseCode(restResponse.getResponseCode());
                        response.setResponseMessage(restResponse.getResponseMessage());
                    }
                }
            }
        } catch (Exception e) {
            log.error("addSentra saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("addSentra error: " + e.getMessage());
        } finally {
            log.debug("Sentra ID : " + sentraId);
            response.setSentraId(sentraId);
            response.setLocalId(request.getLocalId());
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_MODY_SENTRA);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.insertRrn("addSentra", request.getRetrievalReferenceNumber().trim());
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        AuditLog auditLog = new AuditLog();
                        auditLog.setActivityType(AuditLog.MODIF_SENTRA);
                        auditLog.setCreatedBy(request.getUsername());
                        auditLog.setCreatedDate(new Date());
                        if (!request.toString().equals(null) && !request.toString().equals("")
                                && request.toString().length() > 1000)
                            auditLog.setDescription(request.toString().substring(0, 1000) + " | " + response.toString());
                        auditLog.setReffNo(request.getRetrievalReferenceNumber());
                        auditLogService.insertAuditLog(auditLog);
                    }
                });
            } catch (Exception e) {
                log.error("addSentra saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            log.info("addSentra RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            return response;
        }

    }

    @RequestMapping(value = WebGuiConstant.GROUP_SUBMIT_DATA_REQUEST, method = {RequestMethod.POST})
    public
    @ResponseBody
    GroupResponse doAddGroup(@RequestBody String requestString,
                             @PathVariable("apkVersion") String apkVersion) throws Exception {

        log.info("addSentraGroup INCOMING SUBMIT GROUP REQUEST : " + requestString);
        Object obj = new JsonUtils().fromJson(requestString, GroupRequest.class);
        final GroupRequest request = (GroupRequest) obj;
        final GroupResponse response = new GroupResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        String groupId = "";
        log.debug("TRY TO VALIDATING REQUEST");
        try {
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(),
                    request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei()
                        + " ,sessionKey : " + request.getSessionKey());
            } else if (!"REJECT".equalsIgnoreCase(request.getStatus())
                    && !"DRAFT".equalsIgnoreCase(request.getStatus())
                    && !"APPROVED".equalsIgnoreCase(request.getStatus())) {
                response.setResponseCode(WebGuiConstant.RC_UNKNOWN_STATUS);
            } else {
                // cek sentraIdnya ada apa ngga
                Sentra sentra = new Sentra();
                sentra = sentraService.findBySentraId(request.getSentraId());
                if (null != sentra) {
                    Timer timer = new Timer();
                    timer.start("[" + WebGuiConstant.GROUP_SUBMIT_DATA_REQUEST + "] [sentraService.getGroupByGroupId]");
                    Group group = sentraService.getGroupByGroupId(request.getGroupId());
                    timer.stop();
                    if (group == null) {
                        group = new Group();
                    }
                    if ("update".equals(request.getAction()) && group.getStatus().equals(WebGuiConstant.STATUS_APPROVED)) {
                        log.error("GROUP ALREADY APPROVED");
                        String label = getMessage("webservice.rc.label." + WebGuiConstant.RC_GROUP_ALREADY_EXIST);
                        response.setResponseMessage(label);
                        log.info("RESPONSE MESSAGE : " + response);
                        return response;
                    }
                    if (WebGuiConstant.STATUS_APPROVED.equals(group.getStatus())
                            && !"update".equals(request.getAction())) {
                        GroupResponse sentraResponse = new GroupResponse();
                        log.error("GROUP ALREADY APPROVED");
                        sentraResponse.setResponseCode(WebGuiConstant.RC_GROUP_ALREADY_EXIST);
                        String label = getMessage("webservice.rc.label." + sentraResponse.getResponseCode());
                        sentraResponse.setResponseMessage(label);
                        log.info("RESPONSE MESSAGE : " + sentraResponse);
                        return sentraResponse;
                    } else if (!WebGuiConstant.STATUS_APPROVED.equals(sentra.getStatus())) {
                        GroupResponse sentraResponse = new GroupResponse();
                        log.error("SENTRA HAS NOT BEEN APPROVED");
                        sentraResponse.setResponseCode(WebGuiConstant.RC_SENTRA_NOT_APPROVED);
                        String label = getMessage("webservice.rc.label." + sentraResponse.getResponseCode());
                        sentraResponse.setResponseMessage(label);
                        log.info("RESPONSE MESSAGE : " + sentraResponse);
                        return sentraResponse;
                    } else if (!WebGuiConstant.STATUS_APPROVED.equals(request.getStatus())) {
                        group.setGroupLeader(request.getGroupLeader());
                        group.setGroupName(request.getGroupName());
                        if ("delete".equals(request.getAction())) {
                            group.setIsDeleted(true);
                        }
                        group.setLatitude(request.getLatitude());
                        group.setLongitude(request.getLongitude());
                        group.setSentra(sentra);
                        group.setCreatedBy(request.getUsername());
                        group.setCreatedDate(new Date());
                        group.setStatus(request.getStatus());
                        if ("delete".equals(request.getAction()) || "update".equals(request.getAction())) {
                            if (request.getGroupId() != null) {
                                group.setGroupId(Long.parseLong(request.getGroupId()));
                                groupId = request.getGroupId();
                            } else {
                                log.error("UNKNOWN SENTRA ID");
                                response.setResponseCode(WebGuiConstant.RC_UNKNOWN_SENTRA_ID);
                            }
                        }
                        if (("delete".equals(request.getAction()) || "update".equals(request.getAction()))
                                && !sentraService.isValidGroup(Long.parseLong(request.getGroupId()))) {
                            log.error("UNKNOWN SENTRA ID");
                            response.setResponseCode(WebGuiConstant.RC_UNKNOWN_SENTRA_ID);
                        } else {
                            if (!terminalService.isDuplicate(request.getRetrievalReferenceNumber(), "addGroup")) {
                                log.debug("SAVING GROUP DATA");
                                sentraService.saveGroup(group);
                                groupId = group.getGroupId().toString();
                            } else {
                                Group groupExist = sentraService.getGroupByRrn(request.getRetrievalReferenceNumber());
                                if (groupExist != null) {
                                    groupId = groupExist.getGroupId().toString();
                                }
                            }
                        }
                    } else // kalo approved
                    {
                        // insert ke prospera
                        if (response.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)
                                && WebGuiConstant.STATUS_APPROVED.equals(request.getStatus())) {
                            String action = null;
                            if (request.getStatus().equals(WebGuiConstant.STATUS_APPROVED)
                                    && group.getStatus().equals(WebGuiConstant.STATUS_DRAFT)) {
                                action = "add";
                            } else if (request.getStatus().equals(WebGuiConstant.STATUS_APPROVED)
                                    && group.getStatus().equals(WebGuiConstant.STATUS_APPROVED)) {
                                action = "update";
                            }
                            group.setApprovedBy(request.getUsername());
                            request.setCustomerNumberCenter(sentra.getProsperaId());
                            request.setReffId(String.valueOf(group.getGroupId()));
                            request.setStatus(null);
                            request.setGroupId(null);
                            request.setSentraId(null);
                            request.setLatitude(null);
                            request.setLongitude(null);
                            request.setUsername(group.getCreatedBy());
                            if (action.equals("update")) {
                                if (request.getGroupLeader() == null) {
                                    log.error("GROUP LEADER NULL");
                                    response.setResponseCode(WebGuiConstant.RC_GROUP_LEADER_NULL);
                                    String label = getMessage("webservice.rc.label." + response.getResponseCode());
                                    response.setResponseMessage(label);
                                    log.info("RESPONSE MESSAGE : " + response);
                                    return response;
                                }
                                request.setCustomerNumberLeader(request.getGroupLeader());
                                groupId = group.getGroupId().toString();
                                request.setGroupId(group.getProsperaId());
                            }
                            request.setTrainedDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
                            request.setTokenKey(null);
                            GroupResponse restResponse = restClient.processGroup(request, action.toLowerCase());
                            if (restResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)
                                    || restResponse.getResponseCode().equals(WebGuiConstant.RC_GROUP_ALREADY_EXIST)) {
                                if (restResponse.getGroupId() == null) {
                                    log.error("GROUP ID NULL");
                                    response.setResponseCode(WebGuiConstant.RC_SENTRAGROUP_NULL);
                                    String label = getMessage("webservice.rc.label." + response.getResponseCode());
                                    response.setResponseMessage(label);
                                    log.info("RESPONSE MESSAGE : " + response);
                                    return response;
                                }
                                group.setProsperaId(restResponse.getGroupId());
                                group.setApprovedDate(new Date());
                                if (request.getGroupLeader() != null) {
                                    group.setGroupLeader(request.getGroupLeader());
                                    group.setUpdatedDate(new Date());
                                    group.setUpdatedBy(request.getUsername());
                                }
                                group.setStatus(WebGuiConstant.STATUS_APPROVED);
                                log.debug("SAVING APPROVED GROUP");
                                sentraService.saveGroup(group);
                            } else if (restResponse.getResponseCode().equals("26")) {
                                log.error("GROUP ALREADY APPROVED");
                                response.setResponseCode(WebGuiConstant.RC_SUCCESS);
                                response.setResponseMessage(WebGuiConstant.RC_GROUP_ALREADY_EXIST);
                                log.info("RESPONSE MESSAGE : " + response);
                                return response;
                            }
                            response.setResponseCode(restResponse.getResponseCode());
                            response.setResponseMessage(restResponse.getResponseMessage());
                        }
                    }
                } else {
                    log.error("UNKNOWN SENTRA ID");
                    response.setResponseCode(WebGuiConstant.RC_UNKNOWN_SENTRA_ID);
                }
            }
        } catch (Exception e) {
            log.error("addSentraGroup error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("addSentraGroup error: " + e.getMessage());
        } finally {
            if (response.getResponseMessage() == null) {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
            }
            response.setGroupId(groupId);
            response.setLocalId(request.getLocalId());
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_MODY_SENTRA_GROUP);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.insertRrn("addGroup", request.getRetrievalReferenceNumber().trim());
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        AuditLog auditLog = new AuditLog();
                        auditLog.setActivityType(AuditLog.MODIF_SENTRA_GROUP);
                        auditLog.setCreatedBy(request.getUsername());
                        auditLog.setCreatedDate(new Date());
                        auditLog.setDescription(request.toString() + " | " + response.toString());
                        auditLog.setReffNo(request.getRetrievalReferenceNumber());
                        auditLogService.insertAuditLog(auditLog);
                    }
                });
            } catch (Exception e) {
                log.error("addSentraGroup saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            log.info("addSentraGroup RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            return response;
        }

    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_SENTRA_REQUEST, method = {RequestMethod.POST})
    public
    @ResponseBody
    SentraResponse doList(@RequestBody final SentraRequest request,
                          @PathVariable("apkVersion") String apkVersion) throws Exception {

        log.info("listSentra INCOMING MESSAGE : " + jsonUtils.toJson(request));
        final SentraResponse response = new SentraResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        log.debug("TRY VALIDATING REQUEST");
        try {
            // validating
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(),
                    request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei()
                        + " ,sessionKey : " + request.getSessionKey());
            } else {
                log.debug("START MAPPING DATA");
                final Page<Sentra> sentraPage = sentraService.getSentraByCreatedDate(request.getUsername(),
                        request.getPage(), request.getGetCountData(), request.getStartLookupDate(),
                        request.getEndLookupDate());
                final List<SentraListResponse> sentraListResponse = new ArrayList<SentraListResponse>();
                for (final Sentra sentra : sentraPage.getContent()) {
                    SentraListResponse sResponse = new SentraListResponse();
                    sResponse.setSentraId(sentra.getSentraId().toString());
                    sResponse.setAreaId(sentra.getAreaId());
                    sResponse.setSentraName(sentra.getSentraName());
                    sResponse.setCreatedBy(sentra.getCreatedBy());
                    sResponse.setLatitude(sentra.getLatitude());
                    sResponse.setLongitude(sentra.getLongitude());
                    sResponse.setProsperaId(sentra.getProsperaId());
                    sResponse.setLocationId(sentra.getLocationId());
                    sResponse.setPrsDay("" + sentra.getPrsDay());
                    sResponse.setRtRw(sentra.getRtRw());
                    sResponse.setSentraAddress(sentra.getSentraAddress());
                    sResponse.setStatus(sentra.getStatus());
                    sResponse.setSentraOwnerName(sentra.getSentraOwnerName());
                    sResponse.setSentraLeader(sentra.getSentraLeader());
                    sResponse.setPostCode(sentra.getPostCode());
                    sResponse.setSentraCode(sentra.getSentraCode());
                    sResponse.setMeetingPlace(sentra.getMeetingPlace());
                    if (sentra.getLastPrs() != null)
                        sResponse.setLastPrs(formatter.format(sentra.getLastPrs()));
                    if (sentra.getMeetingTime() != null)
                        sResponse.setMeetingTime(formatTime.format(sentra.getMeetingTime().getTime()));
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd hh:mm:ss");
                    if (sentra.getCreatedDate() != null) {
                        sResponse.setCreatedDate(formatter.format(sentra.getCreatedDate()));
                    }
                    if (sentra.getStartedDate() != null) {
                        sResponse.setStartedDate(formatter.format(sentra.getStartedDate()));
                    }
                    sResponse.setAssignedTo(sentra.getAssignedTo());
                    SentraPhoto photo = sentraService.getSentraPhoto(sentra.getSentraId());
                    if (photo != null) {
                        sResponse.setHasPhoto("true");
                    } else
                        sResponse.setHasPhoto("false");
                    // setGroupList
                    List<GroupListResponse> gResponse = new ArrayList<GroupListResponse>();
                    List<Group> groupList = new ArrayList<Group>();
                    groupList = sentraService.getGroupBySentraId(sentra.getSentraId());
                    if (groupList.size() != 0) {
                        for (Group group : groupList) {
                            GroupListResponse groupListResponse = new GroupListResponse();
                            groupListResponse.setGroupId(group.getGroupId().toString());
                            groupListResponse.setGroupLeader(group.getGroupLeader());
                            groupListResponse.setGroupName(group.getGroupName());
                            groupListResponse.setGroupStatus(group.getStatus());
                            groupListResponse.setCreatedBy(group.getCreatedBy());
                            if (group.getUpdatedDate() != null) {
                                groupListResponse.setUpdatedDate(formatter.format(group.getUpdatedDate()));
                            }
                            groupListResponse.setUpdatedBy(group.getUpdatedBy());
                            groupListResponse.setCreatedDate(formatter.format(group.getCreatedDate()));
                            gResponse.add(groupListResponse);
                        }
                    }
                    sResponse.setSentraGroup(gResponse);
                    sentraListResponse.add(sResponse);
                }
                log.debug("FINISH MAPPING DATA");
                Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                terminal.setSentraProgress(PercentageSynchronizer.processSyncPercent(request.getPage(),
                        String.valueOf(sentraPage.getTotalPages()), terminal.getSentraProgress()));
                terminalService.updateTerminal(terminal);
                final List<Sentra> listDeletedSentra = sentraService.findIsDeletedSentraList();
                List<String> deletedSentraList = new ArrayList<>();
                for (Sentra deletedSentra : listDeletedSentra) {
                    String id = deletedSentra.getSentraId().toString();
                    deletedSentraList.add(id);
                }
                final List<Group> listDeletedSentraGroup = sentraService.findIsDeletedSentraGroupList();
                List<String> deletedSentraGroupList = new ArrayList<>();
                for (Group deletedSentraGroup : listDeletedSentraGroup) {
                    String id = deletedSentraGroup.getGroupId().toString();
                    deletedSentraGroupList.add(id);
                }
                response.setDeletedSentraGroupList(deletedSentraGroupList);
                response.setDeletedSentraList(deletedSentraList);
                response.setCurrentTotal(String.valueOf(sentraPage.getContent().size()));
                response.setGrandTotal(String.valueOf(sentraPage.getTotalElements()));
                response.setTotalPage(String.valueOf(sentraPage.getTotalPages()));
                response.setSentraList(sentraListResponse);
            }
        } catch (Exception e) {
            log.error("listSentra error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("listSentra error: " + e.getMessage());
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_SENTRA);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
            } catch (Exception e) {
                log.error("listSentra saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            String label = getMessage("webservice.rc.label." + response.getResponseCode());
            if (response.getResponseMessage() == null) response.setResponseMessage(label);
            log.info("listSentra RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            return response;
        }

    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_SUBMIT_SENTRA_PHOTO_REQUEST, method = {RequestMethod.POST})
    public
    @ResponseBody
    PhotoResponse submitSentraPhoto(@RequestBody final PhotoRequest request,
                                    @PathVariable("apkVersion") String apkVersion) throws Exception {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final PhotoResponse response = new PhotoResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.debug("submitSentraPhoto Try to processing Submit Sentra Photo request");
            log.debug("Validating Request");
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                response.setResponseMessage("SUKSES");
                log.debug("trying to submit sentra photo ");
                Sentra sentra = sentraService.findBySentraId(request.getSentraId());
                if (sentra != null) {
                    SentraPhoto sentraPhoto = sentraService.getSentraPhoto(sentra.getSentraId());
                    if (terminalService.isDuplicate(request.getRetrievalReferenceNumber(), "submitSentraPhoto") && sentraPhoto != null) {
                        String labelDuplicate = getMessage("webservice.rc.label." + WebGuiConstant.RC_PHOTO_ALREADY_EXIST);
                        response.setResponseMessage(labelDuplicate);
                        return response;
                    }
                    if (sentraPhoto == null) {
                        sentraPhoto = new SentraPhoto();
                    }
                    sentraPhoto.setSentraId(sentra.getSentraId());
                    if (request.getSentraPhoto() != null) {
                        sentraPhoto.setSentraPhoto(request.getSentraPhoto().getBytes());
                    }
                    sentraService.savePhoto(sentraPhoto);
                } else {
                    log.error("UNKNOWN SENTRA ID");
                    response.setResponseCode(WebGuiConstant.RC_UNKNOWN_SENTRA_ID);
                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelFailed);
                }
            }
        } catch (Exception e) {
            log.error("submitSentraPhoto error : " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("submitSentraPhoto error: " + e.getMessage());
        } finally {
            try {
                // save ke terminal activity
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SUBMIT_DISBURSEMENT_PHOTO);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
            } catch (Exception e) {
                log.error("submitSentraPhoto saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            log.info("submitSentraPhoto RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            return response;
        }
    }

   /* @RequestMapping(value = WebGuiConstant.TERMINAL_GET_SENTRA_REQUEST, method = {RequestMethod.POST})
    public
    @ResponseBody
    SentraResponse doList(@RequestBody final SentraRequest request,
                          @PathVariable("apkVersion") String apkVersion) throws Exception {
        log.info("INCOMING MESSAGE : " + request.toString());

        final SentraResponse responseCode = new SentraResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);

        log.debug("TRY VALIDATING REQUEST");
        // validating
        String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(),
                request.getSessionKey(), apkVersion);

        if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
            responseCode.setResponseCode(validation);
            String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
            responseCode.setResponseMessage(label);
            log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei()
                    + " ,sessionKey : " + request.getSessionKey());
        } else {
            log.debug("START MAPPING DATA");
            log.debug("FINISH MAPPING DATA");
        }

        String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
        responseCode.setResponseMessage(label);
        log.info("RESPONSE MESSAGE : " + responseCode);
        return responseCode;
    }*/

}