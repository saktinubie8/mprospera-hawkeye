package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.CustomerConstant;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.customer.CycleNumberAndCustomer;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaDistrict;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaKelurahan;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaProvince;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaSubDistrict;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.request.SyncCustomerRequest;
import id.co.telkomsigma.btpns.mprospera.response.CustomerPojo;
import id.co.telkomsigma.btpns.mprospera.response.SyncCustomerResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("webserviceSyncCustomerControllerV2")
public class WebserviceSyncCustomerV2Controller extends GenericController {

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private SyncCustomerService syncCustomerService;

    @Autowired
    private SyncCustomerServiceV2 syncCustomerServiceV2;

    @Autowired
    private AreaService areaService;

    @Autowired
    private UserService userService;

    @Autowired
    private SentraService sentraService;

    @Autowired
    private SWService swService;

    JsonUtils jsonUtils = new JsonUtils();

    private final String dateTimeFormatPattern = "yyyyMMdd hh:mm:ss";
    final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateTimeFormatPattern);

    //@RequestMapping(value = WebGuiConstant.TERMINAL_SYNC_CUSTOMERV2_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SyncCustomerResponse syncCustomerV2(@RequestBody final SyncCustomerRequest request, @PathVariable("apkVersion") String apkVersion) throws Exception {

        final SyncCustomerResponse response = new SyncCustomerResponse();
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
        try {
            log.info("syncCustomer Try to processing SYNC CUSTOMER request: " + jsonUtils.toJson(request));
            String username = request.getUsername();
            String imei = request.getImei();
            String sessionKey = request.getSessionKey();
            String page = request.getPage();
            if (page == null || "".equals(page))
                page = "0";
            String startLookupDate = request.getStartLookupDate();
            String endLookupDate = request.getEndLookupDate();
            String getCountData = request.getGetCountData();
            AreaKelurahan kelurahan;
            AreaSubDistrict kecamatan;
            AreaDistrict kota;
            AreaProvince province;
            response.setResponseCode(WebGuiConstant.RC_SUCCESS);
            log.debug("Validating Request");
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                log.debug("Validation success, get customer data");
                User user = (User) userService.loadUserByUsername(request.getUsername());
                List<CycleNumberAndCustomer> customerList = syncCustomerServiceV2.getCustomerByLoc(page, getCountData,
                        startLookupDate, endLookupDate, user.getOfficeCode());
                log.debug("FINISH GET CUSTOMER, Page : " + request.getPage() + " , Total Customer : " + customerList.size());
                final List<Customer> listDeletedCustomer = syncCustomerService.findIsDeletedCustomerList();
                List<String> deletedCustomerList = new ArrayList<>();
                if (listDeletedCustomer != null) {
                    if (!listDeletedCustomer.isEmpty()) {
                        for (Customer deletedCustomer : listDeletedCustomer) {
                            String id = deletedCustomer.getCustomerId().toString();
                            deletedCustomerList.add(id);
                        }
                    }
                }
                log.debug("Start Parsing");
                for (CycleNumberAndCustomer sub : customerList) {
                    Timer timer = new Timer();
                    timer.start("Parsing Customer");
                    CustomerPojo kec = new CustomerPojo();
                    if (sub.getCreatedDate() != null) {
                        LocalDateTime customerCreatedDate = sub.getCreatedDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
                        kec.setCreatedDate(formatter.format(customerCreatedDate));
                    }
                    kec.setCustomerId(sub.getCustomerId().toString());
                    kec.setCustomerCifNumber(sub.getCustomerCifNumber());
                    kec.setCustomerIdNumber(sub.getCustomerIdNumber());
                    kec.setCustomerName(sub.getCustomerName());
                    kec.setCustomerAlias(sub.getCustomerAlias());
                    kec.setCustomerIdName(sub.getCustomerIdName());
                    kec.setCustomerIdAddress(sub.getCustomerIdAddress());
                    kec.setAddress(sub.getAddress());
                    kec.setRtRw(sub.getRtrw());
                    if (sub.getProvince() != null) {
                        province = areaService.findProvinceByAreaName(sub.getProvince());
                        if (province != null)
                            kec.setProvince(province.getAreaId());
                        else
                            kec.setProvince("");
                    } else {
                        kec.setProvince("");
                    }
                    if (sub.getCity() != null) {
                        if (!kec.getProvince().equals("")) {
                            kota = areaService.findKotaByAreaNameAndParent(sub.getCity(), kec.getProvince());
                        } else {
                            kota = areaService.findKotaByAreaName(sub.getCity());
                        }
                        if (kota != null)
                            kec.setKabupaten(kota.getAreaId());
                        else
                            kec.setKabupaten("");
                    } else {
                        kec.setKabupaten("");
                    }
                    if (sub.getKecamatan() != null) {
                        if (!kec.getKabupaten().equals("")) {
                            kecamatan = areaService.findKecamatanByAreaNameAndParent(sub.getKecamatan(), kec.getKabupaten());
                        } else {
                            kecamatan = areaService.findKecamatanByAreaName(sub.getKecamatan());
                        }
                        if (kecamatan != null)
                            kec.setKecamatan(kecamatan.getAreaId());
                        else
                            kec.setKecamatan("");
                    } else {
                        kec.setKecamatan("");
                    }
                    if (sub.getKelurahan() != null) {
                        if (!kec.getKecamatan().equals("")) {
                            kelurahan = areaService.findKelurahanByAreaNameAndParent(sub.getKelurahan(), kec.getKecamatan());
                        } else {
                            kelurahan = areaService.findKelurahanByAreaName(sub.getKelurahan());
                        }
                        if (kelurahan != null)
                            kec.setKelurahan(kelurahan.getAreaId());
                        else
                            kec.setKelurahan("");
                    } else {
                        kec.setKelurahan("");
                    }
                    kec.setPostalCode(sub.getPostalCode());
                    kec.setMotherName(sub.getMotherName());
                    kec.setJumlahTanggungan(sub.getJumlahTanggungan());
                    if (sub.getPlaceOfBirth() != null) {
                        kota = areaService.findByAreaName(sub.getPlaceOfBirth());
                        if (kota != null) {
                            kec.setPlaceOfBirthAreaId(kota.getAreaId());
                            kec.setPlaceOfBirth(kota.getAreaName());
                        } else {
                            kota = areaService.findKotaById(sub.getPlaceOfBirth());
                            if (kota != null) {
                                kec.setPlaceOfBirthAreaId(kota.getAreaId());
                                kec.setPlaceOfBirth(kota.getAreaName());
                            } else {
                                kec.setPlaceOfBirthAreaId("");
                                kec.setPlaceOfBirth(sub.getPlaceOfBirth());
                            }
                        }
                    } else {
                        kec.setPlaceOfBirthAreaId("");
                        kec.setPlaceOfBirth("");
                    }
                    if (sub.getDateOfBirth() != null)
                        kec.setDateOfBirth(formatDate.format(sub.getDateOfBirth()));
                    kec.setSpouseName(sub.getSpouseName());
                    if (sub.getSpousePlaceOfBirth() != null) {
                        kota = areaService.findByAreaName(sub.getSpousePlaceOfBirth());
                        if (kota != null) {
                            kec.setSpousePlaceOfBirthAreaId(kota.getAreaId());
                            kec.setSpousePlaceOfBirth(kota.getAreaName());
                        } else {
                            kota = areaService.findKotaById(sub.getSpousePlaceOfBirth());
                            if (kota != null) {
                                kec.setSpousePlaceOfBirthAreaId(kota.getAreaId());
                                kec.setSpousePlaceOfBirth(kota.getAreaName());
                            } else {
                                kec.setSpousePlaceOfBirthAreaId("");
                                kec.setSpousePlaceOfBirth(sub.getSpousePlaceOfBirth());
                            }
                        }
                    } else {
                        kec.setSpousePlaceOfBirthAreaId("");
                        kec.setSpousePlaceOfBirth("");
                    }
                    if (sub.getSpouseDateOfBirth() != null)
                        kec.setSpouseDateOfBirth(formatDate.format(sub.getSpouseDateOfBirth()));
                    kec.setPhoneNumber(sub.getPhoneNumber());
                    kec.setOtherAddress(sub.getOtherAddress());
                    if (sub.getEducation() != null) {
                        if (sub.getEducation().equals(CustomerConstant.Edu_Desc_SD)) {
                            kec.setPendidikan("1");
                        } else if (sub.getEducation().equals(CustomerConstant.Edu_Desc_SLTP)) {
                            kec.setPendidikan("2");
                        } else if (sub.getEducation().equals(CustomerConstant.Edu_Desc_SLTA)) {
                            kec.setPendidikan("3");
                        } else {
                            kec.setPendidikan("4");
                        }
                    }
                    if (sub.getHouseType() != null) {
                        if (sub.getHouseType().equals(CustomerConstant.Residence_Status_Desc_Milik_Sendiri)) {
                            kec.setStatusRumah("1");
                        } else if (sub.getHouseType().equals(CustomerConstant.Residence_Status_Desc_Orang_Tua)) {
                            kec.setStatusRumah("2");
                        } else {
                            kec.setStatusRumah("3");
                        }
                    } else {
                        kec.setStatusRumah("");
                    }
                    kec.setReligion(sub.getReligion());
                    if (sub.getMaritalStatus() != null) {
                        if (sub.getMaritalStatus().equals(CustomerConstant.Marital_Desc_Menikah)) {
                            kec.setMaritalStatus("1");
                        } else if (sub.getMaritalStatus().equals(CustomerConstant.Marital_Desc_Single)) {
                            kec.setMaritalStatus("2");
                        } else {
                            kec.setMaritalStatus("3");
                        }
                    }
                    kec.setGender(sub.getGender());
                    if (sub.getDateOfGovernmentId() != null) {
                        kec.setDateOfGovernmentId(formatDate.format(sub.getDateOfGovernmentId()));
                    }
                    kec.setAssignedUsername(sub.getAssignedUsername());
                    kec.setCustomerStatus(sub.getCustomerStatus());
                    kec.setProsperaId(sub.getProsperaId());
                    if (sub.getSwId() != null) {
                        SurveyWawancara sw = swService.getSWById("" + sub.getSwId());
                        if (sw != null) {
                            if (sw.getLocalId() != null)
                                kec.setSwId("" + sw.getLocalId());
                        }
                        if (sub.getPdkId() != null)
                            kec.setPdkId(sub.getPdkId().toString());
                    }
                    kec.setCreatedBy(sub.getCreatedBy());
                    if (sub.getGroupId() != null)
                        kec.setGroupId(sub.getGroupId().toString());
                    if (sub.getCycleNumber() != null) {
                        List<String> listProductId = syncCustomerServiceV2.findProductIdByCycleNumber(sub.getCycleNumber());
                        kec.setProductSuggestion(listProductId);
                    } else {
                        kec.setProductSuggestion(null);
                    }
                    response.getCustomerList().add(kec);
                    timer.stop();
                }
                log.debug("Parsing Finish");
                String GrandTotals = syncCustomerServiceV2.getGrandTotal(user.getOfficeCode(), startLookupDate, endLookupDate);
                response.setDeletedCustomerList(deletedCustomerList);
                response.setCurrentTotal(String.valueOf(customerList.size()));
                response.setGrandTotal(GrandTotals);
                response.setTotalPage(String.valueOf((Integer.parseInt(GrandTotals)/Integer.parseInt(getCountData))+1));
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
            }
        } catch (Exception e) {
            log.error("syncCustomer error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("syncCustomer error: " + e.getMessage());
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_CUSTOMER);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
            } catch (Exception e) {
                log.error("syncCustomer saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            log.info("syncCustomer RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            return response;
        }

    }

    
    @RequestMapping(value = WebGuiConstant.TERMINAL_SYNC_CUSTOMERV2_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SyncCustomerResponse syncCustomer(@RequestBody final SyncCustomerRequest request, @PathVariable("apkVersion") String apkVersion) throws Exception {

        final SyncCustomerResponse response = new SyncCustomerResponse();
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
        try {
            log.info("syncCustomer Try to processing SYNC CUSTOMER request: " + jsonUtils.toJson(request));
            String username = request.getUsername();
            String imei = request.getImei();
            String sessionKey = request.getSessionKey();
            String page = request.getPage();
            if (page == null || "".equals(page))
                page = "0";
            String startLookupDate = request.getStartLookupDate();
            String endLookupDate = request.getEndLookupDate();
            String getCountData = request.getGetCountData();
            AreaKelurahan kelurahan;
            AreaSubDistrict kecamatan;
            AreaDistrict kota;
            AreaProvince province;
            response.setResponseCode(WebGuiConstant.RC_SUCCESS);
            log.info("Validating Request");
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                log.info("Validation success, get customer data");
                User user = (User) userService.loadUserByUsername(request.getUsername());
                Page<Customer> customerList = syncCustomerService.getCustomerByLoc(page, getCountData,
                        startLookupDate, endLookupDate, user.getOfficeCode());
                log.info("FINISH GET CUSTOMER, Page : " + request.getPage() + " , Total Customer : " + customerList.getContent().size());
                final List<Customer> listDeletedCustomer = syncCustomerService.findIsDeletedCustomerList();
                List<String> deletedCustomerList = new ArrayList<>();
                if (listDeletedCustomer != null) {
                    if (!listDeletedCustomer.isEmpty()) {
                        for (Customer deletedCustomer : listDeletedCustomer) {
                            String id = deletedCustomer.getCustomerId().toString();
                            deletedCustomerList.add(id);
                        }
                    }
                }
                log.info("Start Parsing");
                for (Customer sub : customerList) {
                    Timer timer = new Timer();
                    timer.start("Parsing Customer");
                    CustomerPojo kec = new CustomerPojo();
                    if (sub.getCreatedDate() != null) {
                        LocalDateTime customerCreatedDate = sub.getCreatedDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
                        kec.setCreatedDate(formatter.format(customerCreatedDate));
                    }
                    kec.setCustomerId(sub.getCustomerId().toString());
                    kec.setCustomerCifNumber(sub.getCustomerCifNumber());
                    kec.setCustomerIdNumber(sub.getCustomerIdNumber());
                    kec.setCustomerName(sub.getCustomerName());
                    kec.setCustomerAlias(sub.getCustomerAlias());
                    kec.setCustomerIdName(sub.getCustomerIdName());
                    kec.setCustomerIdAddress(sub.getCustomerIdAddress());
                    kec.setAddress(sub.getAddress());
                    kec.setRtRw(sub.getRtrw());
                    if (sub.getProvince() != null) {
                        province = areaService.findProvinceByAreaName(sub.getProvince());
                        if (province != null)
                            kec.setProvince(province.getAreaId());
                        else
                            kec.setProvince("");
                    } else {
                        kec.setProvince("");
                    }
                    if (sub.getCity() != null) {
                        if (!kec.getProvince().equals("")) {
                            kota = areaService.findKotaByAreaNameAndParent(sub.getCity(), kec.getProvince());
                        } else {
                            kota = areaService.findKotaByAreaName(sub.getCity());
                        }
                        if (kota != null)
                            kec.setKabupaten(kota.getAreaId());
                        else
                            kec.setKabupaten("");
                    } else {
                        kec.setKabupaten("");
                    }
                    if (sub.getKecamatan() != null) {
                        if (!kec.getKabupaten().equals("")) {
                            kecamatan = areaService.findKecamatanByAreaNameAndParent(sub.getKecamatan(), kec.getKabupaten());
                        } else {
                            kecamatan = areaService.findKecamatanByAreaName(sub.getKecamatan());
                        }
                        if (kecamatan != null)
                            kec.setKecamatan(kecamatan.getAreaId());
                        else
                            kec.setKecamatan("");
                    } else {
                        kec.setKecamatan("");
                    }
                    if (sub.getKelurahan() != null) {
                        if (!kec.getKecamatan().equals("")) {
                            kelurahan = areaService.findKelurahanByAreaNameAndParent(sub.getKelurahan(), kec.getKecamatan());
                        } else {
                            kelurahan = areaService.findKelurahanByAreaName(sub.getKelurahan());
                        }
                        if (kelurahan != null)
                            kec.setKelurahan(kelurahan.getAreaId());
                        else
                            kec.setKelurahan("");
                    } else {
                        kec.setKelurahan("");
                    }
                    kec.setPostalCode(sub.getPostalCode());
                    kec.setMotherName(sub.getMotherName());
                    kec.setJumlahTanggungan(sub.getJumlahTanggungan());
                    if (sub.getPlaceOfBirth() != null) {
                        kota = areaService.findByAreaName(sub.getPlaceOfBirth());
                        if (kota != null) {
                            kec.setPlaceOfBirthAreaId(kota.getAreaId());
                            kec.setPlaceOfBirth(kota.getAreaName());
                        } else {
                            kota = areaService.findKotaById(sub.getPlaceOfBirth());
                            if (kota != null) {
                                kec.setPlaceOfBirthAreaId(kota.getAreaId());
                                kec.setPlaceOfBirth(kota.getAreaName());
                            } else {
                                kec.setPlaceOfBirthAreaId("");
                                kec.setPlaceOfBirth(sub.getPlaceOfBirth());
                            }
                        }
                    } else {
                        kec.setPlaceOfBirthAreaId("");
                        kec.setPlaceOfBirth("");
                    }
                    if (sub.getDateOfBirth() != null)
                        kec.setDateOfBirth(formatDate.format(sub.getDateOfBirth()));
                    kec.setSpouseName(sub.getSpouseName());
                    if (sub.getSpousePlaceOfBirth() != null) {
                        kota = areaService.findByAreaName(sub.getSpousePlaceOfBirth());
                        if (kota != null) {
                            kec.setSpousePlaceOfBirthAreaId(kota.getAreaId());
                            kec.setSpousePlaceOfBirth(kota.getAreaName());
                        } else {
                            kota = areaService.findKotaById(sub.getSpousePlaceOfBirth());
                            if (kota != null) {
                                kec.setSpousePlaceOfBirthAreaId(kota.getAreaId());
                                kec.setSpousePlaceOfBirth(kota.getAreaName());
                            } else {
                                kec.setSpousePlaceOfBirthAreaId("");
                                kec.setSpousePlaceOfBirth(sub.getSpousePlaceOfBirth());
                            }
                        }
                    } else {
                        kec.setSpousePlaceOfBirthAreaId("");
                        kec.setSpousePlaceOfBirth("");
                    }
                    if (sub.getSpouseDateOfBirth() != null)
                        kec.setSpouseDateOfBirth(formatDate.format(sub.getSpouseDateOfBirth()));
                    kec.setPhoneNumber(sub.getPhoneNumber());
                    kec.setOtherAddress(sub.getOtherAddress());
                    if (sub.getEducation() != null) {
                        if (sub.getEducation().equals(CustomerConstant.Edu_Desc_SD)) {
                            kec.setPendidikan("1");
                        } else if (sub.getEducation().equals(CustomerConstant.Edu_Desc_SLTP)) {
                            kec.setPendidikan("2");
                        } else if (sub.getEducation().equals(CustomerConstant.Edu_Desc_SLTA)) {
                            kec.setPendidikan("3");
                        } else {
                            kec.setPendidikan("4");
                        }
                    }
                    if (sub.getHouseType() != null) {
                        if (sub.getHouseType().equals(CustomerConstant.Residence_Status_Desc_Milik_Sendiri)) {
                            kec.setStatusRumah("1");
                        } else if (sub.getHouseType().equals(CustomerConstant.Residence_Status_Desc_Orang_Tua)) {
                            kec.setStatusRumah("2");
                        } else {
                            kec.setStatusRumah("3");
                        }
                    } else {
                        kec.setStatusRumah("");
                    }
                    kec.setReligion(sub.getReligion());
                    if (sub.getMaritalStatus() != null) {
                        if (sub.getMaritalStatus().equals(CustomerConstant.Marital_Desc_Menikah)) {
                            kec.setMaritalStatus("1");
                        } else if (sub.getMaritalStatus().equals(CustomerConstant.Marital_Desc_Single)) {
                            kec.setMaritalStatus("2");
                        } else {
                            kec.setMaritalStatus("3");
                        }
                    }
                    kec.setGender(sub.getGender());
                    if (sub.getDateOfGovernmentId() != null) {
                        kec.setDateOfGovernmentId(formatDate.format(sub.getDateOfGovernmentId()));
                    }
                    kec.setAssignedUsername(sub.getAssignedUsername());
                    kec.setCustomerStatus(sub.getCustomerStatus());
                    kec.setProsperaId(sub.getProsperaId());
                    if (sub.getSwId() != null) {
                        SurveyWawancara sw = swService.getSWById("" + sub.getSwId());
                        if (sw != null) {
                            if (sw.getLocalId() != null)
                                kec.setSwId("" + sw.getLocalId());
                        }
                        if (sub.getPdkId() != null)
                            kec.setPdkId(sub.getPdkId().toString());
                    }
                    kec.setCreatedBy(sub.getCreatedBy());
                    if (sub.getGroup() != null)
                        kec.setGroupId(sub.getGroup().getGroupId().toString());
                   /* CustomerWithAbsence custAbsence = syncCustomerService.findByCustomerId(sub.getCustomerId());
                    if (custAbsence != null)
                        if (custAbsence.getTotalAbsence() != null)
                            kec.setTotalMangkir(custAbsence.getTotalAbsence().toString());*/
                    response.getCustomerList().add(kec);
                    timer.stop();
                }
                log.info("Parsing Finish");
                response.setDeletedCustomerList(deletedCustomerList);
                response.setCurrentTotal(String.valueOf(customerList.getContent().size()));
                response.setGrandTotal(String.valueOf(customerList.getTotalElements()));
                response.setTotalPage(String.valueOf(customerList.getTotalPages()));
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
            }
        } catch (Exception e) {
            log.error("syncCustomer error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("syncCustomer error: " + e.getMessage());
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_CUSTOMER);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
            } catch (Exception e) {
                log.error("syncCustomer saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            log.info("syncCustomer RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            return response;
        }

    }
    
    
    
    
    
    
    
   /* @RequestMapping(value = WebGuiConstant.TERMINAL_SYNC_CUSTOMER_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SyncCustomerResponse syncCustomerDummy(@RequestBody final SyncCustomerRequest request,
                                      @PathVariable("apkVersion") String apkVersion) throws Exception {
        final SyncCustomerResponse responseCode = new SyncCustomerResponse();

        SimpleDateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
        try {
            log.info("Try to processing SYNC CUSTOMER request");
            String username = request.getUsername();
            String imei = request.getImei();
            String sessionKey = request.getSessionKey();
            String page = request.getPage();

            if (page == null || "".equals(page))
                page = "0";

            String startLookupDate = request.getStartLookupDate();
            String endLookupDate = request.getEndLookupDate();
            String getCountData = request.getGetCountData();

            AreaKelurahan kelurahan;
            AreaSubDistrict kecamatan;
            AreaDistrict kota;
            AreaProvince province;

            responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);

            log.info("Validating Request");

            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);

            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.info("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                log.info("Validation success, get customer data");

                User user = (User) userService.loadUserByUsername(request.getUsername());

                Page<Customer> customerList = null;
             //   log.info("FINISH GET CUSTOMER, Page : "+request.getPage()+" , Total Customer : "+customerList.getContent().size());
                final List<Customer> listDeletedCustomer = syncCustomerService.findIsDeletedCustomerList();
                List<String> deletedCustomerList = new ArrayList<>();
                if(listDeletedCustomer != null){
                    if(!listDeletedCustomer.isEmpty()){
                        for (Customer deletedCustomer : listDeletedCustomer) {
                            String id = deletedCustomer.getCustomerId().toString();
                            deletedCustomerList.add(id);
                        }
                    }
                }

                responseCode.setDeletedCustomerList(deletedCustomerList);
                responseCode.setCurrentTotal("0");
                responseCode.setGrandTotal("0");
                responseCode.setTotalPage("0");

                threadPoolTaskExecutor.execute(new Runnable() {

                    @Override
                    public void run() {

                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());

                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_CUSTOMER);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());

                        List<MessageLogs> messageLogs = new ArrayList<>();

                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
            }
            String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
            responseCode.setResponseMessage(label);
            return responseCode;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }*/

}