package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.GroupRequest;
import id.co.telkomsigma.btpns.mprospera.response.GroupResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("webServiceAddGroupV2Controller")
public class WebServiceAddGroupV2Controller extends GenericController {

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private SentraService sentraService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private RESTClient restClient;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private AuditLogService auditLogService;

    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.ADD_GROUP_V2_REQUEST, method = {RequestMethod.POST})
    public
    @ResponseBody
    GroupResponse doAddGroup(@RequestBody String requestString,
                             @PathVariable("apkVersion") String apkVersion) throws Exception {

        log.info("addSentraGroupV2 INCOMING SUBMIT GROUP REQUEST : " + requestString);
        Object obj = new JsonUtils().fromJson(requestString, GroupRequest.class);
        final GroupRequest request = (GroupRequest) obj;
        final GroupResponse response = new GroupResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        String groupId = "";
        String localId = "";
        log.info("TRY TO VALIDATING REQUEST");
        try {
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(), request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei()
                        + " ,sessionKey : " + request.getSessionKey());
            } else if (!"APPROVED".equalsIgnoreCase(request.getStatus())) {
                response.setResponseCode(WebGuiConstant.RC_UNKNOWN_STATUS);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
            } else {
                // cek sentra id-nya ada apa ngga
                Sentra sentra = new Sentra();
                sentra = sentraService.findBySentraId(request.getSentraId());
                if (null != sentra) {
                    Group group = new Group();
                    if ("update".equals(request.getAction())) {
                        if (request.getGroupId() != null) {
                            if (!"".equals(request.getGroupId())) {
                                group = sentraService.getGroupByGroupId(request.getGroupId());
                                if (group == null) {
                                    log.error("UNKNOWN GROUP ID");
                                    response.setResponseCode(WebGuiConstant.RC_SENTRAGROUP_NULL);
                                    String label = getMessage("webservice.rc.label." + response.getResponseCode());
                                    response.setResponseMessage(label);
                                    log.info("RESPONSE MESSAGE : " + response);
                                    return response;
                                } else {
                                    log.info("EXISTING GROUP");
                                    group.setApprovedDate(new Date());
                                }
                            } else {
                                log.error("GROUP ID BLANK");
                                response.setResponseCode(WebGuiConstant.RC_SENTRAGROUP_NULL);
                                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                                response.setResponseMessage(label);
                                log.info("RESPONSE MESSAGE : " + response);
                                return response;
                            }
                        } else {
                            log.error("GROUP ID NULL");
                            response.setResponseCode(WebGuiConstant.RC_SENTRAGROUP_NULL);
                            String label = getMessage("webservice.rc.label." + response.getResponseCode());
                            response.setResponseMessage(label);
                            log.info("RESPONSE MESSAGE : " + response);
                            return response;
                        }
                    } else {
                        log.info("NEW GROUP");
                        group.setCreatedDate(new Date());
                        group.setCreatedBy(request.getUsername());
                    }
                    group.setGroupLeader(request.getGroupLeader());
                    group.setGroupName(request.getGroupName());
                    group.setLatitude(request.getLatitude());
                    group.setLongitude(request.getLongitude());
                    group.setSentra(sentra);
                    group.setCreatedBy(request.getUsername());
                    group.setCreatedDate(new Date());
                    group.setStatus(request.getStatus());
                    group.setRrn(request.getRetrievalReferenceNumber());
                    localId = request.getLocalId();
                    // insert ke prospera
                    if (!terminalService.isDuplicate(request.getRetrievalReferenceNumber(), "addGroup")) {
                        log.info("SAVING GROUP DATA");
                        if (response.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)
                                && WebGuiConstant.STATUS_APPROVED.equals(request.getStatus())) {
                            group.setApprovedBy(request.getUsername());
                            request.setCustomerNumberCenter(sentra.getProsperaId());
                            request.setStatus(null);
                            request.setGroupId(null);
                            request.setSentraId(null);
                            request.setLatitude(null);
                            request.setLongitude(null);
                            request.setUsername(group.getCreatedBy());
                            if (request.getAction().equals("update")) {
                                if (request.getGroupLeader() == null) {
                                    log.error("GROUP LEADER NULL");
                                    response.setResponseCode(WebGuiConstant.RC_GROUP_LEADER_NULL);
                                    String label = getMessage("webservice.rc.label." + response.getResponseCode());
                                    response.setResponseMessage(label);
                                    log.info("RESPONSE MESSAGE : " + response);
                                    return response;
                                }
                                request.setCustomerNumberLeader(request.getGroupLeader());
                                groupId = group.getGroupId().toString();
                            }
                            request.setTrainedDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
                            String action = null;
                            if ("insert".equals(request.getAction())) {
                                action = "add";
                                request.setReffId("");
                            } else {
                                action = "update";
                                request.setReffId(String.valueOf(group.getGroupId()));
                            }
                            GroupResponse restResponse = restClient.processGroup(request, action.toLowerCase());
                            if (restResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)) {
                                if (restResponse.getGroupId() == null) {
                                    log.error("GROUP ID NULL");
                                    response.setResponseCode(WebGuiConstant.RC_SENTRAGROUP_NULL);
                                    String label = getMessage("webservice.rc.label." + response.getResponseCode());
                                    response.setResponseMessage(label);
                                    log.info("RESPONSE MESSAGE : " + response);
                                    return response;
                                }
                                group.setProsperaId(restResponse.getGroupId());
                                if (request.getGroupLeader() != null) {
                                    group.setGroupLeader(request.getGroupLeader());
                                }
                                group.setStatus(WebGuiConstant.STATUS_APPROVED);
                                log.info("SAVING GROUP");
                                sentraService.saveGroup(group);
                                if (group.getGroupId() != null) {
                                    groupId = group.getGroupId().toString();
                                }
                            } else if (restResponse.getResponseCode().equals("26")) {
                                log.error("GROUP ALREADY APPROVED");
                                response.setResponseCode(WebGuiConstant.RC_SUCCESS);
                                response.setResponseMessage(WebGuiConstant.RC_GROUP_ALREADY_EXIST);
                                log.info("RESPONSE MESSAGE : " + response);
                                return response;
                            }
                            response.setResponseCode(restResponse.getResponseCode());
                            response.setResponseMessage(restResponse.getResponseMessage());
                        }
                    } else {
                        Group groupExist = sentraService.getGroupByRrn(request.getRetrievalReferenceNumber());
                        if (groupExist != null) {
                            groupId = groupExist.getGroupId().toString();
                        }
                    }
                } else {
                    log.error("UNKNOWN SENTRA ID");
                    response.setResponseCode(WebGuiConstant.RC_UNKNOWN_SENTRA_ID);
                    String label = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(label);
                }
            }
        } catch (Exception e) {
            log.error("addSentraGroupV2  error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("addSentraGroupV2 error: " + e.getMessage());
        } finally {
            response.setResponseMessage("SUKSES");
            response.setGroupId(groupId);
            response.setLocalId(localId);
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_MODY_SENTRA_GROUP);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.insertRrn("addGroup", request.getRetrievalReferenceNumber().trim());
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        AuditLog auditLog = new AuditLog();
                        auditLog.setActivityType(AuditLog.MODIF_SENTRA_GROUP);
                        auditLog.setCreatedBy(request.getUsername());
                        auditLog.setCreatedDate(new Date());
                        auditLog.setDescription(request.toString() + " | " + response.toString());
                        auditLog.setReffNo(request.getRetrievalReferenceNumber());
                        auditLogService.insertAuditLog(auditLog);
                    }
                });
            } catch (Exception e) {
                log.error("addSentraGroupV2 saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            log.info("addSentraGroupV2 RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            return response;
        }
    }

}