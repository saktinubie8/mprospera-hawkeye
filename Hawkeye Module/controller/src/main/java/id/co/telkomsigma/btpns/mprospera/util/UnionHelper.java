package id.co.telkomsigma.btpns.mprospera.util;

import id.co.telkomsigma.btpns.mprospera.response.CustomerPojo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Dzulfiqar on 14/07/2017.
 */
public class UnionHelper {

    public static List<CustomerPojo> union(List<CustomerPojo> list1, List<CustomerPojo> list2) {
        HashSet<CustomerPojo> set = new HashSet<CustomerPojo>();
        set.addAll(list1);
        set.addAll(list2);
        return new ArrayList<CustomerPojo>(set);
    }

}