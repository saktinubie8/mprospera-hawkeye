package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import com.fasterxml.jackson.core.JsonParseException;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.saf.Saf;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import id.co.telkomsigma.btpns.mprospera.request.*;
import id.co.telkomsigma.btpns.mprospera.response.*;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.net.ssl.*;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Component("restClient")
public class RESTClient {

    protected final Log LOGGER = LogFactory.getLog(getClass());

    protected static final String PROCESS_ADD = "add";
    protected static final String PROCESS_UPDATE = "update";

    private String HOST;
    private String PORT;
    private String MAIN_URI;

    @Autowired
    private SentraService sentraService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private LoanService loanService;

    @Autowired
    private SafService safService;

    @Autowired
    ParameterService parameterService;

    @PostConstruct
    protected void init() {
        HOST = parameterService.loadParamByParamName("esb.host", "https://10.7.17.163");
        PORT = parameterService.loadParamByParamName("esb.port", "8443");
        MAIN_URI = parameterService.loadParamByParamName("esb.uri", "/btpns/mprospera");
    }

    public Boolean echoSuccess() {
        String url = HOST + ":" + PORT + MAIN_URI + "/echo";
        RestTemplate restTemplate = getRestTemplate();
        try {
            LOGGER.debug("SEND ECHO...");
            ResponseEntity<String> response = restTemplate.postForEntity(url, null, String.class);
            LOGGER.debug("ECHO SUCCESS...");
            return response.getStatusCode().is2xxSuccessful();
        } catch (HttpClientErrorException e) {
            HttpStatus httpStatus = e.getStatusCode();
            LOGGER.error("ECHO FAILED..., " + httpStatus.toString() + " : " + httpStatus.getReasonPhrase());
            return e.getStatusCode().is2xxSuccessful();
        } catch (Exception e) {
            LOGGER.error("ECHO FAILED..., " + e.getMessage());
            return false;
        }
    }

    public SentraResponse addSentra(SentraRequest request) throws KeyManagementException, NoSuchAlgorithmException {
        return processSentra(request, PROCESS_ADD);
    }

    public SentraResponse updateSentra(SentraRequest request) throws KeyManagementException, NoSuchAlgorithmException {
        return processSentra(request, PROCESS_UPDATE);
    }

    public SentraResponse processSentra(SentraRequest request, String action)
            throws NoSuchAlgorithmException, KeyManagementException {

        SentraResponse response = new SentraResponse();
        if (echoSuccess()) {
            String url = HOST + ":" + PORT + MAIN_URI + "/sentra/" + action;
            LOGGER.debug("POST URL : " + url);
            try {
                response = getRestTemplate().postForObject(url, request, SentraResponse.class);
                LOGGER.debug("Got response..., " + response.toString());
            } catch (HttpClientErrorException e) {
                LOGGER.error("POST REQUEST ERROR..., " + e.getMessage());
                try {
                    HttpStatus httpStatus = e.getStatusCode();
                    addSaf(new JsonUtils().toJson(request), "ADDSENTRA", url, "XX", "ESB " + httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
                    response.setResponseCode(WebGuiConstant.RC_SUCCESS);
                    response.setResponseMessage("SUCCESS");
                } catch (Exception e1) {
                    LOGGER.debug("FAILED SAVING SAF..., " + e1.getMessage());
                    response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                    response.setResponseMessage("processSentra fail SAVING SAF: " + e1.getMessage());
                }
            } catch (Exception e) {
                LOGGER.error("POST REQUEST ERROR..., " + e.getMessage());
                try {
                    addSaf(new JsonUtils().toJson(request), "ADDSENTRA", url, "XX", "ESB " + e.getMessage());
                    response.setResponseCode(WebGuiConstant.RC_SUCCESS);
                    response.setResponseMessage("SUCCESS");
                } catch (Exception e1) {
                    LOGGER.debug("FAILED SAVING SAF..., " + e1.getMessage());
                    response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                    response.setResponseMessage("processSentra fail SAVING SAF: " + e1.getMessage());
                }
            }
        } else {
            LOGGER.error("POST REQUEST ERROR..., ECHO FAILED...");
            response.setResponseCode("SU");
            response.setResponseMessage("ESB WEBSERVICE UNAVAILABLE");
        }
        return response;
    }

    public GroupResponse addGroup(GroupRequest request) throws NoSuchAlgorithmException, KeyManagementException {
        return processGroup(request, PROCESS_ADD);
    }

    public GroupResponse updateGroup(GroupRequest request) throws NoSuchAlgorithmException, KeyManagementException {
        return processGroup(request, PROCESS_UPDATE);
    }

    public GroupResponse processGroup(GroupRequest request, String action)
            throws NoSuchAlgorithmException, KeyManagementException {

        GroupResponse response = new GroupResponse();
        if (echoSuccess()) {
            String url = HOST + ":" + PORT + MAIN_URI + "/group/" + action;
            LOGGER.info("POST URL : " + url);
            try {
                response = getRestTemplate().postForObject(url, request, GroupResponse.class);
                LOGGER.info("Got response..., " + response.toString());
                return response;
            } catch (HttpClientErrorException e) {
                LOGGER.error("POST REQUEST ERROR..., " + e.getMessage());
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("processGroup ESB Error: " + e.getMessage());
                return response;
            } catch (Exception e) {
                LOGGER.error("POST REQUEST ERROR..., " + e.getMessage());
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("processGroup ESB Error: " + e.getMessage());
                return response;
            }
        } else {
            LOGGER.error("POST REQUEST ERROR..., ECHO FAILED...");
            response.setResponseCode("SU");
            response.setResponseMessage("ESB WEBSERVICE UNAVAILABLE");
            return response;
        }
    }

    public AddCustomerResponse addCustomer(AddCustomerRequest request)
            throws NoSuchAlgorithmException, KeyManagementException {

        AddCustomerResponse response = new AddCustomerResponse();
        if (echoSuccess()) {
            String url = HOST + ":" + PORT + MAIN_URI + "/addCustomer";
            LOGGER.debug("POST URL : " + url);
            try {
                response = getRestTemplate().postForObject(url, request, AddCustomerResponse.class);
                LOGGER.debug("Got response..., " + response.toString());
                return response;
            } catch (HttpClientErrorException e) {
                LOGGER.error("POST REQUEST ERROR..., " + e.getMessage());
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("addCustomer ESB Error: " + e.getMessage());
                return response;
            } catch (Exception e) {
                LOGGER.error("POST REQUEST ERROR..., " + e.getMessage());
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("addCustomer ESB Error: " + e.getMessage());
                return response;
            }
        } else {
            LOGGER.error("POST REQUEST ERROR..., ECHO FAILED...");
            response.setResponseCode("SU");
            response.setResponseMessage("ESB WEBSERVICE UNAVAILABLE");
            return response;
        }
    }

    public AddLoanResponse addLoan(ESBLoanRequest request) throws NoSuchAlgorithmException, KeyManagementException {

        AddLoanResponse response = new AddLoanResponse();
        if (echoSuccess()) {
            String url = HOST + ":" + PORT + MAIN_URI + "/addLoan";
            LOGGER.debug("POST URL : " + url);
            try {
                response = getRestTemplate().postForObject(url, request, AddLoanResponse.class);
                LOGGER.debug("Got response..., " + response.toString());
                return response;
            } catch (HttpClientErrorException e) {
                LOGGER.error("POST REQUEST ERROR..., " + e.getMessage());
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("addLoan ESB Error: " + e.getMessage());
                return response;
            } catch (Exception e) {
                LOGGER.error("POST REQUEST ERROR..., " + e.getMessage());
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("addLoan ESB Error: " + e.getMessage());
                return response;
            }
        } else {
            LOGGER.error("POST REQUEST ERROR..., ECHO FAILED...");
            response.setResponseCode("SU");
            response.setResponseMessage("ESB WEBSERVICE UNAVAILABLE");
            return response;
        }
    }

    public DeviationResponse addDeviation(DeviationRequest request)
            throws NoSuchAlgorithmException, KeyManagementException {

        DeviationResponse response = new DeviationResponse();
        if (echoSuccess()) {
            String url = HOST + ":" + PORT + MAIN_URI + "/claim/deviation";
            LOGGER.debug("POST URL : " + url);
            try {
                DeviationRequest restRequest = new DeviationRequest();
                restRequest.setTransmissionDateAndTime(request.getTransmissionDateAndTime());
                restRequest.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
                restRequest.setImei(request.getImei());
                restRequest.setUsername(request.getUsername());
                restRequest.setNoappid(request.getNoappid());
                restRequest.setUserApproval(request.getUserApproval());
                restRequest.setRole(request.getRole());
                response = getRestTemplate().postForObject(url, restRequest, DeviationResponse.class);
                LOGGER.debug("Got response..., " + response.toString());
                return response;
            } catch (HttpClientErrorException e) {
                LOGGER.error("POST REQUEST ERROR..., " + e.getMessage());
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("addDeviation ESB Error: " + e.getMessage());
                return response;
            } catch (Exception e) {
                LOGGER.error("POST REQUEST ERROR..., " + e.getMessage());
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("addDeviation ESB Error: " + e.getMessage());
                return response;
            }
        } else {
            LOGGER.error("POST REQUEST ERROR..., ECHO FAILED...");
            response.setResponseCode("SU");
            response.setResponseMessage("ESB WEBSERVICE UNAVAILABLE");
            return response;
        }
    }

    public ESBSurveyResponse inputSurvey(ESBSurveyRequest request)
            throws NoSuchAlgorithmException, KeyManagementException {

        ESBSurveyResponse response = new ESBSurveyResponse();
        if (echoSuccess()) {
            String url = HOST + ":" + PORT + MAIN_URI + "/inputSurvey";
            LOGGER.debug("POST URL : " + url);
            try {
                response = getRestTemplate().postForObject(url, request, ESBSurveyResponse.class);
                LOGGER.debug("Got response..., " + response.toString());
                return response;
            } catch (HttpClientErrorException e) {
                LOGGER.error("POST REQUEST ERROR..., " + e.getMessage());
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("inputSurvey ESB Error: " + e.getMessage());
                return response;
            } catch (Exception e) {
                LOGGER.error("POST REQUEST ERROR..., " + e.getMessage());
                response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                response.setResponseMessage("inputSurvey ESB Error: " + e.getMessage());
                return response;
            }
        } else {
            LOGGER.error("POST REQUEST ERROR..., ECHO FAILED...");
            response.setResponseCode("SU");
            response.setResponseMessage("ESB WEBSERVICE UNAVAILABLE");
            return response;
        }
    }

    protected void addSaf(String json, String method, String url, String rc, String rm) {
        LOGGER.debug("ADDING TO SAF..., " + json);
        Saf saf = new Saf();
        saf.setMethod(method);
        saf.setUrl(url);
        saf.setResponseCode(rc);
        saf.setResponseMessage(rm);
        saf.setCreatedDate(new Date());
        saf.setRawMessage(json.getBytes());
        try {
            // safService.save(saf);
            LOGGER.debug("SAF IS SAVED SUCCESFULLY...");
        } catch (Exception e) {
            LOGGER.debug("FAIL SAVING SAF...");
        }
    }

    protected RestTemplate getRestTemplate() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
                    throws CertificateException {
            }

            @Override
            public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
                    throws CertificateException {
            }
        }};

        // Install the all-trusting trust manager
        SSLContext sc = null;
        try {
            sc = SSLContext.getInstance("SSL");
        } catch (NoSuchAlgorithmException e1) {
            LOGGER.debug("SSL INSTANCE FETCHING FAILED..., " + e1.getMessage());
        }
        try {
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (KeyManagementException e) {
            LOGGER.debug("SSL CONTEXT INITIALIZING FAILED..., " + e.getMessage());
        }
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        // Create all-trusting host name verifier
        final HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        RestTemplate restTemplate = new RestTemplate(new SimpleClientHttpRequestFactory() {
            @Override
            protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
                if (connection instanceof HttpsURLConnection) {
                    ((HttpsURLConnection) connection).setHostnameVerifier(allHostsValid);
                    ;
                }
                super.prepareConnection(connection, httpMethod);
            }
        });

        return restTemplate;
    }

    // @Scheduled(cron = "${saf.scheduler}")
    public void safScheduler() throws JsonParseException, IOException {
        try {
            Thread.sleep(new Random().nextInt(Integer.MAX_VALUE) * 10);
        } catch (InterruptedException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
        if (echoSuccess()) {
            List<Saf> safs = safService.findAll();
            for (final Saf saf : safs) {
                LOGGER.debug(
                        "SAF RUNNING ==============================================================================");
                if (saf.getMethod().toUpperCase().equals("ADDSW")) {
                    String url = saf.getUrl();
                    SWRequest request = new SWRequest();
                    synchronized (request) {
                        LOGGER.debug("SAF == POST URL : " + url);
                        try {
                            request = (SWRequest) new JsonUtils().fromJson(new String(saf.getRawMessage()), SWRequest.class);
                        } catch (JsonParseException e1) {
                            LOGGER.debug("SAF == JSON PARSE FAILED, " + e1.getMessage());
                        } catch (IOException e1) {
                            LOGGER.debug("SAF == IO EXCEPTION, " + e1.getMessage());
                        }
                        try {
                            SWResponse response = getRestTemplate().postForObject(url, request, SWResponse.class);
                            LOGGER.debug("SAF == Got response..., " + response.toString());
                            if (response.getResponseCode().equals("00")) {
                                try {
                                    safService.delete(saf.getId());
                                    LOGGER.debug("SAF == MESSAGING SUCCESS..., SAF REMOVED...");
                                } catch (Exception e) {
                                    LOGGER.debug("SAF == MESSAGING SUCCESS..., FAILED TO REMOVE SAF");
                                }
                            } else
                                LOGGER.debug("SAF == MESSAGING FAILED..., FAILED TO REMOVE SAF");
                        } catch (HttpClientErrorException e) {
                            HttpStatus httpStatus = e.getStatusCode();
                            LOGGER.debug("SAF == MESSAGING FAILED..., RC: XX, RM: " + "ESB " + httpStatus.toString()
                                    + " - " + httpStatus.getReasonPhrase());
                        }
                    }
                } else if (saf.getMethod().toUpperCase().equals("ADDSENTRA")) {
                    String url = saf.getUrl();
                    SentraRequest request = new SentraRequest();
                    synchronized (request) {
                        LOGGER.debug("SAF == POST URL : " + url);
                        try {
                            request = (SentraRequest) new JsonUtils().fromJson(new String(saf.getRawMessage()), SentraRequest.class);
                        } catch (JsonParseException e1) {
                            LOGGER.debug("SAF == JSON PARSE FAILED, " + e1.getMessage());
                        } catch (IOException e1) {
                            LOGGER.debug("SAF == IO EXCEPTION, " + e1.getMessage());
                        }
                        try {
                            SentraResponse response = getRestTemplate().postForObject(url, request, SentraResponse.class);
                            LOGGER.debug("SAF == Got response..., " + response.toString());
                            if (response.getResponseCode().equals("00")) {
                                try {
                                    Sentra sentra = sentraService.findBySentraId(request.getSentraId());
                                    sentra.setProsperaId(response.getSentraId());
                                    sentraService.save(sentra);
                                    safService.delete(saf.getId());
                                    LOGGER.debug("SAF == MESSAGING SUCCESS..., SAF REMOVED...");
                                } catch (Exception e) {
                                    LOGGER.debug("SAF == MESSAGING SUCCESS..., FAILED TO REMOVE SAF");
                                }
                            } else
                                LOGGER.debug("SAF == MESSAGING FAILED..., FAILED TO REMOVE SAF");
                        } catch (HttpClientErrorException e) {
                            HttpStatus httpStatus = e.getStatusCode();
                            LOGGER.debug("SAF == MESSAGING FAILED..., RC: XX, RM: " + "ESB " + httpStatus.toString()
                                    + " - " + httpStatus.getReasonPhrase());
                        }
                    }
                } else if (saf.getMethod().toUpperCase().equals("ADDGROUP")) {
                    String url = saf.getUrl();
                    GroupRequest request = new GroupRequest();
                    synchronized (request) {
                        LOGGER.debug("SAF == POST URL : " + url);
                        try {
                            request = (GroupRequest) new JsonUtils().fromJson(new String(saf.getRawMessage()), GroupRequest.class);
                        } catch (JsonParseException e1) {
                            LOGGER.debug("SAF == JSON PARSE FAILED, " + e1.getMessage());
                        } catch (IOException e1) {
                            LOGGER.debug("SAF == IO EXCEPTION, " + e1.getMessage());
                        }
                        try {
                            GroupResponse response = getRestTemplate().postForObject(url, request, GroupResponse.class);
                            LOGGER.debug("SAF == Got response..., " + response.toString());
                            if (response.getResponseCode().equals("00")) {
                                try {
                                    Group group = sentraService.getGroupByGroupId(request.getGroupId());
                                    group.setProsperaId(response.getGroupId());
                                    sentraService.saveGroup(group);
                                    safService.delete(saf.getId());
                                    LOGGER.debug("SAF == MESSAGING SUCCESS..., SAF REMOVED...");
                                } catch (Exception e) {
                                    LOGGER.debug("SAF == MESSAGING SUCCESS..., FAILED TO REMOVE SAF");
                                }
                            } else
                                LOGGER.debug("SAF == MESSAGING FAILED..., FAILED TO REMOVE SAF");
                        } catch (HttpClientErrorException e) {
                            HttpStatus httpStatus = e.getStatusCode();
                            LOGGER.debug("SAF == MESSAGING FAILED..., RC: XX, RM: " + "ESB " + httpStatus.toString()
                                    + " - " + httpStatus.getReasonPhrase());
                        }
                    }
                } else if (saf.getMethod().toUpperCase().equals("ADDCUSTOMER")) {
                    String url = saf.getUrl();
                    AddCustomerRequest request = new AddCustomerRequest();
                    synchronized (request) {
                        LOGGER.debug("SAF == POST URL : " + url);
                        try {
                            request = (AddCustomerRequest) new JsonUtils().fromJson(new String(saf.getRawMessage()), AddCustomerRequest.class);
                        } catch (JsonParseException e1) {
                            LOGGER.debug("SAF == JSON PARSE FAILED, " + e1.getMessage());
                        } catch (IOException e1) {
                            LOGGER.debug("SAF == IO EXCEPTION, " + e1.getMessage());
                        }
                        try {
                            AddCustomerResponse response = getRestTemplate().postForObject(url, request, AddCustomerResponse.class);
                            LOGGER.debug("SAF == Got response..., " + response.toString());
                            if (response.getResponseCode().equals("00")) {
                                try {

                                    Timer timer = new Timer();
                                    timer.start("[safScheduler] [customerService.findById]");
                                    Customer customer = customerService.findById(request.getCustomerId());
                                    timer.stop();

                                    customer.setProsperaId(response.getProspectId());
                                    customerService.save(customer);
                                    safService.delete(saf.getId());
                                    LOGGER.debug("SAF == MESSAGING SUCCESS..., SAF REMOVED...");
                                } catch (Exception e) {
                                    LOGGER.debug("SAF == MESSAGING SUCCESS..., FAILED TO REMOVE SAF");
                                }
                            } else
                                LOGGER.debug("SAF == MESSAGING FAILED..., FAILED TO REMOVE SAF");
                        } catch (HttpClientErrorException e) {
                            HttpStatus httpStatus = e.getStatusCode();
                            LOGGER.debug("SAF == MESSAGING FAILED..., RC: XX, RM: " + "ESB " + httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
                        }
                    }
                } else if (saf.getMethod().toUpperCase().equals("ADDLOAN")) {
                    String url = saf.getUrl();
                    LoanRequest request = new LoanRequest();
                    synchronized (request) {
                        LOGGER.debug("SAF == POST URL : " + url);
                        try {
                            request = (LoanRequest) new JsonUtils().fromJson(new String(saf.getRawMessage()), LoanRequest.class);
                        } catch (JsonParseException e1) {
                            LOGGER.debug("SAF == JSON PARSE FAILED, " + e1.getMessage());
                        } catch (IOException e1) {
                            LOGGER.debug("SAF == IO EXCEPTION, " + e1.getMessage());
                        }
                        try {
                            LoanResponse response = getRestTemplate().postForObject(url, request, LoanResponse.class);
                            LOGGER.debug("SAF == Got response..., " + response.toString());
                            if (response.getResponseCode().equals("00")) {
                                try {
                                    Loan loan = loanService.findById(request.getLoanId());
                                    loan.setProsperaId(response.getProspectId());
                                    loanService.save(loan);
                                    safService.delete(saf.getId());
                                    LOGGER.debug("SAF == MESSAGING SUCCESS..., SAF REMOVED...");
                                } catch (Exception e) {
                                    LOGGER.debug("SAF == MESSAGING SUCCESS..., FAILED TO REMOVE SAF");
                                }
                            } else
                                LOGGER.debug("SAF == MESSAGING FAILED..., FAILED TO REMOVE SAF");
                        } catch (HttpClientErrorException e) {
                            HttpStatus httpStatus = e.getStatusCode();
                            LOGGER.debug("SAF == MESSAGING FAILED..., RC: XX, RM: " + "ESB " + httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
                        }
                    }
                } else if (saf.getMethod().toUpperCase().equals("INPUTSURVEY")) {
                    String url = saf.getUrl();
                    ESBSurveyRequest request = new ESBSurveyRequest();
                    synchronized (request) {
                        LOGGER.debug("SAF == POST URL : " + url);
                        try {
                            request = (ESBSurveyRequest) new JsonUtils().fromJson(new String(saf.getRawMessage()), ESBSurveyRequest.class);
                        } catch (JsonParseException e1) {
                            LOGGER.debug("SAF == JSON PARSE FAILED, " + e1.getMessage());
                        } catch (IOException e1) {
                            LOGGER.debug("SAF == IO EXCEPTION, " + e1.getMessage());
                        }
                        try {
                            ESBSurveyResponse response = getRestTemplate().postForObject(url, request, ESBSurveyResponse.class);
                            LOGGER.debug("SAF == Got response..., " + response.toString());
                            if (response.getResponseCode().equals("00")) {
                                try {
                                    safService.delete(saf.getId());
                                    LOGGER.debug("SAF == MESSAGING SUCCESS..., SAF REMOVED...");
                                } catch (Exception e) {
                                    LOGGER.debug("SAF == MESSAGING SUCCESS..., FAILED TO REMOVE SAF");
                                }
                            } else
                                LOGGER.debug("SAF == MESSAGING FAILED..., FAILED TO REMOVE SAF");
                        } catch (HttpClientErrorException e) {
                            HttpStatus httpStatus = e.getStatusCode();
                            LOGGER.debug("SAF == MESSAGING FAILED..., RC: XX, RM: " + "ESB " + httpStatus.toString()
                                    + " - " + httpStatus.getReasonPhrase());
                        }
                    }
                }
                LOGGER.debug("SAF END ==================================================================================\n");
            }
        }
    }

}