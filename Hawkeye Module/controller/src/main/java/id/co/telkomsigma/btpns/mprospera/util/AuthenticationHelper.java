package id.co.telkomsigma.btpns.mprospera.util;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.user.User;

@Component
public class AuthenticationHelper {

    @Autowired
    private UserManager userManager;

    public Boolean validateSessionKey(String username) {
        User user = userManager.getUserByUsername(username);
        if (user.getSessionKey() == null || user.getSessionKey().equals(""))
            return false;

        Long loginTime = user.getSessionCreatedDate().getTime();
        Long currentTime = new Date().getTime();
        if (currentTime - loginTime > Long.parseLong(user.getSessionTime()))
            return false;
        return true;
    }

}