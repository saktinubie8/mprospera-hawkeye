package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.CustomerConstant;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerWithAbsence;
import id.co.telkomsigma.btpns.mprospera.model.loan.DeviationOfficerMapping;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanDeviation;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanWithEmergencyFund;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaDistrict;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaKelurahan;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaProvince;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaSubDistrict;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import id.co.telkomsigma.btpns.mprospera.model.sw.*;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.request.*;
import id.co.telkomsigma.btpns.mprospera.response.*;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.*;

@Controller("webServiceSyncDeviationV2Controller")
public class WebServiceSyncDeviationV2Controller extends GenericController {

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private UserService userService;

    @Autowired
    private AreaService areaService;

    @Autowired
    private LoanService loanService;

    @Autowired
    private AP3RService ap3RService;

    @Autowired
    private SWService swService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private SyncCustomerService syncCustomerService;

    @Autowired
    private SentraService sentraService;

    SimpleDateFormat formatDateTime = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
    SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy");
    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
    SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");

    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.TERMINAL_SYNC_DEVIATION_V2_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    ListDeviationResponse doListNonMs(@RequestBody final DeviationRequest request,
                                      @PathVariable("apkVersion") String apkVersion) throws Exception {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final ListDeviationResponse response = new ListDeviationResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("syncDeviationV2 INCOMING MESSAGE : " + jsonUtils.toJson(request));
            log.info("VALIDATING REQUEST...");
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(),
                    request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage("syncDeviationV2 Validation Failed: " + label);
                log.info("syncDeviationV2 Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                log.info("Validation success, get Deviation data");
                response.setResponseMessage("SUKSES");
                List<LoanDeviation> deviationPage = new ArrayList<>();
                User userMS = userService.findUserByUsername(request.getUsername());
                Location wismaMs = areaService.findLocationById(userMS.getOfficeCode());
                if (userMS.getRoleUser().equals("2")) {
                    deviationPage = loanService.findDeviationByMs(wismaMs);
                } else {
                    deviationPage = loanService.findByUserNonMs(userMS.getUserId());
                }
                log.info("Finishing get Deviation data");
                AP3R ap3rLocal = new AP3R();
                final List<SyncDeviationResponse> deviationResponses = new ArrayList<SyncDeviationResponse>();
                for (final LoanDeviation loanDeviation : deviationPage) {
                    SyncDeviationResponse deviationPojo = new SyncDeviationResponse();
                    deviationPojo.setDeviationId(loanDeviation.getDeviationId().toString());
                    deviationPojo.setStatus(loanDeviation.getStatus());
                    if (loanDeviation.getLoanId() != null)
                        deviationPojo.setLoanId(loanDeviation.getLoanId().toString());
                    if (loanDeviation.getAp3rId() != null)
                        ap3rLocal = ap3RService.findById(loanDeviation.getAp3rId());
                    if (ap3rLocal != null) {
                        deviationPojo.setAp3rId(ap3rLocal.getLocalId());
                        SWProductMapping swProductMapping = swService.findProductMapById(ap3rLocal.getSwProductId());
                        if (swProductMapping != null) {
                            deviationPojo.setPlafon(swProductMapping.getRecommendedPlafon().toString());
                        }
                    }
                    deviationPojo.setDeviationCode(loanDeviation.getDeviationCode());
                    deviationPojo.setDeviationReason(loanDeviation.getDeviationReason());
                    if (loanDeviation.getCreatedBy() != null) {
                        deviationPojo.setCreatedBy(loanDeviation.getCreatedBy());
                        User userDeviation = userService.findUserByUsername(loanDeviation.getCreatedBy());
                        if (userDeviation != null) {
                            if (!userDeviation.getOfficeCode().equals("")) {
                                Location wisma = areaService.findLocationById(userDeviation.getOfficeCode());
                                WismaSWReq wismaMap = new WismaSWReq();
                                wismaMap.setOfficeId(userDeviation.getOfficeCode());
                                if (wisma != null) {
                                    wismaMap.setOfficeCode(wisma.getLocationCode());
                                    wismaMap.setOfficeName(wisma.getName());
                                }
                                deviationPojo.setWisma(wismaMap);
                            }
                        }
                    }
                    if (loanDeviation.getCreatedDate() != null)
                        deviationPojo.setCreatedDate(formatDateTime.format(loanDeviation.getCreatedDate()));
                    deviationPojo.setBatch(loanDeviation.getBatch());
                    List<DeviationApprovalHistory> deviationHistory = new ArrayList<>();
                    List<DeviationOfficerMapping> deviationMappingList = loanService
                            .findMappingByLoanDeviationId(loanDeviation.getDeviationId().toString());
                    for (DeviationOfficerMapping deviationMap : deviationMappingList) {
                        DeviationApprovalHistory deviationHistoryMap = new DeviationApprovalHistory();
                        User userApproval = userService.loadUserByUserId(deviationMap.getOfficer());
                        if (userApproval != null) {
                            deviationHistoryMap.setName(userApproval.getName());
                            deviationHistoryMap.setRole(userApproval.getRoleName());
                            deviationHistoryMap.setSupervisorId(userApproval.getUserId().toString());
                        }
                        deviationHistoryMap.setStatus(deviationMap.getStatus());
                        if (!deviationMap.getStatus().equals(WebGuiConstant.STATUS_APPROVED)) {
                            if (deviationMap.getUpdatedDate() != null)
                                deviationHistoryMap.setTanggal(formatDate.format(deviationMap.getUpdatedDate()));
                        }
                        deviationHistoryMap.setDate(deviationMap.getDate());
                        deviationHistoryMap.setLevel(deviationMap.getLevel());
                        deviationHistory.add(deviationHistoryMap);
                    }
                    final List<LoanDeviation> listDeletedDeviation = loanService.findIsDeletedDeviationList();
                    List<String> deletedDeviationList = new ArrayList<>();
                    for (LoanDeviation deletedDeviation : listDeletedDeviation) {
                        String id = deletedDeviation.getDeviationId().toString();
                        deletedDeviationList.add(id);
                    }
                    deviationPojo.setApprovalHistories(deviationHistory);
                    deviationResponses.add(deviationPojo);
                }
                final List<LoanDeviation> listDeletedDeviation = loanService.findIsDeletedDeviationList();
                List<String> deletedDeviationList = new ArrayList<>();
                for (LoanDeviation deletedDeviation : listDeletedDeviation) {
                    String id = deletedDeviation.getDeviationId().toString();
                    deletedDeviationList.add(id);
                }
                response.setDeletedIds(deletedDeviationList);
                log.info("Finishing create response Deviation data");
                response.setDeviationList(deviationResponses);
            }
        } catch (Exception e) {
            log.error("syncDeviationV2 error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("syncDeviationV2 error: " + e.getMessage());
        } finally {
            try {
                log.info("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_SW);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
            } catch (Exception e) {
                log.error("syncDeviationV2 saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            log.info("Send Response to Device");
            log.info("syncDeviationV2 RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            return response;
        }

    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_DETAIL_DEVIATION_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    DeviationResponse getDetailDeviation(@RequestBody final DetailDeviationRequest request,
                                         @PathVariable("apkVersion") String apkVersion) throws Exception {

        log.info("INCOMING GET DETAIL DEVIATION MESSAGE : " + request.toString());
        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        String startLookupDate = request.getStartLookupDate();
        String endLookupDate = request.getEndLookupDate();
        final DeviationResponse response = new DeviationResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("getDetailDeviation INCOMING MESSAGE : " + jsonUtils.toJson(request));
            log.info("VALIDATING REQUEST...");
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(), request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage("getDetailDeviation validation failed: " + label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                log.info("Validation success, get Deviation data");
                response.setResponseMessage("SUKSES");
                List<LoanDeviation> deviationPage = new ArrayList<>();
                User userMS = userService.findUserByUsername(request.getUsername());
                Location wismaMs = areaService.findLocationById(userMS.getOfficeCode());
                deviationPage = loanService.findByBatch(request.getBatch());
                log.info("Finishing get Deviation data");
                AP3R ap3rLocal = new AP3R();
                final List<DeviationResponse> deviationResponses = new ArrayList<DeviationResponse>();
                for (final LoanDeviation loanDeviation : deviationPage) {
                    DeviationResponse deviationPojo = new DeviationResponse();
                    deviationPojo.setDeviationId(loanDeviation.getDeviationId().toString());
                    deviationPojo.setStatus(loanDeviation.getStatus());
                    if (loanDeviation.getLoanId() != null)
                        deviationPojo.setLoanId(loanDeviation.getLoanId().toString());
                    if (loanDeviation.getAp3rId() != null)
                        ap3rLocal = ap3RService.findById(loanDeviation.getAp3rId());
                    if (ap3rLocal != null)
                        deviationPojo.setAp3rId(ap3rLocal.getLocalId());
                    deviationPojo.setDeviationCode(loanDeviation.getDeviationCode());
                    deviationPojo.setDeviationReason(loanDeviation.getDeviationReason());
                    if (loanDeviation.getCreatedBy() != null) {
                        deviationPojo.setCreatedBy(loanDeviation.getCreatedBy());
                        User userDeviation = userService.findUserByUsername(loanDeviation.getCreatedBy());
                        if (userDeviation != null) {
                            if (!userDeviation.getOfficeCode().equals("")) {
                                Location wisma = areaService.findLocationById(userDeviation.getOfficeCode());
                                WismaSWReq wismaMap = new WismaSWReq();
                                wismaMap.setOfficeId(userDeviation.getOfficeCode());
                                if (wisma != null) {
                                    wismaMap.setOfficeCode(wisma.getLocationCode());
                                    wismaMap.setOfficeName(wisma.getName());
                                }
                                deviationPojo.setWisma(wismaMap);
                            }
                        }
                    }
                    if (loanDeviation.getCreatedDate() != null)
                        deviationPojo.setCreatedDate(formatDateTime.format(loanDeviation.getCreatedDate()));
                    deviationPojo.setBatch(loanDeviation.getBatch());
                    List<DeviationApprovalHistory> deviationHistory = new ArrayList<>();
                    List<DeviationOfficerMapping> deviationMappingList = loanService
                            .findMappingByLoanDeviationId(loanDeviation.getDeviationId().toString());
                    for (DeviationOfficerMapping deviationMap : deviationMappingList) {
                        DeviationApprovalHistory deviationHistoryMap = new DeviationApprovalHistory();
                        User userApproval = userService.loadUserByUserId(deviationMap.getOfficer());
                        if (userApproval != null) {
                            deviationHistoryMap.setName(userApproval.getName());
                            deviationHistoryMap.setRole(userApproval.getRoleName());
                            deviationHistoryMap.setSupervisorId(userApproval.getUserId().toString());
                        }
                        deviationHistoryMap.setStatus(deviationMap.getStatus());
                        if (!deviationMap.getStatus().equals(WebGuiConstant.STATUS_APPROVED)) {
                            if (deviationMap.getUpdatedDate() != null)
                                deviationHistoryMap.setTanggal(formatDate.format(deviationMap.getUpdatedDate()));
                        }
                        deviationHistoryMap.setDate(deviationMap.getDate());
                        deviationHistoryMap.setLevel(deviationMap.getLevel());
                        deviationHistory.add(deviationHistoryMap);
                    }
                    final List<LoanDeviation> listDeletedDeviation = loanService.findIsDeletedDeviationList();
                    List<String> deletedDeviationList = new ArrayList<>();
                    for (LoanDeviation deletedDeviation : listDeletedDeviation) {
                        String id = deletedDeviation.getDeviationId().toString();
                        deletedDeviationList.add(id);
                    }
                    deviationPojo.setApprovalHistories(deviationHistory);
                    deviationResponses.add(deviationPojo);
                }

                final List<LoanDeviation> listDeletedDeviation = loanService.findIsDeletedDeviationList();
                List<String> deletedDeviationList = new ArrayList<>();
                for (LoanDeviation deletedDeviation : listDeletedDeviation) {
                    String id = deletedDeviation.getDeviationId().toString();
                    deletedDeviationList.add(id);
                }
                response.setDeletedIds(deletedDeviationList);
                log.info("Finishing create response Deviation data");
                log.info("get surrounding data");
                List<AP3R> ap3rList = new ArrayList<>();
                if (userMS.getRoleUser().equals("2")) {
                    ap3rList = ap3RService.findByAp3rIdInLoanDeviationAndWismaWithBatch(request.getUsername(), request.getBatch());
                } else {
                    ap3rList = ap3RService.findByAp3rIdInLoanDeviationWithBatch(request.getUsername(), request.getBatch());
                }
                List<AP3RList> ap3rListResponse = new ArrayList<>();
                for (final AP3R ap3r : ap3rList) {
                    AP3RList ap3rMap = new AP3RList();
                    ap3rMap.setAp3rId(ap3r.getLocalId());
                    if (ap3r.getSwId() != null) {
                        SurveyWawancara swLocal = swService.getSWById(ap3r.getSwId().toString());
                        if (swLocal != null)
                            ap3rMap.setSwId(swLocal.getLocalId());
                    }
                    if (ap3r.getSwProductId() != null) {
                        SWProductMapping swProductMapping = swService.findProductMapById(ap3r.getSwProductId());
                        ap3rMap.setSwProductId(swProductMapping.getLocalId());
                    }
                    if (ap3r.getHasSaving() != null) {
                        ap3rMap.setHaveAccount("" + ap3r.getHasSaving());
                    } else {
                        ap3rMap.setHaveAccount("");
                    }
                    if (ap3rMap.getHaveAccount().equals("t")) {
                        ap3rMap.setHaveAccount("true");
                    } else {
                        ap3rMap.setHaveAccount("false");
                    }
                    if (ap3r.getOpenSavingReason() != null) {
                        ap3rMap.setAccountPurpose("" + ap3r.getOpenSavingReason());
                    } else {
                        ap3rMap.setAccountPurpose("");
                    }
                    if (ap3r.getFundSource() != null) {
                        ap3rMap.setFundSource("" + ap3r.getFundSource());
                    } else {
                        ap3rMap.setFundSource("");
                    }
                    if (ap3r.getTransactionInYear() != null) {
                        ap3rMap.setYearlyTransaction("" + ap3r.getTransactionInYear());
                    } else {
                        ap3rMap.setYearlyTransaction("");
                    }
                    if (ap3r.getLoanReason() != null) {
                        ap3rMap.setFinancingPurpose("" + ap3r.getLoanReason());
                    } else {
                        ap3rMap.setFinancingPurpose("");
                    }
                    ap3rMap.setBusinessTypeId(ap3r.getBusinessField());
                    List<FundedThingsReq> productMapList = new ArrayList<>();
                    List<FundedThings> productList = ap3RService.findByAp3rId(ap3r);
                    for (FundedThings product : productList) {
                        FundedThingsReq productMap = new FundedThingsReq();
                        productMap.setName(product.getProductName());
                        productMap.setPrice(product.getPrice().toString());
                        productMapList.add(productMap);
                    }
                    ap3rMap.setFinancedGoods(productMapList);
                    ap3rMap.setDisburseDate(formatter.format(ap3r.getDisbursementDate()));
                    ap3rMap.setHighRiskCustomer("" + ap3r.getHighRiskCustomer());
                    if (ap3rMap.getHighRiskCustomer().equals("t")) {
                        ap3rMap.setHighRiskCustomer("true");
                    } else {
                        ap3rMap.setHighRiskCustomer("false");
                    }
                    ap3rMap.setHighRiskBusiness("" + ap3r.getHighRiskBusiness());
                    if (ap3rMap.getHighRiskBusiness().equals("t")) {
                        ap3rMap.setHighRiskBusiness("true");
                    } else {
                        ap3rMap.setHighRiskBusiness("false");
                    }
                    ap3rMap.setStatus(ap3r.getStatus());
                    ap3rMap.setCreatedBy(ap3r.getCreatedBy());
                    if (ap3r.getDueDate() != null)
                        ap3rMap.setDueDate(formatter.format(ap3r.getDueDate()));
                    if (ap3r.getRejectedReason() != null)
                        ap3rMap.setRejectionReason(ap3r.getRejectedReason());
                    else
                        ap3rMap.setRejectionReason("");
                    if (ap3r.getApprovalStatus() != null)
                        ap3rMap.setApprovalStatus(ap3r.getApprovalStatus().toString());
                    else
                        ap3rMap.setApprovalStatus("0");
                    LoanDeviation loanDeviation = loanService.findDeviationByAp3rId(ap3r.getAp3rId());
                    if (loanDeviation != null) {
                        if (loanDeviation.getStatus() != null) {
                            if (loanDeviation.getStatus().equals(WebGuiConstant.STATUS_WAITING_FOR_APPROVAL) || loanDeviation.getStatus().equals(WebGuiConstant.STATUS_APPROVED)) {
                                ap3rMap.setDeviationExists("true");
                            } else {
                                ap3rMap.setDeviationExists("false");
                            }
                        } else {
                            ap3rMap.setDeviationExists("false");
                        }
                    } else {
                        ap3rMap.setDeviationExists("false");
                    }
                    ap3rListResponse.add(ap3rMap);
                }
                List<Customer> newCustomerList = new ArrayList<>();
                List<Customer> existCustomerList = new ArrayList<>();
                if (userMS.getRoleUser().equals("2")) {
                    newCustomerList = customerService.getNewCustomerLoanDeviationAndWismaWithBatch(request.getUsername(), request.getBatch());
                    existCustomerList = customerService.getExistCustomerLoanDeviationAndWismaWithBatch(request.getUsername(), request.getBatch());
                } else {
                    newCustomerList = customerService.getNewCustomerLoanDeviationWithBatch(request.getUsername(), request.getBatch());
                    existCustomerList = customerService.getExistCustomerLoanDeviationWithBatch(request.getUsername(), request.getBatch());
                }
                List<CustomerPojo> customer1 = new ArrayList<>();
                List<CustomerPojo> customer2 = new ArrayList<>();
                for (Customer customer : newCustomerList) {
                    CustomerPojo customerMap = new CustomerPojo();
                    customerMap.setCreatedDate(formatDateTime.format(customer.getCreatedDate()));
                    customerMap.setCustomerId(customer.getCustomerId().toString());
                    customerMap.setCustomerCifNumber(customer.getCustomerCifNumber());
                    customerMap.setCustomerIdNumber(customer.getCustomerIdNumber());
                    customerMap.setCustomerName(customer.getCustomerName());
                    customerMap.setCustomerAlias(customer.getCustomerAlias());
                    customerMap.setCustomerIdName(customer.getCustomerIdName());
                    customerMap.setCustomerIdAddress(customer.getCustomerIdAddress());
                    customerMap.setAddress(customer.getAddress());
                    customerMap.setRtRw(customer.getRtrw());
                    AreaKelurahan kelurahan = new AreaKelurahan();
                    AreaSubDistrict kecamatan = new AreaSubDistrict();
                    AreaDistrict kota = new AreaDistrict();
                    AreaProvince province = new AreaProvince();
                    if (customer.getKelurahan() != null) {
                        kelurahan = areaService.findKelurahanByAreaName(customer.getKelurahan());
                        if (kelurahan != null)
                            customerMap.setKelurahan(kelurahan.getAreaId());
                        else
                            customerMap.setKelurahan("");
                    } else {
                        customerMap.setKelurahan("");
                    }
                    if (customer.getKecamatan() != null) {
                        kecamatan = areaService.findKecamatanByAreaName(customer.getKecamatan());
                        if (kecamatan != null)
                            customerMap.setKecamatan(kecamatan.getAreaId());
                        else
                            customerMap.setKecamatan("");
                    } else {
                        customerMap.setKecamatan("");
                    }
                    if (customer.getCity() != null) {
                        kota = areaService.findKotaByAreaName(customer.getCity());
                        if (kota != null)
                            customerMap.setKabupaten(kota.getAreaId());
                        else
                            customerMap.setKabupaten("");
                    } else {
                        customerMap.setKabupaten("");
                    }
                    if (customer.getProvince() != null) {
                        province = areaService.findProvinceByAreaName(customer.getProvince());
                        if (province != null)
                            customerMap.setProvince(province.getAreaId());
                        else
                            customerMap.setProvince("");
                    } else {
                        customerMap.setProvince("");
                    }
                    customerMap.setPostalCode(customer.getPostalCode());
                    customerMap.setMotherName(customer.getMotherName());
                    customerMap.setJumlahTanggungan(customer.getJumlahTanggungan());
                    if (customer.getPlaceOfBirth() != null) {
                        kota = areaService.findByAreaName(customer.getPlaceOfBirth());
                        if (kota != null) {
                            customerMap.setPlaceOfBirthAreaId(kota.getAreaId());
                            customerMap.setPlaceOfBirth(kota.getAreaName());
                        } else {
                            kota = areaService.findKotaById(customer.getPlaceOfBirth());
                            if (kota != null) {
                                customerMap.setPlaceOfBirthAreaId(kota.getAreaId());
                                customerMap.setPlaceOfBirth(kota.getAreaName());
                            } else {
                                customerMap.setPlaceOfBirthAreaId("");
                                customerMap.setPlaceOfBirth(customer.getPlaceOfBirth());
                            }
                        }
                    } else {
                        customerMap.setPlaceOfBirthAreaId("");
                        customerMap.setPlaceOfBirth("");
                    }
                    if (customer.getDateOfBirth() != null)
                        customerMap.setDateOfBirth(formatter.format(customer.getDateOfBirth()));
                    customerMap.setSpouseName(customer.getSpouseName());
                    if (customer.getSpousePlaceOfBirth() != null) {
                        kota = areaService.findByAreaName(customer.getSpousePlaceOfBirth());
                        if (kota != null) {
                            customerMap.setSpousePlaceOfBirthAreaId(kota.getAreaId());
                            customerMap.setSpousePlaceOfBirth(kota.getAreaName());
                        } else {
                            kota = areaService.findKotaById(customer.getSpousePlaceOfBirth());
                            if (kota != null) {
                                customerMap.setSpousePlaceOfBirthAreaId(kota.getAreaId());
                                customerMap.setSpousePlaceOfBirth(kota.getAreaName());
                            } else {
                                customerMap.setSpousePlaceOfBirthAreaId("");
                                customerMap.setSpousePlaceOfBirth(customer.getSpousePlaceOfBirth());
                            }
                        }
                    } else {
                        customerMap.setSpousePlaceOfBirthAreaId("");
                        customerMap.setSpousePlaceOfBirth("");
                    }
                    if (customer.getSpouseDateOfBirth() != null)
                        customerMap.setSpouseDateOfBirth(formatter.format(customer.getSpouseDateOfBirth()));
                    customerMap.setPhoneNumber(customer.getPhoneNumber());
                    customerMap.setOtherAddress(customer.getOtherAddress());
                    if (customer.getEducation() != null) {
                        if (customer.getEducation().equals(CustomerConstant.Edu_Desc_SD)) {
                            customerMap.setPendidikan("1");
                        } else if (customer.getEducation().equals(CustomerConstant.Edu_Desc_SLTP)) {
                            customerMap.setPendidikan("2");
                        } else if (customer.getEducation().equals(CustomerConstant.Edu_Desc_SLTA)) {
                            customerMap.setPendidikan("3");
                        } else {
                            customerMap.setPendidikan("4");
                        }
                    }
                    if (customer.getHouseType() != null) {
                        if (customer.getHouseType().equals(CustomerConstant.Residence_Status_Desc_Milik_Sendiri)) {
                            customerMap.setStatusRumah("1");
                        } else if (customer.getHouseType().equals(CustomerConstant.Residence_Status_Desc_Orang_Tua)) {
                            customerMap.setStatusRumah("2");
                        } else {
                            customerMap.setStatusRumah("3");
                        }
                    } else {
                        customerMap.setStatusRumah("");
                    }
                    customerMap.setReligion(customer.getReligion());
                    if (customer.getMaritalStatus() != null) {
                        if (customer.getMaritalStatus().equals(CustomerConstant.Marital_Desc_Menikah)) {
                            customerMap.setMaritalStatus("1");
                        } else if (customer.getMaritalStatus().equals(CustomerConstant.Marital_Desc_Single)) {
                            customerMap.setMaritalStatus("2");
                        } else {
                            customerMap.setMaritalStatus("3");
                        }
                    }
                    customerMap.setGender(customer.getGender());
                    if (customer.getDateOfGovernmentId() != null)
                        customerMap.setDateOfGovernmentId(formatDate.format(customer.getDateOfGovernmentId()));
                    customerMap.setAssignedUsername(customer.getAssignedUsername());
                    customerMap.setCustomerStatus(customer.getCustomerStatus());
                    customerMap.setProsperaId(customer.getProsperaId());
                    if (customer.getSwId() != null) {
                        SurveyWawancara sw = swService.getSWById("" + customer.getSwId());
                        if (sw != null) {
                            if (sw.getLocalId() != null)
                                customerMap.setSwId("" + sw.getLocalId());
                        }
                        if (customer.getPdkId() != null)
                            customerMap.setPdkId(customer.getPdkId().toString());
                    }
                    Group group = sentraService.getGroupByCustomerId(customer.getCustomerId());
                    customerMap.setCreatedBy(customer.getCreatedBy());
                    customerMap.setGroupId(group.getGroupId().toString());
                    CustomerWithAbsence custAbsence = syncCustomerService.findByCustomerId(customer.getCustomerId());
                    if (custAbsence != null)
                        if (custAbsence.getTotalAbsence() != null)
                            customerMap.setTotalMangkir(custAbsence.getTotalAbsence().toString());
                    customer1.add(customerMap);
                }
                log.debug("SIZE 1st LIST : " + customer1.size());
                for (Customer customer : existCustomerList) {
                    CustomerPojo customerMapExist = new CustomerPojo();
                    customerMapExist.setCreatedDate(formatDateTime.format(customer.getCreatedDate()));
                    customerMapExist.setCustomerId(customer.getCustomerId().toString());
                    customerMapExist.setCustomerCifNumber(customer.getCustomerCifNumber());
                    customerMapExist.setCustomerIdNumber(customer.getCustomerIdNumber());
                    customerMapExist.setCustomerName(customer.getCustomerName());
                    customerMapExist.setCustomerAlias(customer.getCustomerAlias());
                    customerMapExist.setCustomerIdName(customer.getCustomerIdName());
                    customerMapExist.setCustomerIdAddress(customer.getCustomerIdAddress());
                    customerMapExist.setAddress(customer.getAddress());
                    customerMapExist.setRtRw(customer.getRtrw());
                    AreaKelurahan kelurahan = new AreaKelurahan();
                    AreaSubDistrict kecamatan = new AreaSubDistrict();
                    AreaDistrict kota = new AreaDistrict();
                    AreaProvince province = new AreaProvince();
                    if (customer.getKelurahan() != null) {
                        kelurahan = areaService.findKelurahanByAreaName(customer.getKelurahan());
                        if (kelurahan != null)
                            customerMapExist.setKelurahan(kelurahan.getAreaId());
                        else
                            customerMapExist.setKelurahan("");
                    } else {
                        customerMapExist.setKelurahan("");
                    }
                    if (customer.getKecamatan() != null) {
                        kecamatan = areaService.findKecamatanByAreaName(customer.getKecamatan());
                        if (kecamatan != null)
                            customerMapExist.setKecamatan(kecamatan.getAreaId());
                        else
                            customerMapExist.setKecamatan("");
                    } else {
                        customerMapExist.setKecamatan("");
                    }
                    if (customer.getCity() != null) {
                        kota = areaService.findKotaByAreaName(customer.getCity());
                        if (kota != null)
                            customerMapExist.setKabupaten(kota.getAreaId());
                        else
                            customerMapExist.setKabupaten("");
                    } else {
                        customerMapExist.setKabupaten("");
                    }
                    if (customer.getProvince() != null) {
                        province = areaService.findProvinceByAreaName(customer.getProvince());
                        if (province != null)
                            customerMapExist.setProvince(province.getAreaId());
                        else
                            customerMapExist.setProvince("");
                    } else {
                        customerMapExist.setProvince("");
                    }
                    customerMapExist.setPostalCode(customer.getPostalCode());
                    customerMapExist.setMotherName(customer.getMotherName());
                    customerMapExist.setJumlahTanggungan(customer.getJumlahTanggungan());
                    if (customer.getPlaceOfBirth() != null) {
                        kota = areaService.findByAreaName(customer.getPlaceOfBirth());
                        if (kota != null) {
                            customerMapExist.setPlaceOfBirthAreaId(kota.getAreaId());
                            customerMapExist.setPlaceOfBirth(kota.getAreaName());
                        } else {
                            kota = areaService.findKotaById(customer.getPlaceOfBirth());
                            if (kota != null) {
                                customerMapExist.setPlaceOfBirthAreaId(kota.getAreaId());
                                customerMapExist.setPlaceOfBirth(kota.getAreaName());
                            } else {
                                customerMapExist.setPlaceOfBirthAreaId("");
                                customerMapExist.setPlaceOfBirth(customer.getPlaceOfBirth());
                            }
                        }
                    } else {
                        customerMapExist.setPlaceOfBirthAreaId("");
                        customerMapExist.setPlaceOfBirth("");
                    }
                    if (customer.getDateOfBirth() != null)
                        customerMapExist.setDateOfBirth(formatter.format(customer.getDateOfBirth()));
                    customerMapExist.setSpouseName(customer.getSpouseName());
                    if (customer.getSpousePlaceOfBirth() != null) {
                        kota = areaService.findByAreaName(customer.getSpousePlaceOfBirth());
                        if (kota != null) {
                            customerMapExist.setSpousePlaceOfBirthAreaId(kota.getAreaId());
                            customerMapExist.setSpousePlaceOfBirth(kota.getAreaName());
                        } else {
                            kota = areaService.findKotaById(customer.getSpousePlaceOfBirth());
                            if (kota != null) {
                                customerMapExist.setSpousePlaceOfBirthAreaId(kota.getAreaId());
                                customerMapExist.setSpousePlaceOfBirth(kota.getAreaName());
                            } else {
                                customerMapExist.setSpousePlaceOfBirthAreaId("");
                                customerMapExist.setSpousePlaceOfBirth(customer.getSpousePlaceOfBirth());
                            }
                        }
                    } else {
                        customerMapExist.setSpousePlaceOfBirthAreaId("");
                        customerMapExist.setSpousePlaceOfBirth("");
                    }
                    if (customer.getSpouseDateOfBirth() != null)
                        customerMapExist.setSpouseDateOfBirth(formatter.format(customer.getSpouseDateOfBirth()));
                    customerMapExist.setPhoneNumber(customer.getPhoneNumber());
                    customerMapExist.setOtherAddress(customer.getOtherAddress());
                    if (customer.getEducation() != null) {
                        if (customer.getEducation().equals(CustomerConstant.Edu_Desc_SD)) {
                            customerMapExist.setPendidikan("1");
                        } else if (customer.getEducation().equals(CustomerConstant.Edu_Desc_SLTP)) {
                            customerMapExist.setPendidikan("2");
                        } else if (customer.getEducation().equals(CustomerConstant.Edu_Desc_SLTA)) {
                            customerMapExist.setPendidikan("3");
                        } else {
                            customerMapExist.setPendidikan("4");
                        }
                    }
                    if (customer.getHouseType() != null) {
                        if (customer.getHouseType().equals(CustomerConstant.Residence_Status_Desc_Milik_Sendiri)) {
                            customerMapExist.setStatusRumah("1");
                        } else if (customer.getHouseType().equals(CustomerConstant.Residence_Status_Desc_Orang_Tua)) {
                            customerMapExist.setStatusRumah("2");
                        } else {
                            customerMapExist.setStatusRumah("3");
                        }
                    } else {
                        customerMapExist.setStatusRumah("");
                    }
                    customerMapExist.setReligion(customer.getReligion());
                    if (customer.getMaritalStatus() != null) {
                        if (customer.getMaritalStatus().equals(CustomerConstant.Marital_Desc_Menikah)) {
                            customerMapExist.setMaritalStatus("1");
                        } else if (customer.getMaritalStatus().equals(CustomerConstant.Marital_Desc_Single)) {
                            customerMapExist.setMaritalStatus("2");
                        } else {
                            customerMapExist.setMaritalStatus("3");
                        }
                    }
                    customerMapExist.setGender(customer.getGender());
                    if (customer.getDateOfGovernmentId() != null)
                        customerMapExist.setDateOfGovernmentId(formatDate.format(customer.getDateOfGovernmentId()));
                    customerMapExist.setAssignedUsername(customer.getAssignedUsername());
                    customerMapExist.setCustomerStatus(customer.getCustomerStatus());
                    customerMapExist.setProsperaId(customer.getProsperaId());
                    if (customer.getSwId() != null) {
                        SurveyWawancara sw = swService.getSWById("" + customer.getSwId());
                        if (sw != null) {
                            if (sw.getLocalId() != null)
                                customerMapExist.setSwId("" + sw.getLocalId());
                        }
                        if (customer.getPdkId() != null)
                            customerMapExist.setPdkId(customer.getPdkId().toString());
                    }
                    Group group = sentraService.getGroupByCustomerId(customer.getCustomerId());
                    customerMapExist.setCreatedBy(customer.getCreatedBy());
                    customerMapExist.setGroupId(group.getGroupId().toString());
                    CustomerWithAbsence custAbsence = syncCustomerService.findByCustomerId(customer.getCustomerId());
                    if (custAbsence != null)
                        if (custAbsence.getTotalAbsence() != null)
                            customerMapExist.setTotalMangkir(custAbsence.getTotalAbsence().toString());
                    for (Iterator<CustomerPojo> iterator = customer1.iterator(); iterator.hasNext(); ) {
                        CustomerPojo customerPojo = iterator.next();
                        if (customerPojo.getCustomerId().equals(customerMapExist.getCustomerId())) {
                            iterator.remove();
                        }
                    }
                    customer2.add(customerMapExist);
                }
                log.debug("SIZE 2rd LIST : " + customer2.size());
                Set<CustomerPojo> unionCustomer = new LinkedHashSet<>();
                unionCustomer.addAll(customer1);
                unionCustomer.addAll(customer2);
                log.debug("SIZE 3rd LIST : " + unionCustomer.size());
                List<CustomerPojo> customerResponse = new ArrayList<>(unionCustomer);
//    			bugfix deviasi appid tidak muncul
                List<LoanListResponse> loanListResponse = new ArrayList<>();
                for (CustomerPojo cust : customerResponse) {
                    Customer customer = customerService.findById(cust.getCustomerId());
                    List<Loan> loanList = loanService.findLoanByCustomerAndStatus(customer, WebGuiConstant.STATUS_ACTIVE);
                    for (Loan loan3 : loanList) {
                        LoanListResponse loanMap = new LoanListResponse();
                        loanMap.setLoanId(loan3.getLoanId().toString());
                        loanMap.setAccountNumber(loan3.getAccountNumber());
                        if (loan3.getAp3rId() != null) {
                            AP3R ap3rLocalMap = ap3RService.findById(loan3.getAp3rId());
                            loanMap.setAp3rId(ap3rLocalMap.getLocalId());
                        }
                        loanMap.setAppId(loan3.getAppId());
                        loanMap.setCreatedBy(loan3.getCreatedBy());
                        loanMap.setCustomerId(loan3.getCustomer().getCustomerId().toString());
                        loanMap.setCustomerName(loan3.getCustomerName());
                        if (loan3.getLoanAmountRecommended() != null)
                            loanMap.setLoanAmountRecommended(loan3.getLoanAmountRecommended().toString());
                        loanMap.setProsperaId(loan3.getProsperaId());
                        loanMap.setStatus(loan3.getStatus());
                        if (loan3.getSwId() != null) {
                            SurveyWawancara swLocal = swService.getSWById(loan3.getSwId().toString());
                            loanMap.setSwId(swLocal.getLocalId());
                        }
                        if (loan3.getCreatedDate() != null)
                            loanMap.setCreatedDate(formatDateTime.format(loan3.getCreatedDate()));
                        if (loan3.getDisbursementDate() != null)
                            loanMap.setDisbursementDate(formatDateTime.format(loan3.getDisbursementDate()));
                        if (loan3.getProductId() != null)
                            loanMap.setProductId(loan3.getProductId().toString());
                        if (loan3.getPlafond() != null)
                            loanMap.setPlafond(loan3.getPlafond().toString());
                        if (loan3.getLoanId() != null) {
                            LoanWithEmergencyFund useEmergencyFund = loanService
                                    .findLoanWithEmergencyFundByLoanId(loan3.getLoanId());
                            if (useEmergencyFund != null) {
                                loanMap.setUseEmergencyFundTotal(useEmergencyFund.getUseEmergencyFundTotal().toString());
                            } else {
                                loanMap.setUseEmergencyFundTotal("");
                            }
                        }
                        loanListResponse.add(loanMap);
                    }
                }
                List<SurveyWawancara> swList = new ArrayList<>();
                if (userMS.getRoleUser().equals("2")) {
                    swList = swService.getSWForMS(request.getUsername(), startLookupDate, endLookupDate);
                } else {
                    swList = swService.findSwIdInLoanDeviationWithBatch(request.getUsername(), request.getBatch());
                }
                List<ListSWResponse> swResponse = new ArrayList<>();
                for (final SurveyWawancara sw : swList) {
                    ListSWResponse swPojo = new ListSWResponse();
                    swPojo.setSwId(sw.getLocalId());
                    // get wisma SW
                    WismaSWReq wisma = new WismaSWReq();
                    Location wismaSw = areaService.findLocationById(sw.getWismaId());
                    if (wismaSw != null) {
                        wisma.setOfficeId(wismaSw.getLocationId());
                        wisma.setOfficeCode(wismaSw.getLocationCode());
                        wisma.setOfficeName(wismaSw.getName());
                        swPojo.setWisma(wisma);
                    }
                    if (sw.getCustomerRegistrationType() != null)
                        swPojo.setCustomerType("" + sw.getCustomerRegistrationType());
                    else
                        swPojo.setCustomerType("0");
                    if (sw.getSurveyDate() != null)
                        swPojo.setSurveyDate(formatDateTime.format(sw.getSurveyDate()));
                    // get list produk pembiayaan SW
                    List<SWProductListResponse> swProductList = new ArrayList<>();
                    List<SWProductMapping> productList = swService.findProductMapBySwId(sw.getSwId());
                    for (SWProductMapping product : productList) {
                        SWProductListResponse swProduct = new SWProductListResponse();
                        swProduct.setSwProductId(product.getLocalId());
                        swProduct.setProductId(product.getProductId().getProductId().toString());
                        if (product.getPlafon() != null)
                            swProduct.setSelectedPlafon(product.getPlafon());
                        else
                            swProduct.setSelectedPlafon(product.getPlafon().ZERO);
                        swProduct.setRecommendationProductId(product.getProductId().getProductId().toString());
                        if (product.getRecommendedPlafon() != null)
                            swProduct.setRecommendationSelectedPlafon(product.getRecommendedPlafon());
                        else
                            swProduct.setRecommendationSelectedPlafon(product.getRecommendedPlafon().ZERO);
                        swProductList.add(swProduct);
                    }
                    swPojo.setSwProducts(swProductList);
                    List<AP3R> ap3rListForSw = ap3RService.findAp3rListBySwId(sw.getSwId());
                    // get profil SW
                    SWProfileResponse profile = new SWProfileResponse();
                    if (sw.getCustomerId() != null) {
                        swPojo.setCustomerId(sw.getCustomerId().toString());
                        Customer customer = customerService.findById(sw.getCustomerId().toString());
                        if (customer != null) {
                            Group group = sentraService.getGroupByCustomerId(customer.getCustomerId());
                            Sentra sentra = sentraService.getSentraByGroupId(group.getGroupId());
                            profile.setLongName(customer.getCustomerName());
                            swPojo.setCustomerCif(customer.getCustomerCifNumber());
                            swPojo.setSentraId(sentra.getSentraId().toString());
                            swPojo.setSentraName(sentra.getSentraName());
                            swPojo.setGroupId(group.getGroupId().toString());
                            swPojo.setGroupName(group.getGroupName());
                            swPojo.setCustomerExists("true");
                            for (AP3R ap3r : ap3rListForSw) {
                                Loan loan = loanService.findByAp3rAndCustomer(ap3r.getAp3rId(), customer);
                                if (loan != null) {
                                    swPojo.setLoanExists("true");
                                } else {
                                    swPojo.setLoanExists("false");
                                }
                            }
                        } else {
                            swPojo.setCustomerExists("false");
                        }
                    } else {
                        swPojo.setCustomerExists("false");
                    }
                    swPojo.setIdCardNumber(sw.getCustomerIdNumber());
                    profile.setAlias(sw.getCustomerAliasName());
                    swPojo.setIdCardName(sw.getCustomerIdName());
                    if (sw.getGender() != null)
                        profile.setGender("" + sw.getGender());
                    else
                        profile.setGender("0");
                    if (sw.getReligion() != null)
                        profile.setReligion(sw.getReligion());
                    else
                        profile.setReligion("0");
                    profile.setBirthPlace(sw.getBirthPlace());
                    profile.setBirthPlaceRegencyName(sw.getBirthPlaceRegencyName());
                    if (sw.getBirthDate() != null)
                        profile.setBirthDay(formatter.format(sw.getBirthDate()));
                    if (sw.getIdExpiryDate() != null) {
                        profile.setIdCardExpired(formatter.format(sw.getIdExpiryDate()));
                    } else {
                        profile.setIdCardExpired("");
                    }
                    if (sw.getMaritalStatus() != null)
                        profile.setMarriedStatus("" + sw.getMaritalStatus());
                    else
                        profile.setMarriedStatus("0");
                    profile.setLongName(sw.getCustomerName());
                    if (sw.getWorkType() != null)
                        profile.setWorkStatus("" + sw.getWorkType());
                    else
                        profile.setWorkStatus("0");
                    if (sw.getCreatedDate() != null)
                        swPojo.setCreatedAt(formatter.format(sw.getCreatedDate()));
                    profile.setPhoneNumber(sw.getPhoneNumber());
                    profile.setNpwp(sw.getNpwp());
                    profile.setMotherName(sw.getMotherName());
                    if (sw.getDependentCount() != null)
                        profile.setDependants(sw.getDependentCount().toString());
                    else
                        profile.setDependants("0");
                    if (sw.getEducation() != null)
                        profile.setEducation("" + sw.getEducation());
                    else
                        profile.setEducation("0");
                    if (sw.getLifeTime() != null) {
                        if (sw.getLifeTime() == true) {
                            profile.setIsLifeTime("true");
                        } else {
                            profile.setIsLifeTime("false");
                        }
                    } else {
                        profile.setIsLifeTime("false");
                    }
                    swPojo.setProfile(profile);
                    // get alamat SW
                    SwAddressResponse address = new SwAddressResponse();
                    address.setName(sw.getAddress());
                    address.setStreet(sw.getRtrw());
                    address.setPostCode(sw.getPostalCode());
                    if (sw.getKelurahan() != null)
                        address.setVillageId(sw.getKelurahan());
                    else
                        address.setVillageId("0");
                    if (sw.getKecamatan() != null)
                        address.setRegencyId(sw.getKecamatan());
                    else
                        address.setRegencyId("0");
                    if (sw.getProvince() != null)
                        address.setProvinceId(sw.getProvince());
                    else
                        address.setProvinceId("0");
                    if (sw.getCity() != null)
                        address.setDistrictId(sw.getCity());
                    else
                        address.setDistrictId("0");
                    address.setAdditionalAddress(sw.getDomisiliAddress());
                    address.setPlaceOwnerShip("" + sw.getHouseType());
                    address.setPlaceCertificate("" + sw.getHouseCertificate());
                    swPojo.setAddress(address);
                    // get pasangan SW
                    SWPartnerResponse partner = new SWPartnerResponse();
                    partner.setName(sw.getCoupleName());
                    partner.setBirthPlace(sw.getCoupleBirthPlaceRegencyName());
                    partner.setBirthPlaceId(sw.getCoupleBirthPlace());
                    if (sw.getCoupleBirthDate() != null)
                        partner.setBirthDay(formatter.format(sw.getCoupleBirthDate()));
                    if (sw.getCoupleJob() != null) {
                        if (sw.getCoupleJob().equals('K')) {
                            partner.setJob("Karyawan");
                        } else if (sw.getCoupleJob().equals('W')) {
                            log.debug("Job : " + sw.getCoupleJob());
                            partner.setJob("Wiraswasta");
                        } else if (sw.getCoupleJob().equals('M')) {
                            log.debug("Job : " + sw.getCoupleJob());
                            partner.setJob("Musiman");
                        } else {
                            partner.setJob("");
                        }
                    } else {
                        partner.setJob("");
                    }
                    swPojo.setPartner(partner);
                    // get usaha SW
                    SWBusinessResponse business = new SWBusinessResponse();
                    business.setType(sw.getBusinessField());
                    business.setDesc(sw.getBusinessDescription());
                    if (sw.getBusinessOwnerStatus() != null)
                        business.setBusinessOwnership("" + sw.getBusinessOwnerStatus());
                    else
                        business.setBusinessOwnership("");
                    if (sw.getBusinessShariaType() != null)
                        business.setBusinessShariaCompliance("" + sw.getBusinessShariaType());
                    else
                        business.setBusinessShariaCompliance("");
                    if (sw.getBusinessLocation() != null)
                        business.setBusinessLocation("" + sw.getBusinessLocation());
                    else
                        business.setBusinessLocation("");
                    business.setName(sw.getBusinessName());
                    business.setAddress(sw.getBusinessAddress());
                    if (sw.getBusinessAgeYear() != null)
                        business.setAgeYear(sw.getBusinessAgeYear().toString());
                    else
                        business.setAgeYear("0");
                    if (sw.getBusinessAgeMonth() != null)
                        business.setAgeMonth(sw.getBusinessAgeMonth().toString());
                    else
                        business.setAgeMonth("0");
                    if (sw.getBusinessWorkStatus() != null)
                        business.setBusinessRunner("" + sw.getBusinessWorkStatus());
                    else
                        business.setBusinessRunner("");
                    swPojo.setBusiness(business);
                    // get parameter kalkulasi
                    SWCalculationVariable calcVariable = new SWCalculationVariable();
                    calcVariable.setJenisSiklus("" + sw.getBusinessCycle());
                    calcVariable.setHariOperasional(sw.getBusinessDaysOperation());
                    if (sw.getOneDayBusyIncome() != null)
                        calcVariable.setPendapatanRamai(sw.getOneDayBusyIncome().toString());
                    else
                        calcVariable.setPendapatanRamai(sw.getOneDayBusyIncome().ZERO.toString());
                    if (sw.getTotalDaysInMonthBusy() != null)
                        calcVariable.setJumlahhariramai(sw.getTotalDaysInMonthBusy().toString());
                    else
                        calcVariable.setJumlahhariramai("0");
                    if (sw.getOneDayLessIncome() != null)
                        calcVariable.setPendapatanSepi(sw.getOneDayLessIncome().toString());
                    else
                        calcVariable.setPendapatanSepi(sw.getOneDayLessIncome().ZERO.toString());
                    if (sw.getTotalDaysInMonthLess() != null)
                        calcVariable.setJumlahharisepi(sw.getTotalDaysInMonthLess().toString());
                    else
                        calcVariable.setJumlahharisepi(sw.getOneDayLessIncome().ZERO.toString());
                    if (sw.getTotalLessBusyIncome() != null)
                        calcVariable.setPendapatanPerbulanRamaiSepi(sw.getTotalLessBusyIncome().toString());
                    else
                        calcVariable.setPendapatanPerbulanRamaiSepi(sw.getTotalLessBusyIncome().ZERO.toString());
                    if (sw.getFirstDayIncome() != null)
                        calcVariable.setPeriode1(sw.getFirstDayIncome().toString());
                    if (sw.getSecondDayIncome() != null)
                        calcVariable.setPeriode2(sw.getSecondDayIncome().toString());
                    if (sw.getThirdDayIncome() != null)
                        calcVariable.setPeriode3(sw.getThirdDayIncome().toString());
                    if (sw.getThreePeriodIncome() != null)
                        calcVariable.setPendapatanPerbulan3Periode(sw.getThreePeriodIncome().toString());
                    else
                        calcVariable.setPendapatanPerbulan3Periode(sw.getThreePeriodIncome().ZERO.toString());
                    if (sw.getOpenTime() != null)
                        calcVariable.setJamBuka(formatTime.format(sw.getOpenTime()));
                    if (sw.getCloseTime() != null)
                        calcVariable.setJamTutup(formatTime.format(sw.getCloseTime()));
                    if (sw.getWorkTime() != null)
                        calcVariable.setWaktuKerja(sw.getWorkTime().toString());
                    else
                        calcVariable.setWaktuKerja("0");
                    if (sw.getWorkTimeAfterOpen() != null)
                        calcVariable.setWaktukerjaSetelahbuka(sw.getWorkTimeAfterOpen().toString());
                    else
                        calcVariable.setWaktukerjaSetelahbuka("0");
                    if (sw.getCashOpenTime() != null)
                        calcVariable.setUangkasBoxdiBuka(sw.getCashOpenTime().toString());
                    if (sw.getCashCurrentTime() != null)
                        calcVariable.setUangKasBoxSekarang(sw.getCashCurrentTime().toString());
                    if (sw.getCashExpense() != null)
                        calcVariable.setUangBelanja(sw.getCashExpense().toString());
                    if (sw.getOneDayPredictIncome() != null)
                        calcVariable.setPendapatanPerbulanPerkiraanSehari(sw.getOneDayPredictIncome().toString());
                    else
                        calcVariable.setPendapatanPerbulanPerkiraanSehari(sw.getOneDayPredictIncome().ZERO.toString());
                    if (sw.getOneDayCashIncome() != null)
                        calcVariable.setPendapatanPerbulanKasSehari(sw.getOneDayCashIncome().toString());
                    else
                        calcVariable.setPendapatanPerbulanKasSehari(sw.getOneDayCashIncome().ZERO.toString());
                    if (sw.getAvgThreePeriodIncome() != null)
                        calcVariable.setPendapatanRata2TigaPeriode(sw.getAvgThreePeriodIncome().toString());
                    else
                        calcVariable.setPendapatanRata2TigaPeriode(sw.getAvgThreePeriodIncome().ZERO.toString());
                    List<OtherItemCalculation> itemPendapatanLainList = swService.findOtherItemCalcBySwId(sw.getSwId());
                    List<ItemCalcLainReq> itemCalcLainList = new ArrayList<>();
                    for (OtherItemCalculation itemPendapatanLain : itemPendapatanLainList) {
                        ItemCalcLainReq itemCalcLain = new ItemCalcLainReq();
                        itemCalcLain.setName(itemPendapatanLain.getName());
                        if (itemPendapatanLain.getAmount() != null)
                            itemCalcLain.setAmount(itemPendapatanLain.getAmount().toString());
                        else
                            itemCalcLain.setAmount(itemPendapatanLain.getAmount().ZERO.toString());
                        if (itemPendapatanLain.getPeriode() != null)
                            itemCalcLain.setPeriode(itemPendapatanLain.getPeriode().toString());
                        else
                            itemCalcLain.setPeriode("0");
                        if (itemPendapatanLain.getPendapatanPerbulan() != null)
                            itemCalcLain.setPendapatanPerbulan(itemPendapatanLain.getPendapatanPerbulan().toString());
                        else
                            itemCalcLain.setPendapatanPerbulan(itemPendapatanLain.getPendapatanPerbulan().ZERO.toString());
                        itemCalcLainList.add(itemCalcLain);
                    }
                    if (sw.getOtherIncome() != null)
                        calcVariable.setPendapatanLainnya(sw.getOtherIncome().toString());
                    else
                        calcVariable.setPendapatanLainnya(sw.getOtherIncome().ZERO.toString());
                    calcVariable.setItemCalcLains(itemCalcLainList);
                    swPojo.setCreatedBy(sw.getCreatedBy());
                    swPojo.setCalcVariable(calcVariable);
                    List<DirectBuyList> directBuyList = new ArrayList<>();
                    List<DirectBuyThings> directBuyProductList = swService.findProductBySwId(sw);
                    for (DirectBuyThings directBuyProduct : directBuyProductList) {
                        DirectBuyList product = new DirectBuyList();
                        if (directBuyProduct.getFrequency() != null)
                            product.setFrequency(directBuyProduct.getFrequency().toString());
                        else
                            product.setFrequency("0");
                        if (directBuyProduct.getIndex() != null)
                            product.setIndex(directBuyProduct.getIndex().toString());
                        else
                            product.setIndex("0");
                        product.setItemName(directBuyProduct.getNamaBarang());
                        if (directBuyProduct.getPrice() != null)
                            product.setPrice(directBuyProduct.getPrice());
                        else
                            product.setPrice(directBuyProduct.getPrice().ZERO);
                        if (directBuyProduct.getSellingPrice() != null)
                            product.setSellingPrice(directBuyProduct.getSellingPrice());
                        else
                            product.setSellingPrice(directBuyProduct.getSellingPrice().ZERO);
                        product.setTotalItem(directBuyProduct.getTotalItem().toString());
                        if (directBuyProduct.getType() != null)
                            product.setType(directBuyProduct.getType().toString());
                        else
                            product.setType("0");
                        directBuyList.add(product);
                    }
                    swPojo.setDirectPurchasing(directBuyList);
                    List<AWGMBuyList> awgmBuyList = new ArrayList<>();
                    List<AWGMBuyThings> awgmBuyProductList = swService.findAwgmProductBySwId(sw);
                    for (AWGMBuyThings awgmBuyProduct : awgmBuyProductList) {
                        AWGMBuyList product = new AWGMBuyList();
                        if (awgmBuyProduct.getFrequency() != null)
                            product.setFrequency(awgmBuyProduct.getFrequency().toString());
                        else
                            product.setFrequency("0");
                        if (awgmBuyProduct.getIndex() != null)
                            product.setIndex(awgmBuyProduct.getIndex().toString());
                        else
                            product.setIndex("0");
                        product.setItemName(awgmBuyProduct.getNamaBarang());
                        if (awgmBuyProduct.getBuyPrice() != null)
                            product.setPrice(awgmBuyProduct.getBuyPrice());
                        else
                            product.setPrice(awgmBuyProduct.getBuyPrice().ZERO);
                        if (awgmBuyProduct.getSellPrice() != null)
                            product.setSellingPrice(awgmBuyProduct.getSellPrice());
                        else
                            product.setSellingPrice(awgmBuyProduct.getSellPrice().ZERO);
                        product.setTotalItem(awgmBuyProduct.getTotal().toString());
                        if (awgmBuyProduct.getType() != null)
                            product.setType(awgmBuyProduct.getType().toString());
                        else
                            product.setType("0");
                        awgmBuyList.add(product);
                    }
                    swPojo.setAwgmPurchasing(awgmBuyList);
                    SWExpenseResponse expense = new SWExpenseResponse();
                    if (sw.getTransportCost() != null)
                        expense.setTransportUsaha(sw.getTransportCost().toString());
                    if (sw.getUtilityCost() != null)
                        expense.setUtilitasUsaha(sw.getUtilityCost().toString());
                    if (sw.getStaffSalary() != null)
                        expense.setGajiUsaha(sw.getStaffSalary().toString());
                    if (sw.getRentCost() != null)
                        expense.setSewaUsaha(sw.getRentCost().toString());
                    if (sw.getPrivateTransportCost() != null)
                        expense.setTransportNonUsaha(sw.getPrivateTransportCost().toString());
                    if (sw.getPrivateUtilityCost() != null)
                        expense.setUtitlitasNonUsaha(sw.getPrivateUtilityCost().toString());
                    if (sw.getEducationCost() != null)
                        expense.setPendidikanNonUsaha(sw.getEducationCost().toString());
                    if (sw.getHealthCost() != null)
                        expense.setKesehatanNonUsaha(sw.getHealthCost().toString());
                    if (sw.getFoodCost() != null)
                        expense.setMakanNonUsaha(sw.getFoodCost().toString());
                    if (sw.getInstallmentCost() != null)
                        expense.setAngsuranNonUsaha(sw.getInstallmentCost().toString());
                    if (sw.getOtherCost() != null)
                        expense.setLainlainNonUsaha(sw.getOtherCost().toString());
                    swPojo.setExpense(expense);
                    List<ReferenceList> referenceList = new ArrayList<>();
                    List<NeighborRecommendation> neighborList = swService.findNeighborBySwId(sw);
                    for (NeighborRecommendation neighbor : neighborList) {
                        ReferenceList reference = new ReferenceList();
                        reference.setName(neighbor.getNeighborName());
                        reference.setAddress(neighbor.getNeighborAddress());
                        reference.setGoodNeighbour("" + neighbor.getNeighborRelation());
                        if (reference.getGoodNeighbour().equals("t")) {
                            reference.setGoodNeighbour("true");
                        } else {
                            reference.setGoodNeighbour("false");
                        }
                        reference.setVisitedByLandshark("" + neighbor.getNeighborOutstanding());
                        if (reference.getVisitedByLandshark().equals("t")) {
                            reference.setVisitedByLandshark("true");
                        } else {
                            reference.setVisitedByLandshark("false");
                        }
                        reference.setHaveBusiness("" + neighbor.getNeighborBusiness());
                        if (reference.getHaveBusiness().equals("t")) {
                            reference.setHaveBusiness("true");
                        } else {
                            reference.setHaveBusiness("false");
                        }
                        reference.setRecommended("" + neighbor.getNeighborRecomend());
                        if (reference.getRecommended().equals("t")) {
                            reference.setRecommended("true");
                        } else {
                            reference.setRecommended("false");
                        }
                        referenceList.add(reference);
                    }
                    swPojo.setReferenceList(referenceList);
                    swPojo.setStatus(sw.getStatus());
                    if (sw.getPmId() != null)
                        swPojo.setPmId(sw.getPmId().getPmId().toString());
                    List<SWApprovalHistory> swApprovalList = new ArrayList<>();
                    List<SWUserBWMPMapping> swUserBwmpList = swService.findBySw(sw.getSwId());
                    for (SWUserBWMPMapping swUserBwmp : swUserBwmpList) {
                        SWApprovalHistory swApproval = new SWApprovalHistory();
                        User userApproval = userService.loadUserByUserId(swUserBwmp.getUserId());
                        if (userApproval != null) {
                            swApproval.setLevel(swUserBwmp.getLevel());
                            if (userApproval.getLimit() != null)
                                swApproval.setLimit(userApproval.getLimit());
                            else
                                swApproval.setLimit("0");
                            swApproval.setRole(userApproval.getRoleName());
                            swApproval.setName(userApproval.getName());
                            swApproval.setDate(swUserBwmp.getDate());
                            swApproval.setTanggal(formatDateTime.format(swUserBwmp.getCreatedDate()));
                            swApproval.setStatus(swUserBwmp.getStatus());
                            swApproval.setSupervisorId(userApproval.getUserId().toString());
                            swApprovalList.add(swApproval);
                        }
                    }
                    swPojo.setApprovalHistories(swApprovalList);
                    SwIdPhoto swIdPhoto = swService.getSwIdPhoto(sw.getSwId().toString());
                    if (swIdPhoto != null)
                        swPojo.setHasIdPhoto("true");
                    else
                        swPojo.setHasIdPhoto("false");
                    SwSurveyPhoto swSurveyPhoto = swService.getSwSurveyPhoto(sw.getSwId().toString());
                    if (swSurveyPhoto != null)
                        swPojo.setHasBusinessPlacePhoto("true");
                    else
                        swPojo.setHasBusinessPlacePhoto("false");
                    if (sw.getHasApprovedMs() != null) {
                        if (sw.getHasApprovedMs().equals(true))
                            swPojo.setHasApprovedMs("true");
                        else
                            swPojo.setHasApprovedMs("false");
                    } else
                        swPojo.setHasApprovedMs("false");
                    swResponse.add(swPojo);
                }
                final List<SurveyWawancara> listDeletedSw = swService.findIsDeletedSwList();
                List<String> deletedSwList = new ArrayList<>();
                for (SurveyWawancara deletedSw : listDeletedSw) {
                    String id = deletedSw.getLocalId().toString();
                    deletedSwList.add(id);
                }
                final List<Customer> listDeletedCustomer = syncCustomerService.findIsDeletedCustomerList();
                List<String> deletedCustomerList = new ArrayList<>();
                for (Customer deletedCustomer : listDeletedCustomer) {
                    String id = deletedCustomer.getCustomerId().toString();
                    deletedCustomerList.add(id);
                }
                response.setDeletedCustomerList(deletedCustomerList);
                response.setDeletedSwList(deletedSwList);
                response.setDeviationList(deviationResponses);
                response.setAp3rList(ap3rListResponse);
                response.setLoanList(loanListResponse);
                response.setCustomerList(customerResponse);
                response.setSwList(swResponse);
            }
        } catch (Exception e) {
            log.error("getDetailDeviation error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("getDetailDeviation error: " + e.getMessage());
        } finally {
            log.info("Try to create Terminal Activity");
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_SW);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("Finishing create Terminal Activity");
            } catch (Exception e) {
                log.error("getDetailDeviation saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            log.info("Send Response to Device");
            log.info("getDetailDeviation RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            return response;
        }
    }

}