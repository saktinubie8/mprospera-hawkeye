package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.survey.*;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.request.*;
import id.co.telkomsigma.btpns.mprospera.response.*;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.PercentageSynchronizer;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

@Controller("webServiceSurveyController")
public class WebServiceSurveyController extends GenericController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private SurveyService surveyService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private AuditLogService auditLogService;

    @Autowired
    private AreaService areaService;

    @Autowired
    private UserService userService;

    @Autowired
    private SentraService sentraService;

    @Autowired
    private RESTClient restClient;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private ParameterService paramService;

    @Autowired
    private WSValidationService wsValidationService;

    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.SURVEY_SUBMIT_DATA_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    AddSurveyResponse doAdd(@RequestBody final AddSurveyRequest request, @PathVariable("apkVersion") String apkVersion) throws Exception {

        final AddSurveyResponse response = new AddSurveyResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        String surveyId = "";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        try {
            log.info("addSurvey INCOMING SUBMIT Survey REQUEST : " + jsonUtils.toJson(request));
            // validation imei, session key
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(), request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                log.trace("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei() + " ,sessionKey : " + request.getSessionKey());
            } else {
                Timer timer = new Timer();
                timer.start("[" + WebGuiConstant.SURVEY_SUBMIT_DATA_REQUEST + "] [surveyService.findBySurveyId]");
                Survey survey = surveyService.findBySurveyId((request.getSurveyId()));
                timer.stop();
                if (survey == null) {
                    response.setResponseCode("XX");
                    response.setResponseMessage("SURVEY NOT FOUND...");
                    return response;
                }
                if (WebGuiConstant.STATUS_DONE.equals(survey.getStatus())) {
                    AddSurveyResponse surveyResponse = new AddSurveyResponse();
                    surveyResponse.setResponseCode(WebGuiConstant.RC_GLOBAL_EXCEPTION);
                    String label = getMessage("webservice.rc.label." + surveyResponse.getResponseCode());
                    surveyResponse.setResponseMessage(label);
                    return surveyResponse;
                } else if (!WebGuiConstant.STATUS_DONE.equals(request.getStatus())) {
                    // insert ke Survey
                    survey.setCreatedBy(request.getUsername());
                    survey.setCreatedDate(new Date());
                    survey.setLatitude(request.getLatitude());
                    survey.setLongitude(request.getLongitude());
                    survey.setSurveyTypeId(request.getSurveyTypeId());
                    Question[] questions = request.getQuestionList();
                    List<SurveyDetail> surveyDetailList = new ArrayList<SurveyDetail>();
                    for (int i = 0; i < questions.length; i++) {
                        SurveyDetail surveyDetail = new SurveyDetail();
                        surveyDetail.setSurvey(survey);
                        surveyDetail.setQuestionId(request.getQuestionList()[i].getSurveyQuestionId());
                        surveyDetail.setQuestionType(request.getQuestionList()[i].getQuestionType());
                        if (surveyDetail.getQuestionId().equals("1433") && surveyDetail.getQuestionType().equals("3")) {
                            surveyDetail.setQuestionValue(request.getQuestionList()[i].getSurveyQuestionValue().replaceAll(",", ""));
                        } else {
                            surveyDetail.setQuestionValue(request.getQuestionList()[i].getSurveyQuestionValue());
                        }
                        surveyDetailList.add(surveyDetail);
                    }
                    survey.setSurveyDetail(surveyDetailList);
                    String customerId = request.getCustomerId();
                    Timer timer2 = new Timer();
                    timer2.start("[" + WebGuiConstant.SURVEY_SUBMIT_DATA_REQUEST + "] [customerService.findById]");
                    survey.setCustomer(customerService.findById(customerId));
                    timer2.stop();
                    survey.setCustomerName(customerService.findById(customerId).getCustomerName());
                    survey.setSurveyDate(formatter.parse(request.getSurveyDate()));
                }
                // insert ke prospera
                if (response.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)) {
                    ESBSurveyRequest esbRequest = new ESBSurveyRequest();
                    Customer customer = customerService.findById(request.getCustomerId());
                    esbRequest.setCustomerId(customer.getProsperaId());
                    esbRequest.setReffId(String.valueOf(survey.getSurveyId()));
                    esbRequest.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
                    esbRequest.setSurveyId(survey.getSurveyTypeId());
                    esbRequest.setTransmissionDateAndTime(request.getTransmissionDateAndTime());
                    esbRequest.setUsername(request.getUsername());
                    Timer timer2 = new Timer();
                    timer2.start("[" + WebGuiConstant.SURVEY_SUBMIT_DATA_REQUEST + "] [surveyService.findSurveyType]");
                    ProsperaSurveyType prosSurvey = surveyService.findSurveyType(survey.getSurveyTypeId());
                    timer2.stop();
                    esbRequest.setPpi(prosSurvey.getIsPpi());
                    esbRequest.setImei(request.getImei());
                    List<ESBQuestion> esbQuestionsList = new ArrayList<ESBQuestion>();
                    for (int i = 0; i < request.getQuestionList().length; i++) {
                        ESBQuestion esbQuestion = new ESBQuestion();
                        esbQuestion.setQuestionId(request.getQuestionList()[i].getSurveyQuestionId());
                        esbQuestion.setResponseType(request.getQuestionList()[i].getQuestionType());
                        if (esbQuestion.getQuestionId().equals("1433") && esbQuestion.getResponseType().equals("3")) {
                            esbQuestion.setResponseValue(request.getQuestionList()[i].getSurveyQuestionValue().replaceAll(",", ""));
                        } else {
                            esbQuestion.setResponseValue(request.getQuestionList()[i].getSurveyQuestionValue());
                        }
                        esbQuestionsList.add(esbQuestion);
                    }
                    esbRequest.setQuestions(esbQuestionsList.toArray(new ESBQuestion[0]));
                    ESBSurveyResponse restResponse = restClient.inputSurvey(esbRequest);
                    response.setResponseCode(restResponse.getResponseCode());
                    response.setResponseMessage(restResponse.getResponseMessage());
                    if (response.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)) {
                        survey.setStatus(WebGuiConstant.STATUS_DONE);
                        survey.setUpdatedDate(new Date());
                        survey.setUpdatedBy(request.getUsername());
                        surveyService.save(survey);
                        surveyId = survey.getSurveyId().toString();
                    }
                }
            }
        } catch (Exception e) {
            log.error("addSurvey error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("addSurvey error: " + e.getMessage());
        } finally {
            String label = getMessage("webservice.rc.label." + response.getResponseCode());
            if (response.getResponseMessage() == null) response.setResponseMessage(label);
            response.setSurveyId(surveyId);
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_MODY_SURVEY);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        // save messages masuk
                        MessageLogs incoming = new MessageLogs();
                        incoming.setCreatedDate(new Date());
                        incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        incoming.setRequest(true);
                        incoming.setTerminalActivity(terminalActivity);
                        // save messages keluar
                        MessageLogs outgoing = new MessageLogs();
                        outgoing.setCreatedDate(new Date());
                        outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                        try {
                            outgoing.setMessageRaw(new JsonUtils().toJson(response).getBytes());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        outgoing.setRequest(false);
                        outgoing.setTerminalActivity(terminalActivity);
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        messageLogs.add(incoming);
                        messageLogs.add(outgoing);
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        AuditLog auditLog = new AuditLog();
                        auditLog.setActivityType(AuditLog.MODIF_SURVEY);
                        auditLog.setCreatedBy(request.getUsername());
                        auditLog.setCreatedDate(new Date());
                        auditLog.setDescription(request.toString() + " | " + response.toString());
                        auditLog.setReffNo(request.getRetrievalReferenceNumber());
                        auditLogService.insertAuditLog(auditLog);
                    }
                });
            } catch (Exception e) {
                log.error("addSurvey saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            log.info("addSurvey RESPONSE MESSAGE : " + response);
            return response;
        }

    }

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_SURVEY_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    ListSurveyResponse doList(@RequestBody final ListSurveyRequest request,
                              @PathVariable("apkVersion") String apkVersion) throws Exception {

        final ListSurveyResponse response = new ListSurveyResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.info("listSurvey INCOMING MESSAGE : " + jsonUtils.toJson(request));
            // validating
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(),
                    request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei() + " ,sessionKey : " + request.getSessionKey());
            } else {
                User user = (User) userService.loadUserByUsername(request.getUsername());
                String officeId = user.getOfficeCode();
                Timer timer = new Timer();
                timer.start("[" + WebGuiConstant.TERMINAL_GET_SURVEY_REQUEST + "] [surveyService.getSurveyByCreatedDate]");
                final Page<Survey> surveyPage = surveyService.getSurveyByCreatedDate(request.getPage(), request.getGetCountData(), request.getStartLookupDate(), request.getEndLookupDate(), officeId);
                timer.stop();
                final List<SurveyResponse> surveyResponseList = new ArrayList<SurveyResponse>();
                for (final Survey survey : surveyPage.getContent()) {
                    threadPoolTaskExecutor.execute(new Runnable() {
                        @Override
                        public void run() {
                            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd hh:mm:ss");
                            SurveyResponse surveyResponse = new SurveyResponse();
                            if (survey.getCreatedBy() != null)
                                surveyResponse.setCreatedBy(survey.getCreatedBy());
                            if (survey.getCreatedDate() != null)
                                surveyResponse.setCreatedDate(formatter.format(survey.getCreatedDate()));
                            if (survey.getCustomer() != null)
                                surveyResponse.setCustomerId(String.valueOf(survey.getCustomer().getCustomerId()));
                            if (survey.getCustomerName() != null)
                                surveyResponse.setCustomerName(survey.getCustomerName());
                            if (survey.getDisbursementDate() != null)
                                surveyResponse.setDisbursementDate(formatter.format(survey.getDisbursementDate()));
                            if (survey.getStatus() != null)
                                surveyResponse.setStatus(survey.getStatus());
                            if (survey.getSurveyDate() != null)
                                surveyResponse.setSurveyDate(formatter.format(survey.getSurveyDate()));
                            if (survey.getSurveyId() != null)
                                surveyResponse.setSurveyId(String.valueOf(survey.getSurveyId()));
                            if (survey.getSurveyTypeId() != null)
                                surveyResponse.setSurveyType(survey.getSurveyTypeId());
                            if (survey.getUpdatedDate() != null) {
                                surveyResponse.setUpdatedDate(formatter.format(survey.getUpdatedDate()));
                            }
                            surveyResponse.setUpdatedBy(survey.getUpdatedBy());
                            synchronized (surveyResponseList) {
                                surveyResponseList.add(surveyResponse);
                            }
                        }
                    });
                }
                while (surveyResponseList.size() < surveyPage.getContent().size()) {
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                terminal.setSurveyProgress(PercentageSynchronizer.processSyncPercent(request.getPage(), String.valueOf(surveyPage.getTotalPages()), terminal.getSurveyProgress()));
                terminalService.updateTerminal(terminal);
                response.setCurrentTotal(String.valueOf(surveyPage.getContent().size()));
                response.setGrandTotal(String.valueOf(surveyPage.getTotalElements()));
                response.setTotalPage(String.valueOf(surveyPage.getTotalPages()));
                response.setSurveyList(surveyResponseList);
            }
        } catch (Exception e) {
            log.error("listSurvey error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("listSurvey error: " + e.getMessage());
        } finally {
            String label = getMessage("webservice.rc.label." + response.getResponseCode());
            if (response.getResponseMessage() == null) response.setResponseMessage(label);
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_SURVEY);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
            } catch (Exception e) {
                log.error("listSurvey saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            log.info("listSurvey RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            return response;
        }

    }

    @RequestMapping(value = WebGuiConstant.SURVEY_MASTER_DATA_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    MasterSurveyResponse syncMasterSurvey(@RequestBody final ListMasterSurveyRequest request,
                                          @PathVariable("apkVersion") String apkVersion) throws Exception {

        Date startDt = new Date();
        final MasterSurveyResponse response = new MasterSurveyResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        // validating
        try {
            log.info("listSurveyQuestions INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(), request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei()
                        + " ,sessionKey : " + request.getSessionKey());
            } else {
                Timer timer = new Timer();
                timer.start("[" + WebGuiConstant.SURVEY_MASTER_DATA_REQUEST + "] [surveyService.findAllSurveyType]");
                List<ProsperaSurveyType> surveyTypeList = surveyService.findAllSurveyType();
                timer.stop();
                log.debug("Find All survey duration (miliseconds)=" + (startDt.getTime() - (new Date()).getTime()));
                int count = 0;
                for (ProsperaSurveyType type : surveyTypeList) {
                    SurveyTypeList typePojo = new SurveyTypeList();
                    typePojo.setIsActive(type.getIsActive().toString());
                    typePojo.setSurveyName(type.getSurveyName());
                    if (paramService.loadParamByParamName(WebGuiConstant.PARAMETER_SURVEY_WITH_PHOTO, "Survey PPI")
                            .contains(type.getSurveyName())) {
                        typePojo.setIsWithPhoto("true");
                    } else {
                        typePojo.setIsWithPhoto("false");
                    }
                    typePojo.setSurveyTypeId("" + type.getSurveyTypeId());
                    TreeMap<Integer, SurveyQuestionList> mapper = new TreeMap<>();
                    List<ProsperaSurveyQuestion> questionList = surveyService.findProsperaSurveyQuestionByType(type);
                    for (ProsperaSurveyQuestion question : questionList) {
                        SurveyQuestionList questionPojo = new SurveyQuestionList();
                        questionPojo.setIsActive(question.getQuestion().getState().toString());
                        questionPojo.setIsMandatory("" + question.getIsMandatory());
                        questionPojo.setQuestionOrder("" + question.getOrder());
                        questionPojo.setQuestionText(question.getQuestion().getQuestionText());
                        questionPojo.setQuestionType("" + question.getQuestion().getQuestionType());
                        questionPojo.setSurveyQuestionId("" + question.getSurveyQuestionId());
                        mapper.put(question.getOrder(), questionPojo);
                        TreeMap<Integer, QuestionChoiceList> map = new TreeMap<>();
                        for (ProsperaQuestionChoice choice : question.getQuestion().getChoices()) {
                            QuestionChoiceList choicePojo = new QuestionChoiceList();
                            choicePojo.setChoiceId("" + choice.getChoiceId());
                            choicePojo.setChoiceOrder(choice.getOrder() + "");
                            choicePojo.setChoiceText(choice.getChoiceText());
                            map.put(choice.getOrder(), choicePojo);
                        }
                        for (Integer key : map.keySet()) {
                            questionPojo.getQuestionChoiceList().add(map.get(key));
                        }
                    }
                    for (Integer key1 : mapper.keySet()) {
                        typePojo.getSurveyQuestionList().add(mapper.get(key1));
                    }
                    response.getSurveyTypeList().add(typePojo);
                    count++;
                }
                response.setGrandTotal("" + count);
            }
            String label = getMessage("webservice.rc.label." + response.getResponseCode());
            response.setResponseMessage(label);
        } catch (Exception e) {
            log.error("listSurveyQuestions error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("listHoliday error: " + e.getMessage());
        } finally {
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_MASTER_SURVEY);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
            } catch (Exception e) {
                log.error("listSurveyQuestions saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            log.debug("listSurveyQuestions Total duration (miliseconds)=" + (startDt.getTime() - (new Date()).getTime()));
            log.info("listSurveyQuestions RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            return response;
        }

    }


    @RequestMapping(value = WebGuiConstant.TERMINAL_SUBMIT_SURVEY_PHOTO_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    PhotoResponse submitSurveyPhoto(@RequestBody final PhotoRequest request,
                                    @PathVariable("apkVersion") String apkVersion) throws Exception {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final PhotoResponse response = new PhotoResponse();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        try {
            log.debug("submitSurveyPhoto INCOMING SUBMIT SURVEY PHOTO MESSAGE");
            log.debug("Validating Request");
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
                log.debug("trying to submit survey photo ");
                Survey survey = surveyService.findBySurveyId(request.getSurveyId());
                if (survey != null) {
                    SurveyPhoto surveyPhoto = new SurveyPhoto();
                    surveyPhoto.setSurveyId(survey.getSurveyId());
                    if (request.getSurveyPhoto() != null)
                        surveyPhoto.setSurveyPhoto(request.getSurveyPhoto().getBytes());
                    surveyService.save(surveyPhoto);
                } else {
                    response.setResponseCode(WebGuiConstant.RC_UNKNOWN_SW_ID);
                    String labelFailed = getMessage("webservice.rc.label." + response.getResponseCode());
                    response.setResponseMessage(labelFailed);
                }
            }
        } catch (Exception e) {
            log.error("submitSurveyPhoto error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("submitSurveyPhoto error: " + e.getMessage());
        } finally {
            try {
                // save ke terminal activity
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SUBMIT_DISBURSEMENT_PHOTO);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
            } catch (Exception e) {
                log.error("submitSurveyPhoto saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            log.info("submitSurveyPhoto RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            return response;
        }
    }

}