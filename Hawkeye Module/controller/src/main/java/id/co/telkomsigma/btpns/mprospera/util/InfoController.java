package id.co.telkomsigma.btpns.mprospera.util;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

import org.jboss.vfs.VirtualFile;
import org.slf4j.Marker;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import id.co.telkomsigma.btpns.mprospera.controller.GenericController;

@RestController
public class InfoController extends GenericController {

    @Value("${build.branch}")
    private String buildBranch;

    @Value("${build.changes}")
    private String buildChanges;

    @Value("${build.version}")
    private String buildVersion;

    @RequestMapping(value = "/webservice/info**", method = RequestMethod.GET)
    public @ResponseBody
    InfoResponse info() throws IOException {
        log.info("INFO Requested... ");
        InfoResponse resp = new InfoResponse();
        resp.setLastBuild(new SimpleDateFormat("dd-MM-YYYY HH:mm:ss").format(getClassBuildTime(this)));
        resp.setBuildBranch(ifNull(buildBranch));
        resp.setBuildVersion(ifNull(buildVersion));
        resp.setBuildChanges(ifNull(buildChanges));
        return resp;
    }

    private String ifNull(String e) {
        return (null == e) ? "" : e;
    }

    private Date getClassBuildTime(Object klas) {
        Date d = null;
        Class<?> currentClass = klas.getClass(); //  new Object(){}.getClass().getEnclosingClass();
        URL resource = currentClass.getResource(currentClass.getSimpleName() + ".class");
        if (resource != null) {
            if (resource.getProtocol().equals("file")) {
                try {
                    d = new Date(new File(resource.toURI()).lastModified());
                } catch (URISyntaxException e) {
                    log.error(Marker.ANY_MARKER, e);
                }
            } else if (resource.getProtocol().equals("vfs")) {
                try {
                    VirtualFile vf = (VirtualFile) resource.getContent();
                    d = new Date(vf.getLastModified());
                } catch (Exception e) {
                    log.error(Marker.ANY_MARKER, e);
                }
            } else if (resource.getProtocol().equals("jar")) {
                String path = resource.getPath();
                d = new Date(new File(path.substring(5, path.indexOf("!"))).lastModified());
            } else if (resource.getProtocol().equals("zip")) {
                String path = resource.getPath();
                File jarFileOnDisk = new File(path.substring(0, path.indexOf("!")));
                try (JarFile jf = new JarFile(jarFileOnDisk)) {
                    ZipEntry ze = jf.getEntry(path.substring(path.indexOf("!") + 2));//Skip the ! and the /
                    long zeTimeLong = ze.getTime();
                    Date zeTimeDate = new Date(zeTimeLong);
                    d = zeTimeDate;
                } catch (IOException | RuntimeException e) {
                    log.error(Marker.ANY_MARKER, e);
                }
            }
        }
        return d;
    }

}