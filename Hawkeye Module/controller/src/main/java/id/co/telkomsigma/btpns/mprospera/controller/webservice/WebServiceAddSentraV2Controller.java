package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaDistrict;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaKelurahan;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaProvince;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaSubDistrict;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.SentraRequest;
import id.co.telkomsigma.btpns.mprospera.response.SentraResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("webServiceAddSentraV2Controller")
public class WebServiceAddSentraV2Controller extends GenericController {

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private SentraService sentraService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private AreaService areaService;

    @Autowired
    private RESTClient restClient;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private AuditLogService auditLogService;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
    SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");
    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.ADD_SENTRA_V2_REQUEST, method = {RequestMethod.POST})
    public
    @ResponseBody
    SentraResponse doAdd(@RequestBody String requestString,
                         @PathVariable("apkVersion") String apkVersion) throws Exception {

        log.info("addSentraV2 INCOMING SUBMIT SENTRA REQUEST : " + requestString);
        Object obj = new JsonUtils().fromJson(requestString, SentraRequest.class);
        final SentraRequest request = (SentraRequest) obj;
        final SentraResponse response = new SentraResponse();
        String sentraId = "";
        AreaKelurahan kel = new AreaKelurahan();
        AreaSubDistrict kec = new AreaSubDistrict();
        AreaDistrict kab = new AreaDistrict();
        AreaProvince prov = new AreaProvince();
        Sentra sentra = new Sentra();
        String localId = request.getLocalId();
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        response.setResponseMessage("SUKSES");
        try {
            log.info("TRY VALIDATING REQUEST");
            // validation username, imei, session key, apk version
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(),
                    request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                response.setResponseCode(validation);
                log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei() + " ,sessionKey : " + request.getSessionKey());
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
            } else if (!"APPROVED".equalsIgnoreCase(request.getStatus())) {
                log.error("UNKNOWN STATUS");
                response.setResponseCode(WebGuiConstant.RC_UNKNOWN_STATUS);
                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                response.setResponseMessage(label);
            } else {
                // insert ke Tabel Sentra
                if ("update".equals(request.getAction())) {
                    if (request.getSentraId() != null) {
                        if (!"".equals(request.getSentraId())) {
                            sentra = sentraService.findBySentraId(request.getSentraId());
                            if (sentra == null) {
                                log.error("UNKNOWN SENTRA ID");
                                response.setResponseCode(WebGuiConstant.RC_UNKNOWN_SENTRA_ID);
                                String label = getMessage("webservice.rc.label." + response.getResponseCode());
                                response.setResponseMessage(label);
                                log.info("RESPONSE MESSAGE : " + response);
                                return response;
                            } else {
                                log.info("EXISTING SENTRA");
                                sentra.setUpdatedDate(new Date());
                                request.setSentraId(sentra.getProsperaId());
                                if (request.getSentraLeader() != null) {
                                    request.setCustomerNumberLeader(request.getSentraLeader());
                                } else {
                                    request.setCustomerNumberLeader("");
                                }
                            }
                        } else {
                            log.error("SENTRA ID BLANK");
                            response.setResponseCode(WebGuiConstant.RC_UNKNOWN_SENTRA_ID);
                            String label = getMessage("webservice.rc.label." + response.getResponseCode());
                            response.setResponseMessage(label);
                            log.info("RESPONSE MESSAGE : " + response);
                            return response;
                        }
                    } else {
                        log.error("SENTRA ID NULL");
                        response.setResponseCode(WebGuiConstant.RC_UNKNOWN_SENTRA_ID);
                        String label = getMessage("webservice.rc.label." + response.getResponseCode());
                        response.setResponseMessage(label);
                        log.info("RESPONSE MESSAGE : " + response);
                        return response;
                    }
                } else {
                    log.info("NEW SENTRA");
                    sentra.setCreatedDate(new Date());
                    sentra.setCreatedBy(request.getUsername());
                }
                sentra.setAreaId(request.getAreaId());
                sentra.setLatitude(request.getLatitude());
                sentra.setLongitude(request.getLongitude());
                if (request.getPrsDay() != null) {
                    sentra.setPrsDay(request.getPrsDay().charAt(0));
                }
                sentra.setRtRw(request.getRtRw());
                sentra.setSentraAddress(request.getSentraAddress());
                sentra.setMeetingPlace(request.getMeetingPlace());
                sentra.setSentraName(request.getSentraName());
                sentra.setSentraOwnerName(request.getSentraOwnerName());
                sentra.setSentraLeader(request.getSentraLeader());
                if (request.getStartedDate() != null || request.getStartedDate() != "") {
                    sentra.setStartedDate(formatter.parse(request.getStartedDate()));
                }
                sentra.setLocationId(request.getOfficeCode());
                sentra.setPostCode(request.getPostcode());
                if (request.getMeetingTime() != null) {
                    if (!request.getMeetingTime().equals("")) {
                        sentra.setMeetingTime(new java.sql.Time(formatTime.parse(request.getMeetingTime()).getTime()));
                    }
                }
                if (!terminalService.isDuplicate(request.getRetrievalReferenceNumber(), "addSentra")) {
                    sentra.setApprovedBy(request.getUsername());
                    kel = areaService.findKelurahanById(sentra.getAreaId());
                    if (kel != null) {
                        if (kel.getAreaName() == null) {
                            request.setKelurahan("");
                        } else {
                            request.setKelurahan(kel.getAreaName());
                        }
                        kec = areaService.findSubDistrictById(kel.getParentAreaId());
                        if (kec != null) {
                            if (kec.getAreaName() == null) {
                                request.setKecamatan("");
                            } else {
                                request.setKecamatan(kec.getAreaName());
                            }
                            if (kab != null) {
                                kab = areaService.findDistrictById(kec.getParentAreaId());
                                if (kab.getAreaName() == null) {
                                    request.setKabupaten("");
                                } else {
                                    request.setKabupaten(kab.getAreaName());
                                }
                                prov = areaService.findProvinceById(kab.getParentAreaId());
                                if (prov != null) {
                                    if (prov.getAreaName() == null) {
                                        request.setProvinsi("");
                                    } else {
                                        request.setProvinsi(prov.getAreaName());
                                    }
                                }
                            }
                        }
                    }
                    request.setDayNumber(new SimpleDateFormat("dd").format(sentra.getStartedDate()));
                    request.setGetCountData(null);
                    request.setSentraPicture(null);
                    request.setSentraOwnerName(null);
                    if (request.getSentraLeader() != null) {
                        request.setCustomerNumberLeader(request.getSentraLeader());
                    } else {
                        request.setCustomerNumberLeader("");
                    }
                    if (sentra.getPostCode() == null) {
                        request.setPostcode("");
                    } else {
                        request.setPostcode(sentra.getPostCode());
                    }
                    request.setAreaId(null);
                    request.setStatus(null);
                    if (sentra.getRtRw() == null) {
                        request.setRtRw("");
                    } else {
                        request.setRtRw(sentra.getRtRw());
                    }
                    if (sentra.getPrsDay() == '1') {
                        request.setPrsDay("2");
                    } else if (sentra.getPrsDay() == '2') {
                        request.setPrsDay("3");
                    } else if (sentra.getPrsDay() == '3') {
                        request.setPrsDay("4");
                    } else if (sentra.getPrsDay() == '4') {
                        request.setPrsDay("5");
                    }
                    request.setSentraAddress(sentra.getSentraAddress());
                    request.setMeetingPlace(sentra.getMeetingPlace());
                    request.setSessionKey(null);
                    request.setStartedDate(new SimpleDateFormat("yyyyMMdd").format(sentra.getStartedDate()));
                    request.setUsername(sentra.getCreatedBy());
                    if (request.getMeetingTime() == null) {
                        request.setActionTime("00:00:00");
                    } else {
                        if (request.getMeetingTime().equals("")) {
                            request.setActionTime("00:00:00");
                        } else {
                            request.setActionTime(request.getMeetingTime() + ":00");
                        }
                    }
                    request.setRecurAfter("2");
                    request.setMeetingTime(null);
                    request.setLocalId(null);
                    request.setOfficeCode(areaService.findLocationCodeByLocationId(sentra.getLocationId()));
                    request.setTokenKey(null);
                    log.info("SEND TO ESB");
                    // insert ke prospera
                    SentraResponse restResponse = new SentraResponse();
                    if ("insert".equals(request.getAction())) {
                        request.setReffId("");
                        restResponse = restClient.addSentra(request);
                    } else {
                        request.setReffId(sentra.getProsperaId());
                        request.setSentraId(sentra.getProsperaId());
                        restResponse = restClient.updateSentra(request);
                    }
                    if (restResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)) {
                        if (restResponse.getSentraId() == null) {
                            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                            String label = getMessage("webservice.rc.label." + response.getResponseCode());
                            response.setResponseMessage(label);
                            return response;
                        }
                        sentra.setProsperaId(restResponse.getSentraId());
                        sentra.setSentraCode(restResponse.getKodeSentra());
                        sentra.setApprovedDate(new Date());
                        sentra.setStatus(WebGuiConstant.STATUS_APPROVED);
                        if (request.getSentraLeader() != null) {
                            sentra.setSentraLeader(request.getSentraLeader());
                        }
                        sentraService.save(sentra);
                    } else if (restResponse.getResponseCode().equals("21")) {
                        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
                        response.setResponseMessage(WebGuiConstant.RC_SENTRA_ALREADY_EXIST);
                        return response;
                    }
                    response.setResponseCode(restResponse.getResponseCode());
                    response.setResponseMessage(restResponse.getResponseMessage());
                    sentra.setStatus(request.getStatus());
                    log.info("SAVING SENTRA DATA");
                    if (sentra.getSentraId() != null) {
                        sentraId = sentra.getSentraId().toString();
                    }
                } else {
                    Sentra sentraExist = sentraService.getSentraByRrn(request.getRetrievalReferenceNumber());
                    if (sentraExist != null) {
                        sentraId = sentraExist.getSentraId().toString();
                    }
                }
            }
        } catch (Exception e) {
            log.error("addSentraV2 error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("addSentraV2 error: " + e.getMessage());
        } finally {
            log.info("Sentra ID : " + sentraId);
            response.setSentraId(sentraId);
            response.setLocalId(localId);

            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_MODY_SENTRA);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.insertRrn("addSentra", request.getRetrievalReferenceNumber().trim());
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        AuditLog auditLog = new AuditLog();
                        auditLog.setActivityType(AuditLog.MODIF_SENTRA);
                        auditLog.setCreatedBy(request.getUsername());
                        auditLog.setCreatedDate(new Date());
                        if (!request.toString().equals(null) && !request.toString().equals("")
                                && request.toString().length() > 1000)
                            auditLog.setDescription(request.toString().substring(0, 1000) + " | " + response.toString());
                        auditLog.setReffNo(request.getRetrievalReferenceNumber());
                        auditLogService.insertAuditLog(auditLog);
                    }
                });
            } catch (Exception e) {
                log.error("addSentraV2 saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            log.info("addSentraV2 RESPONSE MESSAGE : " + jsonUtils.toJson(response));
            return response;
        }
    }

}